from django.conf.urls import patterns, url

urlpatterns = patterns('client.views',
                       #url(r'home$',TemplateView.as_view(template_name='client/pincode.html'), name="home"),
                       url(r'^$', 'client', name='show_client'),
                       url(r'^add$', 'add_client', name='add_client'),
                       url(r'^get_warehouses$', 'get_client_warehouses', name='get_client_warehouses'),
                       url(r'^warehouse$', 'client_warehouse', name='show_client_warehouse'),
                       url(r'^warehouse/add$', 'add_client_warehouse', name='add_client_warehouse'),
                       url(r'^vendor/add$', 'add_client_vendor', name='add_client_vendor'),
                       url(r'^export/dashboard$', 'client_export_dashboard', name='client_export_dashboard'),
                       url(r'^manifest/(\d+)$', 'manifest_data', name='manifest_data'),
                       url(r'^awb/(\w+)$', 'awb_history', name='awb_history'),
                       url(r'^vendor/upload$', 'upload_client_vendor_file', name='upload_client_vendor_file'),
                       url(r'^vendor$', 'client_vendor', name='client_vendor'),
                       # url(r'^dashboard_report$', 'report', name='report'),
                       url(r'^report$', 'pickup_performance', name='report'),
                       url(r'^vendor/(\w*\.*\w*)/open_awb$','awb_vendor_detail', name= 'awb_vendor_detail'),
                       url(r'^vendor/download_vendor_dump$', 'download_vendor_dump', name= 'download_vendor_dump'),
                       #url(r'^cs_report$','cs_report',name='cs_report'),
                       # url(r'^cs_cancellation_report$','cs_cancellation_report',name='cs_cancellation_report'),
                       # url(r'^cs_pending_report$','cs_pending_report',name='cs_pending_report'),
)
