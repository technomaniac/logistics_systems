from django.contrib import admin
from .models import *
# Register your models here.
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        'client_code', 'client_name', 'client_type', 'awb_assigned_from', 'awb_assigned_to', 'awb_last_used', 'category',
        'creation_date')
    search_fields = ['client_code', 'client_name', 'category']
    list_filter = ['category', 'creation_date']


class ClientAdditionalAdmin(admin.ModelAdmin):
    list_display = (
        'client', 'sms_notification', 'automated_awb_used','vendor_assign', 'email', 'tan_no', 'pan_no',
        'address', 'phone')
    search_fields = ['client', 'email', 'tan_no', 'pan_no', 'address', 'phone']
    list_filter = ['client', 'sms_notification']



class ClientMISReportAdmin(admin.ModelAdmin):
    list_display = (
        'client', 'emails', 'time', 'type', 'duration')
    search_fields = ['client', 'emails']
    list_filter = ['client', 'type']


class ClientDashboardReportAdmin(admin.ModelAdmin):
    list_display = (
        'client', 'emails', 'time', 'type', 'duration')
    search_fields = ['client', 'emails']
    list_filter = ['client', 'type']

class Client_CS_DashboardReportAdmin(admin.ModelAdmin):
    list_display = (
        'client', 'emails', 'time', 'duration')
    search_fields = ['client', 'emails']
    list_filter = ['client']


class Client_VendorAdmin(admin.ModelAdmin):
    list_display = ('client','vendor_name', 'vendor_code','pincode')
    search_fields = ['vendor_name','vendor_code',]
    list_filter = ['client','vendor_name']

class ClientOutForPickupReportAdmin(admin.ModelAdmin):
    list_display = ('client','emails','time','duration')
    search_fields = ['client', 'emails']
    list_filter = ['client']

class ClientDeliveryReportAdmin(admin.ModelAdmin):
    list_display = ('client','emails','time','duration')
    search_fields = ['client', 'emails']
    list_filter = ['client']


admin.site.register(Client, ClientAdmin)
admin.site.register(Client_Additional, ClientAdditionalAdmin)
admin.site.register(Client_Warehouse)
admin.site.register(Client_Vendor, Client_VendorAdmin)
admin.site.register(Client_MIS_Report, ClientMISReportAdmin)
admin.site.register(Client_Dashboard_Report, ClientDashboardReportAdmin)
admin.site.register(Client_cs_dashboard ,Client_CS_DashboardReportAdmin)
admin.site.register(ClientOutForPickupReport ,ClientOutForPickupReportAdmin)
admin.site.register(DeliveryReportFL , ClientDeliveryReportAdmin)