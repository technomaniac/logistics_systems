from __future__ import unicode_literals
from django.utils.formats import date_format

# @task
# def generate_client_dashboard(manifests):
#     filename = 'REPORT_' + '_' + strftime("%Y-%m-%d_%H:%M", gmtime())
#     response = HttpResponse(content_type='text/csv')
#     response['Content-Disposition'] = 'attachment; filename="' + filename + '.csv"'
#
#     writer = csv.writer(response)
#
#     writer.writerow(CLIENT_DASHBOARD_HEADER)
#     for id in manifests:
#         writer.writerow(get_dashboard_row(Manifest.objects.get(pk=id)))
#
#     # if len(args) > 0:
#     #     message = EmailMessage(filename, "PFA", "system@nuvoex.com", [args[0]])
#     #     message.attach(filename, csvfile.getvalue(), 'text/csv')
#     #     return message.send()
#     # else:
#     # handle_uploaded_file(File(csvfile), filename, MEDIA_ROOT + 'client/dashboard/')
#     return response

def get_dashboard_row(data, type):
    if type == 'RL':
        return [date_format(data.creation_date, "SHORT_DATETIME_FORMAT"), data.get_total_awb_count(),
                data.get_converted_awb_count(), data.get_converted_perc(), data.get_picked_up_count(),
                data.get_picked_up_perc(), data.get_dto_count(), data.get_dto_perc(), data.get_cancelled_count(),
                data.get_cancelled_perc(), data.get_scheduled_count(), data.get_scheduled_perc(),
                data.get_out_for_pickup_count(), data.get_out_for_pickup_perc()]
    else:
        return [date_format(data.creation_date, "SHORT_DATETIME_FORMAT"), data.get_total_awb_count(),
                data.get_delivered_count(), data.get_delivered_perc(), data.get_pending_count(),
                data.get_pending_perc(), data.get_returned_count(), data.get_returned_perc(), data.get_rto_count(),
                data.get_rto_perc()]




        # @task
        # def generateClientRLReport():
        #     manifest = Manifest.objects.filter(creation_date=)