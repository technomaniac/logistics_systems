from django.db import models

from common.models import Time_Model
# Create your models here.

class Client(Time_Model):
    CLIENT_TYPE = (
        ('C', 'Client'),
        ('S', 'SubClient')
    )
    CATEGORY = (
        ('RL', 'Reverse Logistics'),
        ('FL', 'Forward Logistics')
    )
    client_code = models.CharField(max_length=10, unique=True, db_index=True)
    client_name = models.CharField(max_length=50, unique=True)
    client_type = models.CharField(choices=CLIENT_TYPE, max_length=1)
    awb_assigned_from = models.CharField(max_length=15)
    awb_assigned_to = models.CharField(max_length=15)
    awb_left = models.IntegerField(max_length=10)
    category = models.CharField(choices=CATEGORY, max_length=2, blank=True, null=True)
    awb_last_used = models.CharField(max_length=15, null=True, blank=True)

    class Meta:
        # index_together = [
        # ['client_code', 'client_name', 'client_type', 'awb_assigned_from', 'awb_assigned_to', 'awb_left',
        # 'category'], ]
        verbose_name = "Client"

    # def awbs_consumed(self):
    # return self.

    def awbs_alloted(self):
        return int(self.awb_assigned_to[3:]) - int(self.awb_assigned_from[3:]) + 1

    # def awbs_left(self):
    # client = Client.objects.filter(client=self).annotate(awbs_consumed=Count('manifest__awb_status'))
    # awbs = self.awbs_alloted() - int(client[0].awbs_consumed)
    # print awbs
    #     return awbs

    def __unicode__(self):
        return self.client_name


class Client_Additional(Time_Model):
    client = models.OneToOneField('client.Client', related_name='additional', unique=True, db_index=True)
    sms_notification = models.BooleanField(default=False)
    automated_awb_used = models.BooleanField(default=False)
    vendor_assign = models.BooleanField(default=False)
    email = models.EmailField(max_length=50, blank=True, null=True)
    pan_no = models.CharField(max_length=50, blank=True, null=True)
    tan_no = models.CharField(max_length=50, blank=True, null=True)
    bank_name = models.CharField(max_length=50, blank=True, null=True)
    account_no = models.CharField(max_length=50, blank=True, null=True)
    city = models.ForeignKey('zoning.City', null=True, blank=True)
    address = models.CharField(max_length=50, blank=True, null=True)
    phone = models.CharField(max_length=13, blank=True, null=True)

    class Meta:
        # index_together = [
        # ['client', 'sms_notification', 'email', 'pan_no', 'tan_no', 'bank_name', 'account_no', 'city', 'address',
        # 'phone'], ]
        verbose_name = "Client Additional Info"
        verbose_name_plural = "Client Additional Info"

    def __unicode__(self):
        return self.client.client_name


class Client_Warehouse(Time_Model):
    warehouse_name = models.CharField(max_length=50, unique=True, db_index=True)
    client = models.ForeignKey('client.Client', related_name='warehouse', db_index=True)
    pincode = models.ForeignKey('zoning.Pincode', db_index=True)
    address = models.CharField(max_length=50, blank=True, null=True)
    phone = models.CharField(max_length=13, blank=True, null=True)

    class Meta:
        index_together = [['warehouse_name', 'client'], ['warehouse_name', 'pincode', ], ]
        verbose_name = "Client Warehouse"

    def __unicode__(self):
        return self.warehouse_name


class Client_Vendor(Time_Model):
    vendor_code = models.CharField(max_length=10, unique=True, db_index=True)
    vendor_name = models.CharField(max_length=50)
    owner_name = models.CharField(max_length=200, default='', blank=True)
    client = models.ForeignKey('client.Client', db_index=True)
    branch = models.ForeignKey('internal.Branch', null=True, blank=True, db_index=True)
    pincode = models.ForeignKey('zoning.Pincode', null=True, blank=True)
    address = models.TextField(blank=True, default='')
    phone = models.TextField(blank=True, default='')

    class Meta:
        # index_together = [
        # ['vendor_code', 'vendor_name', 'owner_name', 'client', 'branch', 'pincode', 'address', 'phone'], ]
        verbose_name = "Client Vendor"
        unique_together = ('vendor_code', 'client')

    def __unicode__(self):
        return self.vendor_code

    def awb_open_count(self):
        return self.awb_set.exclude(awb_status__status = 'DEL')

    def awb_count(self):
        return self.awb_set

    def get_vendor_branch(self):
        return self.pincode.branch_pincode.branch


# class ClientRLReport(Time_Model):
# client = models.ForeignKey('Client')
# rpi_date = models.DateTimeField(auto_now_add=False)
# pickup_assigned = models.IntegerField(max_length=10, default='', blank=True)
# converted = models.IntegerField(max_length=10, default='', blank=True)
#     converted_perc = models.FloatField(max_length=10, default='', blank=True)
#     picked_up = models.IntegerField(max_length=10, default='', blank=True)
#     picked_up_perc = models.FloatField(max_length=10, default='', blank=True)
#     dtod = models.IntegerField(max_length=10, default='', blank=True)
#     dtod_perc = models.FloatField(max_length=10, default='', blank=True)
#     cancelled = models.IntegerField(max_length=10, default='', blank=True)
#     cancelled_perc = models.FloatField(max_length=10, default='', blank=True)
#     scheduled = models.IntegerField(max_length=10, default='', blank=True)
#     scheduled_perc = models.FloatField(max_length=10, default='', blank=True)
#     out_for_pickup = models.IntegerField(max_length=10, default='', blank=True)
#     out_for_pickup_perc = models.FloatField(max_length=10, default='', blank=True)
#     high_priority = models.IntegerField(max_length=10, default='', blank=True)
#
#     def __unicode__(self):
#         return self.client.client_name


class Client_MIS_Report(Time_Model):
    REPORT_TYPE = (
        ('MIS', 'MIS'),
        ('DAS', 'Dashboard'),
    )
    MIS_TYPE = (
        ('IN', 'Internal'),
        ('CL', 'Client')
    )
    DATE_RANGE = (
        ('1', '1 Day'),
        ('10', '10 Days'),
        ('15', '15 Days'),
        ('20', '20 Days'),
        ('7', '1 Week'),
        ('14', '2 Weeks'),
        ('30', '1 Month'),
        ('61', '2 Months')
    )
    PRIORITY_CASES = (
        ('U', 'Urgent'),
        ('N', 'Normal'),
    )
    client = models.ForeignKey('client.Client')
    #report_type = models.CharField(choices=REPORT_TYPE, max_length=5)
    emails = models.CharField(max_length=500)
    time = models.TimeField(auto_now=False)
    #frequency = models.IntegerField(max_length=10, blank=True, null=True)
    type = models.CharField(choices=MIS_TYPE, max_length=3)
    duration = models.CharField(choices=DATE_RANGE, max_length=5)
    priority = models.CharField(choices=PRIORITY_CASES, max_length=1, null=True, blank=True)

    class Meta:
        verbose_name = "MIS Report"


class Client_Dashboard_Report(Time_Model):
    REPORT_TYPE = (
        ('city', 'City Wise'),
        ('branch', 'Branch Wise')

    )
    DATE_RANGE = (
        ('1', '1 Day'),
        ('10', '10 Days'),
        ('15', '15 Days'),
        ('20', '20 Days'),
        ('7', '1 Week'),
        ('14', '2 Weeks'),
        ('30', '1 Month'),
        ('61', '2 Months')
    )
    client = models.ForeignKey('client.Client')
    emails = models.CharField(max_length=300)
    time = models.TimeField(auto_now=False)
    duration = models.CharField(choices=DATE_RANGE, max_length=5)
    type = models.CharField(choices=REPORT_TYPE, max_length=10)

    class Meta:
        verbose_name = "Dashboard Report"


# class Services(Time_Model):
#
#     SERVICE_TYPES = (
#         ('COD', 'COD'),
#         ('PRE', 'Prepaid'),
#         ('MPU', 'Money Pickup'),
#         ('REV', 'Reverse Pickup')
#     )
#     service = models.CharField(choices=SERVICE_TYPES, max_length=3)
#     description = models.CharField(max_length=50)
#
#
# class Client_Services(Time_Model):
#     client = models.ForeignKey('Client')
#     service = models.ForeignKey('Services')


class Client_cs_dashboard(Time_Model):
    DATE_RANGE = (
        ('1', '1 Day'),
        ('10', '10 Days'),
        ('15', '15 Days'),
        ('20', '20 Days'),
        ('7', '1 Week'),
        ('14', '2 Weeks'),
        ('30', '1 Month'),
        ('61', '2 Months')
    )
    client = models.ForeignKey('client.Client')
    emails = models.CharField(max_length=300)
    time = models.TimeField(auto_now=False)
    duration = models.CharField(choices=DATE_RANGE, max_length=5)

    class Meta:
        verbose_name = 'CS Dashboard Report'

class ClientOutForPickupReport(Time_Model):
    REPORT_TYPE = (
        ('city', 'City Wise'),
        ('branch', 'Branch Wise')

    )
    DATE_RANGE = (
        ('1', '1 Day'),
        ('2', '2 Days'),
        ('5', '5 Days'),
        ('10', '10 Days'),
        ('7', '1 Week'),
        ('14', '2 Weeks'),
        ('30', '1 Month'),
        ('61', '2 Months')
    )
    client = models.ForeignKey('client.Client')
    emails = models.CharField(max_length=3000)
    time = models.TimeField(auto_now=False)
    duration = models.CharField(choices=DATE_RANGE, max_length=5)
    type = models.CharField(choices=REPORT_TYPE, max_length=10)

    class Meta:
        verbose_name = "Out For Pickup Report"

class DeliveryReportFL(Time_Model):
    REPORT_TYPE = (
        ('city', 'City Wise'),
        ('branch', 'Branch Wise')

    )
    DATE_RANGE = (
        ('1', '1 Day'),
        ('2', '2 Days'),
        ('5', '5 Days'),
        ('10', '10 Days'),
        ('7', '1 Week'),
        ('14', '2 Weeks'),
        ('30', '1 Month'),
        ('61', '2 Months')
    )
    client = models.ForeignKey('client.Client')
    emails = models.CharField(max_length=3000)
    time = models.TimeField(auto_now=False)
    duration = models.CharField(choices=DATE_RANGE, max_length=5)
    type = models.CharField(choices=REPORT_TYPE, max_length=10)

    class Meta:
        verbose_name = "Forward Delivery Report"























