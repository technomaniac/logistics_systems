import csv
import re
from time import strftime, gmtime
from celery.contrib.methods import task
from datetime import datetime

from django.db.models import Count
from django.http.response import StreamingHttpResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django_tables2 import RequestConfig
from django.contrib.auth.decorators import login_required
from awb.tables import AWBTable

from client.models import Client, Client_Additional, Client_Warehouse, Client_Vendor
from client.tables import ClientTable, ClientWarehouseTable, ClientDashboardTableRL, ClientDashboardTableFL, \
    ClientAWBTable, ClientVendorTable
from client.forms import ClientForm, ClientAdditionalForm, ClientWarehouseForm, ClientVendorForm
from client.tasks import get_dashboard_row
from common.forms import ExcelUploadForm
from awb.models import Manifest, AWB
from utils.common import Echo
from utils.constants import CLIENT_DASHBOARD_HEADER_RL, CLIENT_DASHBOARD_HEADER_FL
from utils.upload import upload_client_vendor_list, handle_uploaded_file
from logistics.settings import MEDIA_ROOT


@login_required(login_url='/login')
def client(request):
    table = ClientTable(
        Client.objects.annotate(awbs_consumed=Count('manifest__awb_status')).order_by('-awbs_consumed'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'client/client.html', {'table': table, 'model': '    client'})


@login_required(login_url='/login')
def client_warehouse(request):
    table = ClientWarehouseTable(Client_Warehouse.objects.all())
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'model': 'warehouse', 'url': '/client/warehouse/add'})


@login_required(login_url='/login')
def client_vendor(request):
    table = ClientVendorTable(
        Client_Vendor.objects.filter(is_active=True).prefetch_related('client', 'branch', 'pincode'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'model': 'vendor', 'url': '/client/vendor/add'})


@login_required(login_url='/login')
def get_client_warehouses(request):
    if request.is_ajax():
        warehouses = Client_Warehouse.objects.filter(client_id=request.GET['client'])
        return render(request, 'client/warehouses.html', {'warehouses': warehouses})


@login_required(login_url='/login')
def add_client(request):
    if request.method == "POST":
        cform = ClientForm(request.POST, instance=Client())
        caform = ClientAdditionalForm(request.POST, instance=Client_Additional())

        if cform.is_valid() and caform.is_valid():
            client = cform.save(commit=False)
            client.awb_assigned_from = request.POST['client_code'] + '0000001'
            client.awb_assigned_to = request.POST['client_code'] + '0010000'
            client.awb_left = 10000
            client.save()
            instance = caform.save(commit=False)
            instance.client_id = client.pk
            instance.save()
            return HttpResponseRedirect('/client/')
    else:
        cform = ClientForm()
        caform = ClientAdditionalForm()
    return render(request, 'client/addclient.html', {'cform': cform, 'caform': caform})


@login_required(login_url='/login')
def add_client_warehouse(request):
    if request.method == "POST":
        form = ClientWarehouseForm(request.POST, instance=Client_Warehouse())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/client/warehouse')
    else:
        form = ClientWarehouseForm()
    return render(request, 'common/form.html', {'form': form, 'model': 'warehouse'})


@login_required(login_url='/login')
def add_client_vendor(request):
    if request.method == "POST":
        form = ClientVendorForm(request.POST, instance=Client_Vendor())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/client/vendor')
    else:
        form = ClientVendorForm()
    return render(request, 'common/form.html', {'form': form, 'model': 'vendore'})


@login_required(login_url='/login')
def upload_client_vendor_file(request):
    if request.method == "POST":
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/client/')
            upload_client_vendor_list(file)
            return HttpResponseRedirect('/client/vendor')
    else:
        form = ExcelUploadForm()
    return render(request, 'common/upload_form.html', {'form': form})


@login_required(login_url='/login')
def client_dashboard(request):
    request.session['export'] = []
    manifests = []
    # dateRange = [(datetime.today() - timedelta(days=x)).strftime("%Y-%m-%d") for x in xrange(40)]
    # for date in dateRange:
    queryset = Manifest.objects.filter(client=request.user.profile.client).order_by('-creation_date')
    # manifests.append(queryset)

    for m in queryset:
        if Manifest.objects.get(pk=m.pk).get_total_awb_count() > 9:
            manifests.append(m)
            if len(manifests) == 20:
                break

    request.session['export'] = {}
    request.session['export']['client_dashboard'] = [m.pk for m in manifests]
    if request.user.profile.client.category == 'RL':
        table = ClientDashboardTableRL(manifests)
    else:
        table = ClientDashboardTableFL(manifests)
    RequestConfig(request).configure(table)
    return render(request, 'client/dashboard.html', {'table': table})


def client_export_dashboard(request):
    if 'export' in request.session:
        client = Client.objects.get(client_code=request.session['client'])
        filename = 'REPORT_' + strftime("%Y-%m-%d_%H-%M-%S", gmtime())
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="' + filename + '.csv"'

        manifests = request.session['export']['client_dashboard']

        writer = csv.writer(response)
        if client.category == 'RL':
            header = CLIENT_DASHBOARD_HEADER_RL
        else:
            header = CLIENT_DASHBOARD_HEADER_FL
        writer.writerow(header)
        if len(manifests) > 0:
            for id in manifests:
                manifest = Manifest.objects.get(pk=id)
                writer.writerow(get_dashboard_row(manifest, client.category))

            return response
        else:
            return HttpResponse('No data found')


def manifest_data(request, id):
    table = ClientAWBTable(AWB.objects.filter(awb_status__manifest=Manifest.objects.get(pk=id)))
    RequestConfig(request, paginate={"per_page": 1000}).configure(table)
    return render(request, 'client/awb.html', {'table': table})


def awb_history(request, awb):
    awb = AWB.objects.get(awb=awb)
    return render(request, 'client/awb_history.html',
                  {'awb_details': awb, 'awb_hist': awb.get_awb_history(False, False)})

def awb_vendor_detail(request, id):
    vendor = Client_Vendor.objects.get(vendor_code = id)
    table = AWBTable(AWB.objects.filter(vendor__vendor_code = vendor.vendor_code).exclude(awb_status__status = 'DEL'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'id': vendor.vendor_name })


def pickup_performance(request):
    from report.client_report import PerformanceReport

    client = Client.objects.get(client_code='SND')
    report = PerformanceReport(client, 7, report_type='city', city=[7])
    start_date, end_date = report.get_date_range()
    # print start_date, end_date
    return render(request, 'client/performance_report.html',
                  {'report': report.generate_report(), 'client': client, 'start_date': start_date,
                   'end_date': end_date})


    # def report(request):
    # from report.client_report import PerformanceReport
    #
    # client = Client.objects.get(client_code='SND')
    # report = PerformanceReport(client, 3, report_type='city')
    # start_date, end_date = report.get_date_range()
    #     #print start_date, end_date
    #     return render(request, 'client/performance_report.html',
    #                   {'report': report.generate_report(), 'client': client, 'start_date': start_date,
    #                    'end_date': end_date})

@task
def vendor_update():
    c = Client_Vendor.objects.filter(client__client_code = 'PTM')
    for i in range(len(c)):
        if c[i].awb_count().count() == 0:
            c[i].delete()

    for i in range(len(c)):
        c[i].vendor_code = re.sub('\.[^.]*$', '', c[i].vendor_code)
        c[i].save()

def download_vendor_dump(request):
    client_vendor = Client_Vendor.objects.all().order_by('vendor_code')
    # print len(client_vendor)
    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer)
    response = StreamingHttpResponse(
        (writer.writerow([i.vendor_code, i.vendor_name, i.owner_name, i.client.client_name, i.branch.branch_name,
                          i.pincode.pincode, i.phone]) for i in client_vendor),
        content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="vendor_dump_' + datetime.today().strftime(
        '%Y-%m-%d_%H:%M:%S') + '.csv"'
    return response