import django_tables2 as tables

from client.models import Client, Client_Warehouse, Client_Vendor
from awb.models import Manifest, AWB
from common.tables import TableMixin


class ClientTable(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    client_code = tables.TemplateColumn(template_code='{{ record.client_code }}')
    awb_range = tables.TemplateColumn(template_code='{{ record.awb_assigned_from }} - {{ record.awb_assigned_to }}',
                                      verbose_name='AWB Range', sortable=False)
    awbs_alloted = tables.Column(accessor='awbs_alloted', verbose_name='AWBs Alloted', sortable=False)
    awbs_consumed = tables.Column(accessor='awbs_consumed', verbose_name='AWBs Consumed')
    awb_last_used = tables.Column(accessor='awb_last_used', verbose_name='Last AWB Assign')
    #awbs_left = tables.Column(accessor='awbs_left', verbose_name='AWBs Left', sortable=False)
    # pan_no = tables.Column(accessor='additional.pan_no')
    # tan_no = tables.Column(accessor='additional.tan_no')
    # scheduling_time = tables.Column(accessor='additional.scheduling_time')
    # bank = tables.Column(accessor='additional.bank_name')
    # account_no = tables.Column(accessor='additional.account_no')
    # city = tables.Column(accessor='additional.city')
    # address = tables.Column(accessor='additional.address')
    # phone = tables.Column(accessor='additional.phone')

    class Meta:
        # add class="paleblue to <table> tag
        model = Client
        fields = ('s_no', 'client_code', 'client_name', 'category')
        attrs = {"class": "table table-striped table-bordered table-hover"}


class ClientWarehouseTable(tables.Table):
    class Meta:
        # add class="paleblue to <table> tag
        model = Client_Warehouse
        exclude = ['is_active', 'creation_date', 'on_update']
        attrs = {"class": "table table-striped table-bordered table-hover"}


class ClientVendorTable(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    awb_assign_count = tables.TemplateColumn(template_code= '<a href=/client/vendor/{{ record.vendor_code }}/open_awb target="_blank">{{ record.awb_open_count | length }} </a>',
    verbose_name='Open AWBs', sortable=True)
    class Meta:
        # add class="paleblue to <table> tag
        model = Client_Vendor
        sequence = ('s_no',)
        exclude = ['is_active', 'creation_date', 'on_update', 'id']
        attrs = {"class": "table table-striped table-bordered table-hover"}


class ClientDashboardTableRL(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    creation_date = tables.TemplateColumn(
        template_code='<a href=/client/manifest/{{ record.id }} target="_blank">{{ record.creation_date|date:"M d, Y" }}</a>',
        verbose_name='RPI Date')
    pickup_assigned = tables.Column(accessor='get_total_awb_count', verbose_name='PA')
    converted = tables.Column(accessor='get_converted_awb_count', verbose_name='CON')
    converted_perc = tables.Column(accessor='get_converted_perc', verbose_name='CON %')
    picked_up = tables.Column(accessor='get_picked_up_count', verbose_name='PU')
    picked_up_perc = tables.Column(accessor='get_picked_up_perc', verbose_name='PU %')
    dto = tables.Column(accessor='get_dto_count', verbose_name='DTO\'d')
    dto_perc = tables.Column(accessor='get_dto_perc', verbose_name='DTO\'d %')
    cancelled = tables.Column(accessor='get_cancelled_count', verbose_name='CAN')
    cancelled_perc = tables.Column(accessor='get_cancelled_perc', verbose_name='CAN %')
    schedueld = tables.Column(accessor='get_scheduled_count', verbose_name='SCH')
    schedueld_perc = tables.Column(accessor='get_scheduled_perc', verbose_name='SCH %')
    out_for_pickup = tables.Column(accessor='get_out_for_pickup_count', verbose_name='OFP')
    out_for_pickup_perc = tables.Column(accessor='get_out_for_pickup_perc', verbose_name='OFP %')

    class Meta:
        model = Manifest
        fields = ('creation_date',)
        sequence = ('s_no',)
        attrs = {"class": "table table-striped table-bordered table-hover table-responsive"}


class ClientDashboardTableFL(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    creation_date = tables.TemplateColumn(
        template_code='<a href=/client/manifest/{{ record.id }} target="_blank">{{ record.creation_date|date:"M d, Y" }}</a>',
        verbose_name='RPI Date')
    awb = tables.Column(accessor='get_total_awb_count', verbose_name='AWBs')
    delivered = tables.Column(accessor='get_delivered_count', verbose_name='Delivered')
    delivered_perc = tables.Column(accessor='get_delivered_perc', verbose_name='Delivered %')
    pending = tables.Column(accessor='get_pending_count', verbose_name='Pending')
    pending_perc = tables.Column(accessor='get_pending_perc', verbose_name='Pending %')
    returned = tables.Column(accessor='get_returned_count', verbose_name='Returned')
    returned_perc = tables.Column(accessor='get_returned_perc', verbose_name='Returned %')
    rto = tables.Column(accessor='get_rto_count', verbose_name='RTO')
    rto_perc = tables.Column(accessor='get_rto_perc', verbose_name='RTO %')

    class Meta:
        model = Manifest
        fields = ('creation_date',)
        sequence = ('s_no',)
        attrs = {"class": "table table-striped table-bordered table-hover table-responsive"}


class ClientAWBTable(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    # select = tables.TemplateColumn(
    #     template_code='<label><input type="checkbox" checked="true" id="{{ record.id }}"><span class="lbl"></span></label>')
    awb = tables.TemplateColumn(
        template_code='<a href="/client/awb/{{ record.awb }}" target="_blank">{{ record.awb }}</a>')
    order_id = tables.Column(accessor='order_id')
    status = tables.Column(accessor='awb_status.get_readable_choice', order_by='awb_status__status')
    customer_name = tables.TemplateColumn(template_code='{{ record.customer_name|title }}')
    address = tables.TemplateColumn(
        template_code='{{ record.address_1|title }}, {{ record.address_2|title }}, {{ record.city|title }}')
    pincode = tables.Column(accessor='pincode.pincode')

    class Meta:
        model = AWB
        sequence = ( 's_no', 'awb', 'order_id')
        fields = ('awb', 'order_id')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}

