# Create your views here.
import csv
import json
import re
from datetime import *

from django.db.models.query_utils import Q
from django.http.response import HttpResponse, HttpResponseRedirect, StreamingHttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django_tables2.config import RequestConfig
from awb.models import AWB, AWB_History, AWB_Status
from client.models import Client
from common.forms import ExcelUploadForm
from customer_service.models import PhoneNumbers, IncomingCallHistory, CustomerAWB, AWB_Out_Calling, AWBCallOutHistory
from awb.models import AWB, AWB_Status, AWB_History
from customer_service.tables import AWB_Calling_Table, AWB_Out_HistoryBoardTable, Call_Out_Admin_Table
from internal.models import get_user_by_email
from logistics.settings.base import MEDIA_ROOT
from utils.common import Echo
from utils.constants import AWB_FL_REMARKS, AWB_RL_REASON, AWB_FL_REASON, CALL_OUT_REPORT_HEADER, \
    AWB_RL_REMARKS_CALL
from utils.upload import handle_uploaded_file, awb_calling_upload_file


def incoming_call(request):
    template = 'customer_service/call_notifications.html'
    context = dict()
    if request.GET.get('DialWhomNumber') and request.GET.get('Status'):

        call_from = str(request.GET.get('From'))
        customer_no = re.sub(r'^0*', '', call_from)

        data = dict()
        data['call_sid'] = request.GET.get('CallSid')
        data['call_from'] = customer_no
        data['call_to'] = request.GET.get('CallTo')
        data['call_status'] = request.GET.get('Status')
        data['call_direction'] = request.GET.get('Direction')
        data['call_start_time'] = datetime.strptime(request.GET.get('Created'), '%a, %d %b %Y %H:%M:%S')

        agent = get_user_by_email(request.GET.get('AgentEmail')).profile

        try:
            phone_no = PhoneNumbers.objects.get(phone_no=str(request.GET.get('DialWhomNumber')))
            if phone_no.assigned_to != agent:
                phone_no.assigned_to = agent
                phone_no.save()

            data['agent'] = phone_no.assigned_to
        except PhoneNumbers.DoesNotExist:
            phone_no = PhoneNumbers.objects.create(phone_no=str(request.GET.get('DialWhomNumber')),
                                                   assigned_to=agent)

        data['agent_no'] = phone_no
        data['agent'] = phone_no.assigned_to

        awbs = AWB.objects.filter(Q(phone_1=customer_no) | Q(phone_2=customer_no))

        if awbs.exists():
            data['customer_phone'] = customer_no
            data['customer_name'] = awbs[0].customer_name

        try:
            incoming_call = IncomingCallHistory.objects.get(call_sid=request.GET.get('CallSid'))
            incoming_call.call_status = request.GET.get('Status')
            incoming_call.save()
        except IncomingCallHistory.DoesNotExist:
            incoming_call = IncomingCallHistory.objects.create(**data)

        try:
            insert_list = list()
            for awb in awbs:
                insert_list.append(CustomerAWB(customer=incoming_call, awb=awb))
            if insert_list:
                CustomerAWB.objects.bulk_create(insert_list)
        except:
            pass

            # except:
            # pass
    return render(request, template, context)


@csrf_exempt
def call_details(request):
    template = 'customer_service/call_details.html'

    context = dict()
    filter = dict()
    filter['call_status__in'] = ['ringing', 'busy']
    filter['call_closed'] = False

    # print "ohh yes"

    if request.session.get('agent_no'):
        filter['agent_no__phone_no'] = request.session.get('agent_no')

    else:
        try:
            if request.user.profile.role == 'CS':
                filter['agent'] = request.user.profile
        except:
            pass

    exclude = dict()

    if request.POST.get('call_sids'):
        exclude['call_sid__in'] = [str(i) for i in json.loads(request.POST.get('call_sids')) if i != '']

    context['incoming_calls'] = IncomingCallHistory.objects.filter(**filter).exclude(**exclude).order_by(
        '-creation_date').select_related('customer_awb__awb_status',
                                         'agent__user', 'agent_no').prefetch_related('customer_awbs')

    # print 'context', context
    return render(request, template, context)


def close_incoming_call(request):
    # print request.POST
    if request.method == 'POST':
        call = IncomingCallHistory.objects.filter(call_sid=str(request.POST.get('call_sid')))
        # print call, 'call'
        response = call.update(call_closed=True)
        # print 'response', response
        return HttpResponse(response)


def get_customer_awbs(request):
    call_sid = IncomingCallHistory.objects.get(call_sid=request.GET.get('call_sid'))
    query = str(request.GET.get('q')).strip().upper()
    awbs = AWB.objects.filter(Q(phone_1=query) | Q(phone_2=query) | Q(awb=query)).select_related('awb_status')

    if awbs.exists():
        context = dict()
        context['customer_name'] = awbs[0].customer_name
        context['customer_awbs'] = awbs

        insert_list = list()
        for awb in awbs:
            context['customer_name'] = awb.customer_name
            insert_list.append(CustomerAWB(customer=call_sid, awb=awb))

        try:
            CustomerAWB.objects.bulk_create(insert_list)
        except:
            pass

        template = 'customer_service/customer_awbs.html'
        return render(request, template, context)
    else:
        return HttpResponse()


# def calling_outbound_upload(request):
# if request.method == 'POST':
# form = ExcelUploadForm(request.POST, request.FILES)
# if form.is_valid():
# file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
# MEDIA_ROOT + 'uploads/client/')
# email = request.user.email
# awb_calling_upload_file(file, email)
# message = 'Your file is being uploaded'
# return HttpResponseRedirect('/customer/awb/calling_details')
# else:
# form = ExcelUploadForm()
# return render(request, 'awb/volumetric_upload.html', {'form': form, 'msg': 'Calling File Upload'})
#
#
# def calling_details(request):
# if request.method == 'POST' and request.is_ajax():
# phone_no = request.POST['phone_no']
# print phone_no
# a = AWB_Out_Calling.objects.filter(phone_no=phone_no)
# calling_awb = AWB_Calling_Table(AWB_Out_Calling.objects.filter(phone_no=phone_no).order_by('-creation_date'))
# RequestConfig(request, paginate={"per_page": 100}).configure(calling_awb)
# print a.count()
# print calling_awb
# return render(request, 'customer/ajax_call_out_details.html', {'calling_awb': calling_awb})
# else:
# print 'else'
# calling_awb = AWB_Calling_Table(AWB_Out_Calling.objects.all().order_by('-creation_date'))
# RequestConfig(request, paginate={"per_page": 100}).configure(calling_awb)
# return render(request, 'customer/calling_out_details.html', {'calling_awb': calling_awb})
#
#
# def awb_out_history(request, id):
# # if request.method == 'POST' and request.is_ajax():
# # awb = request.POST['awb']

def calling_outbound_upload(request):
    if request.method == 'POST':
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/client/')
            email = request.user.email
            awb_calling_upload_file.delay(file, email)
            message = 'Your file is being uploaded'
            return HttpResponseRedirect('/cs/awb/calling_details')
    else:
        form = ExcelUploadForm()
    return render(request, 'awb/volumetric_upload.html', {'form': form, 'msg': 'Calling File Upload'})

@csrf_exempt
def calling_details(request):
    s = str(datetime.today().date()) + ' 00:00:00'
    e = str(datetime.today().date()) + ' 23:59:59'
    if request.method == 'POST' and request.is_ajax():
        agent_no = request.POST['agent_no']
        request.session['from_no'] = agent_no
        calling_awb = AWB_Calling_Table(
            AWB_Out_Calling.objects.filter(agent_no=agent_no, call_made=False, creation_date__range=[s, e]).order_by(
                '-creation_date'))
        RequestConfig(request, paginate={"per_page": 100}).configure(calling_awb)
        return render(request, 'customer_service/ajax_call_out_details.html', {'calling_awb': calling_awb})
    # elif request.session['agent_no'] != '':
    # calling_awb = AWB_Calling_Table(AWB_Out_Calling.objects.filter(call_made=False, agent_no=request.session['agent_no']).order_by('-creation_date'))
    # RequestConfig(request, paginate={"per_page": 100}).configure(calling_awb)
    # return render(request, 'customer/calling_out_details.html', {'calling_awb': calling_awb})
    else:
        calling_awb = AWB_Calling_Table(
            AWB_Out_Calling.objects.filter(call_made=False, agent_no=request.session.get('from_no'),
                                           creation_date__range=[s, e]).order_by('-creation_date'))
        RequestConfig(request, paginate={"per_page": 100}).configure(calling_awb)
        phone = PhoneNumbers.objects.all()
        return render(request, 'customer_service/calling_out_details.html', {'calling_awb': calling_awb, 'phone': phone})


def awb_out_history(request, id):
    if id != '':
        c = {}
        awb = AWB_Out_Calling.objects.get(pk=id)
        awbs = AWB.objects.get(awb=awb.awb)
        awb_out_call = AWB_Out_Calling.objects.get(pk=id)
        awbs = AWB.objects.get(awb=awb_out_call.awb)
        awb_hist = awbs.get_awb_history(True)
        return render(request, 'customer_service/calling_out_history.html', {'awb_details': awbs, 'awb_hist': awb_hist,
                                                                             'awb': awbs,
                                                                             'awb_rl_remarks': AWB_RL_REMARKS_CALL,
                                                                             'awb_fl_remarks': AWB_FL_REMARKS,
                                                                             'awb_rl_reason': AWB_RL_REASON,
                                                                             'awb_fl_reason': AWB_FL_REASON})
    else:
        return HttpResponse('AWB Not Exist in our System')


def call_made(request):
    if request.method == 'POST':
        # print request.POST
        awb = AWB.objects.get(awb=request.POST['awb'])
        awb_c = AWB_Out_Calling.objects.get(awb=awb)
        # print awb_c
        call_made = request.POST['call_made']
        if call_made == 'true':
            awb_c.call_made = True
        else:
            awb_c.call_made = False
        awb_c.save()
        return HttpResponse(True)
    else:
        return HttpResponse(False)


def call_history_table(request):
    call_history = AWB_Out_HistoryBoardTable(AWBCallOutHistory.objects.all().order_by('-creation_date'))
    RequestConfig(request, paginate={"per_page": 100}).configure(call_history)
    return render(request, 'customer_service/call_out_history_table.html', {'call_history': call_history})


def call_out_report(request):
    if request.method == 'POST':
        # print request.POST
        try:
            start_date = request.POST['start_date'] + ' 00:00:00'
            end_date = request.POST['end_date'] + ' 23:59:59'
            client_code = request.POST['client']
            if client_code != '':
                awb_calling = AWB_Out_Calling.objects.filter(creation_date__range=[start_date, end_date], client__client_code=client_code)
            else:
                awb_calling = AWB_Out_Calling.objects.filter(creation_date__range=[start_date, end_date])
            rows = [[awb.awb.order_id, awb.awb.awb, awb.awb.get_call_client_status(), awb.awb.get_call_client_remark(), awb.awb.get_call_count()] for awb in awb_calling]
            rows.insert(0, CALL_OUT_REPORT_HEADER)
            pseudo_buffer = Echo()
            writer = csv.writer(pseudo_buffer)
            response = StreamingHttpResponse(
                (writer.writerow(row) for row in rows),
                content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename="CALL_OUT_REPORT_' + request.POST['start_date'] \
                                          + '__' + request.POST['end_date'] + '.csv"'
            return response
        except:
            pass
    else:
        return render(request, 'customer_service/call_out_report_form.html', {'clients': Client.objects.all()})
        # return render(request, 'customer_service/call_out_report_form.html')

def call_out_dasboard_admin(request):
    call_out = Call_Out_Admin_Table(AWB_Out_Calling.objects.all().order_by('-creation_date'))
    # print call_out
    RequestConfig(request, paginate={"per_page": 100}).configure(call_out)
    return render(request, 'customer_service/call_out_history_table.html', {'call_history': call_out})
