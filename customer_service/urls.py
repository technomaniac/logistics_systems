from django.conf.urls import patterns, url

urlpatterns = patterns('customer_service.views',
                       # url(r'home$',TemplateView.as_view(template_name='client/pincode.html'), name="home"),
                       # url(r'^manage_fe/', include(fe_patterns), name='manage_fe'),
                       url(r'^call/incoming/', 'incoming_call', name='incoming_call'),

                       url(r'^call/details/', 'call_details', name='call_details'),
                       url(r'^call/close/', 'close_incoming_call', name='close_incoming_call'),
                       url(r'^get_customer_awbs/', 'get_customer_awbs', name='get_csutomer_awbs'),
                       # url(r'^calling_outbound_upload$', 'calling_outbound_upload', name='calling_outbound_detail'),
                       # url(r'^awb/calling_details$', 'calling_details', name='calling_details'),

                       url(r'^calling_outbound_upload$', 'calling_outbound_upload', name='calling_outbound_detail'),
                       url(r'^awb/calling_details$', 'calling_details', name='calling_details'),
                       url(r'^awb/awb_history/(\d+)$', 'awb_out_history', name='calling_out_history'),
                       url(r'^call_made$', 'call_made', name='call_made'),
                       url(r'^call_out_his_table$', 'call_history_table', name='call_history_table'),
                       url(r'^call_out/report$', 'call_out_report', name='call_out_report'),
                       url(r'^call_out_dasboard_admin$', 'call_out_dasboard_admin', name='call_out_dasboard_admin')
)
