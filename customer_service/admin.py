# Register your models here.
from django.contrib import admin

from customer_service.models import *


class AWB_Out_CallAdmin(admin.ModelAdmin):
    search_fields = ['awb__awb']
    list_display = ('awb', 'call_made', 'agent_no', 'customer_no', 'caller_id', 'client', 'dnd_no')


class AWB_Out_HistoryAdmin(admin.ModelAdmin):
    search_fields = ['awb__awb']


class CallHistoryAdmin(admin.ModelAdmin):
    search_fields = ['call_from']
    list_display = ('call_from', 'call_to', 'call_sid', 'call_duration', 'call_status', 'customer_phone')


admin.site.register(AWB_Out_Calling, AWB_Out_CallAdmin)
admin.site.register(AWBCallOutHistory, AWB_Out_HistoryAdmin)
admin.site.register(IncomingCallHistory)
admin.site.register(PhoneNumbers)
