import django_tables2 as tables

from common.tables import TableMixin
from customer_service.models import AWB_Out_Calling, AWBCallOutHistory


__author__ = 'naveen'


class AWB_Calling_Table(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    customer_no = tables.Column(accessor='get_customer_no', verbose_name='Customer No')
    agent_no = tables.Column(accessor='get_agent_no', verbose_name='Care No')
    call_count = tables.Column(accessor='awb.get_call_count', verbose_name='Call Count')
    remark = tables.Column(accessor='awb.get_remark', verbose_name='Remark')
    reason_for_return = tables.Column(accessor='awb.reason_for_return', verbose_name='Customer Issue')
    status = tables.Column(accessor='awb.get_readable_status', verbose_name='Status')
    reason = tables.Column(accessor='awb.get_reason', verbose_name='Reason')
    creation_date = tables.Column(accessor='creation_date', verbose_name='Upload Date')
    call = tables.TemplateColumn(
        template_code='<a href="/cs/awb/awb_history/{{record.id}}" target="_blank"<label><img type="image" '
                      'src="/static/img/call.png" id="out_call" style="width:35px;height:35px"></label></a>')
    call_done = tables.TemplateColumn(template_code='<label></label>',
                                      verbose_name='Call Done')

    class Meta:
        model = AWB_Out_Calling
        fields = ('s_no', 'awb', 'agent_no')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}


class AWB_Out_HistoryBoardTable(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    class Meta:
        model = AWBCallOutHistory
        fields = ('s_no', 'customer_awb', 'agent_no', 'customer_no', 'call_status', 'call_duration', 'call_direction', 'call_price', 'creation_date')
        # exclude = ('is_active', 'creation_date', 'on_update', 'id')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}

class Call_Out_Admin_Table(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    class Meta:
        model = AWB_Out_Calling
        fields = ('s_no', 'awb', 'client', 'agent_no', 'customer_no', 'call_made', 'dnd_no', 'creation_date')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}