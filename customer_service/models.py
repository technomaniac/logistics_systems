from django.db import models

from common.models import Time_Model
from internal.models import Employee


class PhoneNumbers(Time_Model):
    phone_no = models.CharField(unique=True, max_length=50, db_index=True)
    assigned_to = models.ForeignKey(Employee, db_index=True)

    class Meta:
        unique_together = (('phone_no', 'assigned_to'),)

    def __unicode__(self):
        return self.phone_no


class IncomingCallHistory(Time_Model):
    # DIRECTION = (
    # ('IN', 'Incoming'),
    # ('OUT', 'Outgoing'),
    # )
    # call details
    call_sid = models.CharField(max_length=128, null=True, blank=True, unique=True, db_index=True)
    call_from = models.CharField(max_length=20, null=True, blank=True)
    call_to = models.CharField(max_length=20, null=True, blank=True)
    call_duration = models.CharField(max_length=50, null=True, blank=True)
    call_direction = models.CharField(max_length=10, null=True, blank=True)
    call_start_time = models.DateTimeField(null=True, blank=True)
    call_end_time = models.DateTimeField(null=True, blank=True)
    call_status = models.CharField(max_length=50, null=True, blank=True)
    call_closed = models.BooleanField(default=False)
    call_recording = models.CharField(max_length=255, null=True, blank=True)
    # customer details
    customer_name = models.CharField(max_length=128, null=True, blank=True)
    customer_phone = models.CharField(max_length=20, null=True, blank=True)
    # agent details
    agent = models.ForeignKey(Employee, null=True, blank=True)
    agent_no = models.ForeignKey(PhoneNumbers, null=True, blank=True)
    # def save(self, **kwargs):
    # if self.call_from

    class Meta:
        index_together = (('call_status', 'call_closed', 'agent_no'), ('call_status', 'call_closed', 'agent'))

    def save(self, **kwargs):
        super(IncomingCallHistory, self).save(**kwargs)
        from customer_service.tasks import update_incoming_call_task

        if self.call_status == 'busy':
            update_incoming_call_task.delay(self.call_sid)

        if self.call_status == 'free':
            update_incoming_call_task.apply_async(args=[self.call_sid], countdown=20)

    # if self.call_froms
    def __unicode__(self):
        return '%s | %s | %s | %s | %s | %s' % (
            self.call_status, self.call_from, self.agent_no, self.call_start_time, self.call_end_time, self.call_closed)


    def get_customer_awbs(self):
        return self.customer_awbs  # if self.call_from


class CustomerAWB(Time_Model):
    customer = models.ForeignKey(IncomingCallHistory, related_name='customer_awbs', db_index=True)
    awb = models.ForeignKey('awb.AWB')

    class Meta:
        unique_together = (('customer', 'awb'),)


class AWB_Out_Calling(Time_Model):
    awb = models.ForeignKey('awb.AWB', related_name='awb_out_calling', null=True, blank=True, db_index=True)
    agent_no = models.CharField(max_length=20, null=True, blank=True)
    customer_no = models.CharField(max_length=20, null=True, blank=True)
    caller_id = models.CharField(max_length=20, blank=True, null=True)
    call_made = models.BooleanField(default=False)
    client = models.ForeignKey('client.Client', null=True, blank=True, db_index=True)
    dnd_no = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'AWB_Out_Calling'
        verbose_name_plural = 'AWB_Out_Calling'

    def __unicode__(self):
        return unicode(self.awb)

    def get_customer_no(self):
        phone_no = self.awb.get_phone_1()
        len_phone = len(phone_no)
        if len_phone == 10:
            self.customer_no = '0' + phone_no
            self.save()
            return '0' + phone_no
        else:
            diff_len = len_phone - 10
            self.customer_no = '0' + phone_no[diff_len:]
            self.save()
            return '0' + phone_no[diff_len:]


    def get_agent_no(self):
        return self.agent_no


class AWBCallOutHistory(Time_Model):
    customer_awb = models.ForeignKey('awb.AWB', null=True, blank=True, db_index=True)
    agent_no = models.CharField(max_length=20, null=True, blank=True)
    customer_no = models.CharField(max_length=20, null=True, blank=True)
    call_status = models.CharField(max_length=20, null=True, blank=True)
    start_time = models.CharField(max_length=100, null=True, blank=True)
    end_time = models.CharField(max_length=100, null=True, blank=True)
    call_duration = models.CharField(max_length=20, null=True, blank=True)
    call_price = models.CharField(max_length=50, null=True, blank=True)
    call_direction = models.CharField(max_length=20, default='OUT')
    recording_url = models.CharField(max_length=500, null=True, blank=True)
    call_sid = models.CharField(max_length=300, null=True, blank=True)

    class Meta:
        verbose_name = 'AWB_Out_history'
        verbose_name_plural = 'AWB_Out_history'

    def __unicode__(self):
        return self.call_sid
