import datetime
import json

from celery.task import task, periodic_task
import requests
import xmltodict

from customer_service.models import AWBCallOutHistory
from rest_api.views import awb_out_details_api
from custom.custom_mixins import WebSocket
from customer_service.models import IncomingCallHistory
from utils.constants import EXOTEL_CALL_DETAILS_URL
from utils.random import get_title
from utils.time_funcs import convert_sec_to_min


def get_call_details(call_sid):
    URL = EXOTEL_CALL_DETAILS_URL + call_sid
    r = requests.get(URL)
    response = r.content
    # print response
    response_dict = xmltodict.parse(response)
    return response_dict['TwilioResponse']['Call']


@task
def update_incoming_call_task(call_sid):
    call_details = get_call_details(call_sid)

    call_obj = IncomingCallHistory.objects.get(call_sid=call_sid)
    call_obj.call_status = call_details['Status']
    call_obj.call_duration = '%s Min %s Sec' % convert_sec_to_min(call_details['Duration'])
    call_obj.call_end_time = datetime.datetime.strptime(call_details['EndTime'], '%Y-%m-%d %H:%M:%S')
    call_obj.call_recording = call_details['RecordingUrl']

    if call_obj.call_status == 'completed':
        call_obj.call_closed = True

    call_obj.save()

    message = dict()
    message['call_sid'] = call_sid
    message['status'] = get_title(call_details['Status'])

    socket = WebSocket()
    socket.prepare_socket(facility='cs', broadcast=True)
    socket.send_message(**message)


@periodic_task(run_every=datetime.timedelta(hours=1))
def get_out_details():
    call_sids = AWBCallOutHistory.objects.filter(call_status='in-progress')
    for sid in call_sids:
        details = awb_out_details_api(sid.call_sid)

        history = json.loads(details)

        sid.call_status = history['Call']['Status']
        sid.start_time = history['Call']['StartTime']
        sid.end_time = history['Call']['EndTime']
        sid.call_duration = history['Call']['Duration']
        sid.call_price = history['Call']['Price']
        sid.recording_url = history['Call']['RecordingUrl']

        sid.save()


@periodic_task(run_every=datetime.timedelta(minutes=1), queue='cs')
def auto_update_call_details():
    calls = IncomingCallHistory.objects.exclude(call_status='completed')
    if calls.exists():
        for call in calls:
            update_incoming_call_task.delay(call.call_sid)
