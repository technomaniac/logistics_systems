import string
import time

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User
from django.contrib import auth

from common.models import Time_Model
from awb.models import AWB
from utils.random import get_title
from utils.constants import BRANCH_DICT


# Create your models here.

class Branch(Time_Model):
    branch_name = models.CharField(max_length=50, unique=True, db_index=True)
    branch_manager = models.OneToOneField(User, related_name='branch_employee', null=True, blank=True, db_index=True)
    city = models.ForeignKey('zoning.City', null=True, blank=True, db_index=True)
    address = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    phone = models.CharField(max_length=13, null=True, blank=True, db_index=True)

    class Meta:
        verbose_name = 'Branch'
        index_together = [['branch_name', 'branch_manager', 'city', 'address', 'phone'], ]

    def __unicode__(self):
        return self.branch_name

    def get_readable_branch_name(self):
        try:
            return BRANCH_DICT[self.branch_name]
        except:
            return self.branch_name

    def get_awbs(self, status=[], type=''):
        branch_pincodes = self.branch_pincodes.all().select_related('pincode__awb')
        pincodes = []
        for branch_pincode in branch_pincodes:
            pincodes.append(branch_pincode.pincode)
        if status and type:
            awbs = AWB.objects.filter(category=type, awb_status__status__in=status, pincode__in=pincodes)
        else:
            awbs = AWB.objects.filter(pincode__in=pincodes)
        return awbs

    def get_cash(self, type):
        filter = {}
        filter['creation_date__startswith'] = time.strftime("%Y-%m-%d")
        amt = 0
        if type == 'expected':
            queryset = self.drs_set.filter(**filter)
            for drs in queryset:
                amt += drs.get_expected_amount()
        else:
            filter['status'] = 'C'
            queryset = self.drs_set.filter(**filter)
            for drs in queryset:
                amt += drs.get_collected_amount()
        return amt if amt > 0 else 0


    def get_pincode_count(self):
        return self.branch_pincodes.count()

    def get_city(self):
        try:
            return self.city.pk
        except Exception:
            return ''


class Branch_Pincode(Time_Model):
    branch = models.ForeignKey('internal.Branch', related_name="branch_pincodes", db_index=True)
    pincode = models.OneToOneField('zoning.Pincode', related_name="branch_pincode", db_index=True)

    def __unicode__(self):
        return self.branch.branch_name

    class Meta:
        index_together = [['branch', 'pincode'], ]
        verbose_name = 'Branch Pincodes'


class Employee(Time_Model):
    USER_ROLE = (
        ('ADM', 'Admin'),
        ('OM', 'Operation Manager'),
        ('BM', 'Branch Manager'),
        ('FE', 'Field Executive'),
        ('FIN', 'Finance'),
        ('CS', 'Customer Service'),
        ('HR', 'HR'),
        ('CL', 'Client'),
        ('OT', 'Other'),
    )
    user = models.OneToOneField(User, related_name='profile', db_index=True)
    branch = models.ForeignKey('internal.Branch', null=True, blank=True, db_index=True)
    role = models.CharField(choices=USER_ROLE, max_length=3, db_index=True)
    client = models.ForeignKey('client.Client', null=True, blank=True, db_index=True)
    city = models.ForeignKey('zoning.City', null=True, blank=True, db_index=True)
    address = models.CharField(max_length=50, null=True, blank=True, help_text='Optional', db_index=True)
    phone = models.CharField(max_length=50, null=True, blank=False,
                             validators=[RegexValidator('[0-9]+', 'Only numbers')], db_index=True)

    class Meta:
        index_together = [['user', 'branch', 'role', 'client', 'city', 'address', 'phone'], ]

    def clean(self):
        if self.phone:
            pn = set(string.ascii_letters).intersection(set(self.phone))
            if pn:
                raise ValidationError('Only numbers in phone number')

    def __unicode__(self):
        return get_title(self.user.get_full_name())


class Vehicle(Time_Model):
    VEHICLE_TYPE = (
        ('2W', 'Two Wheeler'),
        ('3W', 'Three Wheeler'),
        ('4W', 'Four Wheeler'),
        ('HV', 'Heavy Vehicle')
    )
    vehicle_no = models.CharField(max_length=50, unique=True)
    vehicle_type = models.CharField(choices=VEHICLE_TYPE, max_length=2)
    driver_name = models.ForeignKey(User, blank=True, null=True)
    branch = models.ForeignKey(Branch, blank=True, null=True)

    def __unicode__(self):
        return self.vehicle_no


class UserEmails(Time_Model):
    user = models.ForeignKey(User)
    email = models.EmailField(max_length=50)

    def __unicode__(self):
        return self.email


def get_full_name(self):
    return get_title(self.get_full_name())


auth.models.User.add_to_class('__unicode__', get_full_name)


def get_user_by_email(email):
    try:
        user = User.objects.get(email=email)
    except User.DoesNotExist:
        try:
            user = UserEmails.objects.get(email)
        except:
            user = None
    return user














