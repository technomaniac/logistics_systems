from django.conf.urls import patterns, url, include
from .views import AddEditFe, fe_list, remove_fe
# from .views import EditFe

# fe_patterns = patterns(
#     url(r'^$', fe_list, name='fe_list'),
#     url(r'^(?P<fe_pk>\d+)$', AddEditFe.as_view(), name='edit_fe'),
#     url(r'^add$', AddEditFe.as_view(), name='add_fe'),
#     url(r'^remove$', remove_fe, name='remove_fe'),
# )

urlpatterns = patterns('internal.views',
                       #url(r'home$',TemplateView.as_view(template_name='client/pincode.html'), name="home"),
                       #url(r'^manage_fe/', include(fe_patterns), name='manage_fe'),
                       url(r'^branch$', 'branch', name='show_branch'),
                       url(r'^branch/cash$', 'branch_get_cash', name='branch_get_cash'),
                       url(r'^branch/add$', 'add_branch', name='add_branch'),
                       url(r'^branch_pincode/add$', 'add_branch_pincode', name='add_branch_pincode'),
                       url(r'^employee$', 'employee', name='show_employee'),
                       url(r'^employee/add$', 'add_employee', name='add_employee'),
                       url(r'^vehicle$', 'vehicle', name='show_vehicle'),
                       url(r'^vehicle/add$', 'add_vehicle', name='add_vehicle'),
                       url(r'^vehicle/upload_list$', 'upload_vehicle_list', name='vehicle_upload_list'),
                       url(r'^branch/upload$', 'upload_branch_pincode', name='upload_branch_pincode'),
                       url(r'^branch/(\d+)/pincode$', 'branch_pincode', name='show_branch_pincode'),
                       url(r'^branch/get_all$', 'branch_get_all', name='branch_get_all'),
                       url(r'^branch/report(?P<cash>.*)$', 'branch_cash_report', name='branch_cash_report'),
                       url(r'^manage_fe$', fe_list, name='fe_list'),
                       url(r'^manage_fe/(?P<fe_pk>\d+)$', AddEditFe.as_view(), name='edit_fe'),
                       url(r'^manage_fe/add$', AddEditFe.as_view(), name='add_fe'),
                       url(r'^manage_fe/remove$', remove_fe, name='remove_fe'),
)
