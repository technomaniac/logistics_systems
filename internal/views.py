import re
import time

from django.contrib.auth.models import User
from django.db import transaction
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.base import View
from django_tables2 import RequestConfig
from django.contrib.auth.decorators import login_required

from common.tasks import send_fe_action_notification
from custom.custom_mixins import GeneralLoginRequiredMixin, CommonViewMixin
from custom.forms import automodelform_factory
from internal.tables import BranchTable, EmployeeTable, VehicleTable, BranchPincodeTable, FETable
from internal.models import Branch, Employee, Vehicle, Branch_Pincode
from internal.forms import BranchForm, EmployeeForm, VehicleForm, BranchPincodeForm, BranchDropDownForm, UserForm
from common.forms import ExcelUploadForm
from awb.models import AWB
from awb.tables import AWBCashTable
from userlogin.functionality.actions import autocreateuser
from utils.common import convert_request_querydict_to_dict
from utils.upload import handle_uploaded_file, upload_branch_pincode_file, upload_vehicle_list_file
from logistics.settings import MEDIA_ROOT


@login_required(login_url='/login')
def branch(request):
    table = BranchTable(Branch.objects.all().order_by('id'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'internal/branch.html', {'table': table})


def add_branch(request):
    if request.method == "POST":
        form = BranchForm(request.POST, instance=Branch())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/internal/branch')
    else:
        form = BranchForm()
    return render(request, 'common/form.html', {'form': form, 'model': 'branch'})


def upload_branch_pincode(request):
    if request.method == "POST":
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/internal/')
            upload_branch_pincode_file(file)
            return HttpResponseRedirect('/internal/branch')
    else:
        form = ExcelUploadForm()
    return render(request, 'common/upload_form.html', {'form': form})


def branch_pincode(request, branch_id):
    table = BranchPincodeTable(Branch_Pincode.objects.filter(branch_id=branch_id))
    RequestConfig(request, paginate={"per_page": 1000}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'model': 'branch pincode'})


def employee(request):
    table = EmployeeTable(Employee.objects.all())
    RequestConfig(request, paginate={"per_page": 1000}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'model': 'employee', 'url': 'employee/add'})


def add_employee(request):
    if request.method == "POST":
        form = EmployeeForm(request.POST, instance=Employee())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/internal/employee')
    else:
        form = EmployeeForm()
    return render(request, 'common/form.html', {'form': form, 'model': 'employee'})


def vehicle(request):
    table = VehicleTable(Vehicle.objects.all())
    RequestConfig(request, paginate={"per_page": 1000}).configure(table)
    return render(request, 'internal/vehicle.html', {'table': table})


def add_vehicle(request):
    if request.method == "POST":
        form = VehicleForm(request.POST, instance=Vehicle())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/internal/vehicle')
    else:
        form = VehicleForm()
    return render(request, 'internal/addvehicle.html', {'form': form})


def upload_vehicle_list(request):
    if request.method == "POST":
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/internal/')
            upload_vehicle_list_file(file)
            return HttpResponseRedirect('/internal/vehicle')
    else:
        form = ExcelUploadForm()
    return render(request, 'common/bike_upload_form.html', {'form': form})


def add_branch_pincode(request):
    if request.method == "POST":
        form = BranchPincodeForm(request.POST, instance=Branch_Pincode())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/internal/branch_pincode')
    else:
        form = BranchPincodeForm()
    return render(request, 'internal/add_branch_pincode.html', {'form': form})


def branch_get_all(request):
    if request.method == 'POST' and request.is_ajax():
        if 'branch' in request.session:
            form = BranchDropDownForm(initial={'branch': request.session['branch']})
        else:
            form = BranchDropDownForm(initial={'branch': Branch.objects.get(branch_name='HQ').pk})
        return render(request, 'common/dropdown.html', {'form': form})


def branch_get_cash(request):
    if request.is_ajax():
        if 'branch' in request.session:
            cash = Branch.objects.get(pk=request.session['branch']).get_cash(request.GET['type'])
        else:
            cash = sum([branch.get_cash(request.GET['type']) for branch in Branch.objects.exclude(pk=1)])
        return HttpResponse(re.sub('\.[0]*$', '', str(cash)))


def branch_cash_report(request, cash):
    include = {}
    exclude = {}
    include['category'] = 'COD'
    #include['awb_status__status__in'] = ['DRS', 'DEL']
    include['awb_status__current_drs__creation_date__startswith'] = time.strftime("%Y-%m-%d")
    if 'branch' in request.session:
        include['awb_status__current_branch_id'] = request.session['branch']
    if request.GET['cash'] == 'collected':
        exclude['awb_status__collected_amt'] = None
        include['awb_status__current_drs__status'] = 'C'

    fl_tbl = AWBCashTable(
        AWB.objects.filter(**include).exclude(**exclude).order_by('-awb_status__current_drs__creation_date'))
    RequestConfig(request).configure(fl_tbl)
    return render(request, 'awb/awb.html', {'fl_tbl': fl_tbl})


def fe_list(request):
    if 'branch' in request.session:
        fe_list = Employee.objects.filter(branch_id=request.session['branch'], role='FE',
                                          user__is_staff=True).order_by('-creation_date')
    else:
        fe_list = Employee.objects.filter(role='FE', user__is_staff=True).order_by('-creation_date')
    fe_tbl = FETable(fe_list)
    RequestConfig(request, paginate={"per_page": 1000}).configure(fe_tbl)
    return render(request, 'common/table.html', {'table': fe_tbl, 'model': 'FE', 'url': '/internal/manage_fe/add'})


def remove_fe(request):
    if request.method == 'POST' and request.is_ajax():
        try:
            fe = Employee.objects.get(pk=request.POST['fe_id'])
            user = User.objects.get(profile=fe)
            user.is_staff = False
            user.save()

            send_fe_action_notification('removed', fe, request.user)
            return HttpResponse(True)
        except:
            return HttpResponse('Some error occurred')


class AddEditFe(GeneralLoginRequiredMixin, CommonViewMixin, View):
    template_name = 'common/multiple_form.html'

    def get(self, request, *args, **kwargs):
        return super(AddEditFe, self).get(request, *args, **kwargs)

    @transaction.atomic()
    def non_ajax_post(self, request, *args, **kwargs):
        post_dict = convert_request_querydict_to_dict(request.POST)
        #post_dict.update({'role': 'FE'})
        send_email = False
        user_from = self.user_form(post_dict, prefix='user_form')
        fe_form = self.fe_form(post_dict, request.FILES, prefix='fe_form', instance=self.fe_inst)
        if fe_form.is_valid() and user_from.is_valid():
            if self.fe_inst:
                #print 'old user'
                user = self.fe_inst.user
                user.first_name = user_from.cleaned_data['first_name']
                user.last_name = user_from.cleaned_data['last_name']
                user.save()
            else:
                #print 'user created'
                user = autocreateuser(**user_from.cleaned_data)
                send_email = True
            fe_inst = fe_form.save(commit=False)
            fe_inst.user = user
            fe_inst.role = 'FE'
            if request.session.get('branch'):
                fe_inst.branch = Branch.objects.get(pk=request.session.get('branch'))
            fe_inst.save()
            if send_email:
                send_fe_action_notification('added', fe_inst, request.user)
            return HttpResponseRedirect('/internal/manage_fe')
        else:
            # print 'errors'
            # print user_from.errors, fe_form.errors
            return self.response_class(request=request,
                                       template=self.get_template_names(),
                                       context={'forms': [user_from, fe_form], 'model': 'fe'
                                       })

    def initialize_view(self, request, *args, **kwargs):
        if request.session.get('branch'):
            self.fe_form = automodelform_factory(model=Employee,
                                                 exclude=['role', 'client', 'user', 'is_active', 'branch', 'address',
                                                          'city'], onetoones=['user'])
        else:
            self.fe_form = automodelform_factory(model=Employee,
                                                 exclude=['role', 'client', 'user', 'is_active', 'address', 'city'],
                                                 onetoones=['user'])
        self.user_form = UserForm
        emp = Employee.objects.filter(pk=kwargs.get('fe_pk')).first()
        self.fe_inst = emp
        self.user_inst = emp.user if emp else None

    def get_context(self, request, *args, **kwargs):
        context = dict()
        user_data = dict()
        if self.user_inst:
            user_data.update({'first_name': self.user_inst.first_name, 'last_name': self.user_inst.last_name})
        context['forms'] = [
            self.user_form(initial=user_data, prefix='user_form') if user_data else self.user_form(prefix='user_form'),
            self.fe_form(prefix='fe_form', instance=self.fe_inst)]
        context['model'] = 'fe'
        return context


