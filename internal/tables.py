import django_tables2 as tables
from internal.models import Branch, Vehicle, Employee, Branch_Pincode
from common.tables import TableMixin


class BranchTable(tables.Table):
    pincodes = tables.TemplateColumn(
        template_code='<a href="branch/{{ record.id }}/pincode">{{ record.get_pincode_count }}</a>')
    # action = tables.TemplateColumn(
    #     template_code='<div class="hidden-phone visible-desktop btn-group"><button class="btn btn-mini btn-info" id="editBranch"><i class="icon-edit bigger-120"></i></button></div>')

    class Meta:
        # add class="paleblue to <table> tag
        model = Branch
        exclude = ['creation_date', 'on_update', 'is_active']
        attrs = {"class": "table table-striped table-bordered table-hover"}


class BranchPincodeTable(tables.Table):
    class Meta:
        # add class="paleblue to <table> tag
        model = Branch_Pincode
        exclude = ['creation_date', 'on_update', 'is_active']
        attrs = {"class": "table table-striped table-bordered table-hover"}


class EmployeeTable(tables.Table):
    class Meta:
        # add class="paleblue to <table> tag
        model = Employee
        exclude = ['creation_date', 'on_update', 'is_active']
        attrs = {"class": "table table-striped table-bordered table-hover"}


class VehicleTable(tables.Table):
    class Meta:
        # add class="paleblue to <table> tag
        model = Vehicle
        exclude = ['creation_date', 'on_update', 'is_active']
        attrs = {"class": "table table-striped table-bordered table-hover"}


class FETable(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )
    user = tables.TemplateColumn(
        template_code='<a href="/internal/manage_fe/{{ record.id }}" target="_blank" >{{ record.user.get_full_name }}</a>',
        verbose_name='FE Name', order_by='user__first_name')
    username = tables.Column(accessor='user.username')
    date_joined = tables.DateColumn(accessor='user.date_joined')
    remove = tables.TemplateColumn(
        template_code='<div class="hidden-phone visible-desktop btn-group">'
                      '<button class="btn btn-mini btn-danger removeFE"><i class="icon-edit bigger-120"></i>'
                      '<input type = "hidden" value = "{{ record.pk }}" ></button>'
                      '</div>',
        sortable=False, )

    class Meta:
        model = Employee
        exclude = ['creation_date', 'on_update', 'is_active', 'id', 'client', 'city', 'address', 'role']
        attrs = {"class": "table table-striped table-bordered table-hover"}
        sequence = ('s_no', 'user',)
