from django.contrib import admin
from .models import *
# Register your models here.
#
class UserAdmin(admin.ModelAdmin):
    list_display = (
        'username', 'email', 'first_name', 'last_name', 'is_staff', 'is_active')
    search_fields = ['username', 'email', 'first_name']
    list_filter = ['profile__role', 'is_staff', 'profile__branch']


class BranchAdmin(admin.ModelAdmin):
    list_display = ('branch_name', 'branch_manager', 'phone')
    search_fields = ['branch_name', 'branch_manager']


class Branch_PincodeAdmin(admin.ModelAdmin):
    list_display = ('branch', 'pincode')
    search_fields = ['branch', 'pincode']
    list_filter = ['branch__branch_name', 'pincode__pincode', 'pincode__city']


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('user', 'role', 'branch', 'phone')
    search_fields = ['user__username', 'role', 'phone']
    list_filter = ['role', 'branch', 'user__is_staff']


class UserEmailsAdmin(admin.ModelAdmin):
    list_display = ('user', 'email')
    search_fields = ['user__username', 'email']
    list_filter = ['user', 'email']

#
# admin.site.unregister(User)
# admin.site.register(User, UserAdmin)
admin.site.register(Branch, BranchAdmin)
admin.site.register(Branch_Pincode, Branch_PincodeAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Vehicle)
admin.site.register(UserEmails, UserEmailsAdmin)