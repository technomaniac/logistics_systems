from django.contrib import admin

from zoning.models import *

# Register your models here.


class PincodeAdmin(admin.ModelAdmin):
    list_display = ('pincode',)
    search_fields = ['pincode', ]


class CityAdmin(admin.ModelAdmin):
    list_display = ('city',)
    search_fields = ['city', ]


class ZoningMatrixAdmin(admin.ModelAdmin):
    list_display = (
    'origin_city', 'destination_city', 'air_min', 'air_max', 'surface_min', 'van', 'surface_max', 'zone')
    search_fields = ['zone', ]


admin.site.register(Pincode, PincodeAdmin)
admin.site.register(City, CityAdmin)
# admin.site.register(AWB_History)
admin.site.register(Zoning_Matrix, ZoningMatrixAdmin)

