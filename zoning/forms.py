from django.forms import ModelForm, ModelChoiceField, Select, forms
from zoning.models import City, Pincode


class CityForm(ModelForm):
    class Meta:
        model = City
        fields = '__all__'


class PincodeForm(ModelForm):
    class Meta:
        model = Pincode
        fields = '__all__'


# class ZoneForm(ModelForm):
#     class Meta:
#         model = Zone
#         fields = '__all__'


class CityDropDownForm(forms.Form):
    branch = ModelChoiceField(queryset=City.objects.all().order_by('id'),
                              widget=Select(
                                  attrs={'class': 'select2-choice', 'data-style': 'btn-inverse', 'data-width': '50px'}))
