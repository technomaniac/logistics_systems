from django.db import models

from common.models import Time_Model


# Create your models here.
class Zoning_Matrix(Time_Model):
    ZONE_CODE = (
        ('A', 'Zone A'),
        ('B', 'Zone B'),
        ('C', 'Zone C'),
        ('D', 'Zone D'),
    )
    origin_city = models.ForeignKey('zoning.City', related_name='origin_city')
    destination_city = models.ForeignKey('zoning.City', related_name='destination_city')
    air_min = models.IntegerField(max_length=10, verbose_name='Air (Min Days)')
    air_max = models.IntegerField(max_length=10, verbose_name='Air (Max Days)')
    surface_min = models.IntegerField(max_length=10, verbose_name='Surface (Min Days)')
    surface_max = models.IntegerField(max_length=10, verbose_name='Surface (Max Days)')
    zone = models.CharField(choices=ZONE_CODE, max_length=1, blank=True, default='')
    van = models.IntegerField(max_length=10, verbose_name='Van', null=True, blank=True)
    # def save(self, *args, **kwargs):
    #     zone = Zoning_Matrix.objects.filter(origin_city=self.origin_city, destination_city=self.destination_city) | \
    #            Zoning_Matrix.objects.filter(origin_city=self.destination_city, destination_city=self.origin_city)
    #     if zone.exists():
    #         raise IntegrityError
    #     else:
    #         return super(Zoning_Matrix, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.zone

    class Meta:
        unique_together = (('origin_city', 'destination_city'), ('destination_city', 'origin_city'))
        verbose_name = 'Zoning Matrix'
        verbose_name_plural = 'Zoning Matrix'


class City(Time_Model):
    city = models.CharField(max_length=50, unique=True, db_index=True)
    state = models.CharField(max_length=50, db_index=True)

    class Meta:
        index_together = [['city', 'state'], ]

    def __unicode__(self):
        return self.city


class Pincode(Time_Model):
    pincode = models.IntegerField(max_length=6, unique=True, db_index=True)
    city = models.ForeignKey(City, db_index=True)

    class Meta:
        index_together = [['pincode', 'city'], ]

    def __unicode__(self):
        return unicode(self.pincode)

    def get_branch_pincode(self):
        return self.branch_pincode.branch
