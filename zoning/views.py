import csv
import json
from datetime import datetime

from django.http.response import StreamingHttpResponse

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django_tables2 import RequestConfig
from django.contrib.auth.decorators import login_required

from utils.common import Echo

from zoning.tables import CityTable, PincodeTable
from zoning.models import City, Pincode
from zoning.forms import CityForm, PincodeForm, CityDropDownForm
from common.forms import ExcelUploadForm
from utils.upload import upload_pincode_city_file
from utils.upload import handle_uploaded_file
from logistics.settings import MEDIA_ROOT
from internal.models import Branch, Branch_Pincode


@login_required(login_url='/login')
def upload_file(request):
    if request.method == "POST":
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/zoning/')
            upload_pincode_city_file(file)
            return HttpResponseRedirect('/internal/branch')
    else:
        form = ExcelUploadForm()
    return render(request, 'common/pincode_upload_form.html', {'form': form})


@login_required(login_url='/login')
def city(request):
    table = CityTable(City.objects.all())
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'zoning/city.html', {'table': table})


@login_required(login_url='/login')
def add_city(request):
    if request.method == "POST":
        form = CityForm(request.POST, instance=City())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/zoning/city')
    else:
        form = CityForm()
    return render(request, 'zoning/addcity.html', {'form': form})


# @login_required(login_url='/login')
# def zone(request):
#     table = ZoneTable(Zone.objects.all())
#     RequestConfig(request, paginate={"per_page": 10}).configure(table)
#     return render(request, 'zoning/zone.html', {'table': table})


# @login_required(login_url='/login')
# def add_zone(request):
#     if request.method == "POST":
#         form = ZoneForm(request.POST, instance=Zone())
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect('/zoning/zone')
#     else:
#         form = ZoneForm()
#     return render(request, 'zoning/addzone.html', {'form': form})


@login_required(login_url='/login')
def pincode(request):
    table = PincodeTable(Pincode.objects.select_related().all())
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'zoning/pincode.html', {'table': table})


@login_required(login_url='/login')
def add_pincode(request):
    if request.method == "POST":
        form = PincodeForm(request.POST, instance=Pincode())
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/zoning/pincode')
    else:
        form = PincodeForm()
    return render(request, 'zoning/addpincode.html', {'form': form})


def pincode_search(request):
    term = request.GET.get('term')  #jquery-ui.autocomplete parameter
    pincodes = Pincode.objects.filter(pincode__istartswith=term).order_by('pincode')[:10]  #lookup for a city
    res = []
    for p in pincodes:
        #make dict with the metadatas that jquery-ui.autocomple needs (the documentation is your friend)
        dict = {'id': p.pk, 'label': p.pincode, 'value': p.pk}
        res.append(dict)
    return HttpResponse(json.dumps(res))


def city_get_all(request):
    if request.method == 'GET' and request.is_ajax():
        if 'city' in request.session:
            form = CityDropDownForm(initial={'city': request.session['city']})
        elif 'branch' in request.session:
            branch = Branch.objects.get(pk=request.session['branch'])
            form = CityDropDownForm(initial={'city': branch.get_city()})
        else:
            form = CityDropDownForm()
        return render(request, 'common/dropdown.html', {'form': form})


def download_branch_pincode_dump(request):
    branch_pincodes = Branch_Pincode.objects.all().order_by('branch')
    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer)
    response = StreamingHttpResponse(
        (writer.writerow([i.branch.branch_name, i.pincode.pincode]) for i in branch_pincodes),
        content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="pincode_dump_' + datetime.today().strftime(
        '%Y-%m-%d_%H:%M:%S') + '.csv"'
    return response