import os

import dj_database_url


BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

LOGIN_URL = os.environ.get('LOGIN_URL')

TEMPLATE_CONTEXT_PROCESSORS = (
    # 'django.contrib.messages.context_processors.messages'http://54.169.158.167:8000/admin/,
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'ws4redis.context_processors.default',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.YAMLRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework.renderers.XMLRenderer',
    )
}

TEMPLATE_DIRS = (
    os.path.normpath(os.path.join(BASE_DIR, 'templates')),
    # os.path.normpath(os.path.join(BASE_DIR, 'templates/dashboard/templates')),
)
# SECURITY WARNING: keep the secret key used in production secret!localhost:8000
SECRET_KEY = '81psubnpj4e1u_dg&044x8tvi^dtou_)0zdw^o85546&ep1kpz'
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('DJANGO_DEBUG')

ADMINS = (
    ('admin', 'system@nuvoex.com'),
)

INTERNAL_IPS = (
    ('127.0.0.1', 'localhost', '54.169.158.167')
)
# TEMPLATE_DEBUG = True
ALLOWED_HOSTS = ['*']


# Application definition
INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    # 'django.contrib.messages',
    'django.contrib.staticfiles',
    'storages',
    # 'subdomains',
    'djcelery',
    'django_tables2',
    'debug_toolbar',
    'django_extensions',
    'common',
    'mobile',
    'client',
    'zoning',
    'internal',
    'awb',
    'transit',
    'userlogin',
    # 'south',
    # 'geo_mobile_api',
    'rest_framework.authtoken',
    'customer_service',
    'ws4redis',
    # 'south',
    # 'kombu.transport.django',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    'middlewares.global_request.CurrentRequest',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.cache.UpdateCacheMiddleware',
    # 'subdomains.middleware.SubdomainURLRoutingMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django.middleware.cache.FetchFromCacheMiddleware',
    # 'custom.middlewares.ConsoleExceptionMiddleware',
    # 'johnny.middleware.LocalStoreClearMiddleware',
    # 'johnny.middleware.QueryCacheMiddleware',
)

ROOT_URLCONF = 'logistics.urls'

# SUBDOMAIN_URLCONFS = {
# None: 'logistics.urls',  # no subdomain, e.g. ``example.com``
# 'www': 'logistics.urls',
# 'ship': 'logistics.urls',
# 'm': 'logistics.urls.mobile',
# }

WSGI_APPLICATION = 'logistics.wsgi.application'

DATABASES = {
    'default': dj_database_url.config(default=os.environ.get('DATABASE_URL'))
}

CACHES = {
    "default": {
        "BACKEND": "redis_cache.cache.RedisCache",
        "LOCATION": "localhost:6379",
        "OPTIONS": {
            "DB": 1,
            "CLIENT_CLASS": "redis_cache.client.DefaultClient",
        }
    }
}

SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_DB = 0
SESSION_REDIS_PREFIX = 'session'

SITE_ID = 1

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Kolkata'

USE_I18N = True

USE_L10N = True

STATIC_ROOT = os.path.join(os.getcwd(), "staticfiles")

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.normpath(os.path.join(BASE_DIR, 'static')),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

MEDIA_URL = '/media/'

AUTH_PROFILE_MODULE = "internal.Employee"

SESSION_SAVE_EVERY_REQUEST = True

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'

AUTHENTICATION_BACKEND = (
    'auth.authentication.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
)

import djcelery

djcelery.setup_loader()

BROKER_URL = os.environ.get('BROKER_URL')
# A dictionary of urlconf module paths, keyed by their subdomain.
CELERY_IMPORTS = (
    'awb.tasks',
    'transit.tasks',
    'common.tasks',
    'utils.upload',
    'customer_service.tasks',
    'report.tasks',
    'report.mis',
)

EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_PORT = os.environ.get('EMAIL_PORT')

EXOTEL_SID = 'nuvoex1'
EXOTEL_URL = 'https://twilix.exotel.in/v1/Accounts/{sid}/Sms/send.json'
EXOTEL_NO = '01203843113'
EXOTEL_ALT_NO = '09223183143'
EXOTEL_CALLBACK_URL = 'http://ship.nuvoex.com/transit/awb/{awb_no}/sms_notification/{type}'
EXOTEL_TOKEN = os.environ.get('EXOTEL_TOKEN')

# if os.environ.get('SERVER') == 'production':

EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')

# else:

# EMAIL_HOST_PASSWORD = ''
# EXOTEL_TOKEN = ''

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
AWS_S3_SECURE_URLS = False
AWS_QUERYSTRING_AUTH = False
AWS_S3_ACCESS_KEY_ID = os.environ.get('AWS_S3_ACCESS_KEY_ID')
AWS_S3_SECRET_ACCESS_KEY = os.environ.get('AWS_S3_SECRET_ACCESS_KEY')
AWS_S3_BUCKET_NAME = os.environ.get('AWS_S3_BUCKET_NAME')
AWS_S3_URL = 'http://%s.s3.amazonaws.com/' % AWS_S3_BUCKET_NAME


