from .base import *

DEBUG = False

WEBSOCKET_URL = '/ws/'
WS4REDIS_CONNECTION = {
    'host': 'localhost',
    'port': 6379,
    'db': 1,
    # 'password': 'verysecret',
}

WS4REDIS_PREFIX = 'ws'
WS4REDIS_HEARTBEAT = '--heartbeat--'

CELERY_DEFAULT_QUEUE = 'default'

CELERY_ROUTES = {
    'transit.tasks.send_pickup_delivered_notification': {'queue': 'notifications'},
    'transit.tasks.send_cancel_notification': {'queue': 'notifications'},
    'transit.tasks.send_deferred_notification': {'queue': 'notifications'},
    'transit.tasks.send_issue_notification': {'queue': 'notifications'},

    'transit.tasks.linehual_report': {'queue': 'reports'},
    'awb.tasks.generate_mis': {'queue': 'reports'},
    'awb.tasks.automated_mis_sender': {'queue': 'reports'},
    'awb.tasks.send_mis': {'queue': 'reports'},
    'awb.tasks.automated_dashboard_report_sender': {'queue': 'reports'},
    'awb.tasks.send_dashboard': {'queue': 'reports'},
    'awb.tasks.automated_cs_dashboard_report_sender': {'queue': 'reports'},
    'awb.tasks.send_cs_dashboard': {'queue': 'reports'},
    'awb.tasks.automated_out_for_pickup_report': {'queue': 'reports'},
    'awb.tasks.out_for_pickup_report': {'queue': 'reports'},
    'awb.tasks.awb_cod_collection_report': {'queue': 'reports'},
    'awb.tasks.pendency_report_sender': {'queue': 'reports'},
    'awb.tasks.forward_pendency_report': {'queue': 'reports'},
    'awb.tasks.data_received_report': {'queue': 'reports'},
    'awb.tasks.not_attempted_report': {'queue': 'reports'},
    'awb.tasks.pickup_pendency_report': {'queue': 'reports'},
    'awb.tasks.in_transit_pendency_report': {'queue': 'reports'},
    'awb.tasks.dto_pendency_report': {'queue': 'reports'},
    'awb.tasks.automated_delivery_report': {'queue': 'reports'},
    'awb.tasks.delivery_report': {'queue': 'reports'},
    'awb.tasks.fe_report': {'queue': 'reports'},
    'awb.tasks.cancelled_awb_report_calling': {'queue': 'reports'},
    'awb.tasks.before_pickup_report': {'queue': 'reports'},
    'awb.tasks.pending_for_dto_report': {'queue': 'reports'},

    'customer_service.tasks.update_incoming_call_task': {'queue': 'cs'},
    'customer_service.tasks.auto_update_call_details': {'queue': 'cs'},

    'report.tasks.advance_mis_builder': {'queue': 'priority_high'},
    'report.mis.MISHandler.generate_file': {'queue': 'priority_high'},
    'utils.upload.awb_calling_upload_file': {'queue': 'priority_high'},
}