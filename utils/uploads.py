import os

from logistics.settings import MEDIA_ROOT


def get_uploaded_image_file_path(awb_shipment):
    parent_dir = MEDIA_ROOT + '/shipment_images/'
    if not os.path.exists(parent_dir):
        os.mkdir(parent_dir)
    dir_client = MEDIA_ROOT + '/shipment_images/' + awb_shipment.awb.client_warehouse.client.name
    if not os.path.exists(dir_client):
        os.mkdir(dir_client)
        print 'Directory created %s' % dir_client
    dir_awb_type = dir_client + '/' + awb_shipment.awb.category
    if not os.path.exists(dir_awb_type):
        os.mkdir(dir_awb_type)
        print 'Directory created %s' % dir_awb_type
    dir_awb = dir_awb_type + '/' + awb_shipment.awb.awb
    if not os.path.exists(dir_awb):
        os.mkdir(dir_awb)
        print 'Directory created %s' % dir_awb
    dir1 = dir_awb + '/invoice/'
    if not os.path.exists(dir1):
        os.mkdir(dir1)
        print 'Directory created %s' % dir1
    dir2 = dir_awb + '/shipment/'
    if not os.path.exists(dir2):
        os.mkdir(dir2)
        print 'Directory created %s' % dir2

    return {'invoice': os.path.relpath(dir1, MEDIA_ROOT), 'shipment': os.path.relpath(dir2, MEDIA_ROOT)}






















































































































































































































































