from django.db.utils import ProgrammingError
from django.utils.importlib import import_module
from custom.constants import MODEL_ADMIN_REGISTER_TO_EXCLUDE


def get_seperator_seperated_form_errors(form, seperator='\n'):
    final_text = ''
    the_list = []
    print form.errors.items()
    for source, error in form.errors.items():
        if source !='__all__':
            the_list.append(source +' -'+error.as_text().strip('*'))
        else:
            the_list.append('General -'+error.as_text().strip('*'))

    final_text = seperator.join(the_list)
    return final_text


def register_all_models():
    from django.db.models import get_models, get_model
    from django.contrib import admin
    from django.contrib.auth import get_user_model
    from django.contrib.admin.sites import AlreadyRegistered
    user_model = get_user_model()
    models_to_be_excluded = []
    for model in MODEL_ADMIN_REGISTER_TO_EXCLUDE:
        try:
            app_label, model_name = model.split('.')
        except:
            raise Exception('In module custom.constants, MODEL_ADMIN_REGISTER_TO_EXCLUDE improperly configured. '
                            'It needs to be a list with each list item of the form "app_label.model_name". ')
        try:
            models_to_be_excluded.append(get_model(app_label, model_name))
        except:
            raise Exception('Model with app_label %s and name %s could not be imported' % (app_label, model_name))
    try:
        for a_model in get_models():
            try:
                if a_model in models_to_be_excluded:
                    import_module('apps.%s.admin' % (a_model._meta.app_label))
                elif not user_model == a_model:
                    if a_model._meta.app_label == 'helpdesk':
                        import_module('apps.%s.admin' %(a_model._meta.app_label))
                    admin.site.register(a_model)
                else:
                    import_module('apps.%s.admin' %(user_model._meta.app_label))
            except AlreadyRegistered:
                pass
    except ProgrammingError:
        pass
