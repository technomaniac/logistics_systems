from django.test import testcases
from apps.client.models import Client, ClientWarehouse
from apps.contacts.models import Address
from apps.location.models import Pincode
from apps.nuvoex_dash.models import NuvoexAddress
from utils.parsers.parser import SimpleParser
from utils.parsers.guesser import ManifestGuesser
from apps.awb.functionality.db_dumpers import ClientForwardDumper
from apps.vendors.models import Vendor, Branch


class TestParser(testcases.TestCase):
    fixtures = ['client_test.json']

    def setUp(self):
        from apps.location.models import City, State, Country, Pincode
        v = Vendor.objects.create(code='12ERT', name='test',
                                  legal_name='test')
        c = Country.objects.create(name='india')
        s = State.objects.create(name='haryana', country=c)
        ci = City.objects.create(name='gurgaon', state=s)
        p_2 = Pincode.objects.create(pincode='122345', city=ci)
        branch_add = Address.objects.create(address='asdlkfj', pincode=p_2)
        branch = Branch.objects.create(vendor=v,code='brn',
                                       name='main',
                                       type_of_branch='M',
                                       address=branch_add)
        p = Pincode.objects.create(pincode='123456', city=ci)
        add = Address.objects.create(address='random', pincode=p)
        nuadd = NuvoexAddress.objects.create(address=add,
                                             type_of_address='BRN')
        p = Pincode.objects.create(pincode='112233', city=ci)
        client = Client.objects.get(code='123')
        client_warehouse = ClientWarehouse.objects.create(code='1234',
                                                          name='test_ware',
                                                          client=client,
                                                          address=add)

    def test_forward(self):
        s = SimpleParser(file_name='/home/nirmal/Desktop/Manifest Format.xlsx')
        s.db_dumper_guessers = [ManifestGuesser]
        forw = s._worksheet_xlrd_object.sheet_by_name('Forward')
        s.save_to_db(forw)

    def test_reverse(self):
        s = SimpleParser(file_name='/home/nirmal/Desktop/Manifest Format.xlsx')
        s.db_dumper_guessers = [ManifestGuesser]
        forw = s._worksheet_xlrd_object.sheet_by_name('Reverse')
        s.save_to_db(forw)

    def test_retail(self):
        s = SimpleParser(file_name='/home/nirmal/Desktop/Manifest Format.xlsx')
        s.db_dumper_guessers = [ManifestGuesser]
        forw = s._worksheet_xlrd_object.sheet_by_name('Retail')
        s.save_to_db(forw)

    def test_all(self):
        s = SimpleParser(file_name='/home/nirmal/Desktop/Manifest Format.xlsx')
        s.db_dumper_guessers = [ManifestGuesser]
        s.save_to_db()
        print Pincode.objects.all()