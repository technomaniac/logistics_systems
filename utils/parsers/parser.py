import xlrd
from utils.datastuctures import XlrdDict
from django.db import transaction
from celery import task


class SimpleParser(object):
    worksheets = []
    db_dumper_guessers = []
    _worksheet_xlrd_object = None
    worksheet_types = {}

    def __init__(self, file_name, type_matrix=None, dumper_guessers=None, **extras):  #extras can be client, vendor
        self._worksheet_xlrd_object = xlrd.open_workbook(file_name)
        sheet_names = self._worksheet_xlrd_object.sheet_names()
        if dumper_guessers:
            self.db_dumper_guessers = dumper_guessers
        if type_matrix:
            mat_sheet_names = type_matrix.keys()
            has_all_sheet_names = True
            for i in sheet_names:
                if not (i.lower() in mat_sheet_names or i in mat_sheet_names):
                    has_all_sheet_names = False
            if not has_all_sheet_names:
                raise RuntimeError('type_matrix should contain type of every sheet')
            else:
                self.worksheet_types = type_matrix
        self.extras = extras
        for i in sheet_names:
            self.worksheets.append(self._worksheet_xlrd_object.sheet_by_name(i))

    def get_db_dumper(self, worksheet):
        for g in self.db_dumper_guessers:
            g_ins = g(types=self.worksheet_types, **self.extras)
            dumper = g_ins.get_dumper(worksheet)
            # print dumper, 'dumper'
            if dumper:
                return dumper, g_ins
        return None

    def verify(self, worksheet=None):
        if worksheet:
            dumper, g = self.get_db_dumper(worksheet)
            num_rows = worksheet.nrows - 1
            curr_row = -1
            xrld_dict = None
            while curr_row < num_rows:
                if curr_row == -1:
                    curr_row += 1
                    xlrd_dict = XlrdDict(col_names=worksheet.row(curr_row))
                    continue
                curr_row += 1
                row = worksheet.row(curr_row)
                xlrd_dict(row)
                normalized_dict = g.normalize_data(xlrd_dict.get_dict())
                status = dumper.verify_data(normalized_dict, **self.extras)
                if not status:
                    return False
            return True
        else:
            for i in self:
                self.verify(i)

    @transaction.atomic()
    def save_to_db(self, worksheet=None):
        if worksheet:
            dumper, g = self.get_db_dumper(worksheet)
            num_rows = worksheet.nrows - 1
            curr_row = -1
            xlrd_dict = None
            while curr_row < num_rows:
                if curr_row == -1:
                    curr_row += 1
                    xlrd_dict = XlrdDict(col_names=worksheet.row(curr_row))
                    continue
                curr_row += 1
                row = worksheet.row(curr_row)
                xlrd_dict(row)
                normalized_dict = g.normalize_data(xlrd_dict.get_dict())
                # print normalized_dict
                if dumper.verify_data(normalized_dict, **self.extras):
                    dumper.dump_row(normalized_dict, **self.extras)
                else:
                    return
        else:
            for i in self:
                self.save_to_db(i)

    def __getitem__(self, item):
        return self.worksheets[item]

    def __iter__(self):
        return self.worksheets.__iter__()


@task(name='parser')
def simple_parser_task(*args, **kwargs):
    try:
        parser = SimpleParser(*args, **kwargs)
        parser.save_to_db()
    except:
        pass  #email about the error