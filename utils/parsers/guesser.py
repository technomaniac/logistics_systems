import xlrd
from apps.awb.functionality.db_dumpers import ClientForwardDumper, ClientReverseDumper, ClientRetailDumper
from utils.parsers.constants import MANIFEST_TYPES


class NormalizationMixin(object):

    def normalize_text(self, text):
        the_str = str(text)
        return the_str.strip()

    def normalize_int(self, integer):
        the_int = int(integer)
        return the_int

    def normalize_float(self, float_val):
        return float(float_val)

    def normalize_decimal(self, dec_value):
        return dec_value

    def normalize_string(self, the_str):
        if isinstance(the_str, (unicode, str)):
            return the_str
        if isinstance(the_str, float):
            text = str(the_str)
            if text.endswith('.0'):#because of the way xlrd treats numbers
                return self.normalize_text(self.normalize_int(the_str))
            else:
                return str(the_str)
        if isinstance(the_str, int):
            return str(the_str)


class ManifestGuesser(NormalizationMixin):

    def __init__(self, types=None, **extras):
        self.types = types if types else {}
        self.extras = extras

    def get_dumper(self, worksheet):
        self.worksheet = worksheet
        if self.get_type(worksheet) == MANIFEST_TYPES.FORWARD_MANIFEST_TYPE:
            return ClientForwardDumper()
        elif self.get_type(worksheet) == MANIFEST_TYPES.REVERSE_MANIFEST_TYPE:
            return ClientReverseDumper()
        elif self.get_type(worksheet) == MANIFEST_TYPES.RETAIL_MANIFEST_TYPE:
            return ClientRetailDumper()

    def get_type(self, worksheet):
        self.worksheet = worksheet
        col_row = self.worksheet.row(0)
        col_names = [i.value for i in col_row]
        print self.types, 'types has to be dict'
        if worksheet.name.lower() in self.types.keys():
            return self.types.get(worksheet.name.lower())
            #SHOULD BE FROM CONSTANT TYPES
        elif worksheet.name in self.types.keys():
            return self.types.get(worksheet.name)
        else:
            if worksheet.name.lower() == 'forward':
                return MANIFEST_TYPES.FORWARD_MANIFEST_TYPE
            if worksheet.name.lower() == 'reverse':
                return MANIFEST_TYPES.REVERSE_MANIFEST_TYPE
            if worksheet.name.lower() == 'retail':
                return MANIFEST_TYPES.RETAIL_MANIFEST_TYPE

    def normalize_data(self, xlrd_dict):
        if self.get_type(self.worksheet) == 'forward':
            customer_name = xlrd_dict['customer name'].strip()
            if len(customer_name.split(" ")) >= 2:
                first_name = customer_name.split(" ")[0]
                last_name = " ".join(customer_name.split(" ")[1:])
            else:
                first_name = customer_name
                last_name = ""
            try:
                cod_amount = float(xlrd_dict['cod amount'])
            except:
                cod_amount = None
            return {'awb': {'awb': self.normalize_string(xlrd_dict['awb']),#TODO put this dict in common place
                    'order_id': self.normalize_string(xlrd_dict['order number']),
                    'invoice_no': self.normalize_string(xlrd_dict['invoice number']),
                    'category': xlrd_dict['payment type'].strip(),
                    'expected_amount': cod_amount,
                    'length': xlrd_dict['length'],
                    'breadth': xlrd_dict['bredth'],
                    'height': xlrd_dict['height'],
                    'weight': xlrd_dict['weight'],
                    },
                    'awb_shipment': {
                    'flag': 'N',
                    'status': 'NIN',
                    },
                    'pickup_address': {
                    'pickup_pincode': 'nuvoex', #nuvoex pickup
                    'pickup_address': 'nuvoex', #nuvoex pickup
                    },
                    'delivery_address': {
                    'address': xlrd_dict['customer address'].strip(),
                    'pincode': self.normalize_string(xlrd_dict['pincode'])
                    },
                    'city': xlrd_dict['city'].strip(),
                    'customer_contact': {
                    'first_name': first_name,
                    'last_name': last_name,
                    'phone_number': self.normalize_string(xlrd_dict['customer contact number'])
            }}

        if self.get_type(worksheet=self.worksheet) == 'reverse':
            customer_name = xlrd_dict['pickup customer name']
            if len(customer_name.split(" ")) >= 2:
                first_name = customer_name.split(" ")[0]
                last_name = " ".join(customer_name.split(" ")[1:])
            else:
                first_name = customer_name
                last_name = ""
            try:
                cod_amount = float(xlrd_dict['cod amount'])
            except:
                cod_amount = None
            return {'awb': {'awb': self.normalize_string(xlrd_dict['awb']),
                    'order_id': self.normalize_string(xlrd_dict['order number']),
                    'invoice_no': self.normalize_string(xlrd_dict['invoice number']),
                    'category': 'REV',
                    'expected_amount': cod_amount,
                    'length': xlrd_dict['length'],
                    'breadth': xlrd_dict['bredth'],
                    'height': xlrd_dict['height'],
                    'weight': xlrd_dict['weight'],
                    },
                    'awb_shipment': {
                    'flag': 'N',
                    'status': 'NIN',
                    },
                    'vendor_code': self.normalize_string(xlrd_dict['vendor code']),
                    'pickup_address': {
                    'address': xlrd_dict['pickup customer address'],
                    'pincode': self.normalize_string(xlrd_dict['pickup pincode'])
                    },
                    'delivery_address': {
                    'address': xlrd_dict['delivery address'],
                    'pincode': self.normalize_string(xlrd_dict['delivery pincode'])
                    },
                    'city': xlrd_dict['pickup city'],
                    'customer_contact': {
                    'first_name': first_name,
                    'last_name': last_name,
                    'phone_number': self.normalize_string(xlrd_dict['pickup customer contact number'])
            }}

        if self.get_type(worksheet=self.worksheet) == 'retail':
            customer_name = xlrd_dict['customer name']
            if len(customer_name.split(" ")) >= 2:
                first_name = customer_name.split(" ")[0]
                last_name = " ".join(customer_name.split(" ")[1:])
            else:
                first_name = customer_name
                last_name = ""
            try:
                cod_amount = float(xlrd_dict['cod amount'])
            except:
                cod_amount = None
            return {'awb': {'awb': self.normalize_string(xlrd_dict['awb number']),#TODO put this dict in common place
                    'order_id': self.normalize_string(xlrd_dict['order number']),
                    'invoice_no': self.normalize_string(xlrd_dict['invoice number']),
                    'category': xlrd_dict['payment type'],
                    'expected_amount': cod_amount,
                    'length': xlrd_dict['length'],
                    'breadth': xlrd_dict['bredth'],
                    'height': xlrd_dict['height'],
                    'weight': xlrd_dict['weight'],
                    },
                    'awb_shipment': {
                    'flag': 'N',
                    'status': 'NIN',
                    },
                    'store_code': self.normalize_string(xlrd_dict['store code']),
                    'product_id': self.normalize_string(xlrd_dict['product code']),
                    'product_description': xlrd_dict['product description'],
                    'delivery_address': {
                    'address': xlrd_dict['customer address'],
                    'pincode': self.normalize_string(xlrd_dict['pincode'])
                    },
                    'city': xlrd_dict['city'],
                    'customer_contact': {
                    'first_name': first_name,
                    'last_name': last_name,
                    'phone_number': self.normalize_string(xlrd_dict['customer contact number'])
                    }}