# Manifest Sheet's random header dictionary
from logistics.settings.base import EXOTEL_SID, EXOTEL_TOKEN

TEST_CLIENT_LIST = ['TCF', 'TCP', 'TCR']

NUVOEX_CONTACT = '+91-8882140288'

MANIFEST_HEADER_DICT = {
    'awb': ['awb number', 'request id', 'awb'],
    'order_id': ['order_number', 'order id', 'order no', 'order_number', 'client order no', 'client order id'],
    'customer_name': ['customer name', 'cnee name', 'consignor name', 'cust name'],
    'cnor_name': ['cnor_name', 'cnor name'],
    'invoice_no': ['invoice_no', 'invoice no', 'client invoice no'],
    'address_1': ['shipping_address1', 'consignor address 1', 'cust address line 1'],
    'address_2': ['shipping_address2', 'consignor address 2', 'cust address line 2'],
    'area': ['area', 'cust area'],
    'region': ['shipping_region'],
    'landmark': ['landmark'],
    'city': ['shipping_city', 'consignor city', 'shipping city', 'city', 'cust city'],
    'origin_pincode': ['origin pincode', 'origin_pincode', 'pincode'],
    'pincode': ['return location pin code', 'shipping_postcode', 'shipping postcode', 'cust pincode', 'pincode'],
    'phone_1': ['shipping_phone', 'mobile no.', 'phone 1', 'cust phone'],
    'phone_2': ['alternate contact no', 'alternate mobile number', 'phone 2', 'cust alt phone'],
    'package_value': ['grand_total', 'tot amt', 'package value'],
    'expected_amount': ['amount to be collected', 'cod amount', 'cash amount'],
    'weight': ['weight', 'wt'],
    'length': ['length'],
    'breadth': ['breadth'],
    'height': ['height'],
    'package_sku': ['item_sku', 'package sku'],
    'description': ['description', 'sku description', 'item_description', 'item description', 'package description'],
    'package_price': ['package price'],
    'preferred_pickup_date': ['preferred pickup date'],
    'preferred_pickup_time': ['preferred pickup time'],
    'category': ['category', 'coddod', 'payment mode'],
    'priority': ['priority', 'awb priority'],
    'vendor': ['vendor', 'vendor code', 'vendor name'],
    'reason_for_return': ['reason for return']
}
# MIS_HEADER = ['AWB', 'Client', 'Order ID', 'Consignee', 'Phone', 'Address', 'Category', 'Amount', 'COD Amount',
# 'Weight', 'Delivery Branch', 'Pickup Branch', 'Dispatch Count', 'First Trial', 'First Pending',
# 'First Dispatch', 'Last Dispatch', 'Current Status', 'First Scan Location', 'Date']
MIS_HEADER = ['AWB', 'Client', 'Order ID', 'Priority', 'Consignee', 'Address', 'Phone', 'Pincode', 'Category', 'Amount',
              'COD Amount', 'Weight', 'Length', 'Breadth', 'Height', 'Description', 'Vendor Name', 'Vendor Code',
              'Delivery Branch', 'Pickup Branch', 'Dispatch Count', 'First Pending', 'First Dispatch', 'Last Dispatch',
              'Last Scan', 'Current Status', 'Last Status Updated On', 'First Scan Location', 'CS Call Made',
              'CS Call Count', 'Remark', 'Reason', 'Upload Date', 'Pickup Date', 'Delivered/DTO\'d Date',
              'DTO Creation Date', 'Transfer Mode', 'Reason for Return', 'Not Attempts Count',
              'Customer Not Available Count']

PENDENCY_MIS_HEADER = MIS_HEADER + ['Current DRS', 'Current TB', 'Current MTS', 'Current DTO']

TRANSIT_MIS_HEADER = PENDENCY_MIS_HEADER + ['Date of Hand-Over', 'Co Loader', 'Co Loader AWB']

FE_REPORT_HEADER = ['FE Name', 'FE Branch', 'No of AWBs Assigned', 'No of AWBs PickedUp']

DTO_OPEN_REPORT = ['DTO NO', 'CLIENT', 'VENDOR BRANCH']

MIS_HEADER_CLIENT_RL = ['AWB', 'Order ID', 'Priority', 'Consignee', 'Address', 'Phone', 'Pincode',
                        'Package Value', 'Weight(kg)', 'Length(cm)', 'Breadth(cm)', 'Height(cm)', 'Dispatch Count',
                        'First Dispatch', 'Last Dispatch', 'Current Status', 'Last Status Updated On', 'CS Call Made',
                        'CS Call Count', 'Remark', 'Reason', 'Pickup Date', 'Upload Date', 'DTO\'d Date',
                        'Cancellation Date', 'DTO Creation Date', 'Vendor Code', 'Vendor Name']

MIS_HEADER_CLIENT_FL = ['AWB', 'Order ID', 'Consignee', 'Address', 'Phone', 'Pincode', 'Category', 'Amount',
                        'COD Amount', 'Weight(kg)', 'Length(cm)', 'Breadth(cm)', 'Height(cm)', 'Description',
                        'Current Status', 'Deferred Date', 'Remarks', 'Delivery Branch',
                        'Upload Date', 'Delivered Date', 'First Attempt', 'Total Attempts', 'FE Name']

CLIENT_DASHBOARD_HEADER_RL = ['RPI Date', 'Pickup Assigned', 'Converted', 'Converted %', 'Picked Up', 'Picked Up %',
                              'DTO\'d', 'DTO\'d %', 'Cancelled', 'Cancelled %', 'Scheduled', ' Scheduled %',
                              'Out for Pickup', 'Out for Pickup %']

CLIENT_DASHBOARD_HEADER_FL = ['RPI Date', 'AWBs', 'Delivered', 'Delivered %', 'Pending', 'Pending %',
                              'Returned', 'Returned %', 'RTO', 'RTO %']

DASBOARD_REPORT_HEADER = [('Total Assigned Volume', 'get_total_awb_assigned'),
                          ('Out for Pickup Count', 'get_total_out_for_pickup'),
                          ('Out for Pickup Count %', 'get_total_out_for_pickup_perc')]

COD_REPORT_HEADER = ['AWB', 'Client', 'Branch', 'Collected Amount', 'Status', 'Date']

DTO_REPORT_HEADER = ['AWB', 'Order ID', 'Weight', 'Length', 'Breadth', 'Height', 'DTO Assign']

VENDOR_DUMP_HEADER = ['Vendor code', 'Vendor Name', 'Owner Name', 'client', 'Branch', 'Pincode', 'Phone No.']

MOBILE_AWB_STATUS = ['PP', 'DRS']

CALL_OUT_REPORT_HEADER = ['SUBORDER CODE', 'AWB', 'STATUS', 'REMARKS', 'CALL COUNT', 'CALLING DATE']

MANIFEST_FIELDS = (
    ('awb', 'awb'),
    ('priority', 'priority'),
    ('order_id', 'client order no'),
    ('customer_name', 'cust name'),
    ('address_1', 'cust address line 1'),
    ('address_2', 'cust address line 2'),
    ('city', 'cust city'),
    ('pincode', 'pincode'),
    ('phone_1', 'phone 1'),
    ('phone_2', 'phone 2'),
    ('weight', 'weight'),
    ('description', 'package description'),
    ('package_value', 'package value'),
    ('vendor', 'vendor'),
    # ('client', 'client code'),
    ('length', 'length'),
    ('breadth', 'breadth'),
    ('height', 'height'),
    ('category', 'category'),
    ('reason_for_return', 'reason for return'),
    # ('origin_branch', 'dto/rto branch'),

)

AWB_STATUS = (
    ('DR', 'Data Received'),
    ('ISC', 'In-Scanned'),
    ('PP', 'Dispatched for Pickup'),
    ('PC', 'Pick-up Complete'),
    ('INT', 'In-Transit'),
    ('ITR', 'In-Transit (Return)'),
    ('DPR', 'Dispatched (Return)'),
    ('DCR', 'Pending for Delivery/DTO'),
    ('DRS', 'Dispatched'),
    ('DTO', 'Dispatched to Client'),
    ('DEL', 'Delivered/DTO\'d to Client'),
    ('CAN', 'Cancelled'),
    ('SCH', 'Scheduled'),
    ('RET', 'Returned'),
    ('RBC', 'Rejected by Client'),
    ('CB', 'Called Back'),
    ('DBC', 'Deferred by Customer'),
    ('CNA', 'Customer not Available'),
    ('DFR', 'Dispatched for Return'),
    ('PFR', 'Pending for RTO'),
    ('RTO', 'RTO\'d to Client'),
    ('NA', 'Not Attempted'),
    ('LOS', 'Lost')
)

AWB_CREATE_DRS_STATUS = (
    ('DR', 'Data Received'),
    ('DCR', 'Pending for Delivery'),
    ('DBC', 'Deferred by Customer'),
    ('SCH', 'Scheduled'),
    ('CAN', 'Cancelled'),
    ('CNA', 'Customer not Available'),
    ('NA', 'Not Attempted'),
)

AWB_CC_CALLING_STATUS = (('DBC', 'Deferred by Customer'),
                         ('SCH', 'Scheduled'),
                         ('CAN', 'Cancelled'),
                         ('CNA', 'Customer not Available'),
                         ('NA', 'Not Attempted'),
)

AWB_FL_CREATE_DRS_STATUS_LIST = ['DCR', 'DBC', 'CNA', 'SCH', 'NA', 'CAN']
AWB_RL_CREATE_DRS_STATUS_LIST = ['DR', 'DBC', 'CNA', 'SCH', 'NA']
IN_TRANSIT_STATUS_LIST = ['TB', 'TBD', 'MTS', 'MTD']
AWB_CAN_STATUS_LIST = ['CAN']
CAN_CS_REPORT_CLIENT = ['PTM', 'LKT', 'FNY']

AWB_RL_REMARKS = (
    ('Customer does not want to return', 'Customer does not want to return'),
    # ('Client Process Delay', 'Client Process Delay'),
    # ('Customer deferring more than 3 days', 'Customer deferring more than 3 days'),
    ('Customer wants replacement/refund first', 'Customer wants replacement/refund first'),
    ('Pickup already done by self/other courier', 'Pickup already done by self/other courier'),
    ('Couldn\'t speak to customer(Care)', 'Couldn\'t speak to customer(Care)'),
    ('Incorrect update : by branch', 'Incorrect update : by branch'),
    ('Defer : Customer Initiated', 'Defer : Customer Initiated'),
    ('Dispatched / Pickedup already', 'Dispatched / Pickedup already'),
    ('Confirmed Cancel', 'Confirmed Cancel'),
    ('Customer not contactable: Invalid/Wrong Number', 'Customer not contactable: Invalid/Wrong Number'),
    ('Address not found : Wrong/Incomplete Address', 'Address not found : Wrong/Incomplete Address'),
    ('NSZ : Incorrect pincode : Correct pincode is not serviceable', 'NSZ : Incorrect pincode : Correct pincode is not serviceable'),
    ('Other', 'Other')
)

AWB_RL_REMARKS_CALL = (
    ('Customer does not want to return', 'Customer does not want to return'),
    # ('Client Process Delay', 'Client Process Delay'),
    ('Customer deferring more than 3 days', 'Customer deferring more than 3 days'),
    ('Customer wants replacement/refund first', 'Customer wants replacement/refund first'),
    ('Pickup already done by self/other courier', 'Pickup already done by self/other courier'),
    ('Couldn\'t speak to customer', 'Couldn\'t speak to customer'),
    ('Incorrect update : by branch', 'Incorrect update : by branch'),
    ('Defer : Customer Initiated', 'Defer : Customer Initiated'),
    ('Dispatched / Pickedup already', 'Dispatched / Pickedup already'),
    ('Delivery not done yet', 'Delivery not done yet'),
    ('Customer wants to talk to client', 'Customer wants to talk to client'),
    ('Customer out of station', 'Customer out of station'),
    ('Address update', 'Address update'),
    ('Delivery Not taken', 'Delivery Not taken'),
    ('No service zone', 'No service zone'),
    ('Brand packaging not available', 'Brand packaging not available'),
    ('Wants to give partial product', 'Wants to give partial product'),
    ('Number Not provided', 'Number Not provided'),
    ('Send With /Against other AWB no', 'Send With /Against other AWB no'),
    # ('Confirmed Cancel', 'Confirmed Cancel'),
    ('Other', 'Other')
)

AWB_FL_REMARKS = (
    ('Customer doesn\'t want the product', 'Customer doesn\'t want the product'),
    # ('Client Process Delay', 'Client Process Delay'),
    ('Customer away for a long time', 'Customer away for a long time'),
    ('Wants Open Delivery', 'Wants Open Delivery'),
    ('Payment related dispute', 'Payment related dispute'),
    ('Couldn\'t speak to customer', 'Couldn\'t speak to customer'),
    ('Other', 'Other')
)

AWB_RL_REASON = (
    ('No Answer', 'No Answer'),
    ('Switch off/Not Reachable', 'Switch off/Not Reachable'),
    ('Customer busy', 'Customer busy'),
    ('Wrong Number', 'Wrong Number')
)

AWB_FL_REASON = (
    ('No Answer', 'No Answer'),
    ('Switch off/Not Reachable', 'Switch off/Not Reachable'),
    ('Customer busy', 'Customer busy'),
    ('Wrong Number', 'Wrong Number')
)


BRANCH_DICT = {'LXM': 'Laxminagar', 'SKT': 'Saket', 'GG1': 'Gurgaon', 'GGN HUB': 'Gurgaon HUB', 'RAJ': 'Rajouri',
               'BLR': 'Bangalore', 'MUM': 'Mumbai', 'PUN': 'Pune', 'HYD': 'Hyderabad', 'CHE': 'Chennai'}

CLIENT_PERFORMANCE_REPORT_RL = [('RPI Date', 'get_date'),
                                ('Pickup Assigned', 'get_total_awb_count'),

                                ('Attempts %', 'get_total_attempted_perc'),
                                ('Total Pickups', 'get_pickup_awb_count'),
                                ('Picked Up(24 Hrs) %', 'get_pickup_perc_d1'),
                                ('Picked Up(48 Hrs) %', 'get_pickup_perc_in_d2'),
                                ('Dispatched % (10:30 AM)', 'get_dispatched_perc_10_30_am'),
                                ('Dispatched % (2:30 PM)', 'get_dispatched_perc_2_30_pm'),


                                # ('Picked Up %', 'get_pickup_awb_perc'),
                                # ('Cancelled by Cust', 'get_cancelled_awb_count'),


                                ('Cancelled %', 'get_cancelled_by_picked_up_awb_perc'),
                                ('Deferred %', 'get_deferred_awb_perc'),
                                ('Misc Pending %', 'get_misc_pending_awb_perc'),


                                # ('Cancelled Confirmed', 'get_cancelled_conf_awb_count'),
                                # ('Cancelled Confirmed %', 'get_cancelled_conf_awb_perc'),
                                # ('Scheduled', 'get_scheduled_awb_count'),
                                # ('Scheduled %', 'get_scheduled_awb_perc'),
                                # ('Out for Pickup', 'get_out_pickup_awb_count'),
                                # ('Out for Pickup %', 'get_out_pickup_awb_perc'),
                                # ('DTO\'d', 'get_dtod_awb_count'),
                                # ('Lost', 'get_lost_awb_count'),
                                # ('Scheduled/Pending Pickup 1 Day', 'get_scheduled_pending_pickup_per_d1'),
                                # ('Scheduled/Pending Pickup > 1 Day', 'get_scheduled_pending_pickup_per_gt_d1'),
                                # ('Pickup in 1 Day %', 'get_pickup_perc_d1'),
                                # ('Pickup in 2 Days %', 'get_pickup_perc_d2'),
                                # ('Pickup in > 2 Days %', 'get_pickup_perc_gt_d2'),
                                # ('Delivered in < 2 Days %', 'get_delivered_perc_lte_d2'),
                                # ('Delivered in 2-3 Days %', 'get_delivered_perc_d2_d3'),
                                # ('Delivered in > 3 Days %', 'get_delivered_perc_gt_d3'),


                                ('In Transit % (Out of Pickup)', 'get_dto_in_transit_awb_perc'),
                                ('DTC % (Out of Pickup)', 'get_dtc_awb_perc'),
                                ('DTO\'d % (Out of Pickup)', 'get_dtod_awb_perc'),


                                # ('DTO Rejected %', 'get_dto_rejected_awb_perc'),
]

CLIENT_PICKUP_PERFORMANCE_REPORT_RL = [('Pickup Date', 'get_date'),
                                       ('Total Pickups', 'get_pickup_awb_count'),
                                       ('In Transit %', 'get_in_transit_awb_perc'),
                                       ('DTC %', 'get_dtc_awb_perc'),
                                       ('DTO\'d %', 'get_dtod_awb_perc'),
                                       # ('DTO Rejected %', 'get_dto_rejected_awb_perc'),
]

CALLING_SUMMARY_REPORT = [('Date', 'get_date'),
                          ('Total Calls', 'get_total_calls'),
                          ('Connected Calls %', 'get_connected_call_perc'),
                          ('Unconnected Calls %', 'get_unconnected_call_perc'),
]

DELIVERY_REPORT_FL_HEADER = [('Date', 'get_date'),
                             ('No of Deliveries', 'get_total_delivered')]

CALLING_CANCELLED_CONNECTED_REPORT = [('Date', 'get_date'),
                                      ('Total Cancellations', 'get_total_cancellations'),
                                      ('Customer Does not want to return %', 'get_customer_not_return_perc'),
                                      # ('Schedule %','get_schedule_call_perc'),
                                      ('Long Defer %', 'get_long_defer_perc'),
                                      ('Cx not reachable %', 'get_customer_not_reachable_perc'),
                                      ('Misc', 'get_misc_call_perc'),

]

CALLING_PENDING_AND_DEFER_REPORT = [('Date', 'get_date'),
                                    ('Total Cases', 'get_total_cases_pending_defer'),
                                    ('Pending : Customer Initiated %', 'get_customer_pending_perc'),
                                    ('Defer : Customer Initiated %', 'get_customer_defer_perc'),
                                    ('Dispatched/Pickedup already %', 'get_pickedup_already_perc'),
                                    ('Confirmed Cancel %', 'get_confirmed_cancel_perc'),
                                    ('Cx not reachable %', 'get_customer_pending_not_reachable_perc'),
]

TB_LINEHUAL_REPORT = [('Total TB', 'get_total_tbs'),
                      ('TB Delivered 1st Day %', 'get_tb_1st_day_perc'),
                      ('TB Delivered 2nd Day %', 'get_tb_2nd_day_perc'),
                      ('TB Delivered After 2 days %', 'get_tb_after_2_day_perc'),
                      ('TB Left %', 'get_tb_left_perc'),
]

DTO_PENDENCY_REPORT = [('')]

TB_REPORT_HEADER = [('Date of Hand Over', 'get_hand_over_date'),
                    ('TB Number', 'get_tb_id'),
                    ('Status', 'get_tb_status'),
                    ('Origin Branch', 'get_origin_branch'),
                    ('Destination Branch', 'get_delivery_branch'),
                    ('Mode', 'get_transfer_mode_absolute'),
                    ('L (cm)', 'get_length'),
                    ('B (cm)', 'get_breadth'),
                    ('H (cm)', 'get_height'),
                    ('Received Date', 'get_received_date'),
                    ('Dead Weight (kg)', 'get_weight'),
                    ('MTS Number', 'get_mts'),
                    ('Volumetric (WGT)', 'get_volumetric_weight'),
                    ('Compiled (WGT)', 'get_compiled_weight'),
                    ('Co-Loader', 'get_co_loader_mts'),
                    ('Co-Loader AWB', 'get_co_loader_awb_mts'),
]

# PICKUP_SMS_NOTIFICATION_TEMPLATE = "Dear {cust_name},\nYour {client_name} reverse pickup order {awb_no}, is out for pickup by {fe_name}, Mob {fe_contact}. Pickup will be done by 5 PM today."
# Reverse Logistics
PICKUP_SMS_NOTIFICATION_TEMPLATE = "Dear {cust_name},\nYour {client_name} reverse pickup order {awb_no}, is out for pickup by {fe_name}, Mob {fe_contact}. This should be picked up today."
CANCEL_SMS_NOTIFICATION_TEMPLATE_RL = "Dear {cust_name},\nIt seems you have cancelled your {client_name} reverse pickup order {awb_no}. In case of any issues, please call +91-8882140288."
DEFERRED_SMS_NOTIFICATION_TEMPLATE_RL = "Dear {cust_name},\nYour {client_name} reverse pickup order {awb_no} has been deferred to {deferred_date}. Please reachout to our customer care team +91-8882140288 incase you wish to change the pickup date."
# Forward Logistics
DELIVERED_SMS_NOTIFICATION_TEMPLATE = "Dear {cust_name},\nYour {client_name} order {awb_no}, is out for delivery by {fe_name}, Mob {fe_contact}. Delivery should be done today itself."
CANCEL_SMS_NOTIFICATION_TEMPLATE_FL = "Dear {cust_name},\nIt seems you have cancelled your {client_name} order {awb_no}. In case of any issues, please call +91-8882140288."
DEFERRED_SMS_NOTIFICATION_TEMPLATE_FL = "Dear {cust_name},\nYour {client_name} order {awb_no} has been deferred to {deferred_date}. Please reach out to us on +91-8882140288 in case you wish to change the delivery date."

ISSUE_SMS_NOTIFICATION_TEMPLATE = 'Dear {cust_name},\nDue to {reason} we were\'nt able to attempt your {client_name} order {awb_no} today. We will attempt it again in next 48 working hrs.'
# ISSUE_SMS_NOTIFICATION_TEMPLATE_FL = 'Dear {cust_name},\nDue to {reason} we were not able to attempt your {client_name} shipment {awb_no} today. We will try to close this case in next 48 working Hours. In-case of any query please reach out to us on {nuvoex_contact_no}'

SAFE_EXPRESS_URL = 'http://www.safexpress.com/shipment_inq.aspx?sno={sno}'

FE_REMOVE_TEMPLATE = 'This is a system generated mail please do not reply.\n\nFE - {fe_name} has been removed from {branch_name} branch by {user_name}'
FE_CREATE_TEMPLATE = 'This is a system generated mail please do not reply.\n\nFE - {fe_name} has been added in {branch_name} branch by {user_name}'

INVOICE_BARCODE = dict(module_height=6.0, text_distance=0.5, font_size=10, quiet_zone=1.0)
PRINTER_BARCODE = dict(module_height=12.0, text_distance=0.5, font_size=10, center_text=True, module_width=0.15)

EXOTEL_CALL_DETAILS_URL = 'https://{sid}:{token}@twilix.exotel.in/v1/Accounts/{sid}/Calls/'.format(
    sid=EXOTEL_SID, token=EXOTEL_TOKEN)
