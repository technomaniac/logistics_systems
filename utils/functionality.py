import collections
from datetime import datetime


def check_date_gte_current(date, format):
    if datetime.strptime(date, format).date() >= datetime.now().date():
        return True
    else:
        return False


def convert_request_querydict_to_dict(request_querydict):
    d = dict(request_querydict)
    new_d = {}
    for i, j in d.items():
        if isinstance(j, (collections.Iterable, collections.Sized)):
            if len(j) > 1 and isinstance(j, list):
                new_d[i] = j
            elif isinstance(j, list):
                new_d[i] = j[0] if len(j) != 0 else ''
            else:
                new_d[i] = j
        else:
            new_d[i] = j
    return new_d


def change_date_format(date, old_format, new_format):
    return datetime.strptime(str(date), old_format).strftime(new_format)
