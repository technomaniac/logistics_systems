from datetime import datetime, timedelta
import re
import json
import collections

from django.core.cache import cache
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.query_utils import Q
from django.http.response import HttpResponse
from django.db.models.fields.related import OneToOneField, ForeignKey
from django.db.models.related import RelatedObject
from django.utils.encoding import smart_str

from awb.models import AWB
from middlewares.global_request import get_current_request
from transit.models import MTS, DRS, RTO, DTO
from internal.models import UserEmails
from transit.models import TB


def get_model_fields():
    pass


def is_fkey(model, field_name):
    field = model._meta.get_field_by_name(field_name)[0]
    if isinstance(field, (ForeignKey)):
        return True
    return False


def get_all_onetoone_models(model):
    final = {}
    for field_name in model._meta.get_all_field_names():
        if is_onetoone(model, field_name):
            final[field_name] = model._meta.get_field_by_name[0].related.parent_model
    return final


def is_onetoone(model, field_name):
    field = model._meta.get_field_by_name(field_name)[0]
    if isinstance(field, (OneToOneField)):
        return True
    return False


def is_reverse(model, field_name):
    field = model._meta.get_field_by_name(field_name)[0]
    if isinstance(field, (RelatedObject)):
        return True

    return False


def get_names_for_prefetch(model, models_to_exclude=(), depth=None):
    to_exclude = list(models_to_exclude)
    models_traversed = []
    models_traversed.extend(to_exclude)
    # print model, 'model'
    if depth < 2:
        raise Exception('Go learn relationships..')

    def get_all_related_models_fields_name(model, prefix=''):
        final = []
        if depth and prefix:
            _depth = prefix.split('__')
            if len(_depth) == depth:
                return []
        print model, 'inside main'
        models_traversed.append(model)
        if model == TB:
            print model._meta.get_all_field_names(), 'field namess'
        for field_name in model._meta.get_all_field_names():
            is_one = is_onetoone(model, field_name)
            is_for = is_fkey(model, field_name)
            is_rev = is_reverse(model, field_name)
            the_field = model._meta.get_field_by_name(field_name)[0]
            if is_rev or is_for or is_rev:
                if is_for or is_one:
                    rel_model = the_field.rel.to
                else:
                    rel_model = the_field.model
                    field_name = the_field.get_accessor_name()

                temp_prefix = prefix + field_name
                if not rel_model in models_traversed:
                    if not prefix:
                        final.append(field_name)
                    else:
                        final.append(temp_prefix)
                    temp_prefix += '__'
                    final_rel = get_all_related_models_fields_name(rel_model, temp_prefix)
                    final.extend(final_rel)
        return final

    _final = get_all_related_models_fields_name(model)
    return _final


def search_query(query):
    awbs = []
    tbs = []
    mtss = []
    drss = []
    dtos = []
    rtos = []
    for q in query:
        if q[:2] == 'TB':
            try:
                tb = TB.objects.get(tb_id=q)
                tbs.append(tb)
            except TB.DoesNotExist:
                pass
        elif q[:2] == 'MT':
            try:
                mts = MTS.objects.get(mts_id=q)
                mtss.append(mts)
            except MTS.DoesNotExist:
                pass
        elif q[:2] == 'DR':
            try:
                drs = DRS.objects.get(drs_id=q)
                drss.append(drs)
            except DRS.DoesNotExist:
                pass
        elif q[:2] == 'DT':
            try:
                dto = DTO.objects.get(dto_id=q)
                dtos.append(dto)
            except DTO.DoesNotExist:
                pass
        elif q[:2] == 'RT':
            try:
                rto = RTO.objects.get(rto_id=q)
                rtos.append(rto)
            except RTO.DoesNotExist:
                pass
        else:
            try:
                mts_found = MTS.objects.filter(co_loader_awb=q)
                mtss += [mts for mts in mts_found]
                awb_found = AWB.objects.filter(
                    Q(awb=q) | Q(phone_1=q) | Q(phone_2=q) | Q(customer_name__icontains=q) | Q(order_id=q))
                awbs += [awb for awb in awb_found]
            except AWB.DoesNotExist:
                pass
    return list(set(awbs)), list(set(tbs)), list(set(mtss)), list(set(drss)), list(set(dtos)), list(set(rtos))


def filter_query(query):
    return re.match(r'(^[A-Z]{2,3})([0-9]{6,15})', query)


def check_date_gte_current(date, format):
    if datetime.strptime(date, format).date() >= datetime.now().date():
        return True
    else:
        return False


def get_date_range(days, format):
    end_date = datetime.today()
    start_date = end_date - timedelta(days=days)
    return start_date.strftime(format), end_date.strftime(format)


def get_date_list(days, format):
    base = datetime.today()
    return [(base - timedelta(days=x)).strftime(format) for x in range(0, days)]


def get_last_working_days_list(days, format, holidays=[]):
    base = datetime.today() - timedelta(days=1)
    return [(base - timedelta(days=x)).strftime(format) for x in range(0, days) if
            (base - timedelta(days=x)).weekday() != 6 and (base - timedelta(days=x)) not in holidays]


def get_user_emails(user):
    emails = []
    if user.email != '':
        emails.append(user.email)
    for u in UserEmails.objects.filter(user=user):
        emails.append(u.email)
    return list(set(emails))


def get_percentage(param, base):
    try:
        perc = (float(param) / float(base)) * 100
        return round(perc, 2)
    except:
        return ''


def get_date_diff(a, b):
    try:
        diff = abs(a - b)
        return diff.days
    except:
        return ''


def get_json_response(python_dict):
    json_string = json.dumps(python_dict, cls=DjangoJSONEncoder)
    return HttpResponse(json_string, content_type="application/json")


def convert_request_querydict_to_dict(request_querydict):
    d = dict(request_querydict)
    new_d = {}
    for i, j in d.items():
        if isinstance(j, (collections.Iterable, collections.Sized)):
            if len(j) > 1 and isinstance(j, list):
                new_d[i] = j
            elif isinstance(j, list):
                new_d[i] = j[0] if len(j) != 0 else ''
            else:
                new_d[i] = j
        else:
            new_d[i] = j
    return new_d


def change_date_format(date, old_format, new_format):
    return datetime.strptime(str(date), old_format).strftime(new_format)


class Echo(object):
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


class QueryBuilder(object):
    def __init__(self, model):
        self.awb_queryset = model.objects.all()
        self.the_q = None
        self.exhausted = False
        self.queryset_given = False

    def add_q(self, the_q, queryset=None):  # default operation which is and
        if queryset:
            self.queryset_given = True
            self.awb_queryset &= queryset
            return
        self.add_q_for_and(the_q)

    def add_q_for_or(self, the_q):
        if self.the_q:
            self.the_q |= the_q
        else:
            self.the_q = the_q

    def add_q_for_and(self, the_q):
        if self.the_q:
            self.the_q &= the_q
        else:
            self.the_q = the_q

    def resolve_query(self):
        assert self.queryset_given or self.the_q, 'No query given'
        if self.the_q:
            return self.awb_queryset.filter(self.the_q)
        else:
            return self.awb_queryset

    def is_empty(self):
        try:
            self.resolve_query()
        except AssertionError:
            return True
        return not self.awb_queryset.exists()

        # def generate_mis(self, action, type, args_dict=None):
        # self.resolve_query()
        # assert self.arg_dict or args_dict, 'no args dict to check mis type and email list given'
        # self.arg_dict = args_dict if args_dict else self.arg_dict  #fucking buggy
        #     generate_mis.apply_async(self.awb_queryset.values_list('pk', flat=True), self.arg_dict, action, type,
        #                              link_error=email_on_failure.s())


def set_cache(key, queryset):
    request = get_current_request()
    new_key = str(key) + '_' + str(request.user.pk)
    if request.session.get('branch'):
        new_key += '_' + str(request.session.get('branch'))
    cache.set(new_key, queryset)


def get_cache(key):
    request = get_current_request()
    new_key = str(key) + '_' + str(request.user.pk)
    if request.session.get('branch'):
        new_key += '_' + str(request.session.get('branch'))
    return cache.get(new_key)


def del_cache(key):
    request = get_current_request()
    new_key = str(key) + '_' + str(request.user.pk)
    if request.session.get('branch'):
        new_key += '_' + str(request.session.get('branch'))
    cache.delete(new_key)


def set_session(key, value):
    request = get_current_request()
    request.session[key] = value


def get_session(key):
    request = get_current_request()
    return request.session.get(key)


def del_session(key):
    request = get_current_request()
    if key in request.session:
        del request.session[key]


def set_message(msg, type):
    request = get_current_request()
    if 'message' not in request.session:
        request.session['message'] = dict()
    request.session['message']['report'] = msg
    request.session['message']['class'] = type


def convert_to_integer_list(param, separater):
    return list(set([int(i) for i in param.split(separater) if i != '']))


def process_data(data):
    if data is not None:
        if type(data) == float or type(data) == int:
            return data
        else:
            return smart_str(data)
    else:
        return ''