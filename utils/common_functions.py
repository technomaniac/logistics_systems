import random
import string
import json
from django.forms.models import model_to_dict
from django.http.response import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from django.db.models import get_model
from django.db import models
import collections
# from apps.common_ds.models import CommonInfo

def convert_request_querydict_to_dict(request_querydict):
    d = dict(request_querydict)
    new_d = {}
    for i, j in d.items():
        if isinstance(j, (collections.Iterable, collections.Sized)):
            if len(j) > 1 and isinstance(j, list):
                new_d[i] = j
            elif isinstance(j, list):
                new_d[i] = j[0] if len(j) != 0 else ''
            else:
                new_d[i] = j
        else:
            new_d[i] = j
    return new_d


def remove_spaces_from_string(the_str):
    the_list = ['' if i==' ' else i for i in the_str]
    return ''.join(the_list)


def get_history_model(model):
    return get_model(model._meta.app_label, model.__name__+'History')


def has_history_model(model):
    if get_history_model(model):
        return True
    else:
        return False


# def get_latest_version(history_model, parent_instance):
#     history = history_model._default_manager.filter(to_model=parent_instance).order_by('-version')
#     if history.exists():
#         return history[0]
#     else:
#         return None
#
#
# def save_history_model(parent_object, user_edited=None, user_created=None):
#     parent_object_dict = parent_object._meta.model._default_manager.filter(pk=parent_object.pk).values()[0]
#     parent_object_dict.pop('id', None)
#     parent_model = parent_object._meta.model
#     history_model = get_history_model(parent_model)
#     latest_revision = get_latest_version(history_model, parent_object)
#     version = 0
#     change_count = 0
#     if latest_revision:
#         history_model_object_dict = history_model._default_manager.filter(version=latest_revision.version,
#                                                                           to_model=parent_object).values()[0]
#         history_model_object_dict.pop('id', None)
#         common_model_field_names = CommonInfo._meta.get_all_field_names()
#         try:
#             common_model_field_names.remove('active')
#         except:
#             print 'While creating history models, could not find field %s in model CommonInfo' % 'active'
#         for key in parent_object_dict:
#             if key not in common_model_field_names and parent_object_dict[key] != history_model_object_dict[key]:
#                 change_count += 1
#     if latest_revision and change_count:
#         version = latest_revision.version + 1
#     if version == 0 and not latest_revision:
#         user_created = user_edited
#     new_history = None
#     if version == 0 and not latest_revision or change_count:
#         new_history = history_model._default_manager.create(version=version, user_created=user_created,
#                                                             user_edited=user_edited, to_model=parent_object,
#                                                             **parent_object_dict)
#     return new_history


def create_random_string(string_length=10):
    random_str = ''.join([random.choice(string.ascii_lowercase+string.ascii_uppercase+'0123456789') for i in xrange(1,string_length)])
    return random_str


def get_unique_hash(initials='', hash_list=None, length=10):
    try:
        garbage = len(hash_list[0])
    except Exception:
        hash_list = []
    random_str = create_random_string(length)
    start = datetime.now()

    while random_str in hash_list:
        print random_str, 'random_str'
        print hash_list, 'hash_list'
        random_str = create_random_string(length)
        end = datetime.now()
        if start - end > timedelta(seconds=5):
            length = length + 1
            start = end

    return initials + random_str


def get_json_response(python_dict):
    json_string = json.dumps(python_dict, cls=DjangoJSONEncoder)
    return HttpResponse(json_string, mimetype='application/json')


def compare_2_model_instances(model1, model2, *exclude_fields):
    # given 2 instances, return a list of field_names where they differ
    model1_object_dict = model1._meta.model._default_manager.filter(pk=model1.pk).values()[0]
    model2_object_dict = model2._meta.model._default_manager.filter(pk=model2.pk).values()[0]
    for field in exclude_fields:
        try:
            model1_object_dict.pop(field)
        except KeyError:
            print('The model class %s has no field named %s. Hence it cannot be excluded' % (
                model1._meta.model.__name__,
                field)
            )
    model1_object_dict.pop('id', None)
    diff_fields = []
    for key in model1_object_dict:
        try:
            if model1_object_dict[key] != model2_object_dict[key]:
                diff_fields.append(key)
        except KeyError:
            raise Exception('The model class %s has no field named %s. Try putting that in excluded fields' %(
                model2._meta.model.__name__,
                key)
            )

    return None if not diff_fields else diff_fields
