import re

from django.db import transaction
from django.utils.encoding import force_unicode
import xlrd


DATA_IMPORTER_EXCEL_DECODER = "cp1252"

DATA_IMPORTER_DECODER = "utf-8"


def objclass2dict(objclass):
    """
    Meta is a objclass on python 2.7 and no have __dict__ attribute.

    This method convert one objclass to one lazy dict without AttributeError
    """

    class Dict(dict):
        def __init__(self, data={}):
            super(Dict, self).__init__(data)
            self.__dict__ = dict(self.items())

        def __getattr__(self, key):
            try:
                return self.__getattribute__(key)
            except AttributeError:
                return False

    obj_list = [i for i in dir(objclass) if not str(i).startswith("__")]
    obj_values = []
    for objitem in obj_list:
        obj_values.append(getattr(objclass, objitem))
    return Dict(zip(obj_list, obj_values))


class BaseImporter(object):
    """
    Base Importer method to create simples imports XLSX files.
    """

    def __new__(cls, **kargs):
        """
        Provide custom methods in subclass Meta
        """
        if hasattr(cls, "Meta"):
            cls.Meta = objclass2dict(cls.Meta)
        return super(BaseImporter, cls).__new__(cls)

    def __init__(self, source=None):
        self.reader = []
        self.error_list = []
        self.valid = False
        self.awb_uploaded = []

        if source:
            self.source = source
            self.start_fields()
            self.open_file()
            self.set_column_field_map()
            self.set_reader()
            self.run_validator()

    class Meta:
        """
        Importer configurations
        """
        validator = None

    @property
    def meta(self):
        """
        Is same to use .Meta
        """
        if hasattr(self, 'Meta'):
            return self.Meta

    def set_column_field_map(self):
        self.header = self.get_header()
        self.column_field_map = dict()

        for field, header in self.field_dict.iteritems():
            self.column_field_map[self.header.index(header)] = field


    def set_reader(self):
        for data in self.cleaned_data():
            self.reader.append(data)

    def get_header(self):
        return [self.process_cell(0, col).lower() for col in xrange(self.cols)]

    def process_cell(self, row, col):
        return self.convert_value(self.to_unicode(self.worksheet.cell_value(row, col)))

    def open_file(self):
        "Read XLSX files"
        self.workbook = xlrd.open_workbook(self.source)
        self.worksheet = self.workbook.sheet_by_index(0)
        self.rows = self.worksheet.nrows
        self.cols = self.worksheet.ncols


    def get_items(self):
        """
        Get values from cells
        :return: list of rows
        """
        return [[self.convert_value(self.to_unicode(self.worksheet.cell_value(row, col))) for col in xrange(self.cols)]
                for row in xrange(self.rows)]


    # def exclude_fields(self):
    # """
    # Exclude fields from Meta.exclude
    # """
    # if self.Meta.exclude and not self._excluded:
    # self._excluded = True
    # for exclude in self.Meta.exclude:
    # if exclude in self.fields:
    # self.fields.remove(exclude)


    def start_fields(self):
        """
        Initial function to find fields or headers values
        This values will be used to process clean and save method
        If this method not have fields and have Meta.model this method
        will use model fields to populate content without id
        """
        if self.Meta.model and not hasattr(self, 'fields'):
            if self.Meta.include:
                self.all_models_fields = [i.name for i in self.Meta.model._meta.fields if i.name != 'id']

                self.field_dict = dict(self.Meta.include)
                for field in self.field_dict.keys():
                    if field not in self.all_models_fields:
                        raise KeyError("%s field not found in %s model" % (field, self.Meta.model.__name__))

                self.field_list = self.field_dict.keys()

            if self.Meta.foreign_keys:
                self.foreign_keys = dict(self.Meta.foreign_keys)


    @staticmethod
    def to_unicode(bytestr):
        """
        Receive string bytestr and try to return a utf-8 string.
        """
        if not isinstance(bytestr, str) and not isinstance(bytestr, unicode):
            return bytestr

        try:
            decoded = bytestr.decode(DATA_IMPORTER_EXCEL_DECODER)  # default by excel csv
        except UnicodeEncodeError:
            decoded = force_unicode(bytestr, DATA_IMPORTER_DECODER)

        return decoded

    @staticmethod
    def convert_value(item):
        """
        Handle different value types for XLSX. Item is a cell object.
        """
        # if item.is_date:
        # return item.internal_value  # return datetime

        if type(item) == float:
            if item % 1 == 0:  # return integers
                return int(item)
        else:
            item = re.sub(' +', ' ', item)
        return item  # return string

    def run_validator(self):
        '''
        Validates the data on database level and updates the error list
        '''
        # print self.reader
        if self.Meta.validator:
            validator = self.Meta.validator(self.reader)
            self.error_list = validator.error_list
            if not self.error_list:
                self.valid = True

    def cleaned_data(self):
        '''
        Generates Dictionary with keys as fields of Model
        '''
        # print self.column_field_map
        for row in xrange(1, self.rows):
            data = dict()
            for col, field in self.column_field_map.iteritems():
                # print self.column_field_map[col], self.process_data(row, col)
                # if field not in self.foreign_keys.keys():
                data[field] = self.process_cell(row, col)

                try:
                    self.pre_clean(field, data)
                except Exception, e:
                    self.error_list.append(('__pre_clean__', repr(e)))

                    # else:
                    # foreign_model = self.Meta.model._meta.get_field(self.column_field_map[col]).rel.to
                    # data[self.column_field_map[col]] = foreign_model.objects.filter(
                    # **{self.foreign_keys[self.column_field_map[col]]: self.process_data(row, col)}).first()
            yield data

    def pre_clean(self, field, data):
        """
        Executed before clean methods
        """

    def update_foreign_keys(self, **data):
        '''
        Generates Dictionary with keys as fields of Model
        '''
        # print self.foreign_keys
        for field in self.foreign_keys.keys():
            foreign_model = self.Meta.model._meta.get_field(field).rel.to
            data[field] = foreign_model.objects.get(**{self.foreign_keys[field]: data[field]})
            # print data[field], self.foreign_keys[field]
            # for field, value in data.iteritems():
            # # print field, value
            # if type(value) == object:
            # print field, value
        # if field in self.foreign_keys.keys():
        # foreign_model = self.Meta.model._meta.get_field(field).rel.to
        # data[field] = foreign_model.objects.get(**{self.foreign_keys[field]: value})
        # print data[field], value, self.foreign_keys[field]
        # print data
        return data

    @transaction.atomic
    def save(self):
        '''
        Saves data into Database
        '''
        if self.valid:
            insert_list = []

            instance = self.Meta.model

            if not instance:
                raise AttributeError("Invalid instance model")

            for data in self.reader:
                insert_list.append(instance(**self.update_foreign_keys(**data)))

            # for key, value in self.update_foreign_keys(**self.reader[0]).iteritems():
            # print key, value
            # print self.error_list
            instance.objects.bulk_create(insert_list)
            return "data uploaded"
        else:
            return self.error_list


class BaseValidator(object):
    error_list = []

    def __init__(self, data_handler):
        if data_handler:
            self.data_handler = data_handler

        self.validate_data()

    def validate_data(self):
        raise NotImplementedError