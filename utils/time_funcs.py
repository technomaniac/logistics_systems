from datetime import datetime
from datetime import timedelta
import calendar


def string_time_to_datetime(string_time, type_of_date='y/m/d', date=False, splitter='/'):
    '''
    string_time should be of format year/month/day/hour/minute/second
    '''
    string_broken = string_time.split(splitter)
    for ind, value in enumerate(string_broken):
        if value == '':
            string_broken.pop(ind)
        else:
            string_broken[ind] = int(value)

    if len(string_broken) < 6:
        for garbage in xrange(6 - len(string_broken)):
            string_broken.append(0)

    the_time = None
    if type_of_date == 'y/m/d':
        the_time = datetime(year=string_broken[0], month=string_broken[1], day=string_broken[2],
                            hour=string_broken[3], minute=string_broken[4], second=string_broken[5])
    elif type_of_date == 'm/d/y':
        the_time = datetime(year=string_broken[2], month=string_broken[0], day=string_broken[1],
                            hour=string_broken[3], minute=string_broken[4], second=string_broken[5])

    if date:
        the_time = the_time.date()
    return the_time


def minutes_to_datetime(minutes_from_day_start, day=None, year=None, month=None):
    now = datetime.now()
    hours = minutes_from_day_start / 60
    minutes = minutes_from_day_start % 60
    day = day if day is not None else now.day
    month = month if month is not None else now.month
    year = year if year is not None else now.year
    return datetime(year=year, month=month, day=day, hour=hours, minute=minutes)


def str_to_time(time_str, is_delta=False):
    time_list = time_str.split('/')
    if len(time_list) == 6 and is_delta == False:
        y = int(time_list[0])
        mo = int(time_list[1])
        d = int(time_list[2])
        h = int(time_list[3])
        mi = int(time_list[4])
        s = int(time_list[5])
        timestamp = datetime(y, mo, d, h, mi, s)
        return timestamp
    elif len(time_list) == 4 and is_delta == True:
        d = int(time_list[0])
        h = int(time_list[1])
        mi = int(time_list[2])
        s = int(time_list[3])
        delta_stamp = timedelta(days=d, hours=h, minutes=mi, seconds=s)
        return delta_stamp
    else:
        return -1


def get_12_hr_datetime_str(ip_datetime):
    return ip_datetime.strftime("%d/%m/%y %I:%M:%S %p")


def get_12_hr_date_str(ip_datetime):
    return ip_datetime.strftime("%d/%m/%y")


def get_delta_str(time_delta):
    delta_str = ''
    if time_delta.days > 0:
        delta_str += unicode(time_delta.days) + ' day'
        if time_delta.days > 1: delta_str += 's'
    hours = time_delta.total_seconds() // 3600
    hours = hours % 24
    minutes = (time_delta.total_seconds() % 3600) // 60
    if time_delta.days > 0 and (hours > 0 or minutes > 0):
        delta_str += ' '
    if hours > 0:
        delta_str += unicode(hours) + ' hour'
        if hours > 1: delta_str += 's'
    if hours > 0 and minutes > 0:
        delta_str += ' '
    if minutes > 0:
        delta_str += unicode(minutes) + ' minute'
        if minutes > 1: delta_str += 's'
    else:
        return 'few seconds ago'
    return delta_str


def inc_date_by(init_time, period_str):
    """
    increment should be aware of how many days in a particular month & year etc and should
    increment accordingly
    if init_time = 31 Oct 2012, init_time + 1 month = 30 Nov 2012 (since 31 Nov 2012 does not exist)
    """
    if period_str == 'H':
        return init_time + timedelta(hours=1)
    elif period_str == 'D':
        return init_time + timedelta(days=1)
    elif period_str == 'W':
        return init_time + timedelta(days=7)
    elif period_str == 'M':
        days_this_month = calendar.monthrange(init_time.year, init_time.month)[1]
        next_month = init_time.month + 1
        next_year = init_time.year
        if next_month == 13:
            next_month = 1
            next_year = init_time.year + 1
        days_next_month = calendar.monthrange(next_year, next_month)[1]
        if init_time.day > days_next_month:
            days_to_add = days_this_month - (init_time.day - days_next_month)
        else:
            days_to_add = days_this_month
        return init_time + timedelta(days=days_to_add)
    elif period_str == 'Y':
        if calendar.isleap(init_time.year) and init_time.month <= 2:
            # 1 Jan 2012 + 1 year should equal 1 Jan 2013
            if init_time.month == 2 and init_time.day == 29:
                # 29 Feb 2012 + 1 year should equal 28 Feb 2013
                return init_time + timedelta(days=364)
            return init_time + timedelta(days=366)
        elif calendar.isleap(init_time.year + 1) and init_time.month >= 3:
            # 1 March 2011 + 1 year should equal 1 March 2012
            return init_time + timedelta(days=366)
        else:
            return init_time + timedelta(days=365)


def dec_date_by(init_time, period_str):
    """
    decrement should be aware of how many days in a particular month & year etc and should
    decrement accordingly
    """
    if period_str == 'H':
        return init_time - timedelta(hours=1)
    elif period_str == 'D':
        return init_time - timedelta(days=1)
    elif period_str == 'W':
        return init_time - timedelta(days=7)
    elif period_str == 'M':
        days_this_month = calendar.monthrange(init_time.year, init_time.month)[1]
        if init_time.month == 1:
            prev_month = 12
            prev_year = init_time.year - 1
        else:
            prev_month = init_time.month - 1
            prev_year = init_time.year
        days_prev_month = calendar.monthrange(prev_year, prev_month)[1]
        days_to_subtract = days_prev_month
        if init_time.day > days_prev_month:
            days_to_subtract += init_time.day - days_prev_month
        return init_time - timedelta(days=days_to_subtract)
    elif period_str == 'Y':
        if calendar.isleap(init_time.year - 1) and init_time.month <= 2:
            # 1 Jan 2013 - 1 year should equal 1 Jan 2012
            return init_time - timedelta(days=366)
        elif calendar.isleap(init_time.year):
            if init_time.month == 2 and init_time.day == 29:
                return init_time - timedelta(days=366)
            elif init_time.month > 2:
                return init_time - timedelta(days=366)
            else:
                return init_time - timedelta(days=365)
        else:
            return init_time - timedelta(days=365)


def convert_sec_to_min(seconds):
    if seconds:
        min = int(seconds) / 60
        sec = int(seconds) % 60
    else:
        min = ''
        sec = ''
    return min, sec