from __future__ import unicode_literals
import re
import os
from time import gmtime, strftime
import base64
import urllib
from datetime import datetime

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from django.core.mail.message import EmailMessage
import xlrd
from barcode.codex import Code39
from barcode.writer import ImageWriter
from django.contrib.auth.models import User
from celery import task
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives

from awb.models import AWB_Status, AWB, AWB_History, AWB_Images
from customer_service.models import AWB_Out_Calling
from internal.models import Branch, Branch_Pincode, Employee, Vehicle
from logistics.settings.base import AWS_S3_ACCESS_KEY_ID, AWS_S3_SECRET_ACCESS_KEY, AWS_S3_BUCKET_NAME, AWS_S3_URL
from zoning.models import Pincode, City
from utils.constants import MANIFEST_HEADER_DICT
from utils.random import get_manifest_header_dict
from logistics.settings import MEDIA_ROOT
from client.models import Client, Client_Vendor


def get_manifest_filename(data, file):
    client_code = str(Client.objects.get(pk=data['client']).client_code).upper()
    datetime = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
    ext = '.' + str(file.name.split('.')[-1]).lower()
    filename = client_code + str('_') + data['category'].upper() + str('_') + datetime + ext
    dir = 'uploads/manifest/'
    handle_uploaded_file(file, filename, MEDIA_ROOT + str(dir))
    return dir + filename


def handle_uploaded_file(file, name, dir):
    try:
        open(dir + name, 'wb+')
    except:
        if not os.path.exists(dir):
            os.makedirs(dir)
    with open(dir + name, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return dir + name


def handle_uploaded_media(file, name, dir):
    try:
        open(dir + name, 'wb+')
    except:
        if not os.path.exists(MEDIA_ROOT + dir):
            os.makedirs(MEDIA_ROOT + dir)
    with open(MEDIA_ROOT + dir + name, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
    return dir + name


@task
def upload_manifest_data(manifest, user):
    try:
        client = manifest.client
        filename = MEDIA_ROOT + str(manifest.file)
        if is_valid_url(filename):
            excel_file = open_file_from_url(filename).name
        else:
            excel_file = filename
        workbook = xlrd.open_workbook(excel_file)
        worksheet = workbook.sheet_by_index(0)
        rows = worksheet.nrows
        cols = worksheet.ncols

        header = {}
        for col in range(0, cols):
            val = str(worksheet.cell_value(0, col)).lower().strip()
            header.update(get_manifest_header_dict(val, MANIFEST_HEADER_DICT, col))

        awb_uploaded = []
        awb_existing = []
        order_id_existing = []
        wrong_pincode = {}
        wrong_awb = []
        for row in range(1, rows):
            try:
                awb = AWB.objects.get(awb=worksheet.cell_value(row, header['awb']))
                awb_existing.append(awb)
            except AWB.DoesNotExist:
                awb = str(worksheet.cell_value(row, header['awb'])).strip()
                order_id = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['order_id'])))
                check_order_id = AWB.objects.filter(order_id=order_id, category='REV',
                                                    awb_status__manifest__client=client).exclude(
                    awb_status__status='CAN')
                order_id_can_length = len([i for i in check_order_id])
                if order_id_can_length > 0:
                    order_id_existing += [i.pk for i in check_order_id]
                else:
                    if awb[:3].upper() == client.client_code:
                        # and len(awb) == 10 and awb[3:] <= Client.objects.get(client_code=client).awb_assigned_to[3:]:
                        try:
                            pincode = Pincode.objects.get(pincode=int(worksheet.cell_value(row, header['pincode'])))
                            vendor_code = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['vendor'])))
                            if type(vendor_code) is str:
                                vendor_code = re.sub('\.[^.]*$', '',
                                                     str(worksheet.cell_value(row, header['vendor']))).lower().strip()
                            else:
                                vendor_code = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['vendor'])))
                            try:
                                vendor = Client_Vendor.objects.get(vendor_code=vendor_code)
                            except:
                                vendor = None
                            bind = {}
                            if client.category == 'RL':
                                bind['category'] = 'REV'
                                # try:
                                # open(MEDIA_ROOT + 'awb/barcode/' + awb + '.png')
                                # except IOError:
                                # if not os.path.exists(MEDIA_ROOT + 'awb/barcode/'):
                                # os.makedirs(MEDIA_ROOT + 'awb/barcode/')
                                bind['barcode'] = generate_barcode(awb)
                            else:
                                bind['category'] = ''
                            for key in header.keys():
                                if key == 'pincode':
                                    bind[key] = pincode
                                elif key == 'phone_1':
                                    bind[key] = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header[key])))
                                elif key == 'phone_2':
                                    bind[key] = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header[key])))
                                elif key == 'order_id':
                                    bind[key] = order_id
                                elif key == 'category':
                                    if bind[key] == '':
                                        if worksheet.cell_value(row, header[key]).upper() == 'COD':
                                            bind[key] = 'COD'
                                        else:
                                            bind[key] = 'PRE'
                                elif key == 'expected_amount':
                                    if worksheet.cell_value(row, header[key]):
                                        bind[key] = int(worksheet.cell_value(row, header[key]))
                                    else:
                                        bind[key] = int(0)
                                elif key == 'priority':
                                    if worksheet.cell_value(row, header[key]).upper().strip() != '':
                                        bind[key] = worksheet.cell_value(row, header[key]).upper().strip()[0]
                                elif key == 'vendor':
                                    bind[key] = vendor
                                    # if worksheet.cell_value(row, header[key]).upper().strip() == 'low':
                                    # bind[key] = 'L'
                                    # else:
                                    # bind[key] = 'N'
                                elif worksheet.cell_value(row, header[key]):
                                    if type(worksheet.cell_value(row, header[key])) is float:
                                        bind[key] = float(worksheet.cell_value(row, header[key]))
                                    else:
                                        try:
                                            bind[key] = str(
                                                worksheet.cell_value(row, header[key]).encode('utf-8')).strip()
                                        except:
                                            bind[key] = worksheet.cell_value(row, header[key])
                                else:
                                    bind[key] = ''

                            awb = AWB(**bind)
                            # if awb.package_value >= 2000 or awb.package_price >= 2000:
                            # awb.priority = 'U'
                            if client.client_code == 'SND' and awb.package_value != '':
                                try:
                                    if awb.package_value >= 2000:
                                        awb.priority = 'U'
                                except:
                                    awb.priority = 'N'
                            awb.save()
                            AWB_Status.objects.create(awb=awb, manifest=manifest, updated_by=user)
                            AWB_History.objects.create(awb=awb, status='DR', updated_by=user)
                            AWB_Images.objects.create(awb=awb, type='BAR', image=generate_printer_barcode(awb.awb))
                            awb_uploaded.append(awb)
                        except Pincode.DoesNotExist:
                            pincode = int(worksheet.cell_value(row, header['pincode']))
                            wrong_pincode[pincode] = str(
                                worksheet.cell_value(row, header['awb']).encode('utf-8')).strip()
                    else:
                        wrong_awb.append(awb)

        send_manifest_upload_report(awb_uploaded, awb_existing, wrong_awb, wrong_pincode,
                                    list(set(order_id_existing)), user.email, filename.split('/')[2])
    except Exception as e:
        send_error_report(str(e), [user.email])


def send_manifest_upload_report(awb_uploaded, awb_existing, wrong_awb, wrong_pincode, order_id_existing,
                                email, filename):
    template = get_template('awb/upload_manifest_report.html')
    context = Context({
        'manifest': filename,
        'awb_uploaded': awb_uploaded,
        'awb_existing': awb_existing,
        'wrong_pincode': wrong_pincode,
        'wrong_awb': wrong_awb,
        'order_id_existing': AWB.objects.filter(pk__in=order_id_existing)
    })
    content = template.render(context)
    msg = EmailMultiAlternatives('Manifest Upload Report', "This is a system generated mail. Please do not reply.",
                                 "system@nuvoex.com",
                                 [email])
    msg.attach_alternative(content, "text/html")
    msg.send()


def send_error_report(body, to):
    message = EmailMessage('Error', body, 'system@nuvoex.com', to)
    # message.attach(filename, csvfile.getvalue(), 'text/csv')
    return message.send()


def upload_branch_pincode_file(file):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols

    header = {}
    for row in range(0, rows):
        if row == 0:
            for col in range(0, cols):
                cell = str(worksheet.cell_value(0, col)).lower().strip()
                if cell == 'pincode':
                    header['pincode'] = col
                if cell == 'branch':
                    header['branch'] = col
        else:
            pincode = Pincode.objects.get(pincode=int(worksheet.cell_value(row, header['pincode'])))
            try:
                branch = Branch.objects.get(branch_name=str(worksheet.cell_value(row, header['branch'])).strip())
                try:
                    branch_pincode = Branch_Pincode.objects.get(pincode=pincode.pk)
                    branch_pincode.branch = branch
                    branch_pincode.save()
                except Branch_Pincode.DoesNotExist:
                    Branch_Pincode.objects.create(branch_id=branch.pk, pincode_id=pincode.pk)
            except Branch.DoesNotExist:
                branch = Branch(branch_name=str(worksheet.cell_value(row, header['branch'])).strip())
                branch.save()
                try:
                    branch_pincode = Branch_Pincode.objects.get(pincode=pincode.pk)
                    branch_pincode.branch = branch
                    branch_pincode.save()
                except Branch_Pincode.DoesNotExist:
                    Branch_Pincode.objects.create(branch_id=branch.pk, pincode_id=pincode.pk)


def upload_pincode_city_file(file):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols

    header = {}
    for row in range(0, rows):
        if row == 0:
            for col in range(0, cols):
                cell = str(worksheet.cell_value(0, col)).lower().strip()
                if cell == 'pincode':
                    header['pincode'] = col
                if cell == 'city':
                    header['city'] = col
                if cell == 'state':
                    header['state'] = col
        else:
            try:
                city = City.objects.get(city=str(worksheet.cell_value(row, header['city'])).strip())
            except City.DoesNotExist:
                city = City(
                    city=str(worksheet.cell_value(row, header['city'])).strip(),
                    state=str(worksheet.cell_value(row, header['state'])).strip()
                )
                city.save()
            try:
                Pincode.objects.get(pincode=int(worksheet.cell_value(row, header['pincode'])))
            except Pincode.DoesNotExist:
                Pincode.objects.create(pincode=int(worksheet.cell_value(row, header['pincode'])), city=city)

    upload_branch_pincode_file(file)


def upload_user_list_file(file):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols

    header = {}
    for row in range(0, rows):
        if row == 0:
            for col in range(0, cols):
                cell = str(worksheet.cell_value(0, col)).lower().strip()
                if cell == 'name':
                    header['name'] = col
                if cell == 'designation':
                    header['role'] = col
                if cell == 'phone':
                    header['phone'] = col
                if cell == 'branch':
                    header['branch'] = col
        else:
            username = re.sub('\s+', '.', str(worksheet.cell_value(row, header['name'])).lower().strip())
            name = re.split('\s+', str(worksheet.cell_value(row, header['name'])).strip())
            first_name = name[0]
            last_name = ''
            password = first_name.lower().strip() + '@123'
            print password
            phone = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['phone'])))
            for i in range(1, len(name)):
                last_name += (name[i] + str(' ')).strip()
            branch = Branch.objects.get(branch_name=str(worksheet.cell_value(row, header['branch'])).strip())
            role = str(worksheet.cell_value(row, header['role'])).strip()
            try:
                user, create = User.objects.get_or_create(username=username, first_name=first_name,
                                                          last_name=last_name,
                                                          password=password, is_staff=True)
            except:
                user = User.objects.get(username=username)
                user.username = username
                user.first_name = first_name
                user.last_name = last_name
                user.password = user.set_password(password)
                user.is_staff = True
                user.save()
                create = user
            if create:
                try:
                    profile = Employee.objects.get(user=user)
                    profile.branch = branch
                    profile.role = role
                    profile.phone = phone
                    profile.save()
                except Employee.DoesNotExist:
                    Employee.objects.create(user=user, branch=branch, role=role, phone=phone)
            else:
                user.username = username
                user.first_name = first_name
                user.last_name = last_name
                user.password = user.set_password(password)
                user.is_staff = True
                user.save()
                try:
                    profile = Employee.objects.get(user=user)
                    profile.branch = branch
                    profile.role = role
                    profile.phone = phone
                    profile.save()
                except Employee.DoesNotExist:
                    Employee.objects.create(user=user, branch=branch, role=role, phone=phone)


def upload_vehicle_list_file(file):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols

    header = {}
    for row in range(0, rows):
        if row == 0:
            for col in range(0, cols):
                cell = str(worksheet.cell_value(0, col)).lower().strip()
                if cell == 'vehicle':
                    header['vehicle'] = col
                if cell == 'branch':
                    header['branch'] = col
        else:
            vehicle_no = str(worksheet.cell_value(row, header['vehicle'])).strip()
            try:
                vehicle = Vehicle.objects.get(vehicle_no=vehicle_no)
            except Vehicle.DoesNotExist:
                vehicle = Vehicle(
                    vehicle_no=vehicle_no,
                    branch=Branch.objects.get(branch_name=str(worksheet.cell_value(row, header['branch'])).strip()),
                    vehicle_type='2W'
                )
                vehicle.save()


def upload_client_vendor_list(file):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols

    header = {}
    for row in range(0, rows):
        if row == 0:
            for col in xrange(0, cols):
                cell = str(worksheet.cell_value(0, col)).lower().strip()
                if cell == 'client':
                    header['client'] = col
                if cell == 'vendor name':
                    header['vendor_name'] = col
                if cell == 'vendor code':
                    header['vendor_code'] = col
                if cell == 'contact person':
                    header['owner_name'] = col
                if cell == 'phone':
                    header['phone'] = col
                if cell == 'address':
                    header['address'] = col
                if cell == 'pincode':
                    header['pincode'] = col
                if cell == 'branch':
                    header['branch'] = col
        else:
            try:
                vendor = Client_Vendor.objects.get(
                    vendor_code=re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['vendor_code']))))

                # vendor.vendor_code=re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['vendor_code'])))
                vendor.vendor_name = unicode(worksheet.cell_value(row, header['vendor_name'])).strip()
                vendor.branch = Branch.objects.get(
                    branch_name=unicode(worksheet.cell_value(row, header['branch'])).upper().strip())
                vendor.owner_name = unicode(worksheet.cell_value(row, header['owner_name'])).strip()
                vendor.phone = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['phone'])))
                vendor.pincode = Pincode.objects.get(
                    pincode=re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['pincode']))))
                vendor.address = unicode(worksheet.cell_value(row, header['address'])).strip()
                print vendor
                vendor.save()

            except Client_Vendor.DoesNotExist:

                Client_Vendor.objects.create(
                    client=Client.objects.get(
                        client_code=unicode(worksheet.cell_value(row, header['client'])).upper().strip()),
                    vendor_code=re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['vendor_code']))),
                    vendor_name=unicode(worksheet.cell_value(row, header['vendor_name'])).strip(),
                    branch=Branch.objects.get(
                        branch_name=unicode(worksheet.cell_value(row, header['branch'])).upper().strip()),
                    owner_name=unicode(worksheet.cell_value(row, header['owner_name'])).strip(),
                    phone=re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['phone']))),
                    pincode=Pincode.objects.get(
                        pincode=re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['pincode'])))),
                    address=unicode(worksheet.cell_value(row, header['address'])).strip(),
                )


# @receiver(models.signals.post_delete, sender=Manifest)
# def auto_delete_file_on_delete(sender, instance, **kwargs):
# """Deletes file from filesystem
# when corresponding `MediaFile` object is deleted.
# """
# if instance.file:
# if os.path.isfile(instance.file.path):
# os.remove(instance.file.path)
#
#
# @receiver(models.signals.pre_save, sender=Manifest)
# def auto_delete_file_on_change(sender, instance, **kwargs):
# """Deletes file from filesystem
# when corresponding `MediaFile` object is changed.
# """
# if not instance.pk:
# return False
#
# try:
# old_file = Manifest.objects.get(pk=instance.pk).file
# except Manifest.DoesNotExist:
# return False
#
# new_file = instance.file
# if not old_file == new_file:
# if os.path.isfile(old_file.path):
# os.remove(old_file.path)


def generate_barcode(text):
    dir = 'awb/barcode/'
    # try:
    # open(MEDIA_ROOT + 'awb/barcode/' + text + '.png')
    # except IOError:
    if not os.path.exists(MEDIA_ROOT + 'awb/barcode/'):
        os.makedirs(MEDIA_ROOT + 'awb/barcode/')
    writer = ImageWriter()
    # options = dict(module_height=12.0, text_distance=0.5, font_size=10, center_text=True,
    # module_width=0.15)
    options = dict(module_height=6.0, text_distance=0.5, font_size=9, quiet_zone=1.0)
    code39 = Code39(text, writer, add_checksum=False)
    code39.save(MEDIA_ROOT + dir + text, options)
    # try:
    # open(MEDIA_ROOT + 'awb/barcode/' + text + '.png')
    # except IOError:
    file = dir + text + '.png'
    # upload_to_s3(file, delete=True)
    return file


def generate_printer_barcode(text):
    dir = 'awb/' + text + '/'
    # try:
    # open(MEDIA_ROOT + 'awb/barcode/' + text + '.png')
    # except IOError:
    if not os.path.exists(MEDIA_ROOT + dir):
        os.makedirs(MEDIA_ROOT + dir)
    writer = ImageWriter()
    options = dict(module_height=10.0, text_distance=0.5, font_size=10, center_text=True,
                   module_width=0.15)
    # options = dict(module_height=6.0, text_distance=0.5, font_size=9, quiet_zone=1.0)
    code39 = Code39(text, writer, add_checksum=False)
    code39.save(MEDIA_ROOT + dir + 'barcode', options)
    file = dir + 'barcode.png'
    # try:
    # open(MEDIA_ROOT + 'awb/barcode/' + text + '.png')
    # except IOError:
    # return upload_to_s3(file, delete=True)
    return file

def decode_upload_image(image_string, dir, name):
    convert = base64.b64decode(image_string)
    if not os.path.exists(MEDIA_ROOT + dir):
        os.makedirs(MEDIA_ROOT + dir)
    t = open(MEDIA_ROOT + dir + name + '.jpeg', 'wb+')
    t.write(convert)
    t.close()
    return dir + name + '.jpeg'

    # def awb_status_restore(file):
    # workbook = xlrd.open_workbook(file)
    # worksheet = workbook.sheet_by_index(0)
    # rows = worksheet.nrows
    # cols = worksheet.ncols
    #
    # header = {}
    # for row in range(0, rows):
    # if row == 0:
    # for col in range(0, cols):
    # cell = str(worksheet.cell_value(0, col)).lower().strip()
    # if cell == 'awb':
    # header['awb'] = col
    # if cell == 'status':
    # header['status'] = col
    # if cell == 'priority':
    # header['priority'] = col
    # else:
    # awb_error = []
    # awb = str(worksheet.cell_value(row, header['awb'])).upper().strip()
    # try:
    # awb = AWB.objects.get(awb=awb)
    #
    # except AWB.DoesNotExist:
    # awb_error.append(awb)
    # def upload_to_s3(file):
    # conn = S3Connection('<aws access key>', '<aws secret key>')
    # bucket = conn.get_bucket('<bucket name>')
    # k = Key(bucket)
    # k.key = file # for example, 'images/bob/resized_image1.png'
    # k.set_contents_from_file(resized_photo)


def open_file_from_url(url):
    file_name = url.split('/')[-1]
    f = open('/tmp/' + file_name, 'wb')
    f.write(urllib.urlopen(url).read())
    f.close()
    return f


def is_valid_url(url):
    regex = re.compile(
        r'^https?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return url is not None and regex.search(url)


@task
def upload_manifest_data_update(file):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols

    header = {}
    for col in range(0, cols):
        val = str(worksheet.cell_value(0, col)).lower().strip()
        header.update(get_manifest_header_dict(val, MANIFEST_HEADER_DICT, col))

    wrong_awb = []

    for row in range(1, rows):
        awb = AWB.objects.filter(awb=worksheet.cell_value(row, header['awb']))
        if awb.exists():
            pincode = Pincode.objects.get(pincode=int(worksheet.cell_value(row, header['pincode'])))
            vendor_code = worksheet.cell_value(row, header['vendor'])
            if type(vendor_code) is str:
                vendor_code = worksheet.cell_value(row, header['vendor']).lower().strip()
            else:
                vendor_code = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['vendor'])))
            try:
                vendor = Client_Vendor.objects.get(vendor_code=vendor_code)
            except:
                vendor = None
            bind = {}
            for key in header.keys():
                if key == 'pincode':
                    bind[key] = pincode
                elif key == 'phone_1':
                    bind[key] = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header[key])))
                elif key == 'phone_2':
                    bind[key] = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header[key])))
                elif key == 'order_id':
                    bind[key] = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header[key])))
                elif key == 'expected_amount':
                    if worksheet.cell_value(row, header[key]):
                        bind[key] = int(worksheet.cell_value(row, header[key]))
                    else:
                        bind[key] = int(0)
                elif key == 'priority':
                    if worksheet.cell_value(row, header[key]).upper().strip() != '':
                        bind[key] = worksheet.cell_value(row, header[key]).upper().strip()[0]
                elif key == 'vendor':
                    bind[key] = vendor
                elif worksheet.cell_value(row, header[key]):
                    if type(worksheet.cell_value(row, header[key])) is float:
                        bind[key] = float(worksheet.cell_value(row, header[key]))
                    else:
                        try:
                            bind[key] = str(
                                worksheet.cell_value(row, header[key]).encode('utf-8')).strip()
                        except:
                            bind[key] = worksheet.cell_value(row, header[key])
                else:
                    bind[key] = ''
            awb.update(**bind)
            # print bind
        else:
            wrong_awb.append(worksheet.cell_value(row, header['awb']))


@task()
def awb_volumetric_detail_upload(file):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols

    header = {}
    for row in range(0, rows):
        if row == 4:
            for col in xrange(0, cols):
                cell = str(worksheet.cell_value(4, col)).lower().strip()
                # print cell
                if cell == 'docket no':
                    header['awb'] = col
                if cell == 'length':
                    header['length'] = col
                if cell == 'width':
                    header['breadth'] = col
                if cell == 'height':
                    header['height'] = col
                if cell == 'weight':
                    header['weight'] = col
        elif row > 4:
            try:
                awb = AWB.objects.get(
                    awb=worksheet.cell_value(row, header['awb']))
                awb.length = worksheet.cell_value(row, header['length'])
                awb.breadth = worksheet.cell_value(row, header['breadth'])
                awb.height = worksheet.cell_value(row, header['height'])
                weight = worksheet.cell_value(row, header['weight'])
                awb.weight = str(weight) + '0'
                # print awb.weight
                awb.save()
            except AWB.DoesNotExist:
                pass


@task
def awb_vendor_upload_file(file, email):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols
    awb_updated = []
    awb_not_exist = []
    vendor_not_exist = []
    header = {}
    for row in range(0, rows):
        if row == 0:
            for col in xrange(0, cols):
                cell = str(worksheet.cell_value(0, col)).lower().strip()
                if cell == 'awb':
                    header['awb'] = col
                if cell == 'vendor code':
                    header['vendor_code'] = col
        else:
            try:
                awb = AWB.objects.get(awb=worksheet.cell_value(row, header['awb']))
                # print awb
                try:
                    vendor = Client_Vendor.objects. \
                        get(vendor_code=re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['vendor_code']))))
                    # print vendor
                    awb.vendor = vendor
                    # print awb.vendor
                    awb.save()
                    awb_updated.append(awb)
                except Client_Vendor.DoesNotExist:
                    vendor_not_exist.append(
                        re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['vendor_code']))))
            except AWB.DoesNotExist:
                awb_not_exist.append(worksheet.cell_value(row, header['awb']))

    send_awb_vendor_update_report(awb_updated, awb_not_exist, vendor_not_exist, email)


def send_awb_vendor_update_report(awb_updated, awb_not_exist, vendor_not_exist, email):
    template = get_template('awb/awb_vendor_update_report.html')
    context = Context({
        'awb_updated': awb_updated,
        'awb_not_exist': awb_not_exist,
        'vendor_not_exist': vendor_not_exist,
    })
    content = template.render(context)
    msg = EmailMultiAlternatives('AWB Vendor Update Report', "This is a system generated mail. Please do not reply.",
                                 "system@nuvoex.com",
                                 [email])
    msg.attach_alternative(content, "text/html")
    msg.send()


@task
def awb_calling_upload_file(file, email):
    workbook = xlrd.open_workbook(file)
    worksheet = workbook.sheet_by_index(0)
    rows = worksheet.nrows
    cols = worksheet.ncols
    header = {}
    for row in range(0, rows):
        if row == 0:
            for col in xrange(0, cols):
                cell = str(worksheet.cell_value(0, col)).lower().strip()
                if cell == 'tracking_number':
                    header['tracking_number'] = col

                if cell == 'phone':
                    header['phone'] = col
        else:
            try:
                awb = AWB.objects.get(awb=worksheet.cell_value(row, header['tracking_number']))
                # try:
                client = Client.objects.get(client_name=awb.get_client())
                awb_c = AWB_Out_Calling.objects.filter(awb=awb)
                if len(awb_c) != 0:
                    phone = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['phone']))).strip()
                    phone = '0' + phone
                    awb_c.update(creation_date=datetime.now(), agent_no=phone, call_made=False, client=client)
                else:
                    phone = re.sub('\.[^.]*$', '', str(worksheet.cell_value(row, header['phone']))).strip()
                    phone = '0' + phone
                    # print client
                    AWB_Out_Calling.objects.create(awb=awb, agent_no=phone, client=client)

            except AWB.DoesNotExist:
                pass


def upload_to_s3(filepath, delete=False):
    # try:
    s3_conn = S3Connection(AWS_S3_ACCESS_KEY_ID, AWS_S3_SECRET_ACCESS_KEY)
    bucket = s3_conn.get_bucket(AWS_S3_BUCKET_NAME)
    key = Key(bucket, filepath)
    # import pdb; pdb.set_trace()
    key.copy(key.bucket, key.name, preserve_acl=True, metadata={'Content-Type': 'image/jpeg'})
    s3_path = AWS_S3_URL + filepath
    if delete:
        os.remove(filepath)
    return s3_path
    # except:
    #     return filepath


