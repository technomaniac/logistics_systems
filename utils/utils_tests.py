from django.test import TestCase
from .searching import SearchGetAttrs
from .common_functions import compare_2_models_instances


# class SearchAttributeTest(TestCase):
#
#     def setUp(self):
#         from apps.vendors.models import Vendor, Person
#         from apps.users.models import CustomUser
#         custom_user = CustomUser.objects.create(email='test@email.com')
#         person = Person.objects.create(user=custom_user)
#         test_vendor = Vendor.objects.create(vendor_admin=person, name='vendor', legal_name='legal vendor')
#         self.model = Vendor
#         self.vendor = self.model.objects.get(name='vendor', legal_name='legal vendor')
#         self.user = custom_user
#         self.user_model = CustomUser
#
#     def test_get_vendor_by_name(self):
#         vendor = self.vendor
#         print self.model.objects.all()[0].name
#         print self.model.objects.all()[0].legal_name
#         q_dict = {'name': 'vendor', 'legal_name': 'legal vendor'}
#         vendor_qs = SearchGetAttrs(model=self.model, get_dict=q_dict).get_query_set()
#         self.assertTrue(vendor_qs, msg='no vendor_qs')
#         self.assertIn(vendor, vendor_qs, msg='no vendor in vendor_qs')
#
#     def test_get_vendor_by_person(self):
#         q_dict = {'vendor_admin__user__email': 'test@email.com'}
#         vendor_qs = SearchGetAttrs(model=self.model, get_dict=q_dict).get_query_set()
#         self.assertEquals(self.vendor.pk, vendor_qs[0].pk)
#
#     def test_reverse(self):
#         q_dict = {'persons__vendor__id': self.vendor.pk}
#         vendor_qs = SearchGetAttrs(model=self.user_model, get_dict=q_dict).get_query_set()
#         self.assertTrue(vendor_qs)


class Compare2ModelInstancesTest(TestCase):

    fixtures = ['test.json']

    def setUp(self):
        from apps.awb.models import AWBShipment, AWBHistory, AWB
        self.model1 = AWB.objects.get(id=1)
        self.model2 = AWB.objects.get(id=2)
        self.model3 = AWBShipment.objects.get(id=1)
        from apps.common_ds.models import CommonInfo
        self.exclude_fields = CommonInfo._meta.get_all_field_names()
        self.exclude_fields.pop('active')

    def test_function(self):
        self.assertEqual(compare_2_models_instances(self.model1, self.model2, *self.exclude_fields), False)
        self.assertNotEqual(compare_2_models_instances(self.model1, self.model1, *self.exclude_fields), True)