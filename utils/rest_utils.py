from django.db.models.fields.related import ForeignRelatedObjectsDescriptor, SingleRelatedObjectDescriptor, ManyRelatedObjectsDescriptor, \
    OneToOneField
from django.db.models.related import RelatedObject
from rest_framework.serializers import ModelSerializer, SerializerMetaclass, HyperlinkedModelSerializer
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import serializers
from common_functionality.common_functions import choose_queryset_for_model
from utils.searching.searching import SearchGetAttrs
from middlewares.global_request import get_current_request


def model_viewset_factory(model):
    _model = model

    class SimpleViewset(ModelViewSet):
        serializer_class = model_serializer_factory(_model)
        model = _model

        def get_queryset(self):
            queryset = None
            try:
                queryset = choose_queryset_for_model(self.request, _model)
            except:
                pass
            if not queryset:
                try:
                    print 'best'
                    return _model.get_queryset_for_user(self.request.user)
                except AttributeError:
                    print 'no attr'
                    return _model.objects.all()

        def list(self, request, *args, **kwargs):
            if request.GET.keys():
                queryset = SearchGetAttrs(model=_model, get_dict=request.GET).get_query_set()
                serializer = self.serializer_class(queryset, many=True)
                return Response(serializer.data)
            else:
                return super(SimpleViewset, self).list(request, *args, **kwargs)

    SimpleViewset.__name__ = _model.__name__+'Viewset'
    return SimpleViewset


def model_serializer_factory(model, fields=None):
    fields = fields if fields else model._meta.get_all_field_names()
    final_fields = []
    for field in fields:
        if not is_reverse(model, field):
            final_fields.append(field)
    _model = model

    if model.__name__ == 'Fe':
        class FeSerializer(HyperlinkedModelSerializer):
            name = serializers.Field(source='contact.name')
            class Meta:
                model = _model
                fields = final_fields + ['name']
        return FeSerializer
    elif model.__name__.lower() == 'awbshipment':
        class AWBShipmentSerializer(HyperlinkedModelSerializer):
            pickup_addr = serializers.Field(source='pickup_address.address')
            delivery_addr = serializers.Field(source='delivery_address.address')
            class Meta:
                model = _model
                fields = final_fields + ['pickup_addr', 'delivery_addr']
        return AWBShipmentSerializer
    else:
        class TheSerializer(HyperlinkedModelSerializer):
            class Meta:
                model = _model
                fields = final_fields
        TheSerializer.__name__ = model.__name__+'Serializer'
        return TheSerializer


def get_model_fields():
    pass


def is_fkey(model, field_name):
    pass


def get_all_onetoone_models(model):
    final = {}
    for field_name in model._meta.get_all_field_names():
        if is_onetoone(model, field_name):
            final[field_name] = model._meta.get_field_by_name[0].related.parent_model
    return final


def is_onetoone(model, field_name):
    field = model._meta.get_field_by_name(field_name)[0]
    if isinstance(field, (OneToOneField)):
        return True
    return False


def is_reverse(model, field_name):
    field = model._meta.get_field_by_name(field_name)[0]
    if isinstance(field, (RelatedObject)):
        return True

    return False
#
#
# class ModelSerializerFactory(object):
#
#     def __init__(self, model):#todo
#         self.model = model
#
#     def generic_modelserializer_factory(self):#todo
#         pass
#
#     def is_fkey(self, field_name):#todo
#         pass
#
#     def is_onetoone(self, field_name):#todo
#         pass
#
#     def create_serializer(self, model, model_fields, extra_fields):#todo
#         s_meta = type('Meta', {'fields': model_fields})
#         serializer_attrs = {}
#         serializer_attrs.update({'Meta': s_meta})
#         s_serializer = type(model.__name__+'Serializer', (ModelSerializer,), serializer_attrs)
#         return s_serializer
#
#     def modelserializer_factory(self, model):
#         all_fields = model._meta.get_all_field_names()
#         forward_1to1 = {}
#         reverse_1to1 = {}
#         for field in all_fields:
#             model
