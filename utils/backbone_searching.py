from rest_framework.response import Response
import re, copy
from utils.rest_utils import model_serializer_factory
from custom.constants import TRY_FIELDS
from custom.regexes import DATE_REGEXES
from middlewares.global_request import get_current_request
from django.utils.datastructures import MultiValueDictKeyError
import datetime


def search_view(model_requested, queryset=None):

    request = get_current_request()
    request_dict = copy.deepcopy(request.POST)
    print request_dict
    try:
        query = request_dict['query']
        del request_dict['query']
    except MultiValueDictKeyError:
        query = None
    try:
        in_field = request_dict['in_field']
        del request_dict['in_field']
    except MultiValueDictKeyError:
        in_field = None
    kwargs = {}
    for item in request_dict:
        key = item
        if 'id' in key:
            try:
                value = int(request_dict[item])
            except ValueError:
                value = request_dict[item]
        elif any(re.search(r[0], request_dict[item]) for r in DATE_REGEXES):
            for r in DATE_REGEXES:
                if re.search(r[0], request_dict[item]):
                    value = datetime.datetime.strptime(request_dict[item], r[1]).date()
                    break
        else:
            value = request_dict[item]
        kwargs[key] = value

    result = []

    temp_universal_search = search_in_model(model_requested, query, in_field, queryset, **kwargs)
    result += temp_universal_search[0]

    print temp_universal_search[1]
    if result:
        model_requested = type(result[0])
        serializerClass = model_serializer_factory(model_requested)
        many = (len(result) > 1)
        serializer = serializerClass(result if many else result[0], many=many)
        return Response(serializer.data)
    else:
        return Response({})

def search_in_model(model_requested, query, in_field=None, queryset=None, **kwargs):

    if query:
        query = re.sub('&', ' ', query)
        query = re.sub(',', ' ', query)
        query = re.sub('-', ' ', query)
        query = unicode(query).lower()
    result = []
    string_result = []
    if queryset:
        if query:
            if in_field:
                temp_kwargs = copy.deepcopy(kwargs)
                temp_kwargs[in_field] = query
                temp_result = queryset.filter(**temp_kwargs)
                if temp_result:
                    result += temp_result
                    string_result.append((in_field, query))
            else:
                try:
                    try_fields = TRY_FIELDS[model_requested.__name__.lower()]
                except KeyError:
                    raise KeyError('You have not specified try_fields for this model in custom.constants.py')
                for field in try_fields:
                    temp_kwargs = copy.deepcopy(kwargs)
                    temp_kwargs[field + '__icontains'] = query
                    temp_result = queryset.filter(**temp_kwargs)
                    if temp_result:
                        result += temp_result
                        string_result.append((field, query))
        else:
            result += queryset.filter(**kwargs)
    return [result, string_result]