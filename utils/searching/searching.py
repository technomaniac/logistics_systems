from django.db.models.query import QuerySet
from django.http.request import HttpRequest, QueryDict
from utils.common_functions import convert_request_querydict_to_dict
from django.db import models
from utils.searching.contants import SEARCHING_CONTANTS
from utils.searching.lookups import get_lookup_registry
from utils.time_funcs import str_to_time
from django.db.models.sql.constants import QUERY_TERMS
from django.db.models.loading import get_models

RELATED_FIELD_TYPES = ['OneToOneField', 'ForeignKey', 'AutoUserOneToOneKey']

# QUERY_TERMS.pop('in')
# QUERY_TERMS.pop('range')
def get_model(model_name):
    model_list = get_models()
    for model in model_list:
        if model.__name__.lower() == model_name.lower():
            return model


def get_related_model(model, related_field_name):
    related_model = None
    if model._meta.get_field_by_name(related_field_name)[0].__class__.__name__ == 'RelatedObject':
        related_model = model._meta.get_field_by_name(related_field_name)[0].model
    else:
        related_model = model._meta.get_field_by_name(related_field_name)[0].related.parent_model
    return related_model


class BaseCleanRequestArguments(object):

    def normalize_kwarg_field_name(self, name):
        if name == 'pk':
            return 'id'
        return name

    def get_field_formatter(self, model, field_name):
        internal_type = ''
        try:
            internal_type = model._meta.get_field_by_name(field_name)[0].get_internal_type()
        except:
            internal_type = model._meta.get_field_by_name(field_name)[0].__class__.__name__
        return getattr(self, 'format_' + internal_type)

    def _format_core(self, param, field_name, kwarg_key, model, func):
        print 'in format core'
        print type(kwarg_key), 'type of kwarg_key'
        print kwarg_key.endswith('in'), 'kwargs_key has in '
        if isinstance(param, (str, unicode)):
            if kwarg_key.endswith('in') or kwarg_key.endswith('range'):
                print map(func, param.strip().split(','))
                return map(func, param.strip().split(','))
        else:
            return param
        return func(param.strip())

    def format_PositiveIntegerField(self, param, field_name, kwarg_key, model):
        return self._format_core(param, field_name, kwarg_key, model, int)

    def format_IntegerField(self, param, field_name, kwarg_key, model):
        return self._format_core(param, field_name, kwarg_key, model, int)

    def format_FloatField(self, param, field_name, kwarg_key, model):
        return self._format_core(param, field_name, kwarg_key, model, float)

    def format_DateField(self, param, field_name, kwarg_key, model):
        return self._format_core(param, field_name, kwarg_key, model, str_to_time)

    def format_DateTimeField(self, param, field_name, kwarg_key, model):
        return self._format_core(param, field_name, kwarg_key, model, str_to_time)

    def format_BooleanField(self, param, field_name, kwarg_key, model):
        if 'true' == param.lower():
            return True
        else:
            return False

    def format_CharField(self, param, field_name, kwarg_key, model):
        return self._format_core(param, field_name, kwarg_key, model, str)

    def format_AutoField(self, param, field_name, kwarg_key, model):
        return self.format_PositiveIntegerField(param, field_name, kwarg_key, model)

    def _format_core_related(self, param, field_name, kwarg_key, model):
        broken = kwarg_key.split('__')
        len_to_comp = 1
        indx = -1
        if broken[-1] in QUERY_TERMS:
            len_to_comp += 1
            indx = -2
        if broken[indx] in model._meta.get_all_field_names() and len(broken) == len_to_comp:
            if model._meta.get_field_by_name(broken[indx])[0].get_internal_type() in RELATED_FIELD_TYPES:
                return self.format_PositiveIntegerField(param, field_name, kwarg_key, model)
            format_func = self.get_field_formatter(model, broken[indx])
            return format_func(param, field_name, kwarg_key, model)
        else:
            next_model = get_related_model(model, broken[0])
            return self._format_core_related(param, field_name, '__'.join(broken[1:]), next_model)


class SimpleCleanRequestArguments(BaseCleanRequestArguments):#todo not the best design, in future write a decorator to add functionality of adding formaters

    def __init__(self, model):
        self.model = model

    def format_AutoUserOneToOneKey(self, param, field_name, kwarg_key, model):
        return self._format_core_related(param, field_name, kwarg_key, model)

    def format_OneToOneField(self,  param, field_name, kwarg_key, model):
        return self._format_core_related(param, field_name, kwarg_key, model)

    def format_ForeignKey(self, param, field_name, kwarg_key, model):
        return self._format_core_related(param, field_name, kwarg_key, model=model)

    def format_RelatedObject(self, param, field_name, kwarg_key, model):
        return self._format_core_related(param, field_name, kwarg_key, model)

    def format_ManyToMany(self, param, field_name, kwarg_key, model):
        return self._format_core_related(param, field_name, kwarg_key, model)


class SearchGetAttrs(object):
    model = None

    def __init__(self, get_dict, model=None):
        try:
            if self._cache:
                raise TypeError('Instance is already bound to a query string')
        except AttributeError:
            pass
        if model:
            self.model = model
        self._cache = {}
        # print self.model, 'model'
        # if not isinstance(self.model, models.Model):
        #     raise TypeError('Invalid model type')
        if isinstance(get_dict, QueryDict):
            self.get_querydict = convert_request_querydict_to_dict(get_dict.copy())#todo only used because its tested.
        elif isinstance(get_dict, dict):
            self.get_querydict = convert_request_querydict_to_dict(get_dict)
        self.field_names = self.model._meta.get_all_field_names()
        self.queryset_kwargs = list()
        self.common_fields = list()
        for get_key in self.get_querydict.keys():
            broken = get_key.split('__')
            if get_key.startswith('querysetlookup'):
                self.queryset_kwargs.append(get_key)
                self.common_fields.append(get_key)
            if broken[0] in self.field_names:
                self.queryset_kwargs.append(get_key)
                self.common_fields.append(broken[0])
        # self.common_fields = set(self.field_names).intersection(set(self.get_querydict.keys()))
        if not self.queryset_kwargs:
            raise TypeError('Invalid querystring')

    def get_query_set(self, formatter=None, request=None):
        special_queryset = None
        if not formatter:
            formatter = SimpleCleanRequestArguments(model=self.model)
        if self._cache:
            return self._cache

        final_queryset_dict = {}
        for field_name, kueryset_kwarg in zip(self.common_fields, self.queryset_kwargs):
            if kueryset_kwarg.startswith('querysetlookup'):
                lookup = kueryset_kwarg.split('__')[1]
                lookup_registry = get_lookup_registry()
                get_model_register = lookup_registry.get_model_register(model=self.model)
                funct, the_type = get_model_register.get_registry().get(lookup)
                if not special_queryset:
                    if the_type == SEARCHING_CONTANTS.IS_MANAGER:
                        special_queryset = funct(request=request, query=self.get_querydict[kueryset_kwarg])
                    else:
                        special_queryset = funct(request=request, model=self.model,
                                                 query=self.get_querydict[kueryset_kwarg])
                else:
                    if the_type == SEARCHING_CONTANTS.IS_MANAGER:
                        special_queryset = special_queryset & funct(request=request,
                                                                    query=self.get_querydict[kueryset_kwarg])
                    else:
                        special_queryset = special_queryset & funct(request=request, model=self.model,
                                                                    query=self.get_querydict[kueryset_kwarg])
                continue
            format_field = formatter.get_field_formatter(self.model, field_name)
            if callable(format_field):
                final_queryset_dict[kueryset_kwarg] = format_field(self.get_querydict[kueryset_kwarg], field_name, kueryset_kwarg, self.model)
            else:
                final_queryset_dict[kueryset_kwarg] = self.get_querydict[kueryset_kwarg]
        if special_queryset != None and final_queryset_dict:
            self._cache = self.model.objects.filter(**final_queryset_dict)
            self._cache = self._cache & special_queryset
        elif special_queryset != None:
            self._cache = special_queryset
        else:
            self._cache = self.model.objects.filter(**final_queryset_dict)
        return self._cache
