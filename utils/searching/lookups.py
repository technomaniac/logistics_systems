from utils.searching.contants import SEARCHING_CONTANTS


class CommonModelLookupRegDs(object):

    def __init__(self, model):
        self.model = model
        self.the_dict = {}

    def add(self, name, func):
        self.the_dict[name] = func

    def remove(self, name):
        del self.the_dict[name]

    def get(self, name):
        try:
            return self.the_dict[name], None
        except:
            return getattr(self.model._default_manager, name), SEARCHING_CONTANTS.IS_MANAGER

    def get_as_dict(self):
        return self.the_dict


class LookupRegistry(object):
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(LookupRegistry, cls).__new__(
                                cls, *args, **kwargs)
        return cls._instance

    def register_model(self, model):
        if not getattr(self, model.__name__, None):
            setattr(self, model.__name__, ModelRegister(model=model))

    def get_model_register(self, model=None, model_name=''):
        if model_name:
            return getattr(self, model_name)
        elif model:
            return getattr(self, model.__name__)
        else:
            raise Exception('No such registry')


class ModelRegister(object):

    def __init__(self, model, registry_ds=CommonModelLookupRegDs):
        self.model = model
        self.registry_ds = registry_ds(model=model)

    def enable(self, lookup_name, func=None, manager_name=''):
        if manager_name:
            manager = getattr(self.model, manager_name)
            self.registry_ds.add(lookup_name, getattr(manager, lookup_name))
        if callable(func):
            self.registry_ds.add(lookup_name, func)

    def disable(self, lookup_name):
        try:
            self.registry_ds.remove(lookup_name)
        except AttributeError:
            raise AttributeError('No such lookup!!')

    def get_registry(self):
        return self.registry_ds


def get_lookup_registry():
    return LookupRegistry()