from django.test import TestCase
from utils.searching.lookups import get_lookup_registry


class LookupTest(TestCase):

    def test_lookup(self):
        from apps.vendors.models import *
        registry = get_lookup_registry()
        registry.register_model(Vendor)
        registry.register_model(VendorContact)
        model_reg = registry.get_model_register(VendorContact)
        def goo():
            print 1, 'hehehehe'
        model_reg.enable('bar', goo)
        regds = model_reg.get_registry()
        print getattr(regds, 'bar')()