def get_model_attribute_by_string(string, model_instance):
    attribute_list = string.split('__')
    temp = model_instance
    for item in attribute_list:
        temp = getattr(temp, item)
        if not temp:
            return 'NA'
    try:
        return temp()
    except TypeError:
        return temp

