from datetime import datetime, timedelta

from awb.models import AWB, AWB_History, AWB_Status
from internal.models import Branch
from report.client_report import PerformanceReport
from utils.common import get_percentage
from utils.constants import CALLING_SUMMARY_REPORT, CALLING_CANCELLED_CONNECTED_REPORT, CALLING_PENDING_AND_DEFER_REPORT
from zoning.models import City


class CSReport(object):
    def __init__(self, client, duration, holidays=(), report_type=None, branch=None, city=None, category=None):
        self.client = client
        self.category = category
        self.duration = int(duration)
        self.holidays = list(holidays)
        self.header = None
        self.date_format = '%Y-%m-%d'
        self.date_list = self.get_date_list()
        self.date_range = self.get_date_range()
        self.start_date = None
        self.end_date = None
        self.cancelled_status__include = ['CAN']
        self.set_header()
        self.branch = None
        if branch is None:
            self.branch_list = Branch.objects.exclude(branch_name='HQ').defer('id')
        else:
            self.branch_list = Branch.objects.filter(pk__in=branch).defer('id')
        if city is None:
            self.city_list = City.objects.all().defer('id')
            self.city_list = [c for c in self.city_list if c.branch_set.exists()]
        else:
            self.city_list = City.objects.filter(pk__in=city).defer('id')
        self.cs_updated__include = ['CS']
        self.call = True
        self.connected_call = True
        self.unconnected_call = False
        self.customer_does_not_return__include = ['Customer does not want to return',
                                                  'Pickup already done by self/other courier']
        self.schedule_call__include = ['SCH']
        self.long_defer__include = ['Customer deferring more than 3 days']
        self.customer_not_reachable__include = ['Couldn\'t speak to customer']
        self.misc_calls__include = ['Customer wants replacement/refund first', 'Other']
        self.total_pending_defer__include = ['Customer deferring more than 3 days',
                                             'Customer wants replacement/refund first',
                                             'Pickup already done by self/other courier',
                                             'Couldn\'t speak to customer', ]
        self.cs_status__exclude = ['CAN', 'SCH']
        self.open_case_remark__include = ['Incorrect update : by branch', 'Defer : Customer Initiated',
                                          'Dispatched / Pickedup already',
                                          'Confirmed Cancel', 'Couldn\'t speak to customer']
        self.pending_remark__include = ['Incorrect update : by branch']
        self.defer_remark__include = ['Defer : Customer Initiated']
        self.pickedup_remark__include = ['Dispatched / Pickedup already']
        self.confirmed_cancel_remark__include = ['Confirmed Cancel']
        self.total_cancel_remark__include = ['Customer does not want to return',
                                             'Pickup already done by self/other courier',
                                             'Customer deferring more than 3 days', 'Couldn\'t speak to customer',
                                             'Customer wants replacement/refund first', 'Other',]

    @property
    def start_date(self):
        return self._start_date + ' 00:00:00'

    @start_date.setter
    def start_date(self, value):
        self._start_date = value

    @property
    def end_date(self):
        return self._end_date + ' 23:59:59'

    @end_date.setter
    def end_date(self, value):
        self._end_date = value

    def generate_report(self):
        report_dict = {}
        report_dict['header'] = self.get_report_header()
        report_dict['header'].insert(0, 'Date')
        report_dict['report'] = {}
        # iterator = self.get_iterator()
        #for i in xrange(len(iterator)):
        self.start_date, self.end_date = self.get_date_range()
        # print self.start_date, self.end_date
        # if self.report_type == 'city':
        #     self.city = iterator[i]
        # else:
        #     self.branch = iterator[i]
        report_dict['report'] = {}
        report_dict['report']['total'] = self.get_report()
        #report_dict['report'][i]['total'].insert(0, iterator[i])
        for d in xrange(len(self.date_list)):
            self.start_date = self.date_list[d]
            self.end_date = self.date_list[d]
            report_dict['report'][d] = self.get_report()
            report_dict['report'][d].insert(0, self.date_list[d])
        # print report_dict
        return report_dict

    def get_dict(self):
        filter = {}
        if self.client is not None:
            filter['awb_status__manifest__client'] = self.client
        if self.category is not None:
            filter['category'] = self.category
        # if self.report_type is not None:
        #     if self.report_type == 'city':
        #         filter['pincode__branch_pincode__branch__city'] = self.city
        #     if self.report_type == 'branch':
        #         filter['pincode__branch_pincode__branch'] = self.branch
        filter['awb_history__on_update__range'] = (self.start_date, self.end_date)
        # print filter
        return filter

    def get_report(self):
        report_list = []
        for k, v in self.header:
            if k.lower() != 'rpi date':
                if hasattr(self, v):
                    report_list.append(getattr(self, v)())
        return report_list

    def get_report_header(self):
        report_header = []
        for k, v in self.header:
            if hasattr(self, v):
                report_header.append(k)
        return report_header

    def get_date_list(self):
        base = datetime.today() - timedelta(days=1)
        return [(base - timedelta(days=x)).strftime(self.date_format) for x in range(0, self.duration) if
                (base - timedelta(days=x)).weekday() != 6 and (base - timedelta(days=x)) not in self.holidays]

    def get_date_range(self):
        return self.get_date_list()[-1], self.get_date_list()[0]

    def get_current_date(self):
        return datetime.today().strftime('%Y-%m-%d')

    def get_date_diff(self, a, b):
        diff = datetime.strptime(a, self.date_format + ' %H:%M:%S') - datetime.strptime(b,
                                                                                        self.date_format + ' %H:%M:%S')
        return abs(diff.days)


class Cs_Report_Summary(CSReport):
    def set_header(self):
        self.header = CALLING_SUMMARY_REPORT


    def get_total_calls(self):
        filter = self.get_dict()
        filter['awb_history__call'] = self.call
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.calls = AWB.objects.filter(**filter)
        # print self.calls
        self.total_calls_count = self.calls.count()
        return self.total_calls_count

    def get_connected_call(self):
        filter = self.get_dict()
        filter['awb_history__connected_call'] = self.connected_call
        filter['awb_history__call'] = self.call
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.connected_calls = AWB.objects.filter(**filter)
        self.total_connected_calls_count = self.connected_calls.count()
        return self.total_connected_calls_count

    def get_connected_call_perc(self):
        filter = self.get_dict()
        filter['awb_history__connected_call'] = self.connected_call
        filter['awb_history__call'] = self.call
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.connected_calls = AWB.objects.filter(**filter)
        # print self.connected_calls
        self.total_connected_calls_count = self.connected_calls.count()
        return get_percentage(self.total_connected_calls_count, self.total_calls_count)

    def get_unconnected_call_perc(self):
        self.total_unconnected_calls_count = self.total_calls_count - self.total_connected_calls_count
        return get_percentage(self.total_unconnected_calls_count, self.total_calls_count)


        # def get_total_cases_pending_defer(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_history__remark__in'] = self.total_pending_defer__include
        #     self.total_cases_pending_defer = AWB.objects.filter(**filter)
        #     self.total_cases_pending_defer_count = self.total_cases_pending_defer.count()
        #     return self.total_cases_pending_defer_count

        # def get_customer_pending_perc(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_history__remark__in'] =


class cs_Cancellation_Report(Cs_Report_Summary):
    def set_header(self):
        self.header = CALLING_CANCELLED_CONNECTED_REPORT

    def get_total_cancellations(self):
        filter = self.get_dict()
        filter['awb_history__remark__in'] = self.total_cancel_remark__include
        filter['awb_history__status__in'] = self.cancelled_status__include
        filter['awb_history__connected_call'] = self.connected_call
        filter['awb_history__call'] = self.call
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.total_cancel = AWB.objects.filter(**filter)
        # print self.total_cancel , 'total_cancel'
        self.total_cancel_count = self.total_cancel.count()
        return self.total_cancel_count

    def get_customer_not_return_perc(self):
        filter = self.get_dict()
        filter['awb_history__remark__in'] = self.customer_does_not_return__include
        filter['awb_history__status__in'] = self.cancelled_status__include
        filter['awb_history__connected_call'] = self.connected_call
        filter['awb_history__call'] = self.call
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.customer_does_not_return = AWB.objects.filter(**filter)
        # print self.customer_does_not_return , 'customer_does_not_return'
        self.customer_does_not_return_count = self.customer_does_not_return.count()
        # print self.customer_does_not_return_count , 'a'
        return get_percentage(self.customer_does_not_return_count, self.total_cancel_count)

    def get_long_defer_perc(self):
        filter = self.get_dict()
        filter['awb_history__remark__in'] = self.long_defer__include
        filter['awb_history__status__in'] = self.cancelled_status__include
        filter['awb_history__connected_call'] = self.connected_call
        filter['awb_history__call'] = self.call
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.long_defer = AWB.objects.filter(**filter)
        # print self.long_defer , 'long_defer'
        self.long_defer_count = self.long_defer.count()
        # print self.long_defer_count , 'b'
        return get_percentage(self.long_defer_count, self.total_cancel_count)

    def get_customer_not_reachable_perc(self):
        filter = self.get_dict()
        filter['awb_history__remark__in'] = self.customer_not_reachable__include
        filter['awb_history__connected_call'] = self.connected_call
        filter['awb_history__status__in'] = self.cancelled_status__include
        filter['awb_history__call'] = self.call
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.customer_not_reachable = AWB.objects.filter(**filter)
        # print self.customer_not_reachable , 'customer_not_reachable'
        self.customer_not_reachable_count = self.customer_not_reachable.count()
        # print self.customer_not_reachable_count , 'c'
        return get_percentage(self.customer_not_reachable_count, self.total_cancel_count)

    def get_misc_call_perc(self):
        filter = self.get_dict()
        filter['awb_history__remark__in'] = self.misc_calls__include
        filter['awb_history__connected_call'] = self.connected_call
        filter['awb_history__call'] = self.call
        filter['awb_history__status__in'] = self.cancelled_status__include
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.misc_call = AWB.objects.filter(**filter)
        # print self.misc_call , 'misc_call'
        self.misc_call_count = self.misc_call.count()
        # print self.misc_call_count , 'd'
        return get_percentage(self.misc_call_count, self.total_cancel_count)


class Pending_Defer_Calling(cs_Cancellation_Report):
    def set_header(self):
        # if self.client.category == 'RL':
        self.header = CALLING_PENDING_AND_DEFER_REPORT
        # else:
        #     self.header = {}

    def get_total_cases_pending_defer(self):
        exclude = {}
        filter = self.get_dict()
        exclude['awb_history__status__in'] = self.cs_status__exclude
        filter['awb_history__call'] = self.call
        filter['awb_history__remark__in'] = self.open_case_remark__include
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        # filter['awb_status__connected_call'] = self.connected_call
        # print exclude
        self.total_cases_pending_defer = AWB.objects.filter(**filter).exclude(**exclude)
        # print self.total_cases_pending_defer
        self.total_cases_pending_defer_count = self.total_cases_pending_defer.count()
        # print  self.total_cases_pending_defer_count , 'aa'
        return self.total_cases_pending_defer_count

    def get_customer_pending_perc(self):
        exclude = {}
        filter = self.get_dict()
        exclude['awb_history__status__in'] = self.cs_status__exclude
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        filter['awb_history__remark__in'] = self.pending_remark__include
        filter['awb_history__call'] = self.call
        # filter['awb_status__connected_call'] = self.connected_call
        self.customer_pending = AWB.objects.filter(**filter).exclude(**exclude)
        self.customer_pending_count = self.customer_pending.count()
        # print self.customer_pending_count , 'bb'
        return get_percentage(self.customer_pending_count, self.total_cases_pending_defer_count)

    def get_customer_defer_perc(self):
        exclude = {}
        filter = self.get_dict()
        exclude['awb_history__status__in'] = self.cs_status__exclude
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        filter['awb_history__remark__in'] = self.defer_remark__include
        filter['awb_history__call'] = self.call
        # filter['awb_status__connected_call'] = self.connected_call
        self.customer_defer = AWB.objects.filter(**filter).exclude(**exclude)
        self.customer_defer_count = self.customer_defer.count()
        # print self.customer_defer_count , 'cc'
        return get_percentage(self.customer_defer_count, self.total_cases_pending_defer_count)

    def get_pickedup_already_perc(self):
        exclude = {}
        filter = self.get_dict()
        exclude['awb_history__status__in'] = self.cs_status__exclude
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        filter['awb_history__remark__in'] = self.pickedup_remark__include
        filter['awb_history__call'] = self.call
        # filter['awb_status__connected_call'] = self.connected_call
        self.pickedup_already = AWB.objects.filter(**filter).exclude(**exclude)
        self.pickedup_already_count = self.pickedup_already.count()
        # print self.pickedup_already_count , 'dd'
        return get_percentage(self.pickedup_already_count, self.total_cases_pending_defer_count)

    def get_confirmed_cancel_perc(self):
        exclude = {}
        filter = self.get_dict()
        exclude['awb_history__status__in'] = self.cs_status__exclude
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        filter['awb_history__remark__in'] = self.confirmed_cancel_remark__include
        filter['awb_history__call'] = self.call
        # filter['awb_status__connected_call'] = self.connected_call
        self.confirmed_cancel = AWB.objects.filter(**filter).exclude(**exclude)
        self.confirmed_cancel_count = self.confirmed_cancel.count()
        # print self.confirmed_cancel_count , 'ee'
        return get_percentage(self.confirmed_cancel_count, self.total_cases_pending_defer_count)

    def get_customer_pending_not_reachable_perc(self):
        exclude = {}
        filter = self.get_dict()
        filter['awb_history__remark__in'] = self.customer_not_reachable__include
        exclude['awb_history__status__in'] = self.cs_status__exclude
        filter['awb_history__call'] = self.call
        # filter['awb_status__connected_call'] = self.unconnected_call
        filter['awb_history__updated_by__profile__role__in'] = self.cs_updated__include
        self.customer_not_reachable = AWB.objects.filter(**filter).exclude(**exclude)
        # print self.customer_not_reachable
        self.customer_not_reachable_count = self.customer_not_reachable.count()
        # print self.customer_not_reachable_count , 'ff'
        return get_percentage(self.customer_not_reachable_count, self.total_cases_pending_defer_count)





