import json

from django.contrib.auth import get_user
from django.db.models import get_model
from django.http.response import HttpResponse
from django.views.generic.base import View

from custom.custom_mixins import GeneralLoginRequiredMixin, CommonViewMixin
from report.socket import SocketHandler
from report.tasks import advance_mis_builder
from utils.constants import AWB_STATUS


class AdvanceReport(GeneralLoginRequiredMixin, CommonViewMixin, View):
    template_name = 'advance_report.html'

    def get_context(self, request, *args, **kwargs):
        context = dict()
        context['status'] = AWB_STATUS
        return context

    def ajax_post(self, request, *args, **kwargs):
        user = get_user(request)
        socket = SocketHandler(facility='mis', broadcast=True)

        socket.send_message_to_socket(message='File generation will be initiated shortly ...', type='success')

        advance_mis_builder.delay(request.POST, str(user.username))
        return HttpResponse()


def autosuggest(request):
    model = get_model(request.GET['app_name'], request.GET['model_name'])
    query_string = request.GET['query_string']
    filter = dict()
    filter[query_string + '__icontains'] = request.GET['term']
    queryset = model.objects.filter(**filter).order_by(query_string)
    result = [{'id': i.pk, 'label': getattr(i, query_string), 'value': i.pk} for i in queryset]
    return HttpResponse(json.dumps(result))
