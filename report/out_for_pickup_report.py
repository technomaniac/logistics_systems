from datetime import datetime, timedelta
from awb.models import AWB
from internal.models import Branch
from utils.common import get_percentage
from utils.constants import DASBOARD_REPORT_HEADER
from zoning.models import City

__author__ = 'naveen'

class OutForPickup(object):
    def __init__(self, client, duration, holidays=(), report_type=None, branch=None, city=None, category=None):
        self.client = client
        self.category = category
        self.duration = int(duration)
        self.holidays = list(holidays)
        self.header = None
        self.date_format = '%Y-%m-%d'
        self.date_list = self.get_date_list()
        self.date_range = self.get_date_range()
        self.start_date = None
        self.end_date = None
        self.set_header()
        self.report_type = report_type
        self.branch = None
        self.city = None
        if branch is None:
            self.branch_list = Branch.objects.exclude(branch_name='HQ').defer('id')
        else:
            self.branch_list = Branch.objects.filter(pk__in=branch).defer('id')
        if city is None:
            self.city_list = City.objects.all().defer('id')
            self.city_list = [c for c in self.city_list if c.branch_set.exists()]
        else:
            self.city_list = City.objects.filter(pk__in=city).defer('id')
        self.assign_awb_status__include = ['DR']
        self.out_for_pickup__exclude = ['DR']

    @property
    def start_date(self):
        return self._start_date + ' 00:00:00'

    @start_date.setter
    def start_date(self, value):
        self._start_date = value

    @property
    def end_date(self):
        return self._end_date + ' 11:30:00'

    @end_date.setter
    def end_date(self, value):
        self._end_date = value

    def generate_report(self):
        report_dict = {}
        self.start_date, self.end_date = self.get_date_range()
        report_dict['header'] = self.get_report_header()
        report_dict['header'].insert(0, 'City')
        report_dict['report'] = {}
        iterator = self.get_iterator()
        report_dict['report']['total'] = self.get_report()
        for i in xrange(len(iterator)):

            #print self.start_date, self.end_date
            if self.report_type == 'city':
                self.city = iterator[i]
                # report_dict['report'][i] = {}
                # report_dict['report'][i]['total'] = self.get_report()
                # report_dict['report'][i]['total'].insert(0, iterator[i])
                #print self.date_list
                # for d in xrange(len(self.date_list)):
                #     self.start_date = self.date_list[d]
                #     self.end_date = self.date_list[d]
                #     #print self.start_date, self.end_date
                #     report_dict['report'][i][d] = self.get_report()
                #     report_dict['report'][i][d].insert(0, self.date_list[d])
            else:
                self.branch = iterator[i]
            report_dict['report'][iterator[i]] = {}
            report_dict['report'][iterator[i]] = self.get_report()
            #report_dict['report'][i]['total'].insert(0, iterator[i])
            # for d in xrange(len(self.date_list)):
            #     self.start_date = self.date_list[d]
            #     self.end_date = self.date_list[d]
            #     report_dict['report'][i][d] = self.get_report()
            #     report_dict['report'][i][d].insert(0, self.date_list[d])

        return report_dict

    def get_dict(self):
        filter = {}
        if self.client is not None:
            filter['awb_status__manifest__client'] = self.client
        if self.category is not None:
            filter['category'] = self.category
        if self.report_type is not None:
            if self.report_type == 'city' and self.city:
                filter['pincode__branch_pincode__branch__city'] = self.city
            if self.report_type == 'branch':
                filter['pincode__branch_pincode__branch'] = self.branch
        filter['awb_history__on_update__range'] = (self.start_date, self.end_date)
        # print filter
        return filter

    def get_report(self):
        report_list = []
        for k, v in self.header:
            if k.lower() != 'rpi date':
                if hasattr(self, v):
                    report_list.append(getattr(self, v)())
        return report_list

    def get_report_header(self):
        report_header = []
        for k, v in self.header:
            if hasattr(self, v):
                report_header.append(k)
        return report_header

    def get_iterator(self):
        if self.report_type == 'city':
            return self.city_list
        else:
            return self.branch_list

    def get_date_list(self):
        base = datetime.today()
        return [(base - timedelta(days=x)).strftime(self.date_format) for x in range(0, self.duration) if
                (base - timedelta(days=x)).weekday() != 6 and (base - timedelta(days=x)) not in self.holidays]

    def get_date_range(self):
        return self.get_date_list()[-1], self.get_date_list()[0]

    def get_current_date(self):
        return datetime.today().strftime('%Y-%m-%d')

    def get_date_diff(self, a, b):
        diff = datetime.strptime(a, self.date_format + ' %H:%M:%S') - \
               datetime.strptime(b, self.date_format + ' %H:%M:%S')
        return abs(diff.days)

class OutForPickupSummary(OutForPickup):
    def set_header(self):
        self.header = DASBOARD_REPORT_HEADER

    def get_total_awb_assigned(self):
        filter = self.get_dict()
        filter['awb_history__status__in'] = self.assign_awb_status__include
        self.awb_assign = AWB.objects.filter(**filter)
        # print self.awb_assign
        self.awb_assign_count = self.awb_assign.count()
        # print self.awb_assign_count ,'awb'
        return self.awb_assign_count

    def get_total_out_for_pickup(self):
        exclude = self.get_dict()
        filter = self.get_dict()
        filter['awb_history__status__in'] = self.assign_awb_status__include
        exclude['awb_status__status__in'] = self.out_for_pickup__exclude
        self.out_for_pickup_awb = AWB.objects.filter(**filter).exclude(**exclude)
        # print self.out_for_pickup_awb
        self.out_for_pickup_awb_count = self.out_for_pickup_awb.count()
        # print self.out_for_pickup_awb_count ,'out'
        return self.out_for_pickup_awb_count

    def get_total_out_for_pickup_perc(self):
        return get_percentage(self.out_for_pickup_awb_count , self.awb_assign_count)

