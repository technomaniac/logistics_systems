from custom.custom_mixins import SetDateRange
from transit.models import TB
from internal.models import Branch
from zoning.models import City
from utils.constants import TB_REPORT_HEADER


class TBReport(SetDateRange):
    def __init__(self, *args, **kwargs):
        if 'duration' in kwargs:
            self.set_date_range(**kwargs)
        else:
            self.start_date = kwargs['start_date']
            self.end_date = kwargs['end_date']
        self.origin_branch = None
        if 'origin_branch' in kwargs:
            self.origin_branch = Branch.objects.get(pk=kwargs['origin_branch'])
        self.delivery_branch = None
        if 'delivery_branch' in kwargs:
            self.delivery_branch = Branch.objects.get(pk=kwargs['delivery_branch'])
        self.origin_city = None
        if 'origin_city' in kwargs:
            self.origin_city = City.objects.get(pk=kwargs['origin_city'])
        self.delivery_city = None
        if 'delivery_city' in kwargs:
            self.delivery_city = City.objects.get(pk=kwargs['delivery_city'])
        self.header = TB_REPORT_HEADER


    def get_report_header(self):
        tb = TB()
        report_header = []
        for k, v in self.header:
            if hasattr(tb, v):
                report_header.append(k)
        return report_header

    def get_filter_dict(self):
        filter = {}
        filter['tb_history__mts__creation_date__range'] = (self.start_date, self.end_date)
        if self.origin_branch:
            filter['origin_branch'] = self.origin_branch
        if self.delivery_branch:
            filter['delivery_branch'] = self.delivery_branch
        if self.origin_city:
            filter['origin_branch__city'] = self.origin_city
        if self.delivery_city:
            filter['delivery_branch__city'] = self.delivery_city
        return filter

    def get_tbs(self):
        return TB.objects.filter(**self.get_filter_dict()).order_by('-creation_date', 'origin_branch')

    def get_report(self):
        report_list = []
        for tb in self.get_tbs():
            row = []
            for k, v in self.header:
                attr = getattr(tb, v)
                if callable(attr):
                    row.append(attr())
            report_list.append(row)
        return report_list






