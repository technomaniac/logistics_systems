from django.db import models

from awb.models import AWB, AWB_History
from report.base import ReportMixin
from utils.common import get_percentage


class PerformanceReportMixin(ReportMixin):
    def get_custom_report(self, *args):
        #print kwargs
        #print date_list
        report_dict = dict()
        for branch in self.branch_list:
            self.start_date = self.date_list[-1] + ' 00:00:00'
            self.end_date = self.date_list[0] + ' 23:59:59'
            self.pickup_branch = branch
            report_dict[branch.branch_name] = dict()
            if len(self.date_list) > 1:
                report_dict[branch.branch_name]['total'] = {attr: getattr(self, attr)() for attr in args if
                                                            hasattr(self, attr)}
                report_dict[branch.branch_name]['total'].update({'duration': len(self.date_list)})
            for i in xrange(len(self.date_list)):
                report_dict[branch.branch_name][i] = dict()
                self.start_date = self.date_list[i] + ' 00:00:00'
                self.end_date = self.date_list[i] + ' 23:59:59'
                report_dict[branch.branch_name][i] = {attr: getattr(self, attr)() for attr in args if
                                                      hasattr(self, attr)}
                report_dict[branch.branch_name][i].update({'date': self.date_list[i]})
        #print report_dict
        return report_dict

    def get_total_awb(self):
        filter = self.get_filter_dict()
        # print filter
        self.total_awbs = AWB.objects.filter(**filter).prefetch_related('awb_status')
        self.total_awbs_count = self.total_awbs.count()
        return self.total_awbs

    def get_deferred_awb(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.deferred_status__include
        self.deferred_awbs = self.total_awbs.filter(**filter)
        return self.deferred_awbs

    def get_deferred_awb_perc(self):
        try:
            return get_percentage(self.deferred_awbs.count(), self.total_awbs_count)
        except:
            return get_percentage(self.get_deferred_awb().count(), self.total_awbs_count)

    def get_pickup_awb(self):
        filter = self.get_filter_dict()
        exclude = {}
        exclude['awb_status__status__in'] = self.picked_up_status__exclude
        self.pickup_awbs = AWB.objects.filter(**filter).exclude(**exclude)
        return self.pickup_awbs


    def get_pickup_awb_perc(self):
        try:
            return get_percentage(self.pickup_awbs.count(), self.total_awbs_count)
        except:
            return get_percentage(self.get_pickup_awb().count(), self.total_awbs_count)

    def get_cancelled_awb(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.cancelled_status__include
        self.cancelled_awbs = AWB.objects.filter(**filter)
        return self.cancelled_awbs

    def get_cancelled_awb_perc(self):
        try:
            return get_percentage(self.cancelled_awbs.count(), self.total_awbs_count)
        except:
            return get_percentage(self.get_cancelled_awb().count(), self.total_awbs_count)

    def get_cancelled_conf_awb(self):
        filter = self.get_filter_dict()
        filter['awb_status__updated_by__profile__role'] = 'CS'
        self.cancelled_conf_awbs = self.cancelled_awbs.filter(**filter)
        return self.cancelled_conf_awbs

    def get_cancelled_conf_awb_perc(self):
        try:
            return get_percentage(self.cancelled_conf_awbs.count(), self.cancelled_awbs.count())
        except:
            return get_percentage(self.get_cancelled_conf_awb().count(), self.get_cancelled_awb().count())

    def get_closed_awb(self):
        self.closed_awbs = self.get_pickup_awb() | self.get_cancelled_awb()
        return self.closed_awbs

    def get_closed_awb_perc(self):
        try:
            return get_percentage(self.closed_awbs.count(), self.total_awbs_count)
        except:
            return get_percentage(self.get_closed_awb().count(), self.total_awbs_count)

    def get_scheduled_awb(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.scheduled_status__include
        self.scheduled_awbs = AWB.objects.filter(**filter)
        return self.scheduled_awbs

    def get_scheduled_awb_perc(self):
        try:
            return get_percentage(self.scheduled_awbs.count(), self.total_awbs_count)
        except:
            return get_percentage(self.get_scheduled_awb().count(), self.total_awbs_count)

    def get_out_pickup_awb(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.out_for_pickup_status__include
        self.out_for_pickup_awbs = AWB.objects.filter(**filter)
        return self.out_for_pickup_awbs

    def get_out_pickup_awb_perc(self):
        try:
            return get_percentage(self.out_for_pickup_awbs.count(), self.total_awbs_count)
        except:
            return get_percentage(self.get_out_pickup_awb().count(), self.total_awbs_count)

    def get_dtod_awb(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.dtod_status__include
        self.dtod_awbs = AWB.objects.filter(**filter)
        return self.dtod_awbs

    def get_dtod_awb_perc(self):
        try:
            return get_percentage(self.dtod_awbs.count(), self.pickup_awbs.count())
        except:
            return get_percentage(self.get_dtod_awb().count(), self.get_pickup_awb().count())

    def get_connected_awb(self):
        filter = self.get_filter_dict()
        exclude = {}
        exclude['awb_status__status__in'] = self.connected_status__exclude
        self.connected_awbs = AWB.objects.filter(**filter).exclude(**exclude)
        return self.connected_awbs

    def get_connected_awb_perc(self):
        try:
            return get_percentage(self.connected_awbs.count(), self.pickup_awbs.count())
        except:
            return get_percentage(self.get_connected_awb().count(), self.get_pickup_awb().count())

    def get_lost_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.lost_status__include
        self.lost_awbs = AWB.objects.filter(**filter)
        self.lost_awbs_count = self.lost_awbs.count()
        return self.lost_awbs_count

    def get_scheduled_pending_pickup_per_d1(self):
        #date_diff = self.get_date_diff(self.get_current_date(), self.end_date)
        awbs = self.scheduled_awbs
        self.scheduled_pending_pickup_d1_count = 0
        for awb in awbs:
            diff = awb.get_scheduled_in_days()
            if diff == 0 and diff != '':
                self.scheduled_pending_pickup_d1_count += 1
        #return get_percentage(self.scheduled_pending_pickup_d1_count, self.picked_up_awbs_count)
        return self.scheduled_pending_pickup_d1_count

    def get_scheduled_pending_pickup_per_gt_d1(self):
        awbs = self.scheduled_awbs
        self.scheduled_pending_pickup__gt_d2_count = 0
        for awb in awbs:
            diff = awb.get_scheduled_in_days()
            if diff > 0 and diff != '':
                self.scheduled_pending_pickup__gt_d2_count += 1
        return self.scheduled_pending_pickup__gt_d2_count
        #return get_percentage(self.scheduled_pending_pickup__gt_d2_count, self.picked_up_awbs_count)

    def get_pickup_perc_d1(self):
        awbs = self.pickup_awbs
        self.pickup_d1_count = 0
        for awb in awbs:
            diff = awb.get_picked_up_in_days()
            if diff == 0 and diff != '':
                self.pickup_d1_count += 1
        return get_percentage(self.pickup_d1_count, self.pickup_awbs.count())
        #return self.pickup_d1_count

    def get_pickup_perc_d2(self):
        awbs = self.pickup_awbs
        self.pickup_d2_count = 0
        for awb in awbs:
            diff = awb.get_picked_up_in_days()
            if diff < 2 and diff >= 1 and diff != '':
                self.pickup_d2_count += 1
        return get_percentage(self.pickup_d2_count, self.pickup_awbs.count())
        #return self.pickup_d2_count

    def get_pickup_perc_gt_d2(self):
        awbs = self.pickup_awbs
        self.pickup_gt_d2_count = 0
        for awb in awbs:
            diff = awb.get_picked_up_in_days()
            if diff >= 2 and diff != '':
                self.pickup_gt_d2_count += 1
        return get_percentage(self.pickup_gt_d2_count, self.pickup_awbs.count())
        #return self.pickup_gt_d2_count


    def get_delivered_perc_lte_d2(self):
        awbs = self.dtod_awbs
        self.delivered_lte_d2_count = 0
        for awb in awbs:
            diff = awb.get_delivered_in_days()
            if diff <= 2:
                self.delivered_lte_d2_count += 1
        #return self.delivered_lte_d2_count
        return get_percentage(self.delivered_lte_d2_count, self.pickup_awbs.count())

    def get_delivered_perc_d2_d3(self):
        awbs = self.dtod_awbs
        self.delivered_d2_d3_count = 0
        for awb in awbs:
            diff = awb.get_delivered_in_days()
            if diff == 3:
                self.delivered_d2_d3_count += 1
        #return self.delivered_d2_d3_count
        return get_percentage(self.delivered_d2_d3_count, self.pickup_awbs.count())

    def get_delivered_perc_gt_d3(self):
        awbs = self.dtod_awbs
        self.delivered_gt_d3_count = 0
        for awb in awbs:
            diff = awb.get_delivered_in_days()
            if diff > 3:
                self.delivered_gt_d3_count += 1
        #return self.delivered_gt_d3_count
        return get_percentage(self.delivered_gt_d3_count, self.pickup_awbs.count())

    def get_dto_in_transit_awb_perc(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.in_transit_status__include
        self.dto_in_transit_awb = AWB.objects.filter(**filter)
        self.dto_in_transit_awb_count = self.dto_in_transit_awb.count()
        #return self.dto_in_transit_awb_count
        return get_percentage(self.dto_in_transit_awb_count, self.pickup_awbs.count())

    def get_dto_rejected_awb_perc(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.dto_rejected_status__include
        self.dto_rejected_awb = AWB.objects.filter(**filter)
        self.dto_rejected_awb_count = self.dto_rejected_awb.count()
        #return self.dto_rejected_awb_count
        return get_percentage(self.dto_rejected_awb_count, self.pickup_awbs.count())

    def get_all_cod_awbs(self):
        filter = {}
        # if self.client:
        #     filter['awb_status__manifest__client_id'] = self.client
        filter['awb__category'] = 'COD'
        if self.pickup_branch:
            filter['awb__pincode__branch_pincode__branch'] = self.pickup_branch

        if self.start_date and self.end_date:
            filter['creation_date__range'] = (self.start_date, self.end_date)
        self.all_cod_awbs = []
        awb_hist = AWB_History.objects.filter(**filter).exclude(drs=None).prefetch_related('awb')
        pk_list = [awb.awb.pk for awb in awb_hist]
        #print len(self.all_cod_awbs),(self.start_date, self.end_date)
        self.all_cod_awbs = AWB.objects.filter(pk__in=pk_list)
        return self.all_cod_awbs

    def get_expected_amount(self):
        expected_amount = self.get_all_cod_awbs().aggregate(expected_amount=models.Sum('expected_amount'))[
            'expected_amount']
        #return self.dto_rejected_awb_count
        return expected_amount


    def get_del_cod_awbs(self):
        filter = {}
        # if self.client:
        #     filter['awb_status__manifest__client_id'] = self.client
        filter['awb__category'] = 'COD'
        if self.pickup_branch:
            filter['awb__pincode__branch_pincode__branch'] = self.pickup_branch
        if self.start_date and self.end_date:
            filter['creation_date__range'] = (self.start_date, self.end_date)
        filter['status'] = 'DEL'
        awb_hist = AWB_History.objects.filter(**filter)
        self.del_cod_awbs = AWB.objects.filter(pk__in=[awb.awb.pk for awb in awb_hist])
        return self.del_cod_awbs


    def get_collected_amount(self):
        collected_amount = self.get_del_cod_awbs().aggregate(collected_amt=models.Sum('awb_status__collected_amt'))[
            'collected_amt']
        #return self.dto_rejected_awb_count
        return collected_amount

        # def get_total_awb_count(self):
        #     filter = self.get_filter_dict()
        #     self.total_awbs = AWB.objects.filter(**filter)
        #     self.total_awbs_count = self.total_awbs.count()
        #     return self.total_awbs_count
        #

        # def get_closed_awb_count(self):
        #     try:
        #         self.closed_awbs_count = self.pickup_awbs.count() + self.cancelled_awbs.count()
        #     except:
        #         self.closed_awbs_count = self.get_pickup_awb().count() + self.get_cancelled_awb().count()
        #     return self.closed_awbs_count
        #
        #
        # def get_closed_awb_perc(self):
        #     try:
        #         return get_percentage(self.closed_awbs_count, self.total_awbs_count)
        #     except:
        #         self.get_closed_awb_count()
        #         return get_percentage(self.closed_awbs_count, self.total_awbs_count)
        #
        # def get_pickup_awb_count(self):
        #     filter = self.get_filter_dict()
        #     exclude = {}
        #     exclude['awb_status__status__in'] = self.picked_up_status__exclude
        #     self.picked_up_awbs = AWB.objects.filter(**filter).exclude(**exclude)
        #     self.picked_up_awbs_count = self.picked_up_awbs.count()
        #     return self.picked_up_awbs_count
        #
        # def get_pickup_awb_perc(self):
        #     try:
        #         return get_percentage(self.picked_up_awbs_count, self.total_awbs_count)
        #     except:
        #         self.get_pickup_awb_count()
        #         return get_percentage(self.picked_up_awbs_count, self.total_awbs_count)
        #
        # def get_cancelled_awb_count(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_status__status__in'] = self.cancelled_status__include
        #     self.cancelled_awbs = AWB.objects.filter(**filter)
        #     self.cancelled_awbs_count = self.cancelled_awbs.count()
        #     return self.cancelled_awbs_count
        #
        # def get_cancelled_by_picked_up_awb_perc(self):
        #     try:
        #         return get_percentage(self.cancelled_awbs_count, self.total_awbs_count)
        #     except:
        #         self.get_cancelled_awb_count()
        #         return get_percentage(self.cancelled_awbs_count, self.total_awbs_count)
        #
        # def get_cancelled_conf_awb_count(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_status__updated_by__profile__role'] = 'CS'
        #     self.cancelled_conf_awbs = self.cancelled_awbs.filter(**filter)
        #     self.cancelled_conf_awbs_count = self.cancelled_conf_awbs.count()
        #     return self.cancelled_conf_awbs_count
        #
        # def get_cancelled_conf_awb_perc(self):
        #     return get_percentage(self.cancelled_conf_awbs_count, self.cancelled_awbs_count)
        #
        # def get_scheduled_awb_count(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_status__status__in'] = self.scheduled_status__include
        #     self.scheduled_awbs = AWB.objects.filter(**filter)
        #     self.scheduled_awbs_count = self.scheduled_awbs.count()
        #     return self.scheduled_awbs_count
        #
        # def get_scheduled_awb_perc(self):
        #     return get_percentage(self.scheduled_awbs_count, self.total_awbs_count)
        #
        # def get_out_pickup_awb_count(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_status__status__in'] = self.out_for_pickup_status__include
        #     self.out_for_pickup_awbs = AWB.objects.filter(**filter)
        #     self.out_for_pickup_awbs_count = self.out_for_pickup_awbs.count()
        #     return self.out_for_pickup_awbs_count
        #
        # def get_out_pickup_awb_perc(self):
        #     return get_percentage(self.out_for_pickup_awbs_count, self.total_awbs_count)
        #
        # def get_dtod_awb_count(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_status__status__in'] = self.dtod_status__include
        #     self.dtod_awbs = AWB.objects.filter(**filter)
        #     self.dtod_awbs_count = self.dtod_awbs.count()
        #     return self.dtod_awbs_count
        #
        # def get_dtod_awb_perc(self):
        #     try:
        #         return get_percentage(self.dtod_awbs_count, self.picked_up_awbs_count)
        #     except:
        #         self.get_dtod_awb_count()
        #         return get_percentage(self.dtod_awbs_count, self.picked_up_awbs_count)
        #
        # def get_connected_awb_count(self):
        #     filter = self.get_filter_dict()
        #     exclude = {}
        #     exclude['awb_status__status__in'] = self.connected_status__exclude
        #     self.connected_awbs = AWB.objects.filter(**filter).exclude(**exclude)
        #     self.connected_awbs_count = self.connected_awbs.count()
        #     return self.connected_awbs_count
        #
        # def get_connected_awb_perc(self):
        #     try:
        #         return get_percentage(self.connected_awbs_count, self.picked_up_awbs_count)
        #     except:
        #         self.get_connected_awb_count()
        #         return get_percentage(self.connected_awbs_count, self.picked_up_awbs_count)
        #
        # def get_lost_awb_count(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_status__status__in'] = self.lost_status__include
        #     self.lost_awbs = AWB.objects.filter(**filter)
        #     self.lost_awbs_count = self.lost_awbs.count()
        #     return self.lost_awbs_count
        #
        # def get_scheduled_pending_pickup_per_d1(self):
        #     #date_diff = self.get_date_diff(self.get_current_date(), self.end_date)
        #     awbs = self.scheduled_awbs
        #     self.scheduled_pending_pickup_d1_count = 0
        #     for awb in awbs:
        #         diff = awb.get_scheduled_in_days()
        #         if diff == 0 and diff != '':
        #             self.scheduled_pending_pickup_d1_count += 1
        #     #return get_percentage(self.scheduled_pending_pickup_d1_count, self.picked_up_awbs_count)
        #     return self.scheduled_pending_pickup_d1_count
        #
        # def get_scheduled_pending_pickup_per_gt_d1(self):
        #     awbs = self.scheduled_awbs
        #     self.scheduled_pending_pickup__gt_d2_count = 0
        #     for awb in awbs:
        #         diff = awb.get_scheduled_in_days()
        #         if diff > 0 and diff != '':
        #             self.scheduled_pending_pickup__gt_d2_count += 1
        #     return self.scheduled_pending_pickup__gt_d2_count
        #     #return get_percentage(self.scheduled_pending_pickup__gt_d2_count, self.picked_up_awbs_count)
        #
        # def get_pickup_perc_d1(self):
        #     awbs = self.picked_up_awbs
        #     self.pickup_d1_count = 0
        #     for awb in awbs:
        #         diff = awb.get_picked_up_in_days()
        #         if diff == 0 and diff != '':
        #             self.pickup_d1_count += 1
        #     return get_percentage(self.pickup_d1_count, self.picked_up_awbs_count)
        #     #return self.pickup_d1_count
        #
        # def get_pickup_perc_d2(self):
        #     awbs = self.picked_up_awbs
        #     self.pickup_d2_count = 0
        #     for awb in awbs:
        #         diff = awb.get_picked_up_in_days()
        #         if diff < 2 and diff >= 1 and diff != '':
        #             self.pickup_d2_count += 1
        #     return get_percentage(self.pickup_d2_count, self.picked_up_awbs_count)
        #     #return self.pickup_d2_count
        #
        # def get_pickup_perc_gt_d2(self):
        #     awbs = self.picked_up_awbs
        #     self.pickup_gt_d2_count = 0
        #     for awb in awbs:
        #         diff = awb.get_picked_up_in_days()
        #         if diff >= 2 and diff != '':
        #             self.pickup_gt_d2_count += 1
        #     return get_percentage(self.pickup_gt_d2_count, self.picked_up_awbs_count)
        #     #return self.pickup_gt_d2_count
        #
        #
        # def get_delivered_perc_lte_d2(self):
        #     awbs = self.dtod_awbs
        #     self.delivered_lte_d2_count = 0
        #     for awb in awbs:
        #         diff = awb.get_delivered_in_days()
        #         if diff <= 2:
        #             self.delivered_lte_d2_count += 1
        #     #return self.delivered_lte_d2_count
        #     return get_percentage(self.delivered_lte_d2_count, self.picked_up_awbs_count)
        #
        # def get_delivered_perc_d2_d3(self):
        #     awbs = self.dtod_awbs
        #     self.delivered_d2_d3_count = 0
        #     for awb in awbs:
        #         diff = awb.get_delivered_in_days()
        #         if diff == 3:
        #             self.delivered_d2_d3_count += 1
        #     #return self.delivered_d2_d3_count
        #     return get_percentage(self.delivered_d2_d3_count, self.picked_up_awbs_count)
        #
        # def get_delivered_perc_gt_d3(self):
        #     awbs = self.dtod_awbs
        #     self.delivered_gt_d3_count = 0
        #     for awb in awbs:
        #         diff = awb.get_delivered_in_days()
        #         if diff > 3:
        #             self.delivered_gt_d3_count += 1
        #     #return self.delivered_gt_d3_count
        #     return get_percentage(self.delivered_gt_d3_count, self.picked_up_awbs_count)
        #
        # def get_dto_in_transit_awb_perc(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_status__status__in'] = self.in_transit_status__include
        #     self.dto_in_transit_awb = AWB.objects.filter(**filter)
        #     self.dto_in_transit_awb_count = self.dto_in_transit_awb.count()
        #     #return self.dto_in_transit_awb_count
        #     return get_percentage(self.dto_in_transit_awb_count, self.picked_up_awbs_count)
        #
        # def get_dto_rejected_awb_perc(self):
        #     filter = self.get_filter_dict()
        #     filter['awb_status__status__in'] = self.dto_rejected_status__include
        #     self.dto_rejected_awb = AWB.objects.filter(**filter)
        #     self.dto_rejected_awb_count = self.dto_rejected_awb.count()
        #     #return self.dto_rejected_awb_count
        #     return get_percentage(self.dto_rejected_awb_count, self.picked_up_awbs_count)

