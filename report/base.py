from internal.models import Branch
from utils.common import get_date_list


class ReportMixin(object):
    def __init__(self, *args, **kwargs):
        self.picked_up_status__exclude = ['DR', 'PP', 'CAN', 'DBC', 'SCH', 'CNA', 'NA']
        self.cancelled_status__include = ['CAN']
        self.scheduled_status__include = ['SCH', 'DBC', 'CNA', 'DR', 'NA']
        self.deferred_status__include = ['SCH', 'DBC']
        self.in_transit_status__include = ['ISC', 'TB', 'MTS', 'TBD', 'MTD', 'DCR', 'INT']
        self.dto_rejected_status__include = ['RBC', 'CB']
        self.out_for_pickup_status__include = ['PP']
        self.dtod_status__include = ['DEL', 'DTO']
        self.lost_status__include = ['LOS']
        self.connected_status__exclude = ['DR', 'PP', 'PC', 'SCH', 'DBC', 'CNA', 'NA', 'CAN']

        self.client = None
        self.category = None
        self.pickup_branch = None
        self.start_date = None
        self.end_date = None
        if 'duration' in kwargs:
            self.date_list = get_date_list(kwargs['duration'], '%Y-%m-%d')
            self.start_date = self.date_list[-1] + ' 00:00:00'
            self.end_date = self.date_list[0] + ' 23:59:59'
        if 'start_date' in kwargs and 'end_date' in kwargs:
            self.start_date = kwargs['start_date'] + ' 00:00:00'
            self.end_date = kwargs['start_date'] + ' 23:59:59'
            self.date_list = [self.start_date]
        if 'category' in kwargs:
            self.category = kwargs['category']
        if 'pickup_branch' in kwargs:
            if type(kwargs['pickup_branch']) is int:
                self.branch_list = Branch.objects.filter(pk=kwargs['pickup_branch'])

            elif type(kwargs['pickup_branch']) is str:
                self.branch_list = Branch.objects.filter(branch_name=kwargs['pickup_branch'])
            self.pickup_branch = self.branch_list.first()
        else:
            self.branch_list = Branch.objects.exclude(branch_name='HQ').order_by('branch_name')


    def get_filter_dict(self):
        filter = {}
        if self.client:
            filter['awb_status__manifest__client_id'] = self.client
        if self.category:
            filter['category'] = self.category
        if self.pickup_branch:
            filter['pincode__branch_pincode__branch'] = self.pickup_branch
        if self.start_date and self.end_date:
            filter['creation_date__range'] = (self.start_date, self.end_date)
        return filter


    def CSVBuilder(self):
        pass


    def EmailFile(self):
        pass




