from datetime import timedelta, datetime
from awb.models import AWB
from internal.models import Branch
from utils.constants import DELIVERY_REPORT_FL_HEADER
from zoning.models import City

__author__ = 'naveen'

class DeliveryReportFwd(object):
    def __init__(self, client, duration, holidays=(), report_type=None, branch=None, city=None, category=None):
        self.client = client
        self.category = category
        self.duration = int(duration)
        self.holidays = list(holidays)
        self.header = None
        self.date_format = '%Y-%m-%d'
        self.date_list = self.get_date_list()
        self.date_range = self.get_date_range()
        self.start_date = None
        self.end_date = None
        self.report_type = report_type
        self.set_header()
        self.branch = None
        if branch is None:
            self.branch_list = Branch.objects.filter(branch_name__in = ['BLR', 'BLR NORTH', 'LXM',
                                                                         'SKT', 'RAJ', 'GG1', 'BJS']).defer('id')
        else:
            self.branch_list = Branch.objects.filter(pk__in=branch).defer('id')
        if city is None:
            self.city_list = City.objects.all().defer('id')
            self.city_list = [c for c in self.city_list if c.branch_set.exists()]
        else:
            self.city_list = City.objects.filter(pk__in=city).defer('id')

    @property
    def start_date(self):
        return self._start_date + ' 00:00:00'

    @start_date.setter
    def start_date(self, value):
        self._start_date = value

    @property
    def end_date(self):
        return self._end_date + ' 23:59:59'

    @end_date.setter
    def end_date(self, value):
        self._end_date = value

    def generate_report(self):
        report_dict = {}
        report_dict['header'] = self.get_report_header()
        report_dict['header'].insert(0, 'Date')
        report_dict['report'] = {}
        iterator = self.get_iterator()
        for i in xrange(len(iterator)):
            self.start_date, self.end_date = self.get_date_range()
            #print self.start_date, self.end_date
            if self.report_type == 'city':
                self.city = iterator[i]
                # report_dict['report'][i] = {}
                # report_dict['report'][i]['total'] = self.get_report()
                # report_dict['report'][i]['total'].insert(0, iterator[i])
                #print self.date_list
                # for d in xrange(len(self.date_list)):
                #     self.start_date = self.date_list[d]
                #     self.end_date = self.date_list[d]
                #     #print self.start_date, self.end_date
                #     report_dict['report'][i][d] = self.get_report()
                #     report_dict['report'][i][d].insert(0, self.date_list[d])
            else:
                self.branch = iterator[i]
            report_dict['report'][i] = {}
            report_dict['report'][i]['total'] = self.get_report()
            report_dict['report'][i]['total'].insert(0, iterator[i])
            for d in xrange(len(self.date_list)):
                self.start_date = self.date_list[d]
                self.end_date = self.date_list[d]
                report_dict['report'][i][d] = self.get_report()
                report_dict['report'][i][d].insert(0, self.date_list[d])

        return report_dict

    def get_dict(self):
        filter = {}
        if self.client is not None:
            filter['awb_status__manifest__client'] = self.client
        if self.category is not None:
            filter['category'] = self.category
        if self.report_type is not None:
            if self.report_type == 'city':
                filter['pincode__branch_pincode__branch__city'] = self.city
            if self.report_type == 'branch':
                filter['pincode__branch_pincode__branch'] = self.branch
        filter['awb_history__on_update__range'] = (self.start_date, self.end_date)
        # print filter
        return filter

    def get_report(self):
        report_list = []
        for k, v in self.header:
            if k.lower() != 'rpi date':
                if hasattr(self, v):
                    report_list.append(getattr(self, v)())
        return report_list

    def get_iterator(self):
        if self.report_type == 'city':
            return self.city_list
        else:
            return self.branch_list


    def get_report_header(self):
        report_header = []
        for k, v in self.header:
            if hasattr(self, v):
                report_header.append(k)
        return report_header

    def get_date_list(self):
        base = datetime.today() - timedelta(days=1)
        return [(base - timedelta(days=x)).strftime(self.date_format) for x in range(0, self.duration) if
                (base - timedelta(days=x)).weekday() != 6 and (base - timedelta(days=x)) not in self.holidays]

    def get_date_range(self):
        return self.get_date_list()[-1], self.get_date_list()[0]

    def get_current_date(self):
        return datetime.today().strftime('%Y-%m-%d')

    def get_date_diff(self, a, b):
        diff = datetime.strptime(a, self.date_format + ' %H:%M:%S') - datetime.strptime(b,
                                                                                        self.date_format + ' %H:%M:%S')
        return abs(diff.days)


class DeliveryReport(DeliveryReportFwd):
    def set_header(self):
        self.header = DELIVERY_REPORT_FL_HEADER

    def get_total_delivered(self):
        filter = self.get_dict()
        filter['awb_history__status'] = 'DEL'
        total_delivered = AWB.objects.filter(**filter)
        print total_delivered
        total_delivered_count = total_delivered.count()
        print total_delivered_count
        return total_delivered_count