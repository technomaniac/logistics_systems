from datetime import datetime, timedelta

from django.db import connection
from django.db.models.aggregates import Count

from utils.constants import CLIENT_PERFORMANCE_REPORT_RL
from awb.models import AWB
from internal.models import Branch
from zoning.models import City
from utils.common import get_percentage


class PerformanceReport(object):
    def __init__(self, client, duration, holidays=(), report_type=None, branch=None, city=None, category=None):
        self.client = client
        self.category = category
        self.duration = int(duration)
        self.holidays = list(holidays)
        self.header = None
        self.date_format = '%Y-%m-%d'
        self.date_list = self.get_date_list()
        self.date_range = self.get_date_range()
        self.report_type = report_type
        #self.filter_key = self.get_filter_dict()
        self.start_date = None
        self.end_date = None
        self.total_awbs = None
        self.closed_awbs = None
        self.cancelled_awbs = None
        self.cancelled_conf_awbs = None
        self.total_awbs_count = 0
        self.picked_up_awbs_count = 0
        self.closed_awbs_count = 0
        self.cancelled_awbs_count = 0
        self.cancelled_conf_awbs_count = 0
        self.scheduled_awbs_count = 0
        self.pickup_d1_count = 0
        self.pickup_d2_count = 0
        self.pickup_gt_d2_count = 0
        self.total_attempt_status__exclude = ['DR', 'NA']
        self.picked_up_status__exclude = ['DR', 'PP', 'CAN', 'DBC', 'SCH', 'CNA', 'NA']
        self.dispatched_status__exclude = ['DR', 'CAN', 'DBC', 'SCH', 'CNA', 'NA']
        self.cancelled_status__include = ['CAN']
        self.scheduled_status__include = ['SCH', 'DBC', 'CNA', 'DR', 'NA']
        self.misc_pending_status__include = ['SCH', 'CNA', 'DR', 'NA']
        self.deferred_status__include = ['DBC']
        self.in_transit_status__include = ['ISC', 'TB', 'MTS', 'TBD', 'MTD', 'DCR', 'INT']
        self.dto_rejected_status__include = ['RBC', 'CB']
        self.dtc_status__include = ['DTC']
        self.out_for_pickup_status__include = ['PP']
        self.dtod_status__include = ['DEL']
        self.lost_status__include = ['LOS']
        self.city = None
        self.branch = None
        if branch is None:
            self.branch_list = Branch.objects.exclude(branch_name='HQ')
        else:
            self.branch_list = Branch.objects.filter(pk__in=branch)
        if city is None:
            self.city_list = City.objects.all().prefetch_related('branch_set')
            self.city_list = [c for c in self.city_list if len(c.branch_set.all()) > 0]
        else:
            self.city_list = City.objects.filter(pk__in=city)
        self.set_header()

    @property
    def start_date(self):
        return self._start_date + ' 00:00:00'

    @start_date.setter
    def start_date(self, value):
        self._start_date = value

    @property
    def end_date(self):
        return self._end_date + ' 23:59:59'

    @end_date.setter
    def end_date(self, value):
        self._end_date = value

    def set_header(self):
        if self.client.category == 'RL':
            self.header = CLIENT_PERFORMANCE_REPORT_RL
        else:
            self.header = {}

    def get_iterator(self):
        if self.report_type == 'city':
            return self.city_list
        else:
            return self.branch_list

    def get_status_list(self, function):
        pass

    def get_filter_dict(self):
        filter = {}
        if self.client is not None:
            filter['awb_status__manifest__client'] = self.client
        if self.category is not None:
            filter['category'] = self.category
        filter['creation_date__range'] = (self.start_date, self.end_date)
        if self.report_type is not None:
            if self.report_type == 'city':
                filter['pincode__branch_pincode__branch__city'] = self.city
            if self.report_type == 'branch':
                filter['pincode__branch_pincode__branch'] = self.branch
        # if inspect.stack() is not None:
        #     filter.update(self.get_filter_key_pair(inspect.stack()[1][3]))
        return filter

    def get_date_list(self):
        base = datetime.today() - timedelta(days=1)
        return [(base - timedelta(days=x)).strftime(self.date_format) for x in range(0, self.duration) if
                (base - timedelta(days=x)).weekday() != 6 and (base - timedelta(days=x)) not in self.holidays]

    def get_date_range(self):
        return self.get_date_list()[-1], self.get_date_list()[0]

    def get_current_date(self):
        return datetime.today().strftime('%Y-%m-%d')

    def get_date_diff(self, a, b):
        diff = datetime.strptime(a, self.date_format + ' %H:%M:%S') - datetime.strptime(b,
                                                                                        self.date_format + ' %H:%M:%S')
        return abs(diff.days)

    def generate_report(self):
        report_dict = {}
        report_dict['header'] = self.get_report_header()
        report_dict['header'].insert(0, 'RPI Date')
        report_dict['report'] = {}
        iterator = self.get_iterator()
        for i in xrange(len(iterator)):
            self.start_date, self.end_date = self.get_date_range()
            #print self.start_date, self.end_date
            if self.report_type == 'city':
                self.city = iterator[i]
                # report_dict['report'][i] = {}
                # report_dict['report'][i]['total'] = self.get_report()
                # report_dict['report'][i]['total'].insert(0, iterator[i])
                #print self.date_list
                # for d in xrange(len(self.date_list)):
                #     self.start_date = self.date_list[d]
                #     self.end_date = self.date_list[d]
                #     #print self.start_date, self.end_date
                #     report_dict['report'][i][d] = self.get_report()
                #     report_dict['report'][i][d].insert(0, self.date_list[d])
            else:
                self.branch = iterator[i]
            report_dict['report'][i] = {}
            report_dict['report'][i]['total'] = self.get_report()
            report_dict['report'][i]['total'].insert(0, iterator[i])
            for d in xrange(len(self.date_list)):
                self.start_date = self.date_list[d]
                self.end_date = self.date_list[d]
                report_dict['report'][i][d] = self.get_report()
                report_dict['report'][i][d].insert(0, self.date_list[d])

        return report_dict

    def get_report(self):
        report_list = []
        for k, v in self.header:
            if k.lower() != 'rpi date':
                if hasattr(self, v):
                    report_list.append(getattr(self, v)())
        return report_list

    def get_report_header(self):
        report_header = []
        for k, v in self.header:
            if hasattr(self, v):
                report_header.append(k)
        return report_header

    def get_total_awb_count(self):
        filter = self.get_filter_dict()
        #print filter
        #if not get_cache('total_awbs' + self.start_date + self.end_date):
        self.total_awbs = AWB.objects.prefetch_related('awb_history_set').filter(**filter)
        #     set_cache('total_awbs' + self.start_date + self.end_date, self.total_awbs)
        #     print "set"
        # else:
        #     print "cached"
        #     self.total_awbs = get_cache('total_awbs' + self.start_date + self.end_date)
        #print self.total_awbs
        self.total_awbs_count = self.total_awbs.aggregate(Count('awb'))['awb__count']
        return self.total_awbs_count

    def get_closed_awb_count(self):
        self.closed_awbs_count = self.get_pickup_awb_count() + self.get_cancelled_awb_count()
        return self.closed_awbs_count


    def get_closed_awb_perc(self):
        return get_percentage(self.closed_awbs_count, self.total_awbs_count)

    def get_total_attempted_count(self):
        exclude = {}
        filter = self.get_filter_dict()
        exclude['awb_status__status__in'] = self.total_attempt_status__exclude
        self.total_attempted_awbs = self.total_awbs.exclude(**exclude)
        self.total_attempted_count = self.total_attempted_awbs.aggregate(Count('awb'))['awb__count']
        return self.total_attempted_count

    def get_total_attempted_perc(self):
        return get_percentage(self.get_total_attempted_count(), self.total_awbs_count)

    def get_pickup_awb_count(self):
        exclude = {}
        filter = self.get_filter_dict()
        exclude['awb_status__status__in'] = self.picked_up_status__exclude
        self.picked_up_awbs = self.total_awbs.exclude(**exclude)
        self.picked_up_awbs_count = self.picked_up_awbs.aggregate(Count('awb'))['awb__count']
        return self.picked_up_awbs_count

    def get_pickup_awb_perc(self):
        return get_percentage(self.get_pickup_awb_count(), self.total_awbs_count)

    def get_cancelled_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.cancelled_status__include
        self.cancelled_awbs = AWB.objects.filter(**filter)
        self.cancelled_awbs_count = self.cancelled_awbs.count()
        return self.cancelled_awbs_count

    def get_cancelled_by_picked_up_awb_perc(self):
        return get_percentage(self.get_cancelled_awb_count(), self.total_awbs_count)

    def get_cancelled_conf_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__updated_by__profile__role'] = 'CS'
        self.cancelled_conf_awbs = self.cancelled_awbs.filter(**filter)
        self.cancelled_conf_awbs_count = self.cancelled_conf_awbs.count()
        return self.cancelled_conf_awbs_count

    def get_cancelled_conf_awb_perc(self):
        return get_percentage(self.cancelled_conf_awbs_count, self.cancelled_awbs_count)

    def get_scheduled_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.scheduled_status__include
        self.scheduled_awbs = AWB.objects.filter(**filter)
        self.scheduled_awbs_count = self.scheduled_awbs.count()
        return self.scheduled_awbs_count

    def get_scheduled_awb_perc(self):
        return get_percentage(self.scheduled_awbs_count, self.total_awbs_count)


    def get_deferred_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.deferred_status__include
        self.deferred_awbs = AWB.objects.filter(**filter)
        self.deferred_awbs_count = self.deferred_awbs.count()
        return self.deferred_awbs_count

    def get_deferred_awb_perc(self):
        return get_percentage(self.get_deferred_awb_count(), self.total_awbs_count)

    def get_misc_pending_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.misc_pending_status__include
        self.misc_pending_awbs = AWB.objects.filter(**filter)
        self.misc_pending_awbs_count = self.misc_pending_awbs.count()
        return self.misc_pending_awbs_count

    def get_misc_pending_awb_count_perc(self):
        return get_percentage(self.get_misc_pending_awb_count(), self.total_awbs_count)


    def get_out_pickup_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.out_for_pickup_status__include
        self.out_for_pickup_awbs = AWB.objects.filter(**filter)
        self.out_for_pickup_awbs_count = self.out_for_pickup_awbs.count()
        return self.out_for_pickup_awbs_count

    def get_out_pickup_awb_perc(self):
        return get_percentage(self.out_for_pickup_awbs_count, self.total_awbs_count)


    def get_lost_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.lost_status__include
        self.lost_awbs = AWB.objects.filter(**filter)
        self.lost_awbs_count = self.lost_awbs.count()
        return self.lost_awbs_count

    def get_scheduled_pending_pickup_per_d1(self):
        #date_diff = self.get_date_diff(self.get_current_date(), self.end_date)
        awbs = self.scheduled_awbs
        self.scheduled_pending_pickup_d1_count = 0
        for awb in awbs:
            diff = awb.get_scheduled_in_days()
            if diff == 0 and diff != '':
                self.scheduled_pending_pickup_d1_count += 1
        #return get_percentage(self.scheduled_pending_pickup_d1_count, self.picked_up_awbs_count)
        return self.scheduled_pending_pickup_d1_count

    def get_scheduled_pending_pickup_per_gt_d1(self):
        awbs = self.scheduled_awbs
        self.scheduled_pending_pickup__gt_d2_count = 0
        for awb in awbs:
            diff = awb.get_scheduled_in_days()
            if diff > 0 and diff != '':
                self.scheduled_pending_pickup__gt_d2_count += 1
        return self.scheduled_pending_pickup__gt_d2_count
        #return get_percentage(self.scheduled_pending_pickup__gt_d2_count, self.picked_up_awbs_count)

    def get_pickup_perc_d1(self):
        awbs = self.picked_up_awbs
        self.pickup_d1_count = len([awb for awb in awbs if awb.get_picked_up_in_days() == 0])
        # for awb in awbs:
        #     diff = awb.get_picked_up_in_days()
        #     if diff == 0 and diff != '':
        #         self.pickup_d1_count += 1
        return get_percentage(self.pickup_d1_count, self.total_awbs_count)
        #return self.pickup_d1_count

    def get_pickup_perc_in_d2(self):
        #print self.start_date
        start_date = datetime.strptime(self.start_date, '%Y-%m-%d %H:%M:%S')
        diff = abs(datetime.now() - start_date)
        if diff.days > 1:
            awbs = self.picked_up_awbs
            self.pickup_in_d2_count = 0
            for awb in awbs:
                diff = awb.get_picked_up_in_days()
                if diff < 2 and diff != '':
                    self.pickup_in_d2_count += 1
            return get_percentage(self.pickup_in_d2_count, self.total_awbs_count)
        else:
            return 'NA'

    def get_dispatched_perc_10_30_am(self):
        awbs = self.total_awbs.exclude(awb_status__status=self.dispatched_status__exclude)
        self.dispatched_perc_10_30_am_count = 0
        for awb in awbs:
            if awb.get_dispatched_before_time('10:30'):
                self.dispatched_perc_10_30_am_count += 1
        return get_percentage(self.dispatched_perc_10_30_am_count, self.total_awbs_count)

    def get_dispatched_perc_2_30_pm(self):
        awbs = self.total_awbs.exclude(awb_status__status=self.dispatched_status__exclude)
        self.dispatched_perc_2_30_pm_count = 0
        for awb in awbs:
            if awb.get_dispatched_before_time('14:30'):
                self.dispatched_perc_2_30_pm_count += 1
        return get_percentage(self.dispatched_perc_2_30_pm_count, self.total_awbs_count)

    def get_pickup_perc_d2(self):
        awbs = self.total_awbs
        self.pickup_d2_count = 0
        for awb in awbs:
            diff = awb.get_picked_up_in_days()
            if diff < 2 and diff >= 1 and diff != '':
                self.pickup_d2_count += 1
        return get_percentage(self.pickup_d2_count, self.total_awbs_count)
        #return self.pickup_d2_count

    def get_pickup_perc_gt_d2(self):
        awbs = self.total_awbs
        self.pickup_gt_d2_count = 0
        for awb in awbs:
            diff = awb.get_picked_up_in_days()
            if diff >= 2 and diff != '':
                self.pickup_gt_d2_count += 1
        return get_percentage(self.pickup_gt_d2_count, self.total_awbs_count)
        #return self.pickup_gt_d2_count


    def get_delivered_perc_lte_d2(self):
        awbs = self.dtod_awbs
        self.delivered_lte_d2_count = 0
        for awb in awbs:
            diff = awb.get_delivered_in_days()
            if diff <= 2:
                self.delivered_lte_d2_count += 1
        #return self.delivered_lte_d2_count
        return get_percentage(self.delivered_lte_d2_count, self.picked_up_awbs_count)

    def get_delivered_perc_d2_d3(self):
        awbs = self.dtod_awbs
        self.delivered_d2_d3_count = 0
        for awb in awbs:
            diff = awb.get_delivered_in_days()
            if diff == 3:
                self.delivered_d2_d3_count += 1
        #return self.delivered_d2_d3_count
        return get_percentage(self.delivered_d2_d3_count, self.picked_up_awbs_count)

    def get_delivered_perc_gt_d3(self):
        awbs = self.dtod_awbs
        self.delivered_gt_d3_count = 0
        for awb in awbs:
            diff = awb.get_delivered_in_days()
            if diff > 3:
                self.delivered_gt_d3_count += 1
        #return self.delivered_gt_d3_count
        return get_percentage(self.delivered_gt_d3_count, self.picked_up_awbs_count)

    def get_dto_in_transit_awb_perc(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.in_transit_status__include
        self.dto_in_transit_awb = AWB.objects.filter(**filter)
        self.dto_in_transit_awb_count = self.dto_in_transit_awb.count()
        #return self.dto_in_transit_awb_count
        return get_percentage(self.dto_in_transit_awb_count, self.picked_up_awbs_count)

    def get_dto_rejected_awb_perc(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.dto_rejected_status__include
        self.dto_rejected_awb = AWB.objects.filter(**filter)
        self.dto_rejected_awb_count = self.dto_rejected_awb.count()
        #return self.dto_rejected_awb_count
        return get_percentage(self.dto_rejected_awb_count, self.picked_up_awbs_count)

    def get_dtc_awb_perc(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.dtc_status__include
        self.dtc_awbs = AWB.objects.filter(**filter)
        self.dtc_awbs_count = self.dtc_awbs.count()
        #return self.dto_rejected_awb_count
        return get_percentage(self.dtc_awbs_count, self.picked_up_awbs_count)

    def get_dtod_awb_count(self):
        filter = self.get_filter_dict()
        filter['awb_status__status__in'] = self.dtod_status__include
        self.dtod_awbs = AWB.objects.filter(**filter)
        self.dtod_awbs_count = self.dtod_awbs.count()
        return self.dtod_awbs_count

    def get_dtod_awb_perc(self):
        return get_percentage(self.get_dtod_awb_count(), self.picked_up_awbs_count)
        # @property
        # def total_awbs(self):
        #     return self.total_awbs
        #
        # @total_awbs.setter
        # def total_awbs(self, value):
        #     self.total_awbs = value
        #
        # @property
        # def total_awbs(self):
        #     return self.total_awbs
        #
        # @total_awbs.setter
        # def total_awbs(self, value):
        #     self.total_awbs = value
        #
        # @property
        # def closed_awbs(self):
        #     return self.closed_awbs
        #
        # @closed_awbs.setter
        # def closed_awbs(self, value):
        #     self.closed_awbs = value
        #
        # @property
        # def cancelled_awbs(self):
        #     return self.cancelled_awbs
        #
        # @cancelled_awbs.setter
        # def cancelled_awbs(self, value):
        #     self.cancelled_awbs = value
        #
        # @property
        # def cancelled_conf_awbs(self):
        #     return self.cancelled_conf_awbs
        #
        # @cancelled_conf_awbs.setter
        # def cancelled_conf_awbs(self, value):
        #     self.cancelled_conf_awbs = value


class PickupReport(PerformanceReport):
    def set_header(self):
        if self.client.category == 'RL':
            self.header = CLIENT_PERFORMANCE_REPORT_RL
        else:
            self.header = {}


    def generate_report(self):
        report_dict = {}
        report_dict['header'] = self.get_report_header()
        report_dict['header'].insert(0, 'Pickup Date')
        report_dict['report'] = {}
        iterator = self.get_iterator()
        for i in xrange(len(iterator)):
            self.start_date, self.end_date = self.get_date_range()
            #print self.start_date, self.end_date
            if self.report_type == 'city':
                self.city = iterator[i]
            else:
                self.branch = iterator[i]
            report_dict['report'][i] = {}
            report_dict['report'][i]['total'] = self.get_report()
            report_dict['report'][i]['total'].insert(0, iterator[i])
            for d in xrange(len(self.date_list)):
                self.start_date = self.date_list[d]
                self.end_date = self.date_list[d]
                report_dict['report'][i][d] = self.get_report()
                report_dict['report'][i][d].insert(0, self.date_list[d])
        return report_dict

    def get_pickup_awb_count(self):
        sql = 'SELECT' + ' awb_awb.id, awb_awb_status.status ' + 'FROM "awb_awb" '
        sql += 'INNER JOIN "zoning_pincode" ON ( "awb_awb"."pincode_id" = "zoning_pincode"."id" ) '
        sql += 'INNER JOIN "zoning_city" ON ( "zoning_city"."id" = "zoning_pincode"."city_id" ) '
        sql += 'INNER JOIN "awb_awb_status" ON ( "awb_awb"."id" = "awb_awb_status"."awb_id" ) '
        sql += 'INNER JOIN "awb_manifest" ON ( "awb_awb_status"."manifest_id" = "awb_manifest"."id" ) '
        sql += 'INNER JOIN "awb_awb_history" ON ( "awb_awb"."id" = "awb_awb_history"."awb_id" ) '
        #sql += 'WHERE ("awb_awb"."category" = \'' + str(self.category) + '\' '
        sql += 'AND "zoning_city"."id" = ' + str(self.city.pk) + ' '
        sql += 'AND "awb_manifest"."client_id" = ' + str(self.client.pk) + ' '
        sql += 'AND "awb_awb_history"."status" = \'PC\' '
        sql += 'AND "awb_awb"."creation_date" BETWEEN \'' + self.start_date + '\' and \'' + self.end_date + '\' '
        sql += 'AND "awb_awb_history"."creation_date" BETWEEN \'' + self.start_date + '\' and \'' + self.end_date + '\' '
        #print sql
        cursor = connection.cursor()
        cursor.execute(sql)

        self.picked_up_awbs = dict(cursor.fetchall())
        #print self.picked_up_awbs
        # self.picked_up_awbs = [awb for awb in AWB.objects.raw(sql)]
        self.picked_up_awbs_count = len(self.picked_up_awbs.keys())
        return self.picked_up_awbs_count

    def get_in_transit_awb_perc(self):
        self.in_transit_awbs_count = 0
        for awb in self.picked_up_awbs.keys():
            if self.picked_up_awbs[awb] in self.in_transit_status__include:
                self.in_transit_awbs_count += 1
                #return self.dto_rejected_awb_count
        return get_percentage(self.in_transit_awbs_count, self.picked_up_awbs_count)

    def get_dtc_awb_perc(self):
        self.dtc_awbs_count = 0
        for awb in self.picked_up_awbs.keys():
            if self.picked_up_awbs[awb] == 'DTO':
                self.dtc_awbs_count += 1
                #return self.dto_rejected_awb_count
        return get_percentage(self.dtc_awbs_count, self.picked_up_awbs_count)

    def get_dtod_awb_perc(self):
        self.dtod_awbs_count = 0
        for awb in self.picked_up_awbs.keys():
            if self.picked_up_awbs[awb] == 'DEL':
                self.dtod_awbs_count += 1
                #return self.dto_rejected_awb_count
        return get_percentage(self.dtod_awbs_count, self.picked_up_awbs_count)

