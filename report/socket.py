import json

from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage


class SocketHandler(object):
    def __init__(self, **kwargs):
        self.redis_publisher = RedisPublisher(**kwargs)


    def prepare_socket(self, **kwargs):
        self.redis_publisher = RedisPublisher(**kwargs)


    def send_message_to_socket(self, **kwargs):
        self.redis_publisher.publish_message(RedisMessage(json.dumps(kwargs)))