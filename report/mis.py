import csv
import json
import cStringIO as StringIO
from datetime import datetime

from celery.contrib.methods import task
from django.core.mail.message import EmailMessage
from django.http.response import StreamingHttpResponse
from django.utils.formats import date_format
from ws4redis.redis_store import RedisMessage
from ws4redis.publisher import RedisPublisher

from awb.models import AWB
from logistics.settings import MEDIA_ROOT
from utils.common import Echo, get_percentage, process_data
from utils.constants import MIS_HEADER, MIS_HEADER_CLIENT_RL, MIS_HEADER_CLIENT_FL, PENDENCY_MIS_HEADER, \
    TRANSIT_MIS_HEADER


class MISHandler(object):
    def __init__(self, queryset, filename='', subject='', mailing_list=[], type='internal', client=None):
        self.queryset = queryset
        if filename == '':
            self.filename = 'MIS'
        else:
            self.filename = filename
        self.mailing_list = mailing_list
        self.type = type
        self.client = client
        if self.type == 'internal':
            self.header = MIS_HEADER
        elif self.type == 'pendency':
            self.header = PENDENCY_MIS_HEADER
        elif self.type == 'transit':
            self.header = TRANSIT_MIS_HEADER
        else:
            if self.client.category == 'RL':
                self.header = MIS_HEADER_CLIENT_RL
            else:
                self.header = MIS_HEADER_CLIENT_FL


    @property
    def filename(self):
        return self._filename + '_' + datetime.now().strftime('%Y-%m-%d_%H:%M:%S') + '.csv'

    @filename.setter
    def filename(self, value):
        self._filename = value

    @task
    def send_mis(self):
        if self.queryset:
            self.CSVBuilder()
            message = EmailMessage(self.filename, "This is a system generated mail. Please do not reply.",
                                   "system@nuvoex.com", self.mailing_list)

            message.attach(self.filename, self.file.getvalue(), 'text/csv')
        else:
            message = EmailMessage(self.filename,
                                   "This is a system generated mail. Please do not reply.\n\nNo AWB Found",
                                   "system@nuvoex.com", self.mailing_list)
        return message.send()

    def email_file_link(self, filepath):
        link = "http://ship.nuvoex.com/media/" + filepath
        message = EmailMessage('MIS Download Link',
                               "This is a system generated mail. Please do not reply.\n\nDownload File : " + link,
                               "system@nuvoex.com", self.mailing_list)
        message.send()



    def CSVBuilder(self):
        self.file = StringIO.StringIO()
        writer = csv.writer(self.file)
        writer.writerow(self.header)
        for awb in self.queryset:
            writer.writerow(self.get_mis_row(awb))
            # print self.file.__dict__, 'file names'
        return self.file

    def download_mis(self):
        awbs = self.queryset
        awbs.insert(0, PENDENCY_MIS_HEADER)
        pseudo_buffer = Echo()
        writer = csv.writer(pseudo_buffer)
        response = StreamingHttpResponse((writer.writerow(self.pendency_mis_data(awb)) for awb in awbs),
                                         content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="' + self.filename
        return response

    def stream_csv(self):
        awbs = self.get_awbs_as_list()

        def stream():
            buffer_ = StringIO.StringIO()
            writer = csv.writer(buffer_)

            for awb in awbs:
                writer.writerow(self.pendency_mis_data(awb))
                buffer_.seek(0)
                data = buffer_.read()
                buffer_.seek(0)
                buffer_.truncate()
                yield data

        response = StreamingHttpResponse(stream(), content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="' + self.filename
        return response

    @task
    def generate_file(self, **kwargs):
        awbs = self.get_awbs_as_list()
        filepath = 'mis/' + self.filename

        # if socket:
        awbs_found = len(awbs)
        self.prepare_socket(**kwargs)

        if awbs_found > 1:
            message = str(awbs_found) + ' AWBs Found ...'
            self.send_message_to_socket(message=message, type='success')

            with open(MEDIA_ROOT + filepath, 'w') as file:
                writer = csv.writer(file)
                i = 0
                for awb in awbs:
                    try:
                        writer.writerow(self.pendency_mis_data(awb))
                    except:
                        pass
                    i += 1
                    if i % 10 == 0:
                        message = str(i) + '/' + str(awbs_found) + ' AWBs processed... ' \
                                  + str(get_percentage(i, awbs_found)) + '% Completed...'
                        self.send_message_to_socket(message=message, type='error')

            message = 'Download File : <a href="/media/' + filepath + '" >' + self.filename + '</a>'
            self.send_message_to_socket(message=message, type='success')
        else:
            self.send_message_to_socket(message='No AWB Found in this range', type='error')


    def prepare_socket(self, **kwargs):
        self.redis_publisher = RedisPublisher(**kwargs)


    def send_message_to_socket(self, **kwargs):
        self.redis_publisher.publish_message(RedisMessage(json.dumps(kwargs)))

    def get_awbs_as_list(self):
        awbs = [awb for awb in self.queryset]
        awbs.insert(0, PENDENCY_MIS_HEADER)
        return awbs

    def get_mis_row(self, awb):
        if self.type == 'internal':
            return self.internal_mis_data(awb)
        elif self.type == 'pendency':
            return self.pendency_mis_data(awb)

        elif self.type == 'transit':
            return self.transit_mis_data(awb)
        else:
            if self.client.category == 'RL':
                return self.rl_client_data(awb)
            else:
                return self.fl_client_data(awb)

    def internal_mis_data(self, awb):
        return [process_data(data) for data in [awb.get_awb(), awb.get_client(), awb.get_order_id(), awb.get_priority(),
                                                awb.get_customer_name(), awb.get_full_address(), awb.get_phone_1(),
                                                awb.get_pincode(), awb.get_readable_choice(), awb.get_package_value(),
                                                awb.expected_amount,
                                                awb.get_weight(), awb.get_length(), awb.get_breadth(), awb.get_height(),
                                                awb.get_description(), awb.get_vendor_name(), awb.get_vendor_code(),
                                                awb.get_delivery_branch(), awb.get_pickup_branch(), awb.get_drs_count(),
                                                awb.get_first_pending(),
                                                awb.get_first_dispatch(), awb.get_last_dispatch(), awb.get_last_scan(),
                                                awb.awb_status.get_readable_choice(),
                                                awb.get_last_status_updated_date(),
                                                awb.get_first_scan_branch(),
                                                awb.get_last_call_made_time(), awb.get_cs_call_count(),
                                                awb.get_remark(), awb.get_reason(),
                                                date_format(awb.creation_date, "SHORT_DATETIME_FORMAT"),
                                                awb.get_pickup_date(),
                                                awb.get_delivered_date(), awb.get_dto_creation_date(),
                                                awb.get_transfer_mode(),
                                                awb.reason_for_return, awb.get_no_of_attempts_count(),
                                                awb.get_customer_not_available_count()]]

    def pendency_mis_data(self, awb):
        if isinstance(awb, (AWB,)):
            return self.internal_mis_data(awb) + [awb.get_current_drs(), awb.get_current_tb(),
                                                  awb.get_current_mts(),
                                                  awb.get_current_dto()]
        else:
            return awb

    def transit_mis_data(self, awb):
        if isinstance(awb, (AWB,)):
            return self.pendency_mis_data(awb) + [awb.mts_hand_over_date(), awb.current_co_loader(),
                                                  awb.current_co_loader_awb()]
        else:
            return awb


    def rl_client_data(self, awb):
        return [awb.get_awb(), awb.get_order_id(), awb.get_priority(), awb.get_customer_name(),
                awb.get_full_address(),
                awb.get_phone_1(), awb.get_pincode(), awb.get_package_value(), awb.get_weight(), awb.get_length(),
                awb.get_breadth(), awb.get_height(), awb.get_drs_count(), awb.get_first_dispatch(),
                awb.get_last_dispatch(), awb.awb_status.get_status_client(), awb.get_last_status_updated_date(),
                awb.get_last_call_made_time(), awb.get_cs_call_count(), awb.get_remark(), awb.get_reason(),
                awb.get_pickup_date(), date_format(awb.creation_date, "SHORT_DATETIME_FORMAT"),
                awb.get_delivered_date(), awb.get_cancel_date(), awb.get_dto_creation_date(),
                awb.get_vendor_code(), awb.get_vendor_name()]


    def fl_client_data(self, awb):
        return [awb.get_awb(), awb.get_order_id(), awb.get_customer_name(), awb.get_full_address(),
                awb.get_phone_1(),
                awb.get_pincode(), awb.get_readable_choice(), awb.get_package_value(), awb.expected_amount,
                awb.get_weight(), awb.get_length(), awb.get_breadth(), awb.get_height(), awb.get_description(),
                awb.awb_status.get_readable_choice(), awb.get_deferred_date(), awb.get_remark(),
                awb.get_delivery_branch().branch_name, date_format(awb.creation_date, "SHORT_DATETIME_FORMAT"),
                awb.get_delivered_date(), awb.get_first_dispatch(), awb.get_drs_count(), awb.get_current_fe()]


        # class MISHandlerTask(celery.Task, MISHandler):
        # ignore_result = True

        # def __init__(self, queryset, filename='', subject='', mailing_list=[], type='internal', client=None):
        # super(MISHandler, self).__init__(queryset, filename, subject, mailing_list, type, client)
