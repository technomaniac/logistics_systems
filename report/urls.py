from django.conf.urls import patterns, url
# from socketio import sdjango

from report.views import AdvanceReport


#sdjango.autodiscover()

urlpatterns = patterns('report.views',
                       #url(r'home$',TemplateView.as_view(template_name='client/pincode.html'), name="home"),
                       # url(r'^awbs$', , name='awbs'),
                       #  url(r'^awb/(\w+)$', AwbActions.as_view(), name='awb_detail'),
                       #url(r'^socket\.io', include(sdjango.urls)),
                       url(r'^advance_report$', AdvanceReport.as_view()),
                       url(r'^autosuggest$', 'autosuggest', name='autosuggest'),
)