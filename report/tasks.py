from celery.task import task
from django.db.models.query_utils import Q

from awb.models import AWB_History, get_awb_history_filter_dict, convert_status_code_to_exact_code, AWB
from report.mis import MISHandler
from utils.common import QueryBuilder, convert_to_integer_list


@task
def advance_mis_builder(post_data, username):
    filter = dict()

    if post_data.get('start_date') != '' and post_data.get('end_date') != '':

        if post_data.get('date_type') != 'upload_date':
            hist = QueryBuilder(AWB_History)
            hist_filter = get_awb_history_filter_dict(post_data)
            hist.add_q(Q(**hist_filter['filter']))
            hist.add_q(~Q(**hist_filter['exclude']))
            hist = hist.resolve_query().prefetch_related('awb')

            if hist.exists():
                hist_list = list(set([h.awb.pk for h in hist]))
                filter['pk__in'] = hist_list
        else:
            # print hist_list
            filter['creation_date__range'] = (
                post_data.get('start_date') + ' 00:00:00', post_data.get('end_date') + ' 23:59:59')

    if post_data.get('clients') != '':
        filter['awb_status__manifest__client__pk__in'] = convert_to_integer_list(post_data.get('clients'), ',')

    if post_data.get('cities') != '':
        filter['pincode__branch_pincode__branch__city__pk__in'] = convert_to_integer_list(post_data.get('cities'), ',')

    if post_data.get('branchs') != '':
        filter['pincode__branch_pincode__branch__pk__in'] = convert_to_integer_list(post_data.get('branchs'), ',')

    if post_data.get('status') != '':
        filter.update(convert_status_code_to_exact_code(post_data.get('status')))

    awbs = AWB.objects.filter(**filter).order_by('awb',
                                                 'awb_status__manifest__client__client_name').prefetch_related(
        'awb_status', 'awb_history_set')
    # print filter
    # print [awb.awb for awb in awbs]
    mis = MISHandler(awbs)
    mis.generate_file.delay(facility='mis', broadcast=True)