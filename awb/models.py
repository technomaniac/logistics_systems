from __future__ import unicode_literals
import os
from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.core.exceptions import MultipleObjectsReturned
from django.db import models
from django.db.models import Q
from django.utils.formats import date_format

from common.models import Time_Model
from custom.forms import automodelform_factory
from utils.functionality import convert_request_querydict_to_dict, check_date_gte_current, change_date_format
from utils.constants import BRANCH_DICT
from utils.random import get_title
from logistics.settings import MEDIA_ROOT
from zoning.models import Zoning_Matrix


class Manifest(Time_Model):
    STATUS = (
        ('O', 'Open'),
        ('I', 'In-Scanned'),
        ('C', 'Closed'),
        ('A', 'Alert')
    )
    CATEGORY = (
        ('FL', 'Forward Logistics'),
        ('RL', 'Reverse Logistics'),
        ('MP', 'Money Pickup'),
    )
    client = models.ForeignKey('client.Client', db_index=True)
    uploaded_by = models.ForeignKey(User, db_index=True)
    warehouse = models.ForeignKey('client.Client_Warehouse', db_index=True , null=True, blank=True)
    branch = models.ForeignKey('internal.Branch', db_index=True, null=True,blank=True)
    status = models.CharField(choices=STATUS, default='O', max_length=1)
    category = models.CharField(choices=CATEGORY, max_length=2)
    file = models.FileField(upload_to='uploads/manifest/')

    class Meta:
        index_together = [['client', 'uploaded_by', 'warehouse', 'branch', 'status', 'category', 'file'], ]

    def get_readable_choice(self):
        return dict(self.CATEGORY)[self.category]

    def get_expected_awb_count(self):
        return self.awb_status_set.count()

    def get_in_scanned_awb_count(self):
        return self.awb_status_set.exclude(status='DR').count()

    def get_dispatched_awb_count(self):
        return self.awb_status_set.exclude(status='DRS').count()

    def get_delivered_count(self):
        return self.awb_status_set.filter(status='DEL').count()

    def get_delivered_perc(self):
        perc = (float(self.get_delivered_count()) / float(self.get_total_awb_count()) ) * 100
        return round(perc, 2)

    def get_pending_count(self):
        return self.awb_status_set.exclude(status__in=['DEL', 'CAN', 'RTO', 'DFR', 'PFR', 'ITR', 'RET']).count()

    def get_pending_perc(self):
        perc = (float(self.get_pending_count()) / float(self.get_total_awb_count()) ) * 100
        return round(perc, 2)

    def get_returned_count(self):
        return self.awb_status_set.filter(status='CAN').count()

    def get_returned_perc(self):
        perc = (float(self.get_returned_count()) / float(self.get_total_awb_count()) ) * 100
        return round(perc, 2)

    def get_rto_count(self):
        return self.awb_status_set.filter(status='RTO').count()

    def get_rto_perc(self):
        perc = (float(self.get_rto_count()) / float(self.get_total_awb_count()) ) * 100
        return round(perc, 2)

    def get_total_awb_count(self):
        return self.awb_status_set.count()

    def get_converted_awb_count(self):
        if self.category == 'RL':
            return self.awb_status_set.exclude(status__in=['DR', 'PP', 'DBC', 'SCH', 'CNA']).count()
        else:
            return ''

    def get_converted_perc(self):
        perc = (float(self.get_converted_awb_count()) / float(self.get_total_awb_count()) ) * 100
        return round(perc, 2)

    def get_cancelled_count(self):
        return self.awb_status_set.filter(status='CAN').count()

    def get_cancelled_perc(self):
        perc = (float(self.get_cancelled_count()) / float(self.get_total_awb_count())) * 100
        return round(perc, 2)

    def get_picked_up_count(self):
        return self.awb_status_set.exclude(status__in=['DR', 'PP', 'CAN', 'DBC', 'SCH', 'CNA']).count()

    def get_picked_up_perc(self):
        perc = (float(self.get_picked_up_count()) / float(self.get_total_awb_count())) * 100
        return round(perc, 2)

    def get_scheduled_count(self):
        return self.awb_status_set.filter(status__in=['DBC', 'SCH']).count()

    def get_scheduled_perc(self):
        perc = (float(self.get_scheduled_count()) / float(self.get_total_awb_count())) * 100
        return round(perc, 2)

    def get_out_for_pickup_count(self):
        return self.awb_status_set.filter(status__in=['DR', 'PP']).count()

    def get_out_for_pickup_perc(self):
        try:
            perc = (float(self.get_out_for_pickup_count()) / float(self.get_total_awb_count())) * 100
            return round(perc, 2)
        except ZeroDivisionError:
            return 0

    def get_dto_count(self):
        return self.awb_status_set.filter(status='DEL').count()

    def get_dto_perc(self):
        try:
            perc = (float(self.get_dto_count()) / float(self.get_picked_up_count())) * 100
            return round(perc, 2)
        except ZeroDivisionError:
            return 0

    def __unicode__(self):
        return self.file


class AWB(Time_Model):
    AWB_TYPE = (('COD', 'COD'),
                ('REV', 'Reverse Pickup'),
                ('PRE', 'Prepaid'),
                ('MPU', 'Money Pickup'))
    AWB_PRIORITY = (('L', 'LOW'),
                    ('N', 'Normal'),
                    ('H', 'High'),
                    ('U', 'Urgent'))
    awb = models.CharField(max_length=20, unique=True, verbose_name='AWB', db_index=True)
    order_id = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    crp_id = models.CharField(max_length=50, default='', blank=True)
    invoice_no = models.CharField(max_length=300, null=True, blank=True, db_index=True)
    customer_name = models.CharField(max_length=300, null=True, blank=True)
    address_1 = models.TextField(null=True, blank=True)
    address_2 = models.TextField(null=True, blank=True)
    area = models.CharField(max_length=400, null=True, blank=True)
    city = models.CharField(max_length=200, null=True, blank=True)
    pincode = models.ForeignKey('zoning.Pincode', db_index=True)
    vendor = models.ForeignKey('client.Client_Vendor', null=True, blank=True, db_index=True)
    phone_1 = models.CharField(max_length=15, null=True, blank=True, db_index=True)
    phone_2 = models.CharField(max_length=15, null=True, blank=True)
    package_value = models.CharField(max_length=20, null=True, blank=True)
    package_price = models.CharField(max_length=20, null=True, blank=True)
    expected_amount = models.FloatField(max_length=12, null=True, blank=True)
    weight = models.CharField(max_length=15, null=True, blank=True)
    length = models.CharField(max_length=15, null=True, blank=True)
    breadth = models.CharField(max_length=15, null=True, blank=True)
    height = models.CharField(max_length=15, null=True, blank=True)
    package_sku = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    category = models.CharField(choices=AWB_TYPE, max_length=3, null=True, blank=True)
    priority = models.CharField(choices=AWB_PRIORITY, max_length=1, default='N')
    barcode = models.ImageField(upload_to='awb/barcode/', null=True, blank=True)
    reason_for_return = models.TextField(blank=True, null=True)
    origin_branch = models.ForeignKey('internal.Branch', related_name='origin_branch', null=True,blank=True,db_index=True)
    destination_branch = models.ForeignKey('internal.Branch', related_name='destination_branch', null=True,blank=True,db_index=True)
    # preferred_pickup_date = models.CharField(max_length=20, null=True, blank=True)
    # preferred_pickup_time = models.CharField(max_length=20, null=True, blank=True)

    def get_readable_pincode(self):
        return self.pincode.pincode

    def get_readable_status(self):
        return self.awb_status.get_readable_choice()

    def get_status_last_updated(self):
        return self.awb_status.on_update.strftime('%d, %b, %Y %H:%M')

    def get_creation_date(self):
        return self.creation_date.strftime('%d, %b, %Y %H:%M')

    def get_vendor_name(self):
        try:
            return get_title((self.vendor.vendor_name).encode('utf-8'))
        except Exception:
            return ''

    def get_vendor_branch(self):
        try:
            return self.vendor.pincode.branch_pincode.branch
        except:
            return ''

    def get_vendor_code(self):
        try:
            return self.vendor.vendor_code.encode('utf-8')
        except Exception:
            return ''

    def get_current_tb(self):
        try:
            return self.awb_status.current_tb
        except:
            return None

    def current_co_loader_awb(self):
        try:
            return self.get_current_mts().co_loader_awb
        except:
            return None

    def current_co_loader(self):
        try:
            return self.get_current_mts().co_loader
        except:
            return None

    def mts_hand_over_date(self):
        try:
            return self.get_current_mts().creation_date
        except:
            return ''

    def get_current_mts(self):
        try:
            return self.awb_status.current_mts
        except:
            return None

    def get_current_drs(self):
        try:
            return self.awb_status.current_drs
        except:
            return None

    def get_current_dto(self):
        try:
            return self.awb_status.current_dto
        except:
            return None

    def get_current_rto(self):
        try:
            return self.awb_status.current_rto
        except:
            return None

    def get_priority(self):
        return dict(self.AWB_PRIORITY)[self.priority]

    def get_readable_choice(self):
        return dict(self.AWB_TYPE)[self.category]

    def get_current_fe(self):
        try:
            return self.get_current_drs().get_fe_full_name()
        except:
            return ''

    def get_call_count(self):
        return self.awb_history_set.filter(call=True, is_active=True).count()

    def get_delivery_branch(self):
        if self.category == 'REV':
            if self.vendor is not None:
                # try:
                # return self.vendor.pincode.branch_pincode.branch
                # except:
                return self.vendor.branch
            else:
                if self.origin_branch is None:
                    self.origin_branch = self.awb_status.manifest.branch
                    self.save()
                else:
                    self.awb_status.manifest.branch = self.origin_branch
                    self.awb_status.manifest.branch.save()
                return self.origin_branch

        else:
            try:
                if self.destination_branch is None or self.destination_branch != self.pincode.branch_pincode.branch:
                    self.destination_branch = self.pincode.branch_pincode.branch
                    self.save()
                return self.destination_branch
            except:
                return ''

    def get_rto_branch(self):
        if self.category != 'REV':
            if self.origin_branch is None:
                self.origin_branch = self.awb_status.manifest.branch
                self.save()
            else:
                self.awb_status.manifest.branch = self.origin_branch
                self.awb_status.manifest.branch.save()
            return self.origin_branch
        else:
            return ''

    def get_pickup_branch(self):
        try:
            if self.category == 'REV':
                if self.destination_branch is None or self.destination_branch != self.pincode.branch_pincode.branch:
                    self.destination_branch = self.pincode.branch_pincode.branch
                    self.save()
                return self.destination_branch
            else:
                return ''
        except:
            return ''

    def get_transfer_mode(self):
        mts_hist = self.awb_history_set.exclude(mts=None).order_by('creation_date')
        transfer_mode = ''
        for h in xrange(len(mts_hist)):
            transfer_mode += mts_hist[h].mts.get_readable_type()
            if h < len(mts_hist) - 1:
                transfer_mode += ' + '
        return transfer_mode

    def get_transfer_mode_absolute(self):
        mts_hist = self.awb_history_set.exclude(mts=None).order_by('creation_date')
        if len([h for h in mts_hist if h.mts.type == 'A']) > 0:
            return 'Air'
        elif len([h for h in mts_hist if h.mts.type == 'S']) > 0:
            return 'Surface'
        elif len([h for h in mts_hist if h.mts.type == 'V']) > 0:
            return 'Van'

    def get_origin_city(self):
        if self.category == 'REV':
            return self.get_pickup_branch().city
        else:
            return self.awb_status.manifest.branch.city

    def get_destination_city(self):
        try:
            return self.get_delivery_branch().city
        except:
            return None

    def get_picked_date(self):
        try:
            if self.category == 'REV':
                return self.awb_history_set.filter(status='PC').order_by('-creation_date')[0].creation_date
            else:
                return self.awb_history_set.filter(status='ISC').order_by('-creation_date')[0].creation_date
        except:
            return None

    def get_connected_date(self):
        try:
            if self.category == 'REV':
                return self.awb_history_set.exclude(mts=None).order_by('-creation_date')[0].creation_date
            else:
                return self.awb_history_set.filter(status='ISC').order_by('-creation_date')[0].creation_date
        except:
            return None

    def get_expected_delivery_date(self):
        try:
            # pickup_date = self.get_picked_date()
            connected_date = self.get_connected_date()
            zone = Zoning_Matrix.objects.filter(origin_city=self.get_origin_city(),
                                                destination_city=self.get_destination_city())
            zone = zone.first()
            if zone:
                mode = self.get_transfer_mode_absolute()
                expected_time = {}
                if mode == 'Air':
                    expected_time['min'] = zone.air_min
                    expected_time['max'] = zone.air_max
                elif mode == 'Van':
                    expected_time['min'] = zone.van
                    expected_time['max'] = zone.van
                else:
                    expected_time['min'] = zone.surface_min
                    expected_time['max'] = zone.surface_max
                    # return expected_time
                expected_date = connected_date + timedelta(expected_time['max'])
                return expected_date.strftime('%d, %b , %Y')
            else:
                return None
        except:
            return None

    def get_awb_history(self, internal=True, superuser=False):
        hist_dict = {}
        if superuser:
            awb_hist = self.awb_history_set.order_by('-creation_date')
        else:
            awb_hist = self.awb_history_set.filter(is_active=True).order_by('-creation_date')
        i = 0
        for h in awb_hist:
            if h.status != None and h.tb == None and h.mts == None and h.drs == None and h.dto == None and h.rto == None:
                hist_dict[i] = {}
                if not h.is_active:
                    hist_dict[i]['class'] = 'red'
                hist_dict[i]['pk'] = h.pk
                try:
                    hist_dict[i]['branch'] = h.branch.branch_name
                except AttributeError:
                    hist_dict[i]['branch'] = ''
                hist_dict[i]['status'] = get_awb_status('status', h.status, h, hist_dict[i]['branch'], self, internal)
                hist_dict[i]['time'] = h.creation_date.strftime('%d %b, %Y , %H:%M')
                try:
                    hist_dict[i]['updated_by'] = h.updated_by.first_name + ' ' + h.updated_by.last_name
                except Exception:
                    pass
                i = i + 1

            if h.tb != None:
                hist_dict[i] = {}
                if not h.is_active:
                    hist_dict[i]['class'] = 'red'
                hist_dict[i]['pk'] = h.pk
                hist_dict[i]['branch'] = h.tb.origin_branch.branch_name
                hist_dict[i]['status'] = get_awb_status('tb', h.tb.tb_id, h, hist_dict[i]['branch'], self, internal)
                hist_dict[i]['time'] = h.creation_date.strftime('%d %b, %Y , %H:%M')
                try:
                    hist_dict[i]['updated_by'] = h.updated_by.first_name + ' ' + h.updated_by.last_name
                except Exception:
                    pass
                i = i + 1

            if h.mts != None and internal == True:
                hist_dict[i] = {}
                if not h.is_active:
                    hist_dict[i]['class'] = 'red'
                hist_dict[i]['pk'] = h.pk
                hist_dict[i]['branch'] = h.mts.from_branch.branch_name
                hist_dict[i]['status'] = get_awb_status('mts', h.mts.mts_id, h, hist_dict[i]['branch'], self, internal)
                hist_dict[i]['time'] = h.creation_date.strftime('%d %b, %Y , %H:%M')
                try:
                    hist_dict[i]['updated_by'] = h.updated_by.first_name + ' ' + h.updated_by.last_name
                except Exception:
                    pass
                i = i + 1

            if h.drs != None:
                hist_dict[i] = {}
                if not h.is_active:
                    hist_dict[i]['class'] = 'red'
                hist_dict[i]['pk'] = h.pk
                hist_dict[i]['branch'] = h.drs.branch.branch_name
                hist_dict[i]['status'] = get_awb_status('drs', h.drs.drs_id, h, hist_dict[i]['branch'], self, internal)
                hist_dict[i]['time'] = h.creation_date.strftime('%d %b, %Y , %H:%M')
                try:
                    hist_dict[i]['updated_by'] = h.updated_by.first_name + ' ' + h.updated_by.last_name
                except Exception:
                    pass
                i = i + 1

            if h.dto != None:
                hist_dict[i] = {}
                if not h.is_active:
                    hist_dict[i]['class'] = 'red'
                hist_dict[i]['pk'] = h.pk
                hist_dict[i]['branch'] = h.dto.branch.branch_name
                hist_dict[i]['status'] = get_awb_status('dto', h.dto.dto_id, h, hist_dict[i]['branch'], self, internal)
                hist_dict[i]['time'] = h.creation_date.strftime('%d %b, %Y , %H:%M')
                try:
                    hist_dict[i]['updated_by'] = h.updated_by.first_name + ' ' + h.updated_by.last_name
                except Exception:
                    pass
                i = i + 1

            if h.rto != None:
                hist_dict[i] = {}
                if not h.is_active:
                    hist_dict[i]['class'] = 'red'
                hist_dict[i]['pk'] = h.pk
                hist_dict[i]['branch'] = h.rto.branch.branch_name
                hist_dict[i]['status'] = get_awb_status('rto', h.rto.rto_id, h, hist_dict[i]['branch'], self, internal)
                hist_dict[i]['time'] = h.creation_date.strftime('%d %b, %Y , %H:%M')
                try:
                    hist_dict[i]['updated_by'] = h.updated_by.first_name + ' ' + h.updated_by.last_name
                except Exception:
                    pass
                i = i + 1

        return hist_dict


    def __unicode__(self):
        return self.awb

    def get_client(self):
        try:
            return self.awb_status.manifest.client.client_name
        except:
            return None

    def get_client_obj(self):
        return self.awb_status.manifest.client

    def get_full_address(self):
        try:
            address = self.address_1.decode('utf-8').encode('utf-8')
        except Exception:
            address = self.address_1

        if self.address_2 != '':
            try:
                address += ', ' + self.address_2.decode('utf-8').encode('utf-8')
            except Exception:
                address += ', ' + self.address_2

        if self.city:
            try:
                address += ', ' + self.city.encode('utf-8')
            except Exception:
                address += ', ' + self.city

        try:
            return address.encode('utf-8')
        except Exception:
            return address

    def get_customer_name(self):
        try:
            return get_title(self.customer_name.encode('utf-8'))
        except Exception:
            return get_title(self.customer_name)

    def get_phone_1(self):
        try:
            return self.phone_1.encode('utf-8')
        except Exception:
            return self.phone_1

    def get_weight(self):
        try:
            return self.weight.encode('utf-8')
        except Exception:
            return self.weight

    def get_height(self):
        try:
            return self.height.encode('utf-8')
        except Exception:
            return self.height

    def get_length(self):
        try:
            return self.length.encode('utf-8')
        except Exception:
            return self.length

    def get_breadth(self):
        try:
            return self.breadth.encode('utf-8')
        except Exception:
            return self.breadth

    def get_pincode(self):
        # try:
        # return str(self.pincode.pincode).encode('utf-8')
        # except Exception:
        return self.pincode.pincode

    def get_package_value(self):
        try:
            return self.package_value.encode('utf-8')
        except Exception:
            return self.package_value

    def get_description(self):
        try:
            return self.description.encode('utf-8')
        except Exception:
            return self.description

    def get_order_id(self):
        try:
            return self.order_id.encode('utf-8')
        except Exception:
            return self.order_id

    def get_awb(self):
        try:
            return self.awb.encode('utf-8')
        except Exception:
            return self.awb

    def get_remark(self):
        try:
            return self.awb_status.remark.encode('utf-8')
        except Exception:
            return self.awb_status.remark

    def get_reason(self):
        try:
            return self.awb_status.reason.encode('utf-8')
        except Exception:
            return self.awb_status.reason

    def get_drs_count(self):
        try:
            return self.awb_history_set.exclude(drs=None).count()
        except Exception:
            return ''

    def get_first_pending(self):
        try:
            return date_format(self.awb_history_set.filter(status='DCR').order_by('creation_date')[0].creation_date,
                               "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_first_dispatch(self):
        try:
            return date_format(self.awb_history_set.exclude(drs=None).order_by('creation_date')[0].creation_date,
                               "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_last_dispatch(self):
        try:
            return date_format(self.awb_history_set.exclude(drs=None).order_by('-creation_date')[0].creation_date,
                               "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_pickup_date(self):
        try:
            return date_format(self.awb_history_set.filter(status='PC').order_by('-creation_date')[0].creation_date,
                               "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_upload_date(self):
        try:
            self.awb_history_set.filter(status='DR').order_by('-creation_date')[0].creation_date
        except Exception:
            return ''

    def in_transit_pendency_cond(self):
        try:
            p = self.awb_history_set.exclude(mts=None).order_by('-creation_date')[0].creation_date
            # z = self.awb_history_set.exclude(tb=None).order_by('-creation_date')[0].creation_date
            # except Exception:
            # p = ''
            # try:
            c = datetime.now()
            # if z != '':
            #     if abs(c - z) > 5:
            #         print self
            #         return True
            # else:
            # # except Exception:
            # # i = ''
            #     print self
            mode = self.get_transfer_mode_absolute()
            # try:
            diff = abs(c - p)
            if mode == 'Air':
                if diff.days > 2:
                    return True
                else:
                    return False
            elif mode == 'Surface':
                if diff.days > 7:
                    return True
                else:
                    return False
            elif mode == 'Van':
                if diff.days > 0:
                    return True
                else:
                    return False
        except:
            return False
            # except:
            # return False

    def in_transit_mts_pendency_cond(self):
        try:
            z=self.awb_history_set.order_by('-creation_date')[0].creation_date
            # print self,z
            c = datetime.now()
            if abs(c-z).days > 4:
                # print self
                return True
            else:
                # print self, 'else'
                return False
        except:
            return False

    def awb_can_calling_cond(self):
        try:
            if 1 <= self.get_call_count() <= 2:
                d = self.awb_history_set.select_related().filter(updated_by__profile__role__in=['CS'],
                                                                 remark__in=['Couldn\'t speak to customer',
                                                                             'Customer deferring more than 3 days']).order_by(
                    '-creation_date')[0].creation_date
            else:
                d = self.awb_history_set.select_related().order_by('-creation_date')[0].creation_date
        except Exception:
            d = ''
        c = datetime.now()
        try:
            diff = abs(c - d)
            if diff.days > 0:
                return False
            else:
                return True
        except:
            return False


    def dto_pendency_cond(self):
        try:
            d = self.awb_history_set.filter(status='DCR').order_by('-creation_date')[0].creation_date
        except Exception:
            d = ''
        c = datetime.now()
        try:
            diff = abs(c - d)
            if diff.days > 0:
                return True
            else:
                return False
        except:
            return False
    def data_received_cond(self):
        try:
            p = self.awb_history_set.filter(status='DR').order_by('-creation_date')[0].creation_date
        except Exception:
            p = ''
        c = datetime.now()
        try:
            diff = abs(c - p)
            if diff.days <= 5:
                return True
            else:
                return False
        except:
            return False
    def not_attempted_cond(self):
        try:
            p = self.awb_history_set.filter(status='NA').order_by('-creation_date')[0].creation_date
        except Exception:
            p = ''
        c = datetime.now()
        try:
            diff = abs(c - p)
            if diff.days >= 0:
                return True
            else:
                return False
        except:
            return False

    def pickup_pendency_cond(self):
        try:
            p = self.awb_history_set.filter(status='PC').order_by('-creation_date')[0].creation_date
        except Exception:
            p = ''
        c = datetime.now()
        try:
            diff = abs(c - p)
            if diff.days > 1:
                return True
            else:
                return False
        except:
            return False

    def get_picked_up_in_days(self):
        try:
            d = self.awb_history_set.filter(status='DR').order_by('-creation_date')[0].creation_date
        except Exception:
            d = ''
        try:
            p = self.awb_history_set.filter(status='PC').order_by('-creation_date')[0].creation_date
        except Exception:
            p = ''
        try:
            diff = abs(d - p)
            return diff.days
        except:
            return ''

    def get_dispatched_before_time(self, time):
        try:
            d = self.awb_history_set.exclude(drs=None).order_by('-creation_date')[0].creation_date
            dis_time = datetime.strptime(d.strftime('%H:%M'), '%H:%M')
            check_time = datetime.strptime(time, '%H:%M')
            if dis_time <= check_time:
                return True
            else:
                return False
        except Exception:
            return False

    def get_delivered_in_days(self):
        try:
            p = self.awb_history_set.filter(status='PC').order_by('-creation_date')[0].creation_date
        except Exception:
            p = ''
        try:
            d = self.awb_history_set.filter(status__in=['DEL', 'DTO']).order_by('-creation_date')[0].creation_date
        except Exception:
            d = ''
        try:
            diff = abs(p - d)
            return diff.days
        except:
            return ''

    def get_scheduled_in_days(self):
        d = self.creation_date
        try:
            h = self.awb_history_set.filter(status__in=['SCH', 'DBC', 'CNA', 'NA', 'DR']).order_by('-creation_date')
        except Exception:
            h = ''
        if len(h) > 1:
            try:
                diff = abs(d - h[1].creation_date)
                return diff.days
            except:
                return ''
        else:
            return 0

    def get_deferred_date(self):
        try:
            date = self.awb_history_set.filter(status='DBC').order_by('-creation_date')[0].reason
        except:
            date = ''
        if date == '':
            try:
                date = self.awb_status.reason
            except:
                date = ''
        try:
            return datetime.strptime(date, '%Y-%m-%d').strftime('%d %b, %Y')
        except:
            return ''

    def get_reason_for_cancellation(self):
        try:
            return self.awb_history_set.filter(status='CAN').order_by('-creation_date')[0].reason,
        except Exception:
            return ''

    def get_delivered_date(self):
        try:
            return date_format(self.awb_history_set.filter(status='DEL').order_by('-creation_date')[0].creation_date,
                               "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_dto_creation_date(self):
        try:
            return date_format(self.awb_history_set.exclude(dto=None).order_by('-creation_date')[0].creation_date,
                               "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_cancel_date(self):
        try:
            return date_format(self.awb_history_set.filter(status='CAN').order_by('-creation_date')[0].creation_date,
                               "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_last_status_updated_date(self):
        try:
            return date_format(self.awb_history_set.exclude(status='').order_by('-creation_date')[0].creation_date,
                               "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_last_scan(self):
        if self.category == 'REV':
            try:
                return date_format(self.awb_history_set.exclude(dto=None).order_by('-creation_date')[0].creation_date,
                                   "SHORT_DATETIME_FORMAT")
            except Exception:
                return ''
        else:
            try:
                return date_format(self.awb_history_set.exclude(drs=None).order_by('-creation_date')[0].creation_date,
                                   "SHORT_DATETIME_FORMAT")
            except Exception:
                return ''

    def get_first_scan_branch(self):
        if self.category == 'REV':
            try:
                if self.get_pickup_branch() == self.get_delivery_branch():
                    return self.awb_history_set.filter(status='DCR').order_by('creation_date')[0].branch.branch_name
                else:
                    return self.awb_history_set.filter(status='PC').order_by('creation_date')[0].branch.branch_name
            except Exception:
                return ''
        else:
            try:
                return self.awb_history_set.filter(status='ISC').order_by('creation_date')[0].branch.branch_name
            except Exception:
                return ''

    def get_last_call_made_time(self):
        try:
            return date_format(
                self.awb_history_set.filter(updated_by__profile__role='CS').order_by(
                    '-creation_date')[0].creation_date, "SHORT_DATETIME_FORMAT")
        except Exception:
            return ''

    def get_cs_call_count(self):
        try:
            return self.awb_history_set.filter(updated_by__profile__role='CS').count()
        except Exception:
            return ''

    def get_no_of_attempts_count(self):
        try:
            return self.awb_history_set.filter(status='NA').count()
        except Exception:
            return ''

    def get_customer_not_available_count(self):
        try:
            return self.awb_history_set.filter(status='CNA').count()
        except Exception:
            return ''

    def get_status_on_date(self, date):
        try:
            status = self.awb_history_set.filter(creation_date__lte=date + ' 23:59:59').order_by('-creation_date')[
                0].status
            return resolve_status(status, self.awb)
        except Exception:
            return ''

    def get_current_history_obj(self, type):
        try:
            return self.awb_history_set.exclude(type=None).order_by('-creation_date')[0].type
        except Exception:
            return ''

    # emergency functions :P ...created to restore AWB_Status table from AWB_History...
    def get_current_branch_hist(self):
        try:
            return self.awb_history_set.exclude(branch=None).order_by('-creation_date')[0].branch.pk
        except Exception:
            return ''

    def get_current_tb_hist(self):
        try:
            return self.awb_history_set.exclude(tb=None).order_by('-creation_date')[0].tb.pk
        except Exception:
            return ''

    def get_current_mts_hist(self):
        try:
            return self.awb_history_set.exclude(mts=None).order_by('-creation_date')[0].mts.pk
        except Exception:
            return ''

    def get_current_drs_hist(self):
        try:
            return self.awb_history_set.exclude(drs=None).order_by('-creation_date')[0].drs.pk
        except Exception:
            return ''

    def get_current_dto_hist(self):
        try:
            return self.awb_history_set.exclude(dto=None).order_by('-creation_date')[0].dto.pk
        except Exception:
            return ''

    def get_current_rto_hist(self):
        try:
            return self.awb_history_set.exclude(rto=None).order_by('-creation_date')[0].rto.pk
        except Exception:
            return ''

    def get_current_fe_hist(self):
        try:
            return self.awb_history_set.exclude(fe=None).order_by('-creation_date')[0].fe.pk
        except Exception:
            return ''

    def get_awb_age(self):
        try:
            d = datetime.now() - self.creation_date
            return d.days
        except Exception:
            return ''

    def get_sms_notification(self):
        try:
            if self.awb_status.manifest.client.additional.sms_notification:
                return True
            else:
                return False
        except Exception:
            return False

    # Nirmal Baba functions -----

    def get_reverse_actions(self):

        # print 'reverse actions'
        # if self.awb_status.status == 'NIN':
        # return {'awb_actions': [{'awb_action_string': 'Pickup Initiated', 'href': "#", 'action_type': 'PIN'},
        # {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'}]}

        if self.awb_status.status == 'PP':
            return {'awb_actions': [{'awb_action_string': 'Customer not Available', 'href': "#", 'action_type': 'CNA'},
                                    {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'},
                                    {'awb_action_string': 'Deferred By Customer', 'href': "deffered_by_customer.html",
                                     'action_type': 'DBC'},
                                    {'awb_action_string': 'Pickup Completed',
                                     "href": "reverse_pickup_complete.html", 'action_type': 'PC'},
            ]}

            # elif self.awb_status.status == 'PC':
            # return {'awb_actions': [
            # {'awb_action_string': 'Delivered to Origin', 'href': "delievered_to_store.html", 'action_type': 'DTO'}]}
            # else:
            # return {'awb_actions': [{'awb_action_string': 'Pickup Initiated', 'href': "#", 'action_type': 'PIN'},
            # {'awb_action_string': 'Not Initiated', 'href': "#", 'action_type': 'NIN'}]}

    def get_pre_actions(self):
        # if self.awb_status.status == 'NIN':
        # return {'awb_actions': [{'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'},
        # {'awb_action_string': 'Delivery Initiated', 'href': "#", 'action_type': 'DIN'}]}

        # if self.awb_status.status == 'DR':
        # return {'awb_actions': [
        # {'awb_action_string': 'DRS not yet made', "href": "#",
        # 'action_type': 'PCL'},
        # # {'awb_action_string': 'Shipment Received', 'href': "#", 'action_type': 'SRC'},
        # {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'}]}

        if self.awb_status.status == 'DRS':
            return {'awb_actions': [
                {'awb_action_string': 'Customer not Available', 'href': "#", 'action_type': 'CNA'},
                {'awb_action_string': 'Deferred By Customer', 'href': "deffered_by_customer.html",
                 'action_type': 'DBC'},
                {'awb_action_string': 'Delivered to customer', 'href': "delievered_to_customer_pre.html",
                 'action_type': 'DEL'},
                {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'},
            ]}

            # elif self.awb_status.status == ['DBC', 'CNA']:
            #     return {'awb_actions': [
            #         {'awb_action_string': 'Returned to Origin', 'href': "delievered_to_store.html", 'action_type': 'RTO'}]}

            # else:
            #     return {'awb_actions': [{'awb_action_string': 'Delivery Initiated', 'href': "#", 'action_type': 'DIN'},
            #                             {'awb_action_string': 'Not Initiated', 'href': "#", 'action_type': 'NIN'}]}

    def get_cod_actions(self):
        # if self.awb_status.status == 'NIN':
        # return {'awb_actions': [{'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'},
        # {'awb_action_string': 'Delivery Initiated', 'href': "#", 'action_type': 'DIN'}]}
        #
        # if self.awb_status.status == 'DIN':
        # return {'awb_actions': [
        #         {'awb_action_string': 'Picked up from Store/Warehouse', "href": "pickup_completed.html",
        #          'action_type': 'PCL'},
        #         # {'awb_action_string': 'Shipment Received', 'href': "#", 'action_type': 'SRC'},
        #         {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'}]}

        if self.awb_status.status == 'DRS':
            return {'awb_actions': [
                {'awb_action_string': 'Customer not Available', 'href': "#", 'action_type': 'CNA'},
                {'awb_action_string': 'Deferred By Customer', 'href': "deffered_by_customer.html",
                 'action_type': 'DBC'},
                {'awb_action_string': 'Delivered to customer', 'href': "delievered_to_customer_cod.html",
                 'action_type': 'DEL'},
                {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'},
            ]}
            #
            # elif self.awb_status.status in ['DBC', 'CNA']:
            # return {'awb_actions': [
            # {'awb_action_string': 'Returned to Origin', 'href': "delievered_to_store.html", 'action_type': 'RTO'}]}
            # else:
            # return {'awb_actions': [{'awb_action_string': 'Delivery Initiated', 'href': "#", 'action_type': 'DIN'},
            # {'awb_action_string': 'Not Initiated', 'href': "#", 'action_type': 'NIN'}]}

    # def get_money_pickup_actions(self):
    # if self.awb_status.status == 'NIN':
    # return {'awb_actions': [{'awb_action_string': 'Pickup Initiated', 'href': "#", 'action_type': 'PIN'},
    # {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'}]}
    #
    # elif self.awb_status.status == 'PIN':
    # return {'awb_actions': [{'awb_action_string': 'Customer not Available', 'href': "#", 'action_type': 'CNA'},
    # {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type': 'CAN'},
    # {'awb_action_string': 'Deferred By Customer', 'href': "deffered_by_customer.html",
    # 'action_type': 'DBC'},
    # {'awb_action_string': 'Reverse Pickup Completed',
    # "href": "reverse_pickup_complete.html", 'action_type': 'PCL'},
    # ]}
    #
    # elif self.awb_status.status == 'PCL':
    #         return {'awb_actions': [
    #             {'awb_action_string': 'Delivered to Origin', 'href': "delievered_to_store.html", 'action_type': 'DTO'}]}
    #     else:
    #         return {'awb_actions': [{'awb_action_string': 'Pickup Initiated', 'href': "#", 'action_type': 'PIN'},
    #                                 {'awb_action_string': 'Not Initiated', 'href': "#", 'action_type': 'NIN'}]}

    def get_awb_actions(self, request):
        # print 'in non sony get_awb_actinos'
        category = self.category.strip()
        #print category
        phone_list = []
        rt_dict = {}
        if self.phone_1:
            phone_list.append({'awb_action_string': 'Call customer phone 1', 'href': "tel:+91" + str(self.phone_1),
                               'action_type': 'phone1'})
        if self.phone_2:
            phone_list.append({'awb_action_string': 'Call customer phone 2', 'href': "tel:+91" + str(self.phone_2),
                               'action_type': 'phone2'})
        if category == 'REV':
            rt_dict = self.get_reverse_actions()

        if category == 'COD':
            rt_dict = self.get_cod_actions()

        if category == 'PRE':
            # print 'in prepaid'
            rt_dict = self.get_pre_actions()
            # if category == 'MPU':
            #     return self.get_money_pickup_actions()
        # print rt_dict,'rt dict'
        if rt_dict:
            rt_dict['awb_actions'].extend(phone_list)
            #print rt_dict
            return rt_dict
        else:
            return {'http_status': 400, 'msg': 'AWB already cleared contact website admin'}

    def handle_changes_from_app(self, request):
        action = request.POST.get('action')
        awb_type = self.category

        #if awb_type == 'REV':
        if action in zip(*self.awb_status.STATUS)[0]:
            if action == 'PC' or action == 'DEL':
                if awb_type == 'REV' or awb_type == 'PRE':
                    #     self.awb_status.status = action
                    #     self.awb_status.current_branch = request.user.profile.branch
                    #     self.awb_status.updated_by = request.user
                    #     self.awb_status.save()

                    AWB_Status.objects.filter(awb=self).update(status=action,
                                                               current_branch=request.user.profile.branch,
                                                               updated_by=request.user)

                elif awb_type == 'COD':
                    cod_amount = request.POST.get('cod_amount')
                    # print cod_amount
                    if round(float(cod_amount)) != round(self.expected_amount):
                        return {'http_status': 400, 'msg': 'Wrong Cod Amount'}
                    AWB_Status.objects.filter(awb=self).update(status=action, collected_amt=cod_amount,
                                                               current_branch=request.user.profile.branch,
                                                               updated_by=request.user)
                AWB_History.objects.create(awb=self, status=action,
                                           branch=self.awb_status.current_branch,
                                           updated_by=request.user)

            if action == 'CAN':
                AWB_Status.objects.filter(awb=self).update(status='CAN', remark=request.POST['reason'],
                                                           updated_by=request.user)
                AWB_History.objects.create(awb=self, status='CAN', remark=request.POST['reason'],
                                           updated_by=request.user)
            if action == 'DBC':
                date = change_date_format(request.POST['deffer_date'], '%Y-%m-%d', '%Y-%m-%d')
                if check_date_gte_current(date, '%Y-%m-%d'):
                    AWB_Status.objects.filter(awb=self).update(status='DBC', reason=date,
                                                               updated_by=request.user)
                    AWB_History.objects.create(awb=self, status='DBC', reason=date,
                                               updated_by=request.user)
                else:
                    return {'http_status': 400, 'msg': 'Invalid Date'}

            if action == 'CNA':
                AWB_Status.objects.filter(awb=self).update(status='CNA',
                                                           updated_by=request.user)
                AWB_History.objects.create(awb=self, status='CNA', updated_by=request.user)
        else:
            return {'http_status': 400, 'msg': 'Invalid Status'}

        # if awb_type == 'COD':
        #     if action in zip(*self.awb_status.STATUS)[0]:
        #         cod_amount = request.POST.get('cod_amount')
        #         if cod_amount and int(cod_amount) != self.expected_amount:
        #             return {'http_status': 400, 'msg': 'Wrong Cod Amount'}
        #         self.awb_status.status = request.POST.get('action')
        #         self.save()
        #     else:
        #         return {'http_status': 400, 'msg': 'COD Invalid Status'}
        #
        # if awb_type == 'PRE':
        #     if action in zip(*self.awb_status.STATUS)[0]:
        #         self.awb_status.status = request.POST.get('action')
        #         self.save()
        #     else:
        #         return {'http_status': 400, 'msg': 'Prepaid Invalid Status'}

        # if awb_type == 'MPU':
        #     if action in zip(*self.FORWARD_SHIPMENT_STATUS)[0]:
        #         cod_amount = request.POST.get('cod_amount')
        #         if int(cod_amount) != self.expected_amount:
        #             return {'http_status': 400, 'msg': 'Wrong Cod Amount'}
        #         self.awb_status.status = request.POST.get('action')
        #         self.save()
        #     else:
        #         return {'http_status': 400, 'msg': 'MPU Invalid Status'}
        #
        # if awb_type == 'PTP':
        #     if action in zip(*self.PTP_SHIPMENT_STATUS)[0]:
        #         self.awb_status.status = request.POST.get('action')
        #         self.save()
        #     else:
        #         return {'http_status': 400, 'msg': 'MPU Invalid Status'}

        if request.POST.get('action') in self.get_image_status():
            #print 'svaing image'
            return self.save_shipment_image(request)
        return {'http_status': 200}

    def get_image_status(self):
        return ['PC', 'DEL']

    def save_shipment_image(self, request):
        if request.POST.get('action') in self.get_image_status():
            shipment_image_form_class = automodelform_factory(model=AWB_Images, foreign_keys=['awb'])
            invoice_image_form_class = automodelform_factory(model=AWB_Images, foreign_keys=['awb'])
            image_data = convert_request_querydict_to_dict(request.POST)
            # print 'saving image in shipment  image'
            # print self.awb_status.status
            # print image_data['type']
            try:
                _awb_status = AWB_Status.objects.get(awb=self)
            except:
                return {'http_status': 400, 'msg': 'Wrong awb given'}
            image_data.update({'awb': self.pk})
            if _awb_status.status == 'PC' and 'invoice' in request.FILES and 'shipment' in request.FILES:  #and image_data['type'] == 'SHI':  #todo statuses for shipment images
                #AWB_Images.objects.create(image=request.FILES.)
                #print 'in shipment image'
                #
                image_data.update({'type': 'SHI'})
                request.FILES['image'] = request.FILES['shipment']
                shipment_image_form = shipment_image_form_class(image_data, request.FILES, awb=self)
                if shipment_image_form.is_valid():
                    shipment_image_form.save(commit=True)

                image_data.update({'type': 'INV'})
                request.FILES['image'] = request.FILES['invoice']
                invoice_image_form = invoice_image_form_class(image_data, request.FILES, awb=self)
                if invoice_image_form.is_valid():
                    invoice_image_form.save(commit=True)
                    return {'http_status': 200}
                else:
                    #print shipment_image_form.errors
                    return {'http_status': 400, 'msg': 'invalid image data'}

            if _awb_status.status == 'DEL' and 'invoice' in request.FILES:  #todo statuses for invoice images
                #print 'in invoice image'
                image_data.update({'type': 'INV'})
                request.FILES['image'] = request.FILES['invoice']
                invoice_image_form = invoice_image_form_class(image_data, request.FILES, awb=self)
                if invoice_image_form.is_valid():
                    invoice_image_form.save(commit=True)
                    return {'http_status': 200}
                else:
                    #print invoice_image_form.errors
                    return {'http_status': 400, 'msg': 'invalid image data'}

            return {'http_status': 400, 'msg': 'Error'}
        else:
            #print 'invalid action for image'
            return {'http_status': 400, 'msg': 'Invalid action for image'}

            #Nirmal Baba Seva Samapt

    def get_awb_shipment_video(self):
        try:
            return str(self.awb_images_set.filter(type='VID').first().image)
        except:
            pass

    # # def get_invoice_image(self):
    # #     try:
    # #         return <a href='http:/ship.nuvoex.com/' + MEDIA_URL + str(self.awb_images_set.filter(type='INV').first().image)>Invoice Image</a>
    # #     except:
    # #         pass
    # # get_invoice_image.allow_tags = True
    #
    # def get_shipment_image(self):
    #     try:
    #         return MEDIA_ROOT + str(self.awb_images_set.filter(type='SHI').first().image)
    #     except:
    #         pass

    def get_invoice_image(self):
        try:
            return self.awb_images_set.filter(type='INV').first().image
        except:
            pass
    def get_shipment_image(self):
        try:
            return self.awb_images_set.filter(type='SHI').first().image
        except:
            pass

    def get_call_client_remark(self):
        try:
            if self.awb_status.status == 'SCH':
                return 'CUSTOMER NEEDS A PICK UP'.encode('utf-8')
            else:
                remark = self.get_remark()
                reason = self.get_reason()
                if remark == 'Customer does not want to return':
                    return 'CUSTOMER DOES NOT WANT TO RETURN THE PRODUCT'.encode('utf-8')
                elif remark == 'Customer deferring more than 3 days':
                    return 'CUSTOMER ASKING FOR DELAY PICK UP'.encode('utf-8')
                elif remark == 'Customer wants replacement/refund first':
                    return 'CUSTOMER WANTS REPLACEMENT REFUND FIRST'.encode('utf-8')
                elif remark == 'Pickup already done by self/other courier':
                    return 'CUSTOMER HAS SENT THE PRODUCT THROUGH OTHER COURIER'.encode('utf-8')
                elif remark == 'Couldn\'t speak to customer':
                    return 'CUSTOMER IS NOT CONTACTABLE'.encode('utf-8')
                elif remark == 'Incorrect update : by branch':
                    return 'CUSTOMER NEEDS A PICK UP'.encode('utf-8')
                elif remark == 'Defer : Customer Initiated':
                    return 'CUSTOMER ASKING FOR DELAY PICK UP'.encode('utf-8')
                elif remark == 'Dispatched / Pickedup already':
                    return 'CUSTOMER NEEDS A PICK UP'.encode('utf-8')
                elif remark == 'Delivery not done yet':
                    return 'WRONG COMPLAINT'.encode('utf-8')
                elif remark == 'Customer wants to talk to client':
                    return 'CUSTOMER WANTS TO TALK WITH SNAPDEAL'.encode('utf-8')
                elif remark == 'Customer out of station':
                    return 'CUSTOMER OUT OF STATION'.encode('utf-8')
                elif remark == 'Address update':
                    return 'PICK UP REQUIRED FROM ANOTHER ADDRESS'.encode('utf-8')
                elif remark in ['No service zone', 'Brand packaging not available', 'Wants to give partial product', 'Number Not provided']:
                    return 'Separately sent to ritika'.encode('utf-8')
                elif remark == 'Delivery Not taken':
                    return 'Wrong complaint'.encode('utf-8')
                elif remark == 'Send With /Against other AWB no':
                    return 'CUSTOMER HAS SENT THE PRODUCT THROUGH OTHER COURIER'.encode('utf-8')
                elif remark == '' and reason != '':
                    return 'CUSTOMER NEEDS A PICK UP'.encode('utf-8')
                else:
                    return ''.encode('utf-8')
        except:
            return ''.encode('utf-8')

    def get_call_client_status(self):
        try:
            if self.awb_status.status == 'SCH':
                return 'RESCHEDULED'.encode('utf-8')
            else:
                remark = self.get_remark()
                reason = self.get_reason()
                if remark == 'Customer does not want to return':
                    return 'CDNS'.encode('utf-8')
                elif remark in ['Customer deferring more than 3 days', 'Customer wants replacement/refund first', 'Customer wants to talk to client',
                                'Customer out of station', 'Address update']:
                    return 'CUSTOMER DELAY'.encode('utf-8')
                elif remark == 'Pickup already done by self/other courier':
                    return 'HANDED OVER TO COURIER'.encode('utf-8')
                elif remark == 'Couldn\'t speak to customer':
                    return 'NON CONNECT'.encode('utf-8')
                elif remark in ['Incorrect update : by branch', 'Defer : Customer Initiated', 'Dispatched / Pickedup already']:
                    return 'RESCHEDULED'.encode('utf-8')
                elif remark == 'Delivery not done yet':
                    return 'CANCELLED'.encode('utf-8')
                elif remark == '' and reason != '':
                    return 'RESCHEDULED'.encode('utf-8')
                elif remark == 'Delivery Not taken':
                    return 'Cancelled'.encode('utf-8')
                elif remark == 'Send With /Against other AWB no':
                    return 'HANDED OVER TO COURIER'.encode('utf-8')
                else:
                    return ''.encode('utf-8')
        except:
            return ''.encode('utf-8')

class AWB_Status(Time_Model):
    STATUS = (
        ('DR', 'Data Received'),
        ('PP', 'Dispatched for Pickup'),
        ('ISC', 'In-Scanned'),
        ('PS', 'Pick-up Scheduled'),
        ('PC', 'Pickup Complete'),
        ('TB', 'TB Created'),
        ('TBD', 'TB Delivered'),
        ('MTS', 'MTS Created'),
        ('MTD', 'MTS Delivered'),
        ('DCR', 'Delivery Centre Reached'),
        ('DRS', 'DRS Created'),
        ('DTO', 'DTO Created'),
        ('DEL', 'Delivered'),
        ('CAN', 'Cancelled'),
        ('RET', 'Returned'),
        ('INT', 'In-Transit'),
        ('ITR', 'In-Transit (Return)'),
        ('DPR', 'Dispatched (Return)'),
        ('DBC', 'Deferred by Customer'),
        ('CNA', 'Customer not Available'),
        ('RBC', 'Rejected by Client'),
        ('CB', 'Called Back'),
        ('SCH', 'Scheduled'),
        ('DFR', 'Dispatched for Return'),
        ('PFR', 'Pending for RTO'),
        ('RTO', 'RTO\'d to Client'),
        ('NA', 'Not Attempted'),
        ('LOS', 'Lost')
    )
    awb = models.OneToOneField('AWB', related_name="awb_status", db_index=True)
    manifest = models.ForeignKey('Manifest', null=True, blank=True, db_index=True)
    current_branch = models.ForeignKey('internal.Branch', null=True, blank=True, db_index=True)
    current_tb = models.ForeignKey('transit.TB', null=True, blank=True, db_index=True)
    current_mts = models.ForeignKey('transit.MTS', null=True, blank=True, db_index=True)
    current_drs = models.ForeignKey('transit.DRS', null=True, blank=True, db_index=True)
    current_dto = models.ForeignKey('transit.DTO', null=True, blank=True, db_index=True)
    current_rto = models.ForeignKey('transit.RTO', null=True, blank=True, db_index=True)
    current_fe = models.ForeignKey(User, null=True, blank=True, related_name='awb_status_fe', db_index=True)
    collected_amt = models.FloatField(max_length=12, null=True, blank=True)
    status = models.CharField(choices=STATUS, max_length=3, default='DR', db_index=True)
    # zone = models.ForeignKey('zoning.Zoning_Matrix', null=True, blank=True)
    remark = models.CharField(max_length=200, blank=True, default='', db_index=True)
    reason = models.CharField(max_length=200, blank=True, default='', db_index=True)
    updated_by = models.ForeignKey(User, null=True, blank=True, db_index=True)
    call = models.BooleanField(default=False)
    connected_call = models.BooleanField(default=False)

    class Meta:
        index_together = [
            ['awb', 'manifest', 'current_branch', 'current_tb', 'current_mts', 'current_mts', 'current_drs',
             'current_dto', 'current_rto', 'current_fe', 'collected_amt', 'remark', 'reason', 'updated_by'], ]
        verbose_name = 'AWB Status'
        verbose_name_plural = 'AWB Status'

    # def save(self, *args, **kwargs):
    # if self.pk is None:
    # AWB_History.objects.create(awb=self.awb, status=self.awb_status.status, updated_by=self.updated_by)
    # return super(AWB_Status, self).save(*args, **kwargs)

    def get_readable_choice(self):
        if self.status in ['TB', 'TBD', 'MTS', 'MTD']:
            return 'In-Transit'
        elif self.status == 'DCR':
            if self.awb.category == 'REV':
                return 'Pending for DTO'
            else:
                return 'Pending for Delivery'
        elif self.status == 'DRS':
            if self.awb.category == 'REV':
                return 'Out for Pickup'
            else:
                return 'Dispatched'
        elif self.status == 'DTO':
            return 'Dispatched to Client'
        elif self.status == 'DEL' and self.awb.category == 'REV':
            return "DTO'd to Client"
        else:
            try:
                return dict(self.STATUS)[self.status]
            except Exception:
                return ''

    def get_status_client(self):
        if self.status in ['TB', 'TBD', 'MTS', 'MTD', 'DCR', 'PC']:
            return 'Picked Up'
        elif self.status == 'DRS':
            return 'Out for Pickup'
        elif self.status == 'DTO':
            return 'DTO Created'
        elif self.status == 'DEL' and self.manifest.category == 'RL':
            return "DTO"
        elif self.status == 'DR':
            return 'Not Attempted'
        # elif self.status in ['CNA', 'SCH', 'DBC']:
        # return 'Scheduled'
        elif self.status == 'CNA':
            return 'NA/Busy/SOFF and Door found locked'
        else:
            try:
                return dict(self.STATUS)[self.status]
            except Exception:
                return ''

    def get_current_branch(self):
        return self.current_tb.get_current_branch

    def get_awb_dto_pod(self):
        try:
            return self.awb_status.current_dto.pod
        except:
            return None


    def __unicode__(self):
        return self.awb.awb


class AWB_History(Time_Model):
    awb = models.ForeignKey('AWB', db_index=True)
    tb = models.ForeignKey('transit.TB', null=True, blank=True, db_index=True)
    mts = models.ForeignKey('transit.MTS', null=True, blank=True, db_index=True)
    drs = models.ForeignKey('transit.DRS', null=True, blank=True, db_index=True)
    dto = models.ForeignKey('transit.DTO', null=True, blank=True, db_index=True)
    rto = models.ForeignKey('transit.RTO', null=True, blank=True, db_index=True)
    fe = models.ForeignKey(User, null=True, blank=True, related_name='awb_history_fe', db_index=True)
    status = models.CharField(max_length=3, null=True, blank=True, db_index=True)
    branch = models.ForeignKey('internal.Branch', null=True, blank=True, db_index=True)
    remark = models.CharField(max_length=200, blank=True, default='', db_index=True)
    reason = models.CharField(max_length=200, blank=True, default='', db_index=True)
    updated_by = models.ForeignKey(User, null=True, blank=True, db_index=True)
    call = models.BooleanField(default=False)
    connected_call = models.BooleanField(default=False)

    class Meta:
        index_together = [
            ['awb', 'tb', 'mts', 'drs', 'dto', 'rto', 'fe', 'status', 'branch', 'remark', 'reason', 'updated_by'], ]
        verbose_name = 'AWB History'
        verbose_name_plural = 'AWB History'


    def __unicode__(self):
        return self.awb.awb


def awb_image_upload_dir(self, filename):
    dir = 'awb/' + str(self.awb.awb)
    if not os.path.exists(MEDIA_ROOT + dir):
        os.makedirs(MEDIA_ROOT + dir)
    filename = ''
    if self.type == 'INV':
        filename = 'invoice.jpeg'
    elif self.type == 'SHI':
        filename = 'invoice.jpeg'
    elif self.type == 'BAR':
        filename = 'barcode.png'
    return dir + '/' + filename


class AWB_Images(Time_Model):
    IMAGE_TYPE = (
        ('INV', 'Invoice Image'),
        ('BAR', 'Barcode Image'),
        ('MET', 'Meter Reading'),
        ('SHI', 'Shipment Image')
    )
    awb = models.ForeignKey('AWB')
    image = models.ImageField(upload_to=awb_image_upload_dir)
    type = models.CharField(choices=IMAGE_TYPE, max_length=3, blank=True, default='')


class AWB_Notification(Time_Model):
    TYPE = (
        ('DRS', 'Pickup Notification'),
        ('CAN', 'Cancel Notification'),
        ('DBC', 'Deferred Notification'),
        ('ISS', 'Issue Notification'),
    )
    awb = models.ForeignKey('AWB')
    type = models.CharField(choices=TYPE, max_length=3, blank=True, default='')
    status = models.CharField(max_length=20, default='', blank=True)
    send = models.BooleanField(default=False)

    class Meta:
        unique_together = ('awb', 'type')

    def get_notify_type(self):
        if self.awb.category == 'REV' and self.type == 'DRS':
            return 'Pickup Notification'
        elif self.awb.category != 'REV' and self.type == 'DRS':
            return 'Delivery Notification'
        else:
            try:
                return dict(self.TYPE)[self.type]
            except:
                return ''


def get_awb_status(type, status, hist, branch, awb, internal):
    if internal:
        branch_name = branch
    else:
        branch_name = get_branch_name(branch)
    if type == 'status':
        if hist.reason != '':
            reason = hist.reason
        elif awb.awb_status.reason != '':
            reason = awb.awb_status.reason
        else:
            reason = ''
        try:
            if hist.updated_by.profile.role == 'CS':
                prefix = 'CS Call Made : '
            else:
                prefix = ''
        except Exception:
            prefix = ''
        if status == 'DR':
            return 'Data Received'
        elif status == 'ISC':
            if internal:
                return 'In-Scanned at ' + branch_name + ' branch'
            else:
                return 'Received at ' + branch_name + ' branch'
        elif status == 'DCR':
            if awb.category == 'REV':
                return 'Pending for DTO at ' + branch_name + ' branch'
            else:
                return 'Pending for Delivery at ' + branch_name + ' branch'
        elif status == 'RET':
            return prefix + 'Return'
        elif status == 'CAN':
            if reason != '':
                reason = ' | Reason: ' + reason
            if hist.remark != '':
                remark = ' | Remark: ' + hist.remark + reason
            elif awb.awb_status.remark != '':
                remark = ' | Remark: ' + awb.awb_status.remark + reason
            else:
                remark = ''
            return prefix + 'Cancelled' + remark
        elif status == 'DBC':
            return prefix + 'Deferred by Customer for Date : ' + reason
        elif status == 'CNA':
            return prefix + 'Customer not Available'
        elif status == 'CB':
            return prefix + 'Called Back at ' + branch_name + ' branch'
        elif status == 'RBC':
            return prefix + 'Rejected by Client'
        elif status == 'DEL':
            if awb.category == 'REV':
                if internal:
                    return "DTO'd to Client"
                else:
                    return 'Accepted by Client'
            else:
                return 'Delivered'
        elif status == 'PC':
            return 'Pickup Completed at ' + branch_name + ' branch'
        elif status == 'SCH':
            return prefix + 'Scheduled for Date : ' + reason
        else:
            try:
                return dict(AWB_Status.STATUS)[hist.status]
            except Exception:
                return ''
    else:
        if internal:
            transfer_mode = ''
            if status[:3] == 'MTS':
                transfer_mode += ' (' + awb.awb_status.current_mts.get_readable_type() + ')'
            return status + transfer_mode + ' created at ' + branch_name + ' branch'
        else:
            if status[:2] == 'TB':
                return 'Transit Bag Created : In-Transit to ' + get_branch_name(
                    str(awb.get_delivery_branch().branch_name)) + ' branch'
            # elif status[:2] == 'MT':
            # return 'Transit Bag Dispatched : In-Transit to ' + get_branch_name(
            # str(awb.get_delivery_branch().branch_name)) + ' branch'
            elif status[:3] == 'DRS':
                if awb.category == 'REV':
                    return 'Out for Pickup'
                else:
                    return 'Dispatched'
            elif status[:3] == 'DTO':
                return 'Dispatched to Client'
            elif status[:3] == 'RTO':
                return 'Dispatched for RTO to Client'


def get_rto_awbs(del_branch, curr_branch=None):
    filter = {}
    if del_branch != '':
        filter['awb_status__manifest__branch_id'] = del_branch
    filter['category__in'] = ['COD', 'PRE']
    filter['awb_status__status__in'] = ['CAN', 'RET']
    if curr_branch is not None:
        filter['awb_status__current_branch_id'] = curr_branch

    return AWB.objects.filter(**filter)


def get_tb_awbs(category, status, del_branch, curr_branch):
    from internal.models import Branch

    filter = {}
    filter['category__in'] = category
    filter['awb_status__status__in'] = status
    if curr_branch is not None:
        filter['awb_status__current_branch_id'] = curr_branch

    awbs = AWB.objects.filter(**filter).prefetch_related('awb_status', 'awb_status__manifest',
                                                         'awb_status__manifest__branch')
    if del_branch != '' and Branch.objects.get(pk=del_branch).branch_name[-3:] != 'HUB':
        awbs = AWB.objects.filter(pk__in=[awb.pk for awb in awbs if awb.get_delivery_branch().pk == int(del_branch)])

    return awbs


# def get_mis_fields(header):
# from utils.random import mis_header_into_field
#
# fields = mis_header_into_field(header)

def get_branch_name(branch):
    if branch in BRANCH_DICT:
        return BRANCH_DICT[branch]
    else:
        return branch


def resolve_status(status, awb):
    STATUS = AWB_Status.STATUS
    if status in ['TB', 'TBD', 'MTS', 'MTD']:
        return 'In-Transit'
    elif status == 'DCR':
        if awb.awb_status.manifest.category == 'FL':
            return 'Pending for Delivery'
        else:
            return 'Pending for DTO'
    elif status == 'DRS':
        return 'Dispatched'
    elif status == 'DTO':
        return 'Dispatched to Client'
    elif status == 'DEL' and awb.awb_status.manifest.category == 'RL':
        return "DTO'd to Client"
    else:
        try:
            return dict(STATUS)[status]
        except Exception:
            return ''


def search_awbs(query):
    awb_found = []
    for q in query:
        try:
            awbs = AWB.objects.filter(awb=q) | AWB.objects.filter(order_id=q) | AWB.objects.filter(
                invoice_no=q) | AWB.objects.filter(phone_1=q) | AWB.objects.filter(phone_2=q)
            awb_found += [awb for awb in awbs]
        except AWB.DoesNotExist:
            pass
    return list(set(awb_found))


def inscan_awb(query):
    try:
        return AWB.objects.get(Q(awb=query) | Q(invoice_no=query))
    except MultipleObjectsReturned:
        return AWB.objects.get(Q(awb=query))
    except AWB.DoesNotExist:
        raise AWB.DoesNotExist


def get_awb_history_filter_dict(post_data):
    filter = dict()
    filter['exclude'] = dict()
    filter['filter'] = dict()

    filter['filter']['creation_date__range'] = (
        post_data['start_date'] + ' 00:00:00', post_data['end_date'] + ' 23:59:59')

    if post_data['date_type'] == 'picked_date':
        filter['filter']['status'] = 'PC'
    elif post_data['date_type'] == 'in_transit_date':
        filter['exclude']['mts'] = None
    elif post_data['date_type'] == 'dto_created_date':
        filter['exclude']['dto'] = None
    elif post_data['date_type'] == 'dtod_date':
        filter['filter']['status'] = 'DEL'
    # print filter
    return filter


def convert_status_code_to_exact_code(status):
    filter = dict()
    if status == 'INT':
        filter['awb_status__status__in'] = ['TB', 'TBD', 'MTS', 'MTD']
    else:
        filter['awb_status__status'] = status

    return filter

class AWB_Detail(Time_Model):
    awb = models.CharField(max_length=15, unique=True, db_index=True)
    order_id = models.CharField(max_length=50, null=True, blank=True, db_index=True)
    crp_id = models.CharField(max_length=20, default='', blank=True)
    invoice_no = models.CharField(max_length=300, null=True, blank=True, db_index=True)
    customer_name = models.CharField(max_length=300, null=True, blank=True)
    address_1 = models.TextField(null=True, blank=True)
    address_2 = models.TextField(null=True, blank=True)
    area = models.CharField(max_length=400, null=True, blank=True)
    city = models.CharField(max_length=200, null=True, blank=True)
    pincode = models.IntegerField(max_length=6, null=True, blank=True, db_index=True)
    vendor_code = models.CharField(max_length=10, null=True, blank=True, db_index=True)
    phone_1 = models.CharField(max_length=15, null=True, blank=True, db_index=True)
    phone_2 = models.CharField(max_length=15, null=True, blank=True)
    package_value = models.CharField(max_length=20, null=True, blank=True)
    package_price = models.CharField(max_length=20, null=True, blank=True)
    expected_amount = models.FloatField(max_length=12, null=True, blank=True)
    weight = models.CharField(max_length=15, null=True, blank=True)
    length = models.CharField(max_length=15, null=True, blank=True)
    breadth = models.CharField(max_length=15, null=True, blank=True)
    height = models.CharField(max_length=15, null=True, blank=True)
    package_sku = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    category = models.CharField(max_length=15, null=True, blank=True)
    priority = models.CharField(max_length=15, default='N')
    barcode = models.ImageField(upload_to='awb/barcode/', null=True, blank=True)
    reason_for_return = models.TextField(blank=True, null=True)
    manifest = models.FileField(upload_to='uploads/manifest/')
    current_branch_code = models.CharField(max_length=15, null=True, blank=True, db_index=True)
    current_tb_id = models.CharField(max_length=15, unique=True, null=True, blank=True, db_index=True)
    current_mts_id = models.CharField(max_length=15, unique=True, null=True, blank=True, db_index=True)
    current_drs_id = models.CharField(max_length=15, unique=True, null=True, blank=True, db_index=True)
    current_dto_id = models.CharField(max_length=15, unique=True, null=True, blank=True, db_index=True)
    current_rto_id = models.CharField(max_length=15, unique=True, null=True, blank=True, db_index=True)
    current_fe = models.CharField(max_length=30, null=True, blank=True, db_index=True)
    collected_amt = models.FloatField(max_length=12, null=True, blank=True)
    status = models.CharField(max_length=50, default='DR', db_index=True)
    remark = models.CharField(max_length=200, blank=True, default='', db_index=True)
    reason = models.CharField(max_length=200, blank=True, default='', db_index=True)
    updated_by = models.CharField(max_length=30, blank=True, null=True, db_index=True)
    call = models.BooleanField(default=False)
    connected_call = models.BooleanField(default=False)
    pickup_branch_code = models.CharField(max_length=15, null=True, blank=True)
    rto_branch_code = models.CharField(max_length=15, null=True, blank=True)
    dto_branch_code = models.CharField(max_length=15, null=True, blank=True)
    delivery_branch_code = models.CharField(max_length=15, null=True, blank=True)
    client_code = models.CharField(max_length=3, null=True, blank=True, db_index=True)

    class Meta:
        index_together = [
            ['awb', 'pincode', 'phone_1', 'phone_2', 'manifest', 'current_tb_id', 'current_mts_id', 'current_drs_id',
             'current_dto_id',
             'current_rto_id', 'current_fe', 'updated_by', 'client_code'],
        ]
        verbose_name = 'AWB_Detail'
        verbose_name_plural = 'AWB_Detail'

    def __unicode__(self):
        return self.awb

    def get_full_address(self):
        try:
            address = self.address_1.decode('utf-8').encode('utf-8')
        except Exception:
            address = self.address_1

        if self.address_2 != '':
            try:
                address += ', ' + self.address_2.decode('utf-8').encode('utf-8')
            except Exception:
                address += ', ' + self.address_2

        if self.city:
            try:
                address += ', ' + self.city.encode('utf-8')
            except Exception:
                address += ', ' + self.city

        try:
            return address.encode('utf-8')
        except Exception:
            return address

    def get_category(self):
        try:
            if self.category == 'REV':
                return 'Reverse Pickup'
            elif self.category == 'PRE':
                return 'Prepaid'
            elif self.category == 'MPU':
                return 'Money Pickup'
            elif self.category == 'COD':
                return 'COD'
        except:
            return ''

    def get_updated_by(self):
        try:
            # self.updated_by = 'naveen.agarwal'
            user = User.objects.get(username=self.updated_by)
            fullname = user.first_name + ' ' + user.last_name
            return str(fullname)
        except:
            return ''

    def get_expected_delivery_date(self):
        try:
            # pickup_date = self.get_picked_date()
            connected_date = self.get_connected_date()
            zone = Zoning_Matrix.objects.filter(origin_city=self.get_origin_city(),
                                                destination_city=self.get_destination_city())
            zone = zone.first()
            if zone:
                mode = self.get_transfer_mode_absolute()
                expected_time = {}
                if mode == 'Air':
                    expected_time['min'] = zone.air_min
                    expected_time['max'] = zone.air_max
                elif mode == 'Van':
                    expected_time['min'] = zone.van
                    expected_time['max'] = zone.van
                else:
                    expected_time['min'] = zone.surface_min
                    expected_time['max'] = zone.surface_max
                    # return expected_time
                expected_date = connected_date + timedelta(expected_time['max'])
                return expected_date.strftime('%d, %b , %Y')
            else:
                return None
        except:
            return None


class AWB_Detail_History(Time_Model):
    awb = models.ForeignKey('AWB', null=True, blank=True, db_index=True)
    tb = models.CharField(max_length=15, null=True, blank=True)
    mts = models.CharField(max_length=15, null=True, blank=True)
    rto = models.CharField(max_length=15, null=True, blank=True)
    drs = models.CharField(max_length=15, null=True, blank=True)
    dto = models.CharField(max_length=15, null=True, blank=True)
    fe = models.CharField(max_length=100, null=True, blank=True)
    branch = models.CharField(max_length=100, null=True, blank=True)
    reason = models.TextField(null=True, blank=True)
    remark = models.TextField(null=True, blank=True)
    updated_by = models.CharField(max_length=100, null=True, blank=True)
    call = models.BooleanField(default=False)
    connected_call = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'AWB_Detail_History'
        verbose_name_plural = 'AWB_Detail_History'

    def __unicode__(self):
        return self.awb.awb

class AWB_Calling(Time_Model):
    awb = models.ForeignKey('AWB', null=True, blank=True, db_index=True)
    phone_no = models.CharField(max_length=20, null=True, blank=True)

    class Meta:
        verbose_name = 'AWB_Calling'
        verbose_name_plural = 'AWB_Calling'

    def __unicode__(self):
        return self.awb.awb

