from rest_framework import serializers
from awb.models import AWB


class AWBDetail(serializers.ModelSerializer):
    address = serializers.Field(source='get_full_address')
    pincode = serializers.Field(source='get_readable_pincode')
    category = serializers.Field(source='get_readable_choice')
    status = serializers.Field(source='get_readable_status')
    last_updated = serializers.Field(source='get_status_last_updated')

    class Meta:
        model = AWB
        fields = ['awb', 'order_id', 'customer_name', 'address', 'pincode', 'phone_1', 'phone_2', 'package_sku',
                  'description', 'category', 'expected_amount', 'status', 'last_updated']


class AWBStatusDetails(serializers.ModelSerializer):
    status = serializers.Field(source='get_readable_status')
    last_updated = serializers.Field(source='get_status_last_updated')

    class Meta:
        model = AWB
        fields = ['status', 'last_updated', 'awb']


class AWBHistoryDetails(serializers.ModelSerializer):
    address = serializers.Field(source='get_full_address')
    pincode = serializers.Field(source='get_readable_pincode')
    status = serializers.Field(source='get_readable_status')
    last_updated = serializers.Field(source='get_status_last_updated')
    invoice_image = serializers.Field(source='get_invoice_image')
    shipment_image = serializers.Field(source='get_shipment_image')

    class Meta:
        model = AWB
        fields = ['awb', 'order_id', 'customer_name', 'address', 'pincode', 'phone_1', 'phone_2', 'package_sku',
                  'description', 'category', 'expected_amount', 'status', 'last_updated']


class calling_outbound(serializers.ModelSerializer):
    address = serializers.Field(source='get_full_address')
    category = serializers.Field(source='get_category')
    # status = serializers.Field(source='get_status')
    updated_by = serializers.Field(source='get_updated_by')
    expected_delivery_date = serializers.Field(source='get_expected_delivery_date')


    class Meta:
        model = AWBDetail
        fields = ['awb', 'order_id', 'invoice_no', 'customer_name', 'address', 'pincode', 'status', 'updated_by', 'category', 'description',
                  'reason_for_return']



