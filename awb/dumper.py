from django.db import transaction

from django.template.loader import get_template
from django.template import Context

from awb.models import AWB, AWB_Status, AWB_Images, AWB_History

from client.models import Client_Vendor, Client_Additional
from internal.models import Branch
from utils.constants import MANIFEST_FIELDS
from utils.importer import BaseImporter
from utils.upload import generate_barcode, generate_printer_barcode
from zoning.models import Pincode


__author__ = 'naveen'

# class AWBManifestValidator(BaseValidator):
# def validate_data(self):
# row = 0
# for data_dict in self.data_handler:
# try:
# AWB.objects.get(awb=data_dict['awb'])
# error_msg = 'AWB %s exist in system' % (data_dict['awb'])
#                 self.error_list.append([{'row': row, 'error_msg': error_msg}])
#             except AWB.DoesNotExist:
#                 check_order_id = AWB.objects.filter(order_id = data_dict['order_id'],category = 'REV',
#                                                     awb_status__manifest__client = data_dict['client']).exclude(
#                     awb_status__status = 'CAN')
#                 order_id_can_length = len([i for i in check_order_id])
#                 if order_id_can_length > 0:
#                     error_msg = 'Order id %s already exist in system' % (data_dict['order_id'])
#                     self.error_list.append([{'row':row, 'error_msg': error_msg}])
#                 else:
#                     try:
#                         # client_add = Client_Additional.objects.get(client = client)
#                             Client_Vendor.objects.get(vendor_code = data_dict['vendor'])
#                     except Client_Vendor.DoesNotExist:
#                         error_msg = 'Vendor Code %s does not exist in system' % (data_dict['vendor'])
#                         self.error_list.append([{'row':row , 'error_msg':error_msg}])
#             row += 1

class ManifestImporterModel(BaseImporter):
    # fields = ['awb','priority','client order no','cust name','cust address line 1','cust address line 2',
    #           'cust city','pincode','phone 1','phone 2','weight','package description','package value',
    #           'vendor',]

    class Meta:
        model = AWB
        include = MANIFEST_FIELDS
        # foreign_keys = (('pincode', 'pincode'), ('vendor', 'vendor_code'), ('origin_branch', 'branch_name'),
        #                 )
        # validator = AWBManifestValidator

    def __init__(self, source=None, user=None, manifest=None):
        super(ManifestImporterModel, self).__init__(source)
        self.users = user
        self.manifest = manifest

        # def pre_clean(self, field, data):
        #         client = data['client']
        #         client_add = Client_Additional.objects.get(client = client)
        #         if field == 'awb':
        #             if client_add.automated_awb_used:
        #                 while(1):
        #                     print client.awb_last_used
        #                     awb = str(str(client.awb_last_used[:3]) + '0'*(len(client.awb_last_used)-3 -
        #                                                              len(str(int(client.awb_last_used[3:])))) +\
        #                           str(int(client.awb_last_used[3:])+1))
        #                     print awb
        #                     client.awb_last_used = awb
        #                     client.save()
        #                     awbs = AWB.objects.filter(awb = awb)
        #                     if len(awbs) == 0:
        #                         break
        #                 self.awb = awb
        # if field == 'origin_branch':
        #     if data['category'] == 'REV':
        #         self.dto_branch = data[field]
        #
        #     else:
        #         self.rto_branch = data[field]

    # @transaction_atomic
    # def save(self):
    #     bind = {}
    #     row = 0
    #     for data_dict in self.data_handler:

    @transaction.atomic
    def save(self):
        row = 1
        vendor_add = []
        existing_awb = []
        wrong_pincode = []
        for data in self.cleaned_data():
            row += 1
            bind = {}
            try:
                client = self.manifest.client
                # print client
                client_add = Client_Additional.objects.get(client=client)
                order_id = data['order_id']
                check_order_id = AWB.objects.filter(order_id=order_id, category='REV',
                                                    awb_status__manifest__client=client).exclude(
                    awb_status__status='CAN')
                order_id_can_length = len([i for i in check_order_id])
                awb = AWB.objects.filter(awb=data['awb'])
                if len(awb) > 0:
                    # print len(awb)
                    error_msg = 'AWB %s already exist in system' % (data['awb'])
                    self.error_list.append({'row': row, 'error_msg': error_msg})
                    existing_awb.append(awb)
                elif order_id_can_length > 0:
                    error_msg = 'Order id %s already exist in system' % (data['order_id'])
                    self.error_list.append({'row': row, 'error_msg': error_msg})
                else:
                    # if client_add.automated_awb_used:
                    #     while(1):
                    #         # print client.awb_last_used
                    #         awb = str(client.awb_last_used[:3]) + '0'*(len(client.awb_last_used)-3 -
                    #                                                  len(str(int(client.awb_last_used[3:])))) +\
                    #               str(int(client.awb_last_used[3:])+1)
                    #         # print awb
                    #         client.awb_last_used = awb
                    #         # client.save()
                    #         awbs = AWB.objects.filter(awb = awb)
                    #         if len(awbs) == 0:
                    #             break
                    #         else:
                    #             client.save()
                    # else:
                    awb = data['awb']
                    print awb[:3].upper()
                    print client.client_code
                    if awb[:3].upper() == client.client_code:
                        try:
                            # print awb
                            awb = AWB.objects.get(awb=data['awb'])
                            error_msg = 'AWB %s already exist in system' % (data['awb'])
                            self.error_list.append({'row': row, 'error_msg': error_msg})
                        except AWB.DoesNotExist:
                            awb = data['awb']
                        # print awb
                        try:
                            # print 'gus gaye'
                            pincode = Pincode.objects.get(pincode=data['pincode'])

                            # try:
                            #     branch = Branch.objects.get(branch_name=data['origin_branch'])
                            #     # print branch
                            # except:
                            #     error_msg = 'Origin Branch %s does not exist in our system' % (data['origin_branch'])
                            #     self.error_list.append({'row': row, 'error_msg': error_msg})
                            #     # print self.error_list
                            #     branch = None
                            # print pincode
                            # vendor_code = data['vendor']
                            if client_add.vendor_assign:
                                if data['vendor'] == '':
                                    vendor_code = None
                                else:
                                    try:
                                        # print 'v'
                                        vendor_code = Client_Vendor.objects.get(vendor_code=data['vendor'])
                                    except Client_Vendor.DoesNotExist:
                                        # print 'kkk'
                                        vendor_code = Client_Vendor.objects.create(vendor_code=data['vendor'],
                                                                                   client=client,
                                                                                   branch=None, pincode=None)
                                        vendor_add.append(vendor_code)
                                        # print '333'
                            else:
                                vendor_code = None
                            if data['category'] == '':
                                category = 'REV'
                            else:
                                category = data['category']
                            # print category
                            if data['priority'] != '':
                                priority = data['priority']
                                bind['priority'] = priority

                            if category == 'REV':
                                bind['barcode'] = generate_barcode(awb)
                            bind['awb'] = awb
                            # print bind
                            # bind['priority'] = priority
                            bind['order_id'] = order_id
                            bind['customer_name'] = data['customer_name']
                            bind['address_1'] = data['address_1']
                            bind['address_2'] = data['address_2']
                            bind['city'] = data['city']
                            bind['pincode'] = pincode
                            bind['phone_1'] = data['phone_1']
                            bind['phone_2'] = data['phone_2']
                            bind['weight'] = data['weight']
                            bind['description'] = data['description']
                            bind['package_value'] = data['package_value']
                            bind['vendor'] = vendor_code
                            # print bind
                            # bind['client'] = data['client']
                            bind['length'] = data['length']
                            bind['breadth'] = data['breadth']
                            bind['height'] = data['height']
                            bind['category'] = category
                            bind['reason_for_return'] = data['reason_for_return']
                            if data['vendor'] != '':
                                bind['origin_branch'] = vendor_code.branch
                            else:
                                bind['origin_branch'] = self.manifest.branch
                            # print bind
                            bind['destination_branch'] = pincode.branch_pincode.branch
                            # print bind
                            try:
                                # print 'dsds'
                                awbs = AWB(**bind)
                                awbs.save()
                                # awbs.save()
                                #     print awbs
                                # print 'ewrrr'
                                # manifest = Manifest.objects.create(client = bind['client'], uploaded_by = self.users,
                                #                                    warehouse = , branch = 'SKT')
                                AWB_Status.objects.create(awb=awbs, manifest=self.manifest, updated_by=self.users)
                                AWB_History.objects.create(awb=awbs, status='DR', updated_by=self.users)
                                AWB_Images.objects.create(awb=awbs, type='BAR',
                                                          image=generate_printer_barcode(awbs.awb))
                                self.awb_uploaded.append({'awb': awbs})
                                # client.save()
                            except Exception, e:
                                self.error_list.append({'row': row, 'error_msg': e})
                                # print self.error_list
                        except Pincode.DoesNotExist:
                            error_msg = 'Pincode %s not exist in system' % (data['pincode'])
                            self.error_list.append({'row': row, 'error_msg': error_msg})
                            wrong_pincode.append(data['pincode'])
            except:
                pass
                # self.error_list.append({'row': row, 'error_msg': e})

                # if self.error_list:
                # send_manifest_report_new(self.error_list, self.users.email, self.awb_uploaded)
                # break
        # send_manifest_report_new(self.error_list, self.users.email, self.awb_uploaded)

        # print self.error_list
        # if self.error_list:
        #     raise ValidationError('Errors in file')

        send_manifest_report_new(self.error_list, self.users.email, self.awb_uploaded, vendor_add, existing_awb,
                                 wrong_pincode)


def send_manifest_report_new(awb_error, email, awb_uploaded, vendor_add, existing_awb, wrong_pincode):
    template = get_template('awb/manifest_report_new.html')
    context = Context({
        'awb_error': awb_error,
        'awb_uploaded': awb_uploaded,
        'vendor_add': vendor_add,
        'existing_awb': existing_awb,
        'wrong_pincode': wrong_pincode
    })
    print awb_error
    print len(awb_error)
    print awb_uploaded
    print vendor_add
    print existing_awb
    print wrong_pincode
    # content = template.render(context)
    # msg = EmailMultiAlternatives('Manifest Upload Report', "This is a system generated mail. Please do not reply.",
    #                              "system@nuvoex.com", [email])
    # msg.attach_alternative(content, "text/html")
    # msg.send()


# class VendorUpload(BaseImporter):
#
#     class Meta:
#         model = Client_Vendor
#         include = VENDOR_UPLOAD_FIELD
#
#     def __init__(self, source=None, email = None):
#         super(VendorUpload, self).__init__(source)
#         self.email = email
#         self.vendor_uploaded = {}
#
#     def save(self):
#         row= 0
#         for data in self.cleaned_data():
#             row+=1
#             try:
#                 vendor = Client_Vendor.objects.get(vendor_code = data['vendor_code'])
#                 error_msg = 'Vendor code %s already exist in system' % (data['vendor_code'])
#                 self.error_list = ({'row': row, 'error_msg': error_msg})
#             except Client_Vendor.DoesNotExist:
#                 bind = {}
#                 client = Client.objects.get(client_code = data['client'])
#                 branch = Branch.objects.get(branch_name = data['branch'])
#                 pincode = Pincode.objects.get(pincode = data['pincode'])
#                 bind['client'] = client
#                 bind['vendor_name'] = data['vendor_name']
#                 bind['vendor_code'] = data['vendor_code']
#                 bind['owner_name'] = data['owner_name']
#                 bind['phone'] = data['phone']
#                 bind['address'] = data['address']
#                 bind['pincode'] = pincode
#                 bind['branch'] = branch
#
#                 vendor = Client_Vendor.objects.create(**bind)
#                 vendor_uploaded = {}
#                 vendor_uploaded.append({'data': vendor.vendor_code})
#         send_common_report(self.vendor_uploaded, self.error_list, self.email)
#
# def send_common_report(upload_data, error_data, email):
#     template = get_template('common/upload_report.html')
#     context = Context({
#         'error_data': error_data,
#         'upload_data': upload_data
#     })
#     content = template.render(context)
#     msg = EmailMultiAlternatives('Manifest Upload Report', "This is a system generated mail. Please do not reply.",
#                                  "system@nuvoex.com", [email])
#     msg.attach_alternative(content, "text/html")
#     msg.send()
