from django.forms import ModelForm, ModelChoiceField, Select
from awb.models import AWB
from .models import Manifest
from client.models import Client_Warehouse, Client
from zoning.models import Pincode


class UploadManifestForm(ModelForm):
    client = ModelChoiceField(queryset=Client.objects.all(),
                              widget=Select(attrs={'onchange': 'update_warehouse(this)'}))
    warehouse = ModelChoiceField(queryset=Client_Warehouse.objects.all())

    class Meta:
        model = Manifest
        exclude = ['status', 'is_active', 'uploaded_by', 'branch']

class ManifestForm(ModelForm):
    client = ModelChoiceField(queryset=Client.objects.all())
    class Meta:
        model = Manifest
        exclude = ['status', 'is_active', 'warehouse', 'uploaded_by', 'branch', 'category']
        
class AWBAdminForm(ModelForm):
    pincode = ModelChoiceField(queryset=Pincode.objects.all().order_by('pincode'))
    class Meta:
        model = AWB
        exclude = ['awb', 'barcode', 'is_active', 'crp_id', 'area', 'package_price', 'package_sku',
                   'origin_branch', 'destination_branch']
