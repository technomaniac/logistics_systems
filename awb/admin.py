from django.contrib import admin

from awb.models import *
from zoning.models import Pincode

# Register your models here.

class ManifestAdmin(admin.ModelAdmin):
    list_display = ('file', 'branch', 'client', 'uploaded_by', 'warehouse', 'status')
    search_fields = ['file', 'branch', 'client', 'uploaded_by', 'warehouse', 'status']
    list_filter = ['branch', 'client', 'uploaded_by', 'warehouse', 'status']


class AWBAdmin(admin.ModelAdmin):
    #list_display = ('awb', 'customer_name', 'pincode', 'address_1', 'address_2', 'city', 'vendor')
    search_fields = ['awb', 'order_id', 'phone_1', 'phone_2']
    list_filter = ['awb_status__current_branch', 'pincode__city', 'creation_date',
                   'awb_status__on_update', 'vendor']

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "pincode":
            kwargs["queryset"] = Pincode.objects.order_by('pincode')
        return super(AWBAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class AWB_StatusAdmin(admin.ModelAdmin):
    list_display = (
        'awb', 'status', 'current_branch', 'current_drs', 'on_update')
    search_fields = ['awb__awb', 'current_branch__branch_name', 'current_tb__tb_id', 'current_drs__drs_id',
                     'current_dto__dto_id', 'current_mts__mts_id']
    list_filter = ['current_branch__branch_name', 'creation_date', 'manifest__client', 'current_tb__tb_id',
                   'current_drs__drs_id', 'current_mts__mts_id', 'current_dto__dto_id', 'on_update']


class AWB_HistoryAdmin(admin.ModelAdmin):
    search_fields = ['awb__awb']
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'awb':
            kwargs["queryset"] = AWB.objects.select_related().order_by('awb')
        return super(AWB_HistoryAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class AWB_NotificationAdmin(admin.ModelAdmin):
    list_display = (
        'awb', 'status', 'type', 'send', 'creation_date')
    search_fields = ['awb__awb']
    list_filter = ['status', 'type', 'send']

class AWB_DetailAdmin(admin.ModelAdmin):
    search_fields = ['awb']

class AWB_Detail_History_Admin(admin.ModelAdmin):
    search_fields = ['awb__awb']

class AWB_CallingAdmin(admin.ModelAdmin):
    search_fields = ['awb__awb']


admin.site.register(AWB, AWBAdmin)
admin.site.register(AWB_Status, AWB_StatusAdmin)
admin.site.register(AWB_History, AWB_HistoryAdmin)
admin.site.register(Manifest, ManifestAdmin)
admin.site.register(AWB_Notification, AWB_NotificationAdmin)
admin.site.register(AWB_Detail, AWB_DetailAdmin)
admin.site.register(AWB_Detail_History, AWB_Detail_History_Admin)
admin.site.register(AWB_Calling, AWB_CallingAdmin)

