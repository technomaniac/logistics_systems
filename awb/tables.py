import itertools

import django_tables2 as tables

from awb.models import AWB, Manifest
from common.tables import TableMixin


class AWBTable(tables.Table):
    select = tables.TemplateColumn(
        template_code='<label><input type="checkbox" checked="true" id="{{ record.id }}"><span class="lbl"></span></label>')
    awb = tables.TemplateColumn(
        template_code='<a href="/transit/awb/{{ record.id }}" target="_blank">{{ record.awb }}</a>')
    status = tables.Column(accessor='awb_status.get_readable_choice', order_by='awb_status__status')
    tb = tables.TemplateColumn(verbose_name='TB',
                               template_code='<a href="/transit/tb/{{ record.get_current_tb.pk }}" target="_blank">{{ record.get_current_tb.tb_id }}</a>')
    mts = tables.TemplateColumn(verbose_name='MTS',
                                template_code='<a href="/transit/mts/{{ record.get_current_mts.pk }}" target="_blank">{{ record.get_current_mts.mts_id }}</a>')
    drs = tables.TemplateColumn(verbose_name='DRS',
                                template_code='<a href="/transit/drs/{{ record.get_current_drs.pk }}" target="_blank">{{ record.get_current_drs.drs_id }}</a>')
    dto = tables.TemplateColumn(verbose_name='DTO',
                                template_code='<a href="/transit/dto/{{ record.get_current_dto.pk }}" target="_blank">{{ record.get_current_dto.dto_id }}</a>')
    rto = tables.TemplateColumn(verbose_name='RTO',
                                template_code='<a href="/transit/rto/{{ record.get_current_rto.pk }}" target="_blank">{{ record.get_current_rto.rto_id }}</a>')
    current_branch = tables.Column(accessor='awb_status.current_branch.branch_name')
    pickup_branch = tables.Column(accessor='get_pickup_branch',
                                  order_by='pincode__branch_pincode__branch__branch_name')
    rto_branch = tables.Column(accessor='get_rto_branch', order_by='awb_status__manifest__branch',
                               verbose_name='RTO Branch')
    delivery_branch = tables.Column(accessor='get_delivery_branch.branch_name',
                                    verbose_name='Delivery/DTO Branch',
                                    order_by='pincode__branch_pincode__branch__branch_name')
    vendor_code = tables.Column(accessor='get_vendor_code',
                                verbose_name='Vendor Code')

    class Meta:
        model = AWB
        fields = ('select', 'awb', 'order_id', 'priority')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed", "id": "awb_search_tbl"}


class AWBFrontDashboardTable(tables.Table):
    awb = tables.TemplateColumn(
        template_code='<a href="/transit/awb/{{ record.id }}" target="_blank">{{ record.awb }}</a>')
    status = tables.Column(accessor='awb_status.get_readable_choice', order_by='awb_status__status')
    tb = tables.TemplateColumn(verbose_name='TB',
                               template_code='<a href="/transit/tb/{{ record.get_current_tb.pk }}" target="_blank">{{ record.get_current_tb.tb_id }}</a>')
    mts = tables.TemplateColumn(verbose_name='MTS',
                                template_code='<a href="/transit/mts/{{ record.get_current_mts.pk }}" target="_blank">{{ record.get_current_mts.mts_id }}</a>')
    drs = tables.TemplateColumn(verbose_name='DRS',
                                template_code='<a href="/transit/drs/{{ record.get_current_drs.pk }}" target="_blank">{{ record.get_current_drs.drs_id }}</a>')
    dto = tables.TemplateColumn(verbose_name='DTO',
                                template_code='<a href="/transit/dto/{{ record.get_current_dto.pk }}" target="_blank">{{ record.get_current_dto.dto_id }}</a>')
    rto = tables.TemplateColumn(verbose_name='RTO',
                                template_code='<a href="/transit/rto/{{ record.get_current_rto.pk }}" target="_blank">{{ record.get_current_rto.rto_id }}</a>')
    current_branch = tables.Column(accessor='awb_status.current_branch.branch_name')
    pickup_branch = tables.Column(accessor='get_pickup_branch',
                                  order_by='pincode__branch_pincode__branch__branch_name')
    rto_branch = tables.Column(accessor='get_rto_branch', order_by='awb_status__manifest__branch',
                               verbose_name='RTO Branch')
    delivery_branch = tables.Column(accessor='get_delivery_branch.branch_name',
                                    verbose_name='Delivery/DTO Branch',
                                    order_by='pincode__branch_pincode__branch__branch_name')
    expected_cod = tables.Column(accessor='expected_amount', verbose_name='Expected COD')
    collected_cod = tables.Column(accessor='awb_status.collected_amt', verbose_name='Collected COD',
                                  order_by='awb_status__collected_amt')

    class Meta:
        model = AWB
        fields = ('awb',)
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed", "id": "awb_search_tbl"}


class AWBFLTable(tables.Table):
    awb = tables.TemplateColumn(
        template_code='<a href="/transit/awb/{{ record.id }}" >{{ record.awb }}</a>')
    client = tables.Column(accessor='awb_status.manifest.client.client_name')
    category = tables.Column(accessor='category')
    address = tables.TemplateColumn(template_code='{{ record.address_1 }}, {{ record.address_2 }}, {{ record.city }}')
    status = tables.Column(accessor='awb_status.get_readable_choice', order_by='awb_status__status')
    current_branch = tables.Column(accessor='awb_status.current_branch.branch_name')
    delivery_branch = tables.Column(accessor='get_delivery_branch.branch_name', verbose_name='Delivery Branch',
                                    order_by='pincode__branch_pincode__branch__branch_name')

    class Meta:
        model = AWB
        fields = ('awb', 'order_id', 'customer_name', 'address', 'pincode')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}


class AWBRLTable(tables.Table):
    awb = tables.TemplateColumn(
        template_code='<a href="/transit/awb/{{ record.id }}" >{{ record.awb }}</a>')
    client = tables.Column(accessor='awb_status.manifest.client.client_name')
    category = tables.Column(accessor='category')
    address = tables.TemplateColumn(template_code='{{ record.address_1 }}, {{ record.address_2 }}, {{ record.city }}')
    status = tables.Column(accessor='awb_status.get_readable_choice', order_by='awb_status__status')
    pickup_branch = tables.Column(accessor='get_pickup_branch.branch_name', verbose_name='Pick-up Branch',
                                  order_by='pincode__branch_pincode__branch__branch_name')
    current_branch = tables.Column(accessor='awb_status.current_branch.branch_name')
    dto_branch = tables.Column(accessor='get_delivery_branch.branch_name', verbose_name='DTO Branch',
                               order_by='awb_status__manifest__branch__branch_name')

    class Meta:
        model = AWB
        fields = ('awb', 'order_id', 'customer_name', 'address', 'pincode',)
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}


class AWBCashTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super(AWBCashTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )

    awb = tables.TemplateColumn(
        template_code='<a href="/transit/awb/{{ record.id }}" target="_blank">{{ record.awb }}</a>')
    status = tables.Column(accessor='awb_status.get_readable_choice', order_by='awb_status__status')
    delivery_branch = tables.Column(accessor='get_delivery_branch.branch_name', verbose_name='Delivery Branch',
                                    order_by='pincode__branch_pincode__branch__branch_name')
    expected_amount = tables.Column(accessor='expected_amount')
    collected_amount = tables.Column(accessor='awb_status.collected_amt', order_by='awb_status__collected_amt')
    date = tables.DateTimeColumn(accessor='awb_status.current_drs.creation_date',
                                 order_by='awb_status__current_drs__creation_date')
    # current_branch = tables.Column(accessor='awb_status.current_branch.branch_name')
    # delivery_branch = tables.Column(accessor='get_delivery_branch.branch_name', verbose_name='Delivery Branch',
    #                                 order_by='pincode__branch_pincode__branch__branch_name')


    def render_s_no(self):
        return next(self.counter) + 1

    class Meta:
        model = AWB
        fields = ('s_no', 'awb', 'order_id')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}


class ManifestTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super(ManifestTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    manifest = tables.TemplateColumn(
        template_code='<a href="/transit/manifest/{{ record.id }}" >MAN_{{ record.client }}{{ record.id }}</a>')
    awb_expected = tables.Column(accessor='get_expected_awb_count', sortable=False)
    awb_arrived = tables.Column(accessor='get_in_scanned_awb_count', sortable=False)
    awb_delivered = tables.Column(accessor='get_delivered_awb_count', sortable=False)
    upload_date = tables.DateTimeColumn(accessor='creation_date', order_by='creation_date', verbose_name='Upload Date')

    def render_s_no(self):
        return next(self.counter) + 1

    class Meta:
        model = Manifest
        fields = ('s_no', 'manifest', 'client', 'status', 'category', 'uploaded_by', 'branch')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}


class AWBMISTable(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )
    awb = tables.Column(accessor='awb', verbose_name='AWB')


    class Meta:
        model = AWB
        fields = ('s_no', 'awb', 'order_id')
        attrs = {"class": "table table-striped table-bordered table-hover table-condensed"}


