from __future__ import unicode_literals
from datetime import datetime, timedelta, date
from time import gmtime, strftime
import json
import re
import os
import csv

from django.http.response import HttpResponseRedirect
from django.views.generic.base import View
from django_tables2 import RequestConfig
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from django.db import transaction
from django.core import serializers
from django.core.cache import cache
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.http import StreamingHttpResponse



















# from ws4redis.publisher import RedisPublisher
from awb.forms import AWBAdminForm

from awb.models import inscan_awb
from awb.tables import AWBFrontDashboardTable
from awb.tasks import awb_bulk_update, get_awb_mis_download, get_cod_report, upload_manifest_new_task
from custom.custom_mixins import GeneralLoginRequiredMixin, CommonViewMixin
from report.mis import MISHandler
from report.performance import PerformanceReportMixin
from transit.tasks import send_issue_notification
from utils.upload import get_manifest_filename, upload_manifest_data, generate_barcode, handle_uploaded_file, \
    generate_printer_barcode, upload_manifest_data_update, awb_volumetric_detail_upload, awb_vendor_upload_file
from utils.constants import AWB_STATUS, AWB_FL_REMARKS, AWB_RL_REMARKS, MIS_HEADER, AWB_CREATE_DRS_STATUS, \
    AWB_RL_CREATE_DRS_STATUS_LIST, AWB_FL_CREATE_DRS_STATUS_LIST, COD_REPORT_HEADER, AWB_RL_REASON, AWB_FL_REASON, \
    AWB_CC_CALLING_STATUS, TEST_CLIENT_LIST
from .forms import UploadManifestForm
from .tables import AWBTable, ManifestTable, AWBFLTable, AWBRLTable
from .models import AWB, Manifest, AWB_Status, AWB_History, search_awbs, AWB_Images, AWB_Notification
from .tasks import generate_mis
from client.models import Client_Warehouse, Client
from internal.models import Branch_Pincode, Branch
from transit.models import get_open_mtss, TB, MTS
from utils.common import search_query, get_user_emails, QueryBuilder, set_cache, get_cache, set_message
from transit.tables import TBTable, MTSOutgoingTable, DRSTable, DTOTable, RTOTable
from logistics.settings import MEDIA_ROOT
from common.forms import ExcelUploadForm
from utils.common import Echo
from zoning.models import City


class AWBList(GeneralLoginRequiredMixin, CommonViewMixin, View):
    template_name = 'awb/awb.html'

    # def initialize_view(self, request, *args, **kwargs):
    # pass



    def get(self, request, *args, **kwargs):
        attr = str(request.GET.get('attr'))
        # if cache.get(attr + '_' + request.user.pk) is not None:
        # awbs = cache.get(attr + '_' + request.user.pk)
        # else:
        kwargs = dict()
        kwargs['category'] = 'REV'
        if request.GET.get('date') == '':
            kwargs['duration'] = int(request.session.get('dashboard_date_range'))
        else:
            kwargs['start_date'] = request.GET.get('date')
            kwargs['end_date'] = request.GET.get('date')
        kwargs['pickup_branch'] = str(request.GET.get('branch'))
        # print kwargs
        report = PerformanceReportMixin(**kwargs)
        # report.get_total_awb()
        self.awbs = AWB.objects.none()
        if hasattr(report, attr):
            self.awbs = getattr(report, attr)()
        # print awbs
        self.table = AWBFrontDashboardTable(self.awbs)
        if 'export_mis' in request.GET:
            awbs = [MIS_HEADER]
            awbs += self.awbs
            pseudo_buffer = Echo()
            writer = csv.writer(pseudo_buffer)
            response = StreamingHttpResponse((writer.writerow(get_awb_mis_download(awb)) for awb in awbs),
                                             content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename="MIS_' + '_' + str(
                request.GET['date']) + '_' + datetime.today().strftime('%Y-%m-%d_%H:%M:%S') + '.csv"'
            return response
        else:
            RequestConfig(request, paginate={"per_page": 100}).configure(self.table)
            return render(request, 'awb/awb.html', {'rl_tbl': self.table})


# class RealDashboard(View):
# facility = 'unique_changes'
# audience = {'broadcast': True}
#
# def __init__(self):
# self.redis_publisher = RedisPublisher(facility = self.facility , **self.audience)
#
# def get(self,request):
# message = 'message'
# self.redis_publisher.publish_message(message)


@login_required(login_url='/login')
def awb_incoming(request):
    if 'branch' in request.session:
        fl_tbl = AWBFLTable(AWB.objects.filter(pincode__branch_pincode__branch_id=request.session['branch'],
                                               category__in=['COD', 'PRE']).exclude(
            awb_status__status__in=['DEL', 'RET'])).prefetch_related('pincode__branch_pincode__branch',
                                                                     'awb_status__manifest__client')
        rl_tbl = AWBRLTable(
            AWB.objects.filter(pincode__branch_pincode__branch_id=request.session['branch'], category='REV').exclude(
                awb_status__status__in=['DEL', 'RET'])).prefetch_related('pincode__branch_pincode__branch',
                                                                         'awb_status__manifest__client')
    else:
        fl_tbl = AWBFLTable(
            AWB.objects.filter(category__in=['COD', 'PRE']).exclude(
                awb_status__status__in=['DEL', 'RET'])).prefetch_related('pincode__branch_pincode__branch',
                                                                         'awb_status__manifest__client')
        rl_tbl = AWBRLTable(
            AWB.objects.filter(category='REV').exclude(awb_status__status__in=['DEL', 'RET'])).prefetch_related(
            'pincode__branch_pincode__branch',
            'awb_status__manifest__client')
    RequestConfig(request, paginate={"per_page": 10}).configure(fl_tbl)
    RequestConfig(request, paginate={"per_page": 10}).configure(rl_tbl)
    return render(request, 'awb/awb.html', {'fl_tbl': fl_tbl, 'rl_tbl': rl_tbl, 'type': 'incoming'})


def awb_outgoing(request):
    if 'branch' in request.session:
        fl_tbl = AWBFLTable(AWB.objects.filter(awb_status__current_branch_id=request.session['branch'],
                                               category__in=['COD', 'PRE']).exclude(
            awb_status__status__in=['DEL', 'RET']))
        rl_tbl = AWBRLTable(
            AWB.objects.filter(awb_status__current_branch_id=request.session['branch'], category='REV').exclude(
                awb_status__status__in=['DEL', 'RET']))
    else:
        fl_tbl = AWBFLTable(
            AWB.objects.filter(category__in=['COD', 'PRE']).exclude(awb_status__status__in=['DEL', 'RET']))
        rl_tbl = AWBRLTable(AWB.objects.filter(category='REV').exclude(awb_status__status__in=['DEL', 'RET']))
    RequestConfig(request, paginate={"per_page": 10}).configure(fl_tbl)
    RequestConfig(request, paginate={"per_page": 10}).configure(rl_tbl)
    return render(request, 'awb/awb.html', {'fl_tbl': fl_tbl, 'rl_tbl': rl_tbl, 'type': 'outgoing'})


def awb_history(request, awb_id):
    awb = AWB.objects.prefetch_related('awb_status', 'awb_history_set').get(pk=int(awb_id))

    if request.user.is_superuser:
        superuser = True
    else:
        superuser = False
    context = {'awb_hist': awb.get_awb_history(True, superuser), 'awb_details': awb}
    print len(context['awb_hist'])
    try:
        context['awb_invoice_image'] = AWB_Images.objects.get(awb=awb, type='INV').image
    except:
        pass
    try:
        context['awb_shipment_image'] = AWB_Images.objects.get(awb=awb, type='SHI').image
    except:
        pass
    try:
        context['awb_notification'] = AWB_Notification.objects.filter(awb=awb)
    except:
        pass
    return render(request, 'awb/awb_history.html', context)


def awb_history_external(request, awb_id):
    data = {}
    try:
        awb = AWB.objects.get(awb=awb_id)
        data['details'] = serializers.serialize("json", AWB.objects.filter(awb=awb_id))
        data['history'] = awb.get_awb_history(False, False)
        data['pincode'] = awb.pincode.pincode
        data['expected_delivery_date'] = awb.get_expected_delivery_date()
        data['invoice_image'] = serializers.serialize("json", awb.get_invoice_image())
        data['shipment_image'] = serializers.serialize("json", awb.get_shipment_image())
    except:
        pass
    return HttpResponse(json.dumps(data), content_type="application/json")


@login_required(login_url='/login')
def awb_generate_mis(request):
    if request.method == 'POST' and request.POST['start_date'] != '' and request.POST['end_date'] != '':
        bind = {}
        bind.update(request.POST)
        bind['email'] = get_user_emails(request.user)
        type = 'internal'
        if 'client' in request.POST:
            client = Client.objects.get(client_code=str(request.POST['client']))
            bind['client_category'] = client.category
        else:
            client = ''
        if 'client' in request.session:
            bind['client'] = request.session['client']
            client = Client.objects.get(client_code=request.session['client'])
            type = 'client'
            bind['client_category'] = client.category

        start_date = request.POST['start_date'] + ' 00:00:00'
        end_date = request.POST['end_date'] + ' 23:59:59'

        if client != '':
            awbs = AWB.objects.filter(awb_status__manifest__client=client,
                                      creation_date__range=(start_date, end_date))
        else:
            awbs = AWB.objects.filter(creation_date__range=(start_date, end_date))

        if len(bind['email']) > 1:
            mailing_list = ', '.join(bind['email'])
        else:
            mailing_list = str(bind['email'][0])
        if len(awbs) > 0:
            if request.is_ajax():
                result = generate_mis.delay([awb.pk for awb in awbs], bind, 'email', type)
                if result.get():
                    request.session['message'] = {}
                    request.session['message']['class'] = 'success'
                    request.session['message'][
                        'report'] = 'MIS sent successfully on ' + mailing_list + ' please check you mail.'
                    return HttpResponse(True)
                else:
                    request.session['message'] = {}
                    request.session['message']['class'] = 'error'
                    request.session['message']['report'] = 'Server Error'
                    return HttpResponse('')
            else:
                result = generate_mis.delay([awb.pk for awb in awbs], bind, 'download', type)
                file = result.get()
                return render(request, 'awb/awb_generate_mis.html', {'clients': Client.objects.all(), 'file': file})
        else:
            return render(request, 'awb/awb_generate_mis.html', {'clients': Client.objects.all()})
    else:
        return render(request, 'awb/awb_generate_mis.html', {'clients': Client.objects.all()})


class MISDownload(GeneralLoginRequiredMixin, CommonViewMixin, View):
    # self.dashboard.get_dtod_awb_perc()
    def post(self, request, *args, **kwargs):
        if request.method == 'POST' and request.POST['start_date'] != '' and request.POST['end_date'] != '':
            start_date = request.POST['start_date'] + ' 00:00:00'
            end_date = request.POST['end_date'] + ' 23:59:59'
            awbs = [MIS_HEADER]
            awbs += AWB.objects.filter(creation_date__range=(start_date, end_date),
                                       awb_status__manifest__client__client_code=request.POST
                                       ['client'])
            pseudo_buffer = Echo()
            writer = csv.writer(pseudo_buffer)
            response = StreamingHttpResponse((writer.writerow(get_awb_mis_download(awb)) for awb in awbs),
                                             content_type="text/csv")
            response['Content-Disposition'] = 'attachment; filename="MIS_' + str(
                request.POST['client']) + '_' + str(request.POST['start_date']) + '_' + str(
                request.POST['end_date']) + '_' + datetime.today().strftime('%Y-%m-%d_%H:%M:%S') + '.csv"'
            return response


@login_required(login_url='/login')
def manifest(request):
    table = ManifestTable(Manifest.objects.filter(status='O').order_by('-creation_date'))
    RequestConfig(request, paginate={"per_page": 10}).configure(table)
    return render(request, 'common/table.html',
                  {'table': table, 'model': 'manifest', 'url': '/transit/manifest/upload'})


@csrf_exempt
@transaction.atomic
@login_required(login_url='/login')
def upload_manifest_file(request):
    if request.method == 'POST':
        form = UploadManifestForm(request.POST, request.FILES)
        form.warehouse = Client_Warehouse.objects.filter(client_id=request.POST['client'])
        if form.is_valid():
            manifest = form.save(commit=False)
            file = get_manifest_filename(request.POST, request.FILES['file'])
            manifest.file = file
            manifest.uploaded_by = request.user
            pincode = Client_Warehouse.objects.get(pk=int(request.POST['warehouse'])).pincode
            manifest.branch = Branch_Pincode.objects.get(pincode=pincode).branch
            manifest.save()
            # client = Client.objects.get(pk=int(request.POST['client']))
            email = request.user.email
            upload_manifest_data.apply_async(args=[manifest, request.user], queue='priority_high')
            message = 'Your file is being uploaded. Upload report will be sent on ' + email + ' shortly.'
            # if len(awb_uploaded) > 0:
                #     messages.success(request, '%s uploaded successfully' % file.split('/')[2])
            # messages.success(request, '%s uploaded successfully' % file.split('/')[2])
            # else:
            # manifest.delete()
            # messages.error(request, '%s already uploaded' % file.split('/')[2])
            # context = {
            # 'manifest': file.split('/')[2],
            # 'awb_uploaded': awb_uploaded,
            # 'awb_existing': awb_existing,
            # 'wrong_pincode': wrong_pincode,
            # 'wrong_awb': wrong_awb,
            # 'order_id_existing': AWB.objects.filter(pk__in=order_id_existing)
            # }
            return render(request, 'awb/upload_manifest.html', {'message': message})
    else:
        form = UploadManifestForm()
    return render(request, 'awb/upload_manifest.html', {'form': form})


@csrf_exempt
@login_required(login_url='/login')
def upload_manifest_file_update(request):
    if request.method == 'POST':
        print request.FILES['file'].name
        file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                    MEDIA_ROOT + 'uploads/manifest/')
        # print file
        upload_manifest_data_update.delay(file)
    return render(request, 'common/awbs_update_form.html')


@csrf_exempt
@login_required(login_url='/login')
def upload_manifest(request):
    if request.method == 'POST':
        # print request.POST
        form = UploadManifestForm(request.POST, request.FILES)
        form.warehouse = Client_Warehouse.objects.filter(client_id=request.POST['client'])
        if form.is_valid():
            manifest = form.save(commit=False)
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/manifest/')
            manifest.file = file
            manifest.uploaded_by = request.user
            # dir = MEDIA_ROOT + '/upload/manifest/'
            # file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name, dir)
            # manifest = Manifest.objects.create(file = file, uploaded_by = request.user)
            pincode = Client_Warehouse.objects.get(pk=int(request.POST['warehouse'])).pincode
            manifest.branch = Branch_Pincode.objects.get(pincode=pincode).branch
            manifest.save()
            user = request.user
            email = user.email
            upload_manifest_new_task.apply_async(args=[file, user, manifest], queue='priority_high')
            # awb_manifest = ManifestImporterModel(source=file, user=user, manifest=manifest)
            # awb_manifest.save()
            # upload_manifest.apply_async(queue='priority_high')
            message = 'Your file is being uploaded. Upload report will be sent on ' + email + ' shortly.'
        return render(request, 'awb/upload_manifest.html', {'message': message})
    else:
        form = UploadManifestForm()
        return render(request, 'awb/upload_manifest.html', {'form': form})


@login_required(login_url='/login')
def awb_in_scanning(request):
    if request.method == 'POST' and request.is_ajax():
        request.session['message'] = {}
        try:
            awb = inscan_awb(request.POST['awb'])
            # awb = AWB.objects.select_related('awb_status').get(
            # Q(awb=request.POST['awb']) |
            # Q(invoice_no=request.POST['awb'])
            # )
            # if awb.category in ['FL', 'RL']:
            if awb.awb_status.status in ['DR', 'PC']:
                if awb.get_delivery_branch().pk == int(request.session['branch']):
                    AWB_Status.objects.filter(awb=awb.pk).update(status='DCR',
                                                                 current_branch=request.session['branch'])
                    AWB_History.objects.create(status='ISC', branch_id=int(request.session['branch']), awb=awb,
                                               updated_by=request.user)
                    AWB_History.objects.create(status='DCR', branch_id=int(request.session['branch']), awb=awb,
                                               updated_by=request.user)
                    awb.readable_status = AWB_Status.objects.get(awb=awb).get_readable_choice()
                else:
                    AWB_Status.objects.filter(awb=awb.pk).update(status='ISC',
                                                                 current_branch=request.session['branch'])
                    AWB_History.objects.create(status='ISC', branch_id=int(request.session['branch']), awb=awb,
                                               updated_by=request.user)
                    awb.readable_status = 'In-Scanned'
                awb.readable_category = awb.get_readable_choice()
                manifest = awb.awb_status.manifest
                if manifest.get_expected_awb_count() == manifest.get_in_scanned_awb_count():
                    manifest.status = 'I'
                request.session['message']['class'] = 'success'
                if awb.get_vendor_branch():
                    request.session['message']['report'] = "AWB : " + str(awb.awb) + " - " + \
                                                           str(awb.readable_status) + " | Vendor Branch: " + str(
                        awb.get_vendor_branch())
                else:
                    request.session['message']['report'] = "AWB : " + str(awb.awb) + " - " + str(awb.readable_status)

            elif awb.awb_status.status in ['TB', 'TBD', 'MTS', 'MTD']:
                if awb.get_delivery_branch().pk == int(request.session['branch']):
                    AWB_Status.objects.filter(awb=awb.pk).update(status='DCR',
                                                                 current_branch=request.session['branch'])
                    AWB_History.objects.create(status='DCR', branch_id=int(request.session['branch']), awb=awb,
                                               updated_by=request.user)
                    awb.readable_status = AWB_Status.objects.get(awb=awb).get_readable_choice()
                else:
                    AWB_Status.objects.filter(awb=awb.pk).update(status='ISC',
                                                                 current_branch=request.session['branch'])
                    AWB_History.objects.create(status='ISC', branch_id=int(request.session['branch']), awb=awb,
                                               updated_by=request.user)
                    awb.readable_status = 'In-Scanned'
                request.session['message']['class'] = 'success'
                if awb.get_vendor_branch():
                    request.session['message']['report'] = "AWB : " + str(awb.awb) + " - " + \
                                                           str(awb.readable_status) + " | Vendor Branch: " + str(
                        awb.get_vendor_branch())
                else:
                    request.session['message']['report'] = "AWB : " + str(awb.awb) + " - " + str(awb.readable_status)

            elif awb.awb_status.status == 'ITR':
                if awb.get_rto_branch().pk == int(request.session['branch']):
                    AWB_Status.objects.filter(awb=awb).update(status='PFR',
                                                              current_branch=request.session['branch'])
                    AWB_History.objects.create(status='PFR', branch_id=int(request.session['branch']), awb=awb,
                                               updated_by=request.user)
                    # awb.readable_status = AWB_Status.objects.get(awb=awb).get_readable_choice()
                else:
                    # AWB_Status.objects.filter(awb=awb.pk).update(status='ISC',
                    # current_branch=request.session['branch'])
                    AWB_History.objects.create(status='ITR', branch_id=int(request.session['branch']), awb=awb,
                                               updated_by=request.user)
                awb.readable_status = AWB_Status.objects.get(awb=awb).get_readable_choice()
                request.session['message']['class'] = 'success'
                if awb.get_vendor_branch():
                    request.session['message']['report'] = "AWB : " + str(awb.awb) + " - " + \
                                                           str(awb.readable_status) + " | Vendor Branch: " + str(
                        awb.get_vendor_branch())
                else:
                    request.session['message']['report'] = "AWB : " + str(awb.awb) + " - " + str(awb.readable_status)
            else:
                awb.readable_category = awb.get_readable_choice()
                awb.readable_status = awb.awb_status.get_readable_choice()
                request.session['message']['class'] = 'error'
                request.session['message']['report'] = "AWB : " + str(awb.awb) + " - " + awb.readable_status
            # else:
            # pass
            try:
                tbs = TB.objects.select_related().get(tb_id=awb.awb_status.current_tb)
                tbs.get_in_transit_awb()
                tbs.save()
                # print tbs
                mtss = MTS.objects.select_related().get(mts_id=awb.awb_status.current_mts)
                mtss.get_mts_close()
                mtss.save()
            except:
                pass
            return render(request, 'awb/manifest_awb_in_scanning.html', {'awb': awb})
        except AWB.DoesNotExist:
            request.session['message']['class'] = 'error'
            request.session['message']['report'] = "AWB : " + request.POST['awb'] + " - Does not exists"
            return render(request, 'awb/manifest_awb_in_scanning.html')
    else:
        return render(request, 'awb/manifest_in_scanning.html', {'model': 'manifest'})


def manifest_detail(request, mainfest_id):
    table = AWBTable(AWB.objects.filter(awb_status__manifest=mainfest_id))
    RequestConfig(request, paginate={"per_page": 10}).configure(table)
    return render(request, 'common/table.html', {'table': table})


# def awb_print_invoice(request):
# if 'branch' in request.session:
# awbs = AWB.objects.filter(pincode__branch_pincode__branch_id=request.session['branch'], category='REV').exclude(
# awb_status__status__in=['DEL', 'RET'])
# else:
# awbs = AWB.objects.filter(category='REV').exclude(awb_status__status__in=['DEL', 'RET'])
# return render(request, 'awb/awb_print_invoice.html', {'awbs': awbs})
def awb_print_sheet(request, action, type):
    if cache.get(request.GET['type']) is not None:
        # if request.GET['type'] in request.session:
        # print request.GET['type']
        # print request.session[request.GET['type']]
        # awbs = AWB.objects.filter(pk__in=request.session[request.GET['type']])
        awbs = cache.get(request.GET['type'])
        for awb in awbs:
            # try:
            # open(MEDIA_ROOT + 'awb/barcode/' + awb.awb + '.png')
            # except IOError:
            if not os.path.exists(MEDIA_ROOT + 'awb/barcode/'):
                os.makedirs(MEDIA_ROOT + 'awb/barcode/')
            awb.barcode = generate_barcode(awb.awb)
            awb.save()

        if request.GET['action'] == 'barcode':
            context = {
                'awbs': awbs,
                'datetime': strftime("%Y-%m-%d %H:%M", gmtime())
            }
            return render(request, 'awb/awb_print_barcode.html', context)
        else:
            context = {
                'awbs': awbs,
                'date': strftime("%Y-%m-%d", gmtime())
            }
            return render(request, 'awb/awb_print_invoice_sheet.html', context)
    else:
        return HttpResponse('Cache Expire : Please refresh previous page and try again.')


def manifest_invoice_download(request):
    awbs = json.loads(request.POST['awbs'])
    return HttpResponse(awbs)

@csrf_exempt
def search_awb(request, extra=''):
    # r = re.compile(r"[^A-Z]{2,}[^0-9]{5,}[\s;,]{1}")
    # print request.POST
    context = {}
    # if request.POST['q'] != '' and request.POST['q'] is not None and request.POST['q'].isspace() == False:
    if request.POST.get('q') and request.POST['q'] is not None and request.POST['q'].isspace() == False:
        try:
            q = str(request.POST['q'].encode('utf-8')).upper().strip()
        except Exception:
            q = str(request.POST['q']).upper().strip()
        query = re.sub(r"[^A-Z0-9\s\;\,]", '', q)
        query = [q for q in re.split('[\s\,\;]+', query)]
        # print query
        awbs, tbs, mtss, drss, dtos, rtos = search_query(query)

        if len(awbs) > 0:
            cache.set('awb_searched_' + str(request.user.pk), awbs)
            awb_tbl = AWBTable(awbs)
            RequestConfig(request, paginate={"per_page": 1000}).configure(awb_tbl)
            context['awb_tbl'] = awb_tbl

        if len(tbs) > 0:
            tb_tbl = TBTable(tbs)
            RequestConfig(request, paginate={"per_page": 1000}).configure(tb_tbl)
            context['tb_tbl'] = tb_tbl

        if len(mtss) > 0:
            mts_tbl = MTSOutgoingTable(mtss)
            RequestConfig(request, paginate={"per_page": 1000}).configure(mts_tbl)
            context['mts_tbl'] = mts_tbl

        if len(drss) > 0:
            drs_tbl = DRSTable(drss)
            RequestConfig(request, paginate={"per_page": 1000}).configure(drs_tbl)
            context['drs_tbl'] = drs_tbl

        if len(dtos) > 0:
            dto_tbl = DTOTable(dtos)
            RequestConfig(request, paginate={"per_page": 1000}).configure(dto_tbl)
            context['dto_tbl'] = dto_tbl
        if len(rtos) > 0:
            rto_tbl = RTOTable(rtos)
            RequestConfig(request, paginate={"per_page": 1000}).configure(rto_tbl)
            context['rto_tbl'] = rto_tbl

    context['status'] = AWB_STATUS
    return render(request, 'common/search_awb_table.html', context)


def search_awb_external(request, awbs):
    data = []
    if request.GET.get('awbs') and request.GET['awbs'] is not None and request.GET['awbs'].isspace() == False:
        query = re.split('[\s,;]+', str(request.GET['awbs']).upper().strip())
        try:
            awbs_searched = search_awbs(query)
            for awb in awbs_searched:
                data.append({'pk': awb.pk, 'awb': awb.awb, 'status': awb.awb_status.get_readable_choice(),
                             'date': awb.awb_status.on_update.strftime('%d %b, %Y')})
        except:
            pass
    return HttpResponse(json.dumps(data), 'application/json')


def awb_status_update(request):
    if request.method == 'POST' and request.is_ajax():
        awbs = json.loads(request.POST['awbs'])
        for awb in awbs:
            AWB_Status.objects.filter(awb=int(awb)).update(status=request.POST['awb_status'])
            AWB_History.objects.create(awb=awb.pk, status=request.POST['awb_status'],
                                       branch_id=request.session['branch'], updated_by=request.user)
        return HttpResponse(True)
    else:
        return HttpResponse(False)


def awb_field_update(request):
    if request.method == 'POST' and request.is_ajax():
        request.session['message'] = {}
        awb = AWB.objects.get(pk=request.POST['awb'])
        if request.POST['field'] == 'weight':
            awb.weight = request.POST['val']
        elif request.POST['field'] == 'length':
            awb.length = request.POST['val']
        elif request.POST['field'] == 'breadth':
            awb.breadth = request.POST['val']
        elif request.POST['field'] == 'height':
            awb.height = request.POST['val']
        awb.save()
        request.session['message']['class'] = 'success'
        request.session['message']['report'] = 'AWB : ' + str(awb.awb) + ' | ' + request.POST['field'].upper() + \
                                               ' : ' + request.POST['val'] + ' cm'
        return HttpResponse(True)
    else:
        return HttpResponse('')


@login_required(login_url='/login')
def awb_report_cc(request):
    if request.method == 'GET' and request.is_ajax():
        filter = {}
        if request.GET['awb'] != '':
            filter['awb__startswith'] = str(request.GET['awb']).upper()
        if request.GET['client'] != '':
            filter['awb_status__manifest__client__client_code'] = request.GET['client']
        if request.GET['status'] != '':

            if request.GET['status'] == 'INT':
                filter['awb_status__status__in'] = ['TB', 'TBD', 'MTS', 'MTD']
            elif request.GET['status'] == 'DBC':
                d = date.today() - timedelta(days=5)
                filter['awb_status__status'] = request.GET['status']
                filter['creation_date__range'] = (str(d) + ' 00:00:00', str(date.today()) + ' 00:00:00' )
            else:
                filter['awb_status__status'] = request.GET['status']
        if request.GET['call_status'] != '':
            # print bool(int(request.GET['call_status']))
            filter['awb_status__connected_call'] = bool(int(request.GET['call_status']))
            # print filter
            # if request.GET['call_status'] == 'True':
            # filter['awb_status__connected_call'] =
            # print filter
            # else:
            # filter['awb_status__connected_call'] = 'False'
            # print filter
        if request.GET['start_date'] != '' and request.GET['end_date'] != '':
            filter['creation_date__range'] = (
                request.GET['start_date'] + ' 00:00:00', request.GET['end_date'] + ' 23:59:59')
        if request.GET['priority'] != '':
            filter['priority'] = request.GET['priority']
        if request.GET['remark'] != '':
            if request.GET['remark'] == 'None':
                filter['awb_status__remark'] = ''
            else:
                filter['awb_status__remark'] = request.GET['remark']
        if request.GET['role'] != '':
            filter['awb_status__updated_by__profile__role'] = request.GET['role']
        if 'branch' in request.session:
            filter['awb_status__current_branch_id'] = request.session['branch']
        awbs = AWB.objects.filter(**filter).prefetch_related('awb_status', 'awb_history_set')

        key = 'awb_searched_' + str(request.user.pk)
        if cache.get(key):
            cache.delete(key)
        cache.set(key, [awb for awb in awbs])
        call_count = dict()
        return render(request, 'awb/awb_status_update_cc.html',
                      {'awbs': awbs, 'awb_rl_remarks': AWB_RL_REMARKS, 'awb_fl_remarks': AWB_FL_REMARKS,
                       'awb_rl_reason': AWB_RL_REASON, 'awb_fl_reason': AWB_FL_REASON})

    else:
        return render(request, 'awb/awb_report_call_center.html',
                      {'clients': Client.objects.exclude(client_code__in=TEST_CLIENT_LIST).order_by('client_name'),
                       'status': AWB_CC_CALLING_STATUS,
                       'awb_rl_remarks': AWB_RL_REMARKS, 'awb_fl_remarks': AWB_FL_REMARKS,
                       'awb_fl_reason': AWB_FL_REASON,
                       'awb_rl_reason': AWB_RL_REASON})


def awb_update_by_cc(request):
    if request.method == 'POST' and request.is_ajax():
        request.session['message'] = {}
        awb = AWB_Status.objects.get(awb=int(request.POST['awb']))
        awb.call = bool(request.POST['call'])
        # print awb.call
        # print awb.call, 'call'
        # print bool(int(request.POST['connected_call']))
        awb.connected_call = bool(int(request.POST['connected_call']))
        # print awb.connected_call, 'connected'
        awb.status = request.POST['status']
        awb.remark = request.POST['remark']
        awb.reason = request.POST['reason']
        # print awb.reason, 'reason'
        awb.updated_by = request.user
        awb.save()
        AWB_History.objects.create(awb=awb.awb, call=bool(int(request.POST['call'])),
                                   connected_call=bool(int(request.POST['connected_call'])),
                                   status=request.POST['status'],
                                   updated_by=request.user, reason=request.POST['reason'],
                                   remark=request.POST['remark'])
        id = AWB.objects.get(pk=request.POST['awb'])
        request.session['message']['class'] = 'success'
        request.session['message']['report'] = 'AWB : ' + id.awb + ' | Status : ' + id.awb_status.get_readable_choice()
        return HttpResponse(True)
    else:
        request.session['message']['class'] = 'error'
        request.session['message']['report'] = 'Could\'nt Updated : Server Error'
        return HttpResponse('')


def awb_bulk_update_admin(request):
    if request.method == 'POST' and request.is_ajax():
        if request.POST['status'] != '':
            awbs = json.loads(request.POST['awbs'])
            for id in awbs:
                awb = AWB.objects.get(pk=int(id))
                if 'branch' in request.session:
                    branch = request.session['branch']
                else:
                    try:
                        branch = awb.awb_status.current_branch.pk
                    except:
                        branch = None
                AWB_Status.objects.filter(awb=awb).update(status=str(request.POST['status']),
                                                          updated_by=request.user, current_branch=branch)
                AWB_History.objects.create(awb=awb, status=request.POST['status'], branch_id=branch,
                                           updated_by=request.user)
                if request.POST['status'] not in ['INT', 'ITR']:
                    try:
                        tbs = TB.objects.select_related().get(tb_id=awb.awb_status.current_tb)
                        tbs.get_in_transit_awb()
                        tbs.save()
                        # tbs.get_current_status_tb()
                        # tbs.save()
                        mtss = MTS.objects.select_related().get(mts_id=awb.awb_status.current_mts)
                        mtss.get_mts_close()
                        mtss.save()
                    except:
                        pass
            return HttpResponse(True)
        else:
            return HttpResponse(False)


def awb_bulk_update_upload(request):
    if request.method == 'POST':
        pass
    else:
        form = ExcelUploadForm()
        return render(request, 'common/upload_form.html', {'form': form})


def awb_delete_admin(request):
    if request.method == 'POST' and request.is_ajax() and request.user.is_superuser:
        request.session['message'] = {}
        awbs = json.loads(request.POST['awbs'])
        AWB.objects.filter(pk__in=awbs).delete()
    return HttpResponse(True)


def awb_history_action_admin(request, id, action):
    if request.method == 'GET' and request.is_ajax():
        request.session['message'] = {}
        if AWB_History.objects.filter(pk=request.GET['id']).update(is_active=bool(request.GET['action'])):
            request.session['message']['class'] = 'success'
            request.session['message']['report'] = 'History deleted successfully.'
            if bool(request.GET['action']):
                return HttpResponse('')
            else:
                return HttpResponse('red')
        else:
            request.session['message']['class'] = 'error'
            request.session['message']['report'] = 'Server Error: History Couldn\'t be deleted.'
            return HttpResponse(False)


def get_incoming_awbs(request, type):
    context = {}
    rl = AWB.objects.none()
    if request.GET['type'] == 'mts':
        context['type'] = 'AWBs to be received in MTS'
        if 'branch' in request.session:
            mtss = get_open_mtss(request.session['branch'])
        else:
            mtss = get_open_mtss(None)
        fl = AWB.objects.filter(awb_status__current_tb__tb_history__mts__in=mtss, category__in=['COD', 'PRE'],
                                awb_status__status__in=['TB', 'MTS', 'MTD', 'TBD']).prefetch_related(
            'pincode__branch_pincode__branch',
            'awb_status__manifest__client')
        rl = AWB.objects.filter(awb_status__current_tb__tb_history__mts__in=mtss, category='REV',
                                awb_status__status__in=['TB', 'MTS', 'MTD', 'TBD']).prefetch_related(
            'pincode__branch_pincode__branch',
            'awb_status__manifest__client')

        request.session['mts_awb_count'] = len(fl) + len(rl)

    else:
        context['type'] = 'Incoming AWBs'
        if 'branch' in request.session:
            fl = AWB.objects.filter(pincode__branch_pincode__branch_id=request.session['branch'],
                                    category__in=['COD', 'PRE'], awb_status__status='DR').prefetch_related(
                'pincode__branch_pincode__branch',
                'awb_status__manifest__client')
            rl = AWB.objects.filter(pincode__branch_pincode__branch_id=request.session['branch'],
                                    category='REV', awb_status__status='DR').prefetch_related(
                'pincode__branch_pincode__branch',
                'awb_status__manifest__client')
        else:
            fl = AWB.objects.filter(category__in=['COD', 'PRE'], awb_status__status='DR').prefetch_related(
                'pincode__branch_pincode__branch',
                'awb_status__manifest__client')
            rl = AWB.objects.filter(category='REV', awb_status__status='DR').prefetch_related(
                'pincode__branch_pincode__branch',
                'awb_status__manifest__client')

        request.session['manifest_awb_count'] = len(fl) + len(rl)

    if len(fl) > 0:
        fl_tbl = AWBFLTable(fl)
        RequestConfig(request, paginate={"per_page": 10}).configure(fl_tbl)
        context['fl_tbl'] = fl_tbl
    if len(rl) > 0:
        rl_tbl = AWBRLTable(rl)
        RequestConfig(request, paginate={"per_page": 1000}).configure(rl_tbl)
        context['rl_tbl'] = rl_tbl
    return render(request, 'awb/awb.html', context)


def get_incoming_awbs_count(request, type):
    get_incoming_awbs(request, type)
    if request.GET['type'] == 'mts':
        return HttpResponse(request.session['mts_awb_count'])
    else:
        return HttpResponse(request.session['manifest_awb_count'])


def get_barcode(request, query):
    awb = str(request.GET['query'])
    barcode = 'awb/' + awb + '/barcode.png'
    try:
        open(MEDIA_ROOT + barcode)
    except IOError:
        if not os.path.exists(MEDIA_ROOT + 'awb/' + awb + '/'):
            os.makedirs(MEDIA_ROOT + 'awb/' + awb + '/')
        barcode = generate_printer_barcode(awb)

    return HttpResponse(barcode)


@csrf_exempt
def awb_sms_notification_ack(request, awb, type):
    awbs = AWB.objects.filter(awb=awb) | AWB.objects.filter(order_id=awb,
                                                            awb_status__manifest__client__client_code__in=['FFC',
                                                                                                           'FFP',
                                                                                                           'FOR'])
    awb = awbs[0]
    send = False
    status = ''
    if request.method == 'POST' and request.POST['Status'] == 'sent':
        send = True
        status = request.POST['Status']

    try:
        notify = AWB_Notification.objects.get(awb=awb, type=type)
    except AWB_Notification.DoesNotExist:
        notify, created = AWB_Notification.objects.get_or_create(awb=awb, type=type, status=status, send=send)

    notify.status = status
    notify.send = send
    r = notify.save()
    return HttpResponse(r)


def export_mis_searched_awb(request):
    key = 'awb_searched_' + str(request.user.pk)
    if cache.get(key) is not None:
        set_message('MIS will be emailed to you shortly', 'success')
        awbs = cache.get(key)
        # if len(awbs) > 10:
        mis = MISHandler(awbs, type='pendency', mailing_list=[request.user.email])
        mis.send_mis.apply_async(queue='priority_high')
        # awbs.insert(0, MIS_HEADER)
        # pseudo_buffer = Echo()
        # writer = csv.writer(pseudo_buffer)
        # response = StreamingHttpResponse((writer.writerow(get_mis_row_export(awb)) for awb in awbs),
        # content_type="text/csv")
        # response['Content-Disposition'] = 'attachment; filename="MIS_' + datetime.today().strftime(
        # '%Y-%m-%d_%H:%M:%S') + '.csv"'
        # #cache.delete(key)
        # return response
        return HttpResponse('MIS will be emailed to you shortly')
    else:
        return HttpResponse('Cache Expired. Please search again and export')


class AdvanceSearch(GeneralLoginRequiredMixin, CommonViewMixin, View):
    template_name = 'common/advance_search.html'

    # self.dashboard.get_dtod_awb_perc()
    def post(self, request, *args, **kwargs):
        if request.POST['q'] != '' and request.POST['q'] is not None and request.POST['q'].isspace() == False:

            try:
                q = str(request.POST['q'].encode('utf-8')).upper().strip()
            except Exception:
                q = str(request.POST['q']).upper().strip()
            # query = re.sub(r"[^A-Z0-9\s\;\,]", '', q)
            # print query
            query = [q for q in re.split('[\s\,\;\+]+', q)]
            awbs, tbs, mtss, drss, dtos, rtos = search_query(query)
            mis = MISHandler(awbs)
            return mis.download_mis()
            # awbs.insert(0, PENDENCY_MIS_HEADER)
            # #print awbs
            #
            # pseudo_buffer = Echo()
            # writer = csv.writer(pseudo_buffer)
            # response = StreamingHttpResponse((writer.writerow(mis.pendency_mis_data(awb)) for awb in awbs),
            # content_type="text/csv")
            # response['Content-Disposition'] = 'attachment; filename="dump_' + datetime.today().strftime(
            # '%Y-%m-%d_%H:%M:%S') + '.csv"'
            # return response


def awb_upload_file_update(request):
    if request.method == "POST":
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/internal/')
            awb_bulk_update.delay(file, request.user.email)
            return render(request, 'common/upload_form.html', {'msg': 'File uploaded.'})
    else:
        form = ExcelUploadForm()
        return render(request, 'common/upload_form.html', {'form': form})


class IssueNotification(GeneralLoginRequiredMixin, CommonViewMixin, View):
    template_name = 'transit/issue_notification_form.html'

    def get_context(self, request, *args, **kwargs):
        context = dict()
        context['cities'] = City.objects.all()
        context['branchs'] = Branch.objects.exclude(branch_name='HQ')
        context['clients'] = Client.objects.exclude(client_code__in=['TCF', 'TCP', 'TCR'])
        context['status'] = AWB_CREATE_DRS_STATUS
        # context['reason'] =
        return context

    def ajax_post(self, request, *args, **kwargs):
        reason = request.POST['reason']

        awb_dict = get_cache('awb_dict')

        clients = [int(i) for i in json.loads(request.POST['clients']) if i != '']

        for client in clients:
            if awb_dict:
                for awb in awb_dict[client]['awb_list']:
                    # print awb
                    send_issue_notification.delay(awb, reason)

        set_message('Issue Notification Sent', 'success')
        return HttpResponse('')

    def non_ajax_post(self, request, *args, **kwargs):
        rl_query = QueryBuilder(AWB)
        fl_query = QueryBuilder(AWB)

        rl_query.add_q(Q(category='REV'))
        fl_query.add_q(~Q(category=['REV']))

        rl_query.add_q(Q(awb_status__status__in=AWB_RL_CREATE_DRS_STATUS_LIST))
        fl_query.add_q(Q(awb_status__status__in=AWB_FL_CREATE_DRS_STATUS_LIST))

        if request.POST.get('city') != '':
            rl_query.add_q(Q(pincode__branch_pincode__branch__city__pk=request.POST.get('city')))
            fl_query.add_q(Q(pincode__branch_pincode__branch__city__pk=request.POST.get('city')))

        if request.POST.get('branch') != '':
            rl_query.add_q(Q(pincode__branch_pincode__branch__pk=request.POST.get('branch')))
            fl_query.add_q(Q(pincode__branch_pincode__branch__pk=request.POST.get('branch')))

        if request.POST.get('status') != '':
            rl_query.add_q(Q(awb_status__status=request.POST.get('status')))
            fl_query.add_q(Q(awb_status__status=request.POST.get('status')))

        if request.POST.get('client') != '':
            rl_query.add_q(Q(awb_status__manifest__client__pk=request.POST.get('client')))
            fl_query.add_q(Q(awb_status__manifest__client__pk=request.POST.get('client')))

        rl_awbs = rl_query.resolve_query().prefetch_related('awb_status__manifest',
                                                            'awb_status__manifest__client')

        fl_awbs = fl_query.resolve_query().prefetch_related('awb_status__manifest',
                                                            'awb_status__manifest__client')

        # print len(fl_awbs)

        clients = Client.objects.filter(additional__sms_notification=True)

        self.awb_dict = dict()
        # self.fl_awb_dict = dict()

        # i = 0
        for client in clients:
            if client.category == 'RL':
                awbs = [awb for awb in rl_awbs if awb.awb_status.manifest.client == client]
                if awbs:
                    self.awb_dict[client.pk] = dict()
                    self.awb_dict[client.pk]['client'] = client
                    self.awb_dict[client.pk]['awb_list'] = awbs
                    self.awb_dict[client.pk]['awb_count'] = len(awbs)
                    # i += 1
            else:
                awbs = [awb for awb in fl_awbs if awb.awb_status.manifest.client == client]
                if awbs:
                    self.awb_dict[client.pk] = dict()
                    self.awb_dict[client.pk]['client'] = client
                    self.awb_dict[client.pk]['awb_list'] = awbs
                    self.awb_dict[client.pk]['awb_count'] = len(awbs)
                    # i += 1
        # print rl_awb_dict
        context = self.get_context(request, *args, **kwargs)
        if self.awb_dict.keys():
            context['awb_dict'] = self.awb_dict
        else:
            context['error_msg'] = 'No AWB Found'
        # context['fl_awb_dict'] = self.fl_awb_dict
        #
        # set_cache('rl_awb_dict', self.awb_dict)
        set_cache('awb_dict', self.awb_dict)

        return render(request, 'transit/issue_notification_form.html', context)


class FinanceReport(GeneralLoginRequiredMixin, CommonViewMixin, View):
    template_name = 'awb/awb_finance_report.html'

    # self.dashboard.get_dtod_awb_perc()
    def get_context(self, request, *args, **kwargs):
        context = dict()
        context['branchs'] = Branch.objects.exclude(branch_name='HQ')
        context['clients'] = Client.objects.filter(category='FL').exclude(
            client_code__in=['TCF', 'TCP', 'TCR']).order_by('client_name')
        return context

    def non_ajax_post(self, request, *args, **kwargs):
        query = QueryBuilder(AWB_History)
        query.add_q(Q(awb__category='COD'))
        query.add_q(Q(status='DEL'))

        if request.POST.get('branch') != '':
            query.add_q(Q(awb__pincode__branch_pincode__branch__pk=request.POST.get('branch')))

        if request.POST.get('client') != '':
            query.add_q(Q(awb__awb_status__manifest__client__pk=request.POST.get('client')))

        if request.POST.get('start_date') != '' and request.POST.get('end_date'):
            query.add_q(Q(creation_date__range=(
                request.POST.get('start_date') + ' 00:00:00', request.POST.get('end_date') + ' 23:59:59')))

        query = query.resolve_query().prefetch_related('awb')
        awbs = AWB.objects.filter(pk__in=[h.awb.pk for h in query]).order_by('awb',
                                                                             'pincode__branch_pincode__branch').prefetch_related(
            'awb_status')
        # print len(awbs), '--', len(query)
        row = [awb for awb in awbs]
        row.insert(0, COD_REPORT_HEADER)

        pseudo_buffer = Echo()
        writer = csv.writer(pseudo_buffer)
        response = StreamingHttpResponse((writer.writerow(get_cod_report(awb)) for awb in row),
                                         content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename="COD_REPORT_' + datetime.today().strftime(
            '%Y-%m-%d_%H:%M:%S') + '.csv"'
        return response


def awb_download_csv(request):
    awb = request.GET['awb']
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="barcode.dat"'
    writer = csv.writer(response)

    writer.writerow(['AWB'])
    writer.writerow([awb])
    return response


@login_required(login_url='/login')
def awb_volumetric_upload(request):
    if request.method == "POST":
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/awb/volumetric_data')
            awb_volumetric_detail_upload.apply_async(args=[file], queue='priority_high')
            return HttpResponseRedirect('/transit/dto/create')
    else:
        form = ExcelUploadForm()
    return render(request, 'awb/volumetric_upload.html', {'form': form, 'msg': 'Volumetric Data Upload'})


@login_required(login_url='/login')
def awb_vendor_upload(request):
    if request.method == "POST":
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/client/')
            email = request.user.email
            awb_vendor_upload_file.delay(file, email)
            message = 'Your file is being uploaded. Upload report will be sent on ' + email + ' shortly.'
            return render(request, 'awb/volumetric_upload.html', {'msg': message})
    else:
        form = ExcelUploadForm()
    return render(request, 'awb/volumetric_upload.html', {'form': form, 'msg': 'AWB Vendor Update'})


def awb_admin(request):
    if request.method == 'POST':
        context = {}
        try:
            q = str(request.POST['q'].encode('utf-8')).upper().strip()
        except Exception:
            q = str(request.POST['q']).upper().strip()
        query = re.sub(r"[^A-Z0-9\s\;\,]", '', q)
        query = [q for q in re.split('[\s\,\;]+', query)]
        try:
            awb = AWB.objects.get(awb=query[0])
            if awb:
                cache.set('awb_searched_' + str(request.user.pk), awb)
                context['awb'] = awb
                context['msg'] = 'AWB Found'
        except:
            context['msg'] = 'AWB Not Found'
        return render(request, 'awb/awb_admin_update.html', context)
    else:
        message = 'Admin Update'
        return render(request, 'awb/awb_admin.html', {'msg': message})


def awb_admin_update(request, awb_id):
    awb = AWB.objects.get(pk=awb_id)
    if request.method == 'POST':
        form = AWBAdminForm(request.POST, instance=awb)
        if form.is_valid():
            f = form.save(commit=False)
            f.save()
            return HttpResponseRedirect('/transit/awb/admin')
    else:
        form = AWBAdminForm(instance=awb)
    return render(request, 'awb/awb_admin_update_form.html', {'form': form, 'model': 'AWB', 'id': awb.awb})
