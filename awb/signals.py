from celery.contrib.methods import task
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from awb.models import AWB_Detail, AWB, AWB_Status, AWB_History, AWB_Detail_History, Manifest
from client.models import Client
from zoning.models import Pincode
from datetime import *

__author__ = 'naveen'

# def save():
#     pass
    # pincode = Pincode.objects.get(pincode='122001')
    # awb =AWB.objects.create(awb='SND7992221', pincode=pincode, category='REV')
    # user = User.objects.get(username='naveen.agarwal')
    # client = Client.objects.get(client_code='SND')
    # manifest = Manifest.objects.filter(client=client)
    # AWB_Status.objects.create(awb=awb, updated_by=user, manifest=manifest[0], status='DR')
    # AWB_History.objects.create(awb=awb, updated_by=user, status='DR')

@receiver(post_save, sender=AWB)
def AWB_Signal(sender, created, instance, **kwargs):
    # print '2'
    # if created:
    print '1'
    awb = instance.awb
    order_id = instance.order_id
    crp_id = instance.crp_id
    invoice_no = instance.invoice_no
    customer_name = instance.customer_name
    address_1 = instance.address_1
    address_2 = instance.address_2
    area = instance.area
    city = instance.city
    try:
        pincode = instance.pincode.pincode
    except:
        pincode = None
    try:
        vendor_code = instance.vendor.vendor_code
    except:
        vendor_code = None
    phone_1 = instance.phone_1
    phone_2 = instance.phone_2
    package_value = instance.package_value
    package_price = instance.package_price
    expected_amount = instance.expected_amount
    weight = instance.weight
    length = instance.length
    breadth = instance.breadth
    height = instance.height
    package_sku = instance.package_sku
    description = instance.description
    category = instance.category
    priority = instance.get_priority()
    barcode = instance.barcode
    reason_for_return = instance.reason_for_return

    AWB_Detail.objects.create(awb=awb, order_id=order_id, crp_id=crp_id, invoice_no=invoice_no, customer_name=customer_name, address_1=address_1,
                              address_2=address_2, area=area, city=city, pincode=pincode, vendor_code=vendor_code, phone_1=phone_1, phone_2=phone_2,
                              package_value=package_value, package_price=package_price, expected_amount=expected_amount, weight=weight,
                              length=length, breadth=breadth, height=height, package_sku=package_sku, description=description, category=category,
                              priority=priority, barcode=barcode, reason_for_return=reason_for_return)
        
        
@receiver(post_save, sender=AWB_Status)
def AWB_Status_Signal(sender, instance, created, **kwargs):
    # if created:
    awb = instance.awb.awb
    # print awb
    awb_detail_awb = AWB_Detail.objects.filter(awb=awb)
    try:
        awbs = AWB.objects.get(awb=awb)
        manifest = instance.manifest.file
        print '8'
        try:
            current_branch_code = instance.current_branch.branch_name
            print current_branch_code
        except:
            print 'ex_curr_br'
            current_branch_code = None
        try:
            current_tb_id = instance.current_tb.tb_id
        except:
            current_tb_id = None
        try:
            current_mts_id = instance.current_mts.mts_id
        except:
            current_mts_id = None
        try:
            current_drs_id = instance.current_drs.drs_id
        except:
            current_drs_id = None
        try:
            current_dto_id = instance.current_dto.dto_id
        except:
            current_dto_id = None
        try:
            current_rto_id = instance.current_rto.rto_id
        except:
            current_rto_id = None
        try:
            current_fe = instance.current_fe.username
        except:
            current_fe = None
        collected_amt = instance.collected_amt
        status = instance.status
        print status
        remark = instance.remark
        reason = instance.reason
        try:
            updated_by = instance.updated_by.username
        except:
            updated_by = None
        call = instance.call
        connected_call = instance.connected_call
        print '232'
        # vendor = awbs.vendor
        # print vendor
        # rto_branch_code = awbs.
        # print rto_branch
        # if awb.category =='REV':
        #     if vendor is not None:
        #         dto_branch = vendor.branch
        #     else:
        #         dto_branch = instance.manifest.branch
        # else:
        #     dto_branch = None
        if awbs.category == 'REV':
            try:
                pickup_branch_code = awbs.get_pickup_branch().branch_name
                print pickup_branch_code
            except:
                pickup_branch_code = None
            delivery_branch_code = None
            rto_branch_code = None
            try:
                dto_branch_code = awbs.get_delivery_branch().branch_name
                print dto_branch_code
            except:
                dto_branch_code = None
        else:
            dto_branch_code = None
            pickup_branch_code = None
            try:
                rto_branch_code = awbs.get_rto_branch().branch_name
            except:
                rto_branch_code = None
            try:
                delivery_branch_code = awbs.get_delivery_branch().branch_name
            except:
                delivery_branch_code = None
        print rto_branch_code, delivery_branch_code, dto_branch_code
        client_code = instance.manifest.client.client_code
        print '9'
        awb_detail_awb.update(manifest=manifest, current_branch_code=current_branch_code, current_tb_id=current_tb_id, current_mts_id=current_mts_id,
                                  current_dto_id=current_dto_id, current_rto_id=current_rto_id, current_fe=current_fe, collected_amt=collected_amt,
                                  status=status, remark=remark, reason=reason, updated_by=updated_by, call=call, connected_call=connected_call,
                                  client_code=client_code, pickup_branch_code=pickup_branch_code, current_drs_id=current_drs_id,
                                  delivery_branch_code=delivery_branch_code, rto_branch_code=rto_branch_code,
                                  dto_branch_code=dto_branch_code)
        print 'acc'
    except Exception as e:
        print e
        # pass

@receiver(post_save, sender=AWB_History)
def AWB_Detail_History_Signal(sender, instance, created, **kwargs):
    # if created:
    # print 'history'
    awb = instance.awb
    # print awb
    try:
        tb = instance.tb.tb_id
    except:
        tb = None
    print tb
    try:
        mts = instance.mts.mts_id
    except:
        mts = None
    print mts
    try:
        drs = instance.drs.drs_id
    except:
        drs = None
    print drs
    try:
        dto = instance.dto.dto_id
    except:
        dto = None
    print dto
    try:
        rto = instance.rto.rto_id
    except:
        rto = None
    print rto
    try:
        fe = instance.fe.username
    except:
        fe = None
    print fe
    try:
        branch = instance.branch.branch_name
    except:
        branch = None
    try:
        updated_by = instance.updated_by.username
    except:
        updated_by = None
    try:
        AWB_Detail_History.objects.create(awb=awb, tb=tb, mts=mts, drs=drs, dto=dto, rto=rto, fe=fe, branch=branch, reason=instance.reason,
                                          remark=instance.remark, updated_by=updated_by, call=instance.call, connected_call=instance.connected_call)
        print 'history'
    except:
        print 'except'
        pass

@task
def awb_previous_data():
    start_date = '2014-09-01 00:00:00'
    # end_date = str(datetime.now())
    end_date = '2014-09-01 10:0:00'
    awbs = AWB.objects.filter(creation_date__range=[start_date, end_date])
    # print len(awbs)
    for awb in awbs:
        awb.save()
    awb_status = AWB_Status.objects.filter(awb__in=awbs)
    # print len(awb_status)
    # print awb_status[1]
    for awb_s in awb_status:
        awb_s.save()
    # awbssss = AWB.objects.get(awb='SND0321300')
    awb_history = AWB_History.objects.filter(awb__in=awbs)
    print len(awb_history)
    for awb_h in awb_history:
        print awb_h
        awb_h.save()

    # awb_history[1].save()

# def awb_previous_history_data():
#     start_date = '2014-11-01 00:00:00'
#     end_date = str(datetime.now())
#     awb
#     awb_history = AWB_History.objects.filter(creation_date__range=[start_date, end_date])
