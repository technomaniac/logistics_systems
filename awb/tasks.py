from __future__ import unicode_literals
import csv
import re
import cStringIO as StringIO
from copy import deepcopy

from celery.schedules import crontab
from celery.task.base import periodic_task
from django.template.loader import get_template
from django.template import Context
from xlrd import open_workbook
from xlutils.copy import copy as copy
from xlwt import Workbook
from celery.task import task
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.core.files import File

from awb.dumper import ManifestImporterModel
from common.tasks import send_email
from internal.models import Branch
from .models import *
from report.cs_report import Cs_Report_Summary, cs_Cancellation_Report, Pending_Defer_Calling
from report.delivery_report_fwd import DeliveryReport
from report.mis import MISHandler
from report.out_for_pickup_report import OutForPickupSummary
from report.performance import PerformanceReportMixin
from transit.models import DTO
from utils.constants import MIS_HEADER, MIS_HEADER_CLIENT_RL, MIS_HEADER_CLIENT_FL, MANIFEST_HEADER_DICT, \
    COD_REPORT_HEADER, IN_TRANSIT_STATUS_LIST, FE_REPORT_HEADER, AWB_CAN_STATUS_LIST, CAN_CS_REPORT_CLIENT, \
    DTO_OPEN_REPORT
from utils.random import get_manifest_header_dict
from utils.upload import handle_uploaded_file, send_error_report
from utils.common import get_date_range
from logistics.settings import MEDIA_ROOT
from client.models import Client_MIS_Report, Client_Dashboard_Report, Client_Vendor, Client_cs_dashboard, \
    ClientOutForPickupReport, DeliveryReportFL
from report.client_report import PerformanceReport
from zoning.models import Pincode


@task
def generate_mis(mis, bind, action, type):
    csvfile = StringIO.StringIO()
    writer = csv.writer(csvfile)

    if type == 'client':
        if 'client_category' in bind and bind['client_category'] == 'RL':
            header = MIS_HEADER_CLIENT_RL
        else:
            header = MIS_HEADER_CLIENT_FL
        filename = 'MIS_' + bind['client'] + '_' + bind['start_date'][0] + '_' + bind['end_date'][
            0] + '_' + datetime.now().strftime('%H:%M') + '.csv'
        writer.writerow(header)
        for id in mis:
            awb = AWB.objects.get(pk=id)
            writer.writerow(get_mis_row_client(awb, bind['client_category']))
    else:
        filename = 'MIS_' + bind['client'][0] + '_' + bind['start_date'][0] + '_' + bind['end_date'][
            0] + '_' + datetime.now().strftime('%H:%M') + '.csv'
        writer.writerow(MIS_HEADER)
        for id in mis:
            awb = AWB.objects.get(pk=id)
            try:
                writer.writerow(get_mis_row(awb))
            except:
                pass

    if action == 'email':
        message = EmailMessage(filename, "This is a system generated mail. Please do not reply.", "system@nuvoex.com",
                               bind['email'])

        message.attach(filename, csvfile.getvalue(), 'text/csv')
        return message.send()
    else:
        handle_uploaded_file(File(csvfile), filename, MEDIA_ROOT + 'mis/')
        return 'mis/' + filename


@periodic_task(run_every=timedelta(hours=1))
def automated_mis_sender():
    clients = Client_MIS_Report.objects.filter(is_active=True)
    if len(clients) > 0:
        for client in clients:
            if datetime.now().time().strftime('%H') == client.time.strftime('%H'):
                send_mis.delay(client)


@task
def send_mis(client):
    csvfile = StringIO.StringIO()
    writer = csv.writer(csvfile)
    start_date, end_date = get_date_range(int(client.duration), '%Y-%m-%d')
    if client.priority == 'U':
        filename = 'AUTO_MIS_' + client.type + '_' + 'Urgent Case' + '_' + client.client.client_code + '_' + start_date + '_' + end_date + '_' + datetime.now().strftime(
            '%H:%M') + '.csv'
        awbs = AWB.objects.filter(awb_status__manifest__client__client_code=client.client.client_code,
                                  priority='U', creation_date__range=(start_date + ' 00:00:00', end_date + ' 23:59:59'))

    elif client.priority == 'N':
        # filename = 'AUTO_MIS_' + client.type + '_' + client.client.client_code + '_' + start_date + '_' + end_date + '_' + datetime.now().strftime(
        filename = 'AUTO_MIS_' + client.type + '_' + 'Normal Cases' + '_' + client.client.client_code + '_' + start_date + '_' + end_date + '_' + datetime.now().strftime(
            '%H:%M') + '.csv'
        awbs = AWB.objects.filter(awb_status__manifest__client__client_code=client.client.client_code,
                                  priority__in=['L', 'N', 'H'],
                                  creation_date__range=(start_date + ' 00:00:00', end_date + ' 23:59:59'))
    else:
        filename = 'AUTO_MIS_' + client.type + '_' + client.client.client_code + '_' + start_date + '_' + end_date + '_' + datetime.now().strftime(
            '%H:%M') + '.csv'
        awbs = AWB.objects.filter(awb_status__manifest__client__client_code=client.client.client_code,
                                  creation_date__range=(start_date + ' 00:00:00', end_date + ' 23:59:59'))

    if len(awbs) > 0:
        if client.type == 'IN':
            writer.writerow(MIS_HEADER)
            for awb in awbs:
                try:
                    writer.writerow(get_mis_row(awb))
                except:
                    pass
        else:
            if client.client.category == 'RL':
                header = MIS_HEADER_CLIENT_RL
            else:
                header = MIS_HEADER_CLIENT_FL
            writer.writerow(header)
            for awb in awbs:
                try:
                    writer.writerow(get_mis_row_client(awb, client.client.category))
                except:
                    pass
        emails = re.split('[\s\,\;]', client.emails)

        if client.priority == 'U':
            subject = get_subject(client.client.client_name, 'MIS_Urgent_Case', start_date, end_date)
        else:
            subject = get_subject(client.client.client_name, 'MIS', start_date, end_date)
        message = EmailMessage(subject, "This is a system generated mail. Please do not reply.", "system@nuvoex.com",
                               emails)
        message.attach(filename, csvfile.getvalue(), 'text/csv')
        return message.send()
    else:
        return False


@periodic_task(run_every=timedelta(hours=1))
def automated_dashboard_report_sender():
    clients = Client_Dashboard_Report.objects.all()
    if len(clients) > 0:
        for client in clients:
            if datetime.now().time().strftime('%H') == client.time.strftime('%H'):
                send_dashboard.delay(client)


@task
def send_dashboard(client):
    # print start_date, end_date
    dashboard = PerformanceReport(client.client, client.duration, report_type=str(client.type))
    # print dashbbard.get_date_list()
    report = dashboard.generate_report()
    start_date, end_date = dashboard.get_date_range()
    # print report
    template = get_template('client/performance_report.html')
    context = Context({'report': report, 'client': client.client, 'start_date': start_date, 'end_date': end_date})
    content = template.render(context)
    subject = get_subject(client.client.client_name, 'PERFORMANCE', start_date, end_date)
    emails = re.split('[\s\,\;]', client.emails)
    msg = EmailMultiAlternatives(subject, "This is a system generated mail. Please do not reply.", "system@nuvoex.com",
                                 emails)
    msg.attach_alternative(content, "text/html")
    msg.send()
    # csvfile = StringIO.StringIO()
    # writer = csv.writer(csvfile)
    # date_list = get_last_working_days_list(int(client.duration), '%Y-%m-%d')
    # filename = 'Performance_Report_' + client.duration + '_' + 'Days_' + client.client.client_code + '.csv'
    #
    # if client.client.category == 'RL':
    # header_dict = CLIENT_PERFORMANCE_REPORT_RL
    # else:
    # header_dict = {}
    #
    # writer.writerow([i for i in header_dict.keys()])
    # for date in date_list:
    # writer.writerow([i for i in header_dict.keys()])
    #
    # emails = re.split('[\s\,\;]', client.emails)
    # message = EmailMessage(filename, "This is a system generated mail. Please do not reply.", "system@nuvoex.com",
    # emails)
    # message.attach(filename, csvfile.getvalue(), 'text/csv')
    # return message.send()


@periodic_task(run_every=timedelta(hours=1))
def automated_cs_dashboard_report_sender():
    clients = Client_cs_dashboard.objects.all()
    if len(clients) > 0:
        for client in clients:
            if datetime.now().time().strftime('%H') == client.time.strftime('%H'):
                send_cs_dashboard.delay(client)


@task
def send_cs_dashboard(client):
    # print start_date, end_date
    report1 = Cs_Report_Summary(client.client, client.duration)
    report2 = cs_Cancellation_Report(client.client, client.duration)
    report3 = Pending_Defer_Calling(client.client, client.duration)
    start_date, end_date = report1.get_date_range()
    template = get_template('client/cs_summary_report.html')
    context = Context({'report1': report1.generate_report(), 'report2': report2.generate_report(),
                       'report3': report3.generate_report(),
                       'client': client.client, 'start_date': start_date, 'end_date': end_date})
    content = template.render(context)
    subject = get_subject(client.client.client_name, 'CS SUMMARY REPORT', start_date, end_date)
    emails = re.split('[\s\,\;]', client.emails)
    msg = EmailMultiAlternatives(subject, "This is a system generated mail. Please do not reply.",
                                 "system@nuvoex.com", emails)
    msg.attach_alternative(content, "text/html")
    msg.send()


@periodic_task(run_every=timedelta(hours=1))
def automated_out_for_pickup_report():
    clients = ClientOutForPickupReport.objects.all()
    if len(clients) > 0:
        for client in clients:
            if datetime.now().time().strftime('%H') == client.time.strftime('%H'):
                out_for_pickup_report.delay(client)


@task
def out_for_pickup_report(client):
    # print start_date, end_date
    report = OutForPickupSummary(client.client, client.duration, report_type=str(client.type))
    start_date, end_date = report.get_date_range()
    template = get_template('client/out_for_pickup_report.html')
    context = Context({'report': report.generate_report(), 'client': client.client,
                       'start_date': start_date, 'end_date': end_date})
    content = template.render(context)
    subject = get_subject_out(client.client.client_name, 'Out For Pickup Report', start_date)
    emails = re.split('[\s\,\;]', client.emails)
    msg = EmailMultiAlternatives(subject, "This is a system generated mail. Please do not reply.",
                                 "system@nuvoex.com", emails)
    msg.attach_alternative(content, "text/html")
    msg.send()



@task
def get_invoice_workbook(awbs):
    rb = open_workbook(MEDIA_ROOT + 'awb/template/template.xls', formatting_info=True, on_demand=True)
    wb = copy(rb)
    new_book = Workbook()

    sheets = []
    for awb in awbs:
        # outsheet = new_book.add_sheet(awb.awb, cell_overwrite_ok=True)
        w_sheet = deepcopy(wb.get_sheet(0))
        w_sheet.write(2, 6, awb.get_client())
        # give the sheet a new name (disinct_employee.id_number)
        # w_sheet.set_name(distinct_employee.name)
        w_sheet.set_name(awb.awb)
        # add this sheet to the sheet list
        sheets.append(w_sheet)

        # set the sheets of the workbook
    new_book._Workbook__worksheets = sheets
    new_book.save(MEDIA_ROOT + 'awb/invoice/test.xls')


@task
def deliverAWB(id):
    awb = AWB.objects.get(pk=id)
    try:
        invoice = AWB_Images.objects.get(awb=awb, type='INV')
    except AWB_Images.DoesNotExist:
        invoice = None
    if awb.category == 'COD':
        cod = awb.awb_status.collected_amt
        if cod != '' and invoice is not None:
            awb.awb_status.status = 'DEL'
    if awb.category == 'PRE':
        if invoice is not None:
            awb.awb_status.status = 'DEL'
    if awb.category == 'REV':
        try:
            shipment_image = AWB_Images.objects.get(awb=awb, type='SHI')
        except AWB_Images.DoesNotExist:
            shipment_image = None
        if invoice is not None and shipment_image is not None:
            awb.awb_status.status = 'PC'
    if awb.save():
        return awb.awb_status.status
    else:
        return False


@task
def render_to_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()

    return result
    # if not pdf.err:
    # return HttpResponse(result.getvalue(), mimetype='application/pdf')
    # return HttpResponse('We had some errors<pre>%s</pre>' % escape(html))


def get_mis_row(awb):
    return [awb.get_awb(), awb.get_client(), awb.get_order_id(), awb.get_priority(),
            awb.get_customer_name(), awb.get_full_address(), awb.get_phone_1(),
            awb.get_pincode(), awb.get_readable_choice(), awb.get_package_value(), awb.expected_amount,
            awb.get_weight(), awb.get_length(), awb.get_breadth(), awb.get_height(),
            awb.get_description(), awb.get_vendor_name(), awb.get_vendor_code(),
            awb.get_delivery_branch(), awb.get_pickup_branch(), awb.get_drs_count(), awb.get_first_pending(),
            awb.get_first_dispatch(), awb.get_last_dispatch(), awb.get_last_scan(),
            awb.awb_status.get_readable_choice(), awb.get_last_status_updated_date(), awb.get_first_scan_branch(),
            awb.get_last_call_made_time(), awb.get_cs_call_count(), awb.get_remark(), awb.get_reason(),
            date_format(awb.creation_date, "SHORT_DATETIME_FORMAT"), awb.get_pickup_date(), awb.get_delivered_date(),
            awb.get_dto_creation_date(), awb.get_transfer_mode(), awb.reason_for_return, awb.get_no_of_attempts_count(),
            awb.get_customer_not_available_count()]


def get_mis_row_client(awb, type):
    if type == 'RL':
        return [awb.get_awb(), awb.get_order_id(), awb.get_priority(), awb.get_customer_name(), awb.get_full_address(),
                awb.get_phone_1(), awb.get_pincode(), awb.get_package_value(), awb.get_weight(), awb.get_length(),
                awb.get_breadth(), awb.get_height(), awb.get_drs_count(), awb.get_first_dispatch(),
                awb.get_last_dispatch(), awb.awb_status.get_status_client(), awb.get_last_status_updated_date(),
                awb.get_last_call_made_time(), awb.get_cs_call_count(), awb.get_remark(), awb.get_reason(),
                awb.get_pickup_date(), date_format(awb.creation_date, "SHORT_DATETIME_FORMAT"),
                awb.get_delivered_date(), awb.get_cancel_date(), awb.get_dto_creation_date(),
                awb.get_vendor_code(), awb.get_vendor_name()]
    else:
        return [awb.get_awb(), awb.get_order_id(), awb.get_customer_name(), awb.get_full_address(), awb.get_phone_1(),
                awb.get_pincode(), awb.get_readable_choice(), awb.get_package_value(), awb.expected_amount,
                awb.get_weight(), awb.get_length(), awb.get_breadth(), awb.get_height(), awb.get_description(),
                awb.awb_status.get_readable_choice(), awb.get_deferred_date(), awb.get_remark(),
                awb.get_delivery_branch().branch_name, date_format(awb.creation_date, "SHORT_DATETIME_FORMAT"),
                awb.get_delivered_date(), awb.get_first_dispatch(), awb.get_drs_count(), awb.get_current_fe()]

        # @receiver(pre_save, sender=AWB)
        # def awb_threshold_alert(sender, instance, **kwargs):
        # client = instance.awb_status.manifest.client
        # if client.awbs_consumed() % 1000 == 0:
        # pass


@task
def awb_manifest_restore():
    awbs = AWB.objects.filter(awb_status__manifest=None).order_by('awb')
    for awb in awbs:
        try:
            s = AWB_Status.objects.get(awb=awb)
            s.manifest = get_manifest_strict(awb)
            s.save()
        except:
            pass


@task
def awb_status_restore():
    awbs = AWB.objects.filter(awb_status=None).order_by('awb')
    error = []
    for awb in awbs:
        try:
            user = 1
            remark = get_reason(awb)
            reason = get_remark(awb)
            # if get_status(awb) is not None:
            # status = get_status(awb)
            # else:
            status = get_status_strict(awb)
            if status == 'DEL':
                cod = awb.expected_amount
            else:
                cod = 0
            # if status == 'CAN':
            # user = 58
            # remark = 'Couldn\'t speak to customer

            # try:
            ins = AWB_Status.objects.create(
                awb=awb,
                current_branch_id=awb.get_current_branch_hist(),
                current_tb_id=awb.get_current_tb_hist(),
                current_mts_id=awb.get_current_mts_hist(),
                current_dto_id=awb.get_current_dto_hist(),
                current_drs_id=awb.get_current_drs_hist(),
                current_rto_id=awb.get_current_rto_hist(),
                current_fe_id=awb.get_current_fe_hist(),
                status=status,
                collected_amt=cod,
                updated_by_id=user,
                remark=remark,
                reason=reason,
                manifest=get_manifest(awb)
            )
            error.append(ins.pk)
        except Exception:
            error.append(awb)
    return error


def get_manifest(awb):
    try:
        return Manifest.objects.filter(client__client_code=awb.awb[:3], creation_date__startswith=str(
            datetime.combine(awb.creation_date.date(), awb.creation_date.time()))[:16])[0]
    except:
        return None


def get_manifest_strict(awb):
    return Manifest.objects.filter(client__client_code=awb.awb[:3])[0]


def get_status_strict(awb):
    try:
        hist = awb.awb_history_set.order_by('-creation_date')[0]
        if hist.status is not None:
            return hist.status
        if hist.dto is not None:
            return 'DTO'
        if hist.rto is not None:
            return 'DFR'
        if hist.drs is not None:
            return 'DRS'
        if hist.mts is not None:
            return 'MTS'
        if hist.tb is not None:
            return 'TB'
    except:
        return 'DR'


def get_status(awb):
    try:
        hist = awb.awb_history_set.order_by('-creation_date').first()
        if hist.status != '':
            return hist.status
        elif hist.drs is not None:
            return 'DRS'
        elif hist.dto is not None:
            return 'DTO'
        elif hist.rto is not None:
            return 'DFR'
        elif hist.mts is not None:
            return 'MTS'
        elif hist.tb is not None:
            return 'TB'
        else:
            return 'DR'
    except:
        return 'DR'


def get_reason(awb):
    try:
        h = awb.awb_history_set.exclude(reason='').order_by('-creation_date')[0]
        return h.reason
    except:
        return ''


def get_remark(awb):
    try:
        h = awb.awb_history_set.exclude(remark='').order_by('-creation_date')[0]
        return h.remark
    except:
        return ''


def get_subject_out(client, type, start_date):
    return 'NUVOEX ' + client.upper() \
           + ' ' + type + ' (' + datetime.strptime(start_date, '%Y-%m-%d').strftime('%d %b, %Y') + '|' \
           + datetime.now().strftime('%d-%m-%Y %H:%M')


def get_subject(client, type, start_date, end_date):
    return 'NUVOEX ' + client.upper() \
           + ' ' + type + ' REPORT(' + datetime.strptime(start_date, '%Y-%m-%d').strftime('%d %b, %Y') + ' to ' \
           + datetime.strptime(end_date, '%Y-%m-%d').strftime('%d %b, %Y') + ') | ' + datetime.now().strftime(
        '%d-%m-%Y %H:%M')


def get_mis_row_export(awb):
    if isinstance(awb, (AWB,)):
        return get_mis_row(awb)
    else:
        return awb


def get_awb_info(awb):
    if isinstance(awb, (AWB,)):
        return [awb.get_awb(), awb.get_current_tb(), awb.get_current_mts(), awb.get_current_drs(),
                awb.get_current_dto(), awb.get_current_rto()]
    else:
        return awb


def get_awb_mis_download(awb):
    if isinstance(awb, (AWB,)):
        return get_mis_row(awb)
    else:
        return awb


@task
def awb_bulk_update(file, email):
    with open(file, 'rb') as f:
        reader = csv.reader(f)
        header = {}
        i = 0
        error_msg = ''
        for row in reader:
            if i == 0:
                [header.update(get_manifest_header_dict(row[k].lower().strip(), MANIFEST_HEADER_DICT, k)) for k in
                 xrange(len(row))]
            else:
                awb = AWB.objects.filter(awb=row[header['awb']])
                try:
                    update_dict = dict()
                    for key, value in header.iteritems():
                        if key != 'awb':
                            if key == 'pincode':
                                try:
                                    update_dict[key] = Pincode.objects.get(pincode=row[value])
                                except:
                                    error_msg += 'Wrong Pincode : ' + awb.first().awb + ' - ' + row[value] + '\n'
                            elif key == 'vendor' and row[value] != '':
                                try:
                                    update_dict[key] = Client_Vendor.objects.get(vendor_code=row[value])
                                except:
                                    update_dict[key] = None
                                    error_msg += 'Wrong Vendor : ' + awb.first().awb + ' - ' + row[value] + '\n'
                            elif key == 'category':
                                if row[value].lower().strip()[0] == 'r':
                                    update_dict[key] = 'REV'
                                elif row[value].lower().strip()[0] == 'p':
                                    update_dict[key] = 'PRE'
                                elif row[value].lower().strip()[0] == 'c':
                                    update_dict[key] = 'COD'
                            else:
                                update_dict[key] = row[value]
                    awb.update(**update_dict)
                except Exception as e:
                    error_msg += str(e) + '\n'
            i += 1
    # print error_msg
    if error_msg != '':
        send_error_report(error_msg, [email])
    else:
        send_email('Updated', 'AWBs successfully updated.', 'system@nuvoex.com', email)


@task
def awb_status_none():
    awbs = AWB.objects.filter(awb_status__status='')
    for awb in awbs:
        h = AWB_History.objects.filter(awb=awb).exclude(dto=None).order_by('-creation_date')
        if h:
            AWB_History.objects.create(awb=awb, status='DEL', updated_by_id=1)
            AWB_Status.objects.filter(awb=awb).update(status='DEL')


@periodic_task(run_every=crontab(hour=0, minute=1))
def awb_cod_collection_report():
    kwargs = {}
    day = datetime.now() - timedelta(days=1)
    date = day.strftime('%Y-%m-%d')
    kwargs['start_date'] = date
    kwargs['end_date'] = date
    branchs = Branch.objects.exclude(branch_name='HQ')
    csvfile = StringIO.StringIO()
    writer = csv.writer(csvfile)
    filename = 'COD_REPORT_' + day.strftime('%d-%b-%Y') + '.csv'
    writer.writerow(COD_REPORT_HEADER)
    for branch in branchs:
        kwargs['pickup_branch'] = int(branch.pk)
        report = PerformanceReportMixin(**kwargs)
        awbs = report.get_del_cod_awbs().order_by('awb', 'awb_status__manifest__client')
        if awbs.exists():
            for awb in awbs:
                writer.writerow(get_cod_report(awb))
            writer.writerow([])

            subject = 'COD REPORT ' + day.strftime('%d %b, %Y')
            message = EmailMessage(subject, "This is a system generated mail. Please do not reply.",
                                   "system@nuvoex.com",
                                   ['abhishek.verma@nuvoex.com', 'finance@nuvoex.com', 'bm@nuvoex.com',
                                    'sunny.arora@nuvoex.com',
                                    'dinesh.kumar@nuvoex.com'])

            message.attach(filename, csvfile.getvalue(), 'text/csv')
            return message.send()


def get_cod_report(awb):
    if isinstance(awb, (AWB,)):
        return [awb.awb, awb.get_client(), awb.get_delivery_branch().branch_name, awb.expected_amount,
                awb.get_readable_status(), awb.get_delivered_date()]
    else:
        return awb


@periodic_task(run_every=crontab(hour=0, minute=0))
def pendency_report_sender():
    pickup_pendency_report.delay()
    in_transit_pendency_report.delay()
    dto_pendency_report.delay()
    not_attempted_report.delay()
    data_received_report.delay()
    # forward_pendency_report.delay()
    # before_pickup_report.delay()


@task()
def forward_pendency_report():
    queryset = AWB.objects.filter(awb_status__status__in=['ISC', 'CNA', 'DR', 'ITR', 'DCR', 'DBC', 'SCH',
                                                          'INT', 'RBC', 'CAN'], category__in=['COD', 'PRE']
    ).order_by('-creation_date').prefetch_related('awb_status__manifest__client')
    mis = MISHandler(queryset, filename='FORWARD PENDENCY REPORT',
                     mailing_list=['naveen.agarwal@nuvoex.com', 'ops@nuvoex.com'],
                     type='pendency')
    mis.send_mis()


@task()
def data_received_report():
    queryset = AWB.objects.filter(awb_status__status='DR').order_by('awb_status__manifest__client'). \
        prefetch_related('awb_status__manifest__client')
    queryset = [awb for awb in queryset if awb.data_received_cond()]
    mis = MISHandler(queryset, filename='DATA RECEIVED REPORT',
                     mailing_list=['naveen.agarwal@nuvoex.com', 'kushal.zajua@nuvoex.com', 'ops@nuvoex.com'],
                     type='pendency')
    mis.send_mis()


@task()
def not_attempted_report():
    queryset = AWB.objects.filter(awb_status__status='NA').order_by('awb_status__manifest__client'). \
        prefetch_related('awb_status__manifest__client', 'pincode__branch_pincode__branch')
    # queryset = [awb for awb in queryset if awb.not_attempted_cond()]
    mis = MISHandler(queryset, filename='NOT ATTEMPTED REPORT',
                     mailing_list=['naveen.agarwal@nuvoex.com', 'kushal.zajua@nuvoex.com', 'ops@nuvoex.com'],
                     type='pendency')
    mis.send_mis()


@task()
def pickup_pendency_report():
    queryset = AWB.objects.filter(awb_status__status='PC', category='REV').order_by(
        'awb_status__manifest__client').prefetch_related('awb_status__manifest__client',
                                                         'pincode__branch_pincode__branch')
    queryset = [awb for awb in queryset if awb.pickup_pendency_cond()]
    mis = MISHandler(queryset, filename='PICKUP_PENDENCY_REPORT',
                     mailing_list=['abhishek.verma@nuvoex.com', 'ops@nuvoex.com'],
                     type='pendency')
    mis.send_mis()


@task()
def in_transit_pendency_report():
    queryset_mts = AWB.objects.filter(awb_status__status__in=['MTS', 'MTD'], category='REV').order_by(
        'awb_status__manifest__client').prefetch_related('awb_status__manifest__client',
                                                         'pincode__branch_pincode__branch', 'awb_history_set')
    # print len(queryset), 'before'
    queryset_mts = [awb for awb in queryset_mts if awb.in_transit_pendency_cond()]

    queryset_tb = AWB.objects.filter(awb_status__status__in=['TB', 'TBD'], category='REV').order_by(
        'awb_status__manifest__client').prefetch_related('awb_status__manifest__client',
                                                         'pincode__branch_pincode__branch', 'awb_history_set')

    queryset_tb = [awb for awb in queryset_tb if awb.in_transit_mts_pendency_cond()]
    queryset = list(queryset_mts + queryset_tb)
    # print len(queryset), 'after'
    mis = MISHandler(queryset, filename='IN_TRANSIT_PENDENCY_REPORT',
                     mailing_list=['abhishek.verma@nuvoex.com', 'ops@nuvoex.com', 'naveen.agarwal@nuvoex.com'],
                     type='transit')
    mis.send_mis()


@task()
def dto_pendency_report():
    queryset = AWB.objects.filter(awb_status__status='DCR', category='REV').order_by(
        'awb_status__manifest__client').prefetch_related('awb_status__manifest__client',
                                                         'pincode__branch_pincode__branch', 'awb_history_set')
    # print len(queryset), 'before'
    queryset = [awb for awb in queryset if awb.dto_pendency_cond()]
    # print len(queryset), 'after'
    mis = MISHandler(queryset, filename='DTO_PENDENCY_REPORT',
                     mailing_list=['abhishek.verma@nuvoex.com', 'ops@nuvoex.com'],
                     type='pendency')
    mis.send_mis()


@periodic_task(run_every=timedelta(hours=1))
def automated_delivery_report():
    clients = DeliveryReportFL.objects.all()
    if len(clients) > 0:
        for client in clients:
            if datetime.now().time().strftime('%H') == client.time.strftime('%H'):
                delivery_report.delay(client)


@task
def delivery_report(client):
    report = DeliveryReport(client.client, client.duration, report_type=str(client.type))
    start_date, end_date = report.get_date_range()
    template = get_template('client/delivered_report_fwd.html')
    context = Context({'report': report.generate_report(), 'client': client.client,
                       'start_date': start_date, 'end_date': end_date})
    content = template.render(context)
    subject = get_subject(client.client.client_name, 'Forward Delivery Report', start_date, end_date)
    emails = re.split('[\s\,\;]', client.emails)
    msg = EmailMultiAlternatives(subject, "This is a system generated mail. Please do not reply.",
                                 "system@nuvoex.com", emails)
    msg.attach_alternative(content, "text/html")
    msg.send()


@periodic_task(run_every=crontab(hour=0, minute=40))
def fe_report():
    fe = User.objects.filter(profile__role='FE', is_staff=True)
    d = datetime.today().date() - timedelta(days=1)
    start_date = str(d) + " 00:00:00"
    end_date = str(d) + " 23:59:59"
    csvfile = StringIO.StringIO()
    writer = csv.writer(csvfile)
    filename = 'FE_REPORT_' + datetime.now().strftime('%Y-%m-%d_%H:%M:%S') + '.csv'
    writer.writerow(FE_REPORT_HEADER)
    for i in fe:
        if i.first_name != '':
            # print i.username
            fe_branch = i.profile.branch
            awb_assign = AWB.objects.filter(awb_status__current_fe=i,
                                            awb_status__current_drs__creation_date__range=(
                                                start_date, end_date)).count()
            awb_pickedup = AWB.objects.filter(awb_status__current_fe=i,
                                              awb_status__current_drs__creation_date__range=(start_date, end_date),
                                              awb_history__status__in=['PC']).count()
            # awb_assigned_cancel = AWB.objects.filter(awb_status__current_fe = i,
            # awb_status__current_drs__creation_date__range=(start_date, end_date), awb_status__status = 'CNA').count()
            rows = [i, fe_branch, awb_assign, awb_pickedup]
            writer.writerow(rows)
    subject = 'FE REPORT ' + datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
    message = EmailMessage(subject, "This is a system generated mail. Please do not reply.", "system@nuvoex.com",
                           ['naveen.agarwal@nuvoex.com', 'kushal.zajua@nuvoex.com'])

    message.attach(filename, csvfile.getvalue(), 'text/csv')
    return message.send()

@periodic_task(run_every=crontab(hour=1, minute=30))
def dto_open_report_paytm():
    dtos = DTO.objects.filter(status='O', client__client_code='PTM')
    filename = 'Paytm Open DTO Report ' + datetime.now().strftime('%Y-%m-%d_%H:%M:%S') + '.csv'
    csvfile = StringIO.StringIO()
    writer = csv.writer(csvfile)
    writer.writerow(DTO_OPEN_REPORT)
    for dto in dtos:
        rows = [dto.dto_id, dto.client, dto.get_vendor_branch()]
        writer.writerow(rows)
    subject = 'Paytm Open DTO Report ' + datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
    message = EmailMessage(subject, "This is a system generated mail. Please do not reply.", "system@nuvoex.com",
                           ['naveen.agarwal@nuvoex.com', 'ggnhub@nuvoex.com', 'ops@nuvoex.com'])

    message.attach(filename, csvfile.getvalue(), 'text/csv')
    return message.send()

@periodic_task(run_every=crontab(hour=1, minute=50))
def cancelled_awb_report_calling():
    queryset = AWB.objects.select_related().filter(awb_status__status__in=AWB_CAN_STATUS_LIST,
                                                   awb_status__manifest__client__client_code__in=CAN_CS_REPORT_CLIENT).order_by(
        'awb_status__manifest__client').prefetch_related('awb_status__manifest__client',
                                                         'pincode__branch_pincode__branch', 'awb_history_set')
    # print len(queryset), 'before'
    # print queryset
    queryset = [awb for awb in queryset if awb.get_cs_call_count() <= 2 and awb.awb_can_calling_cond()]
    # print len(queryset), 'after'
    # print queryset
    mis = MISHandler(queryset, filename='CANCELLED_AWB_CALLING_REPORT',
                     mailing_list=['naveen.agarwal@nuvoex.com', 'kushal.zajua@nuvoex.com',
                                   'pallavi.srivastava@nuvoex.com', 'akanksha.swarnim@peppertap.com'],
                     type='pendency')
    mis.send_mis()


@periodic_task(run_every=crontab(hour=3, minute=0))
def before_pickup_report():
    queryset = AWB.objects.filter(awb_status__status__in=['DR', 'PP', 'CNA', 'DBC', 'SCH', 'NA'],
                                  category='REV').order_by('-creation_date'). \
        prefetch_related('awb_status__manifest__client')
    mis = MISHandler(queryset, filename='Yet to Picked Shipments',
                     mailing_list=['naveen.agarwal@nuvoex.com', 'kushal.zajua@nuvoex.com', 'ops@nuvoex.com'],
                     type='pendency')
    mis.send_mis()


@periodic_task(run_every=crontab(hour=03, minute=20))
def pending_for_dto_report():
    queryset = AWB.objects.filter(awb_status__status__in=['DCR', 'RBC', 'CB'], category='REV').order_by(
        '-creation_date'). \
        prefetch_related('awb_status')
    # print queryset
    # print len(queryset)
    mis = MISHandler(queryset, filename='Pending For DTO Shipments Report',
                     mailing_list=['ops@nuvoex.com', 'dharamvir.kumar@nuvoex.com', 'naveen.agarwal@nuvoex.com'],
                     type='pendency')
    mis.send_mis()


@task()
def upload_manifest_new_task(file, user, manifest):
    try:
        # print '12121'
        awb_manifest = ManifestImporterModel(source=file, user=user, manifest=manifest)
        awb_manifest.save()
    except:
        pass


