from django.conf.urls import patterns, url

from awb.views import AWBList, AdvanceSearch, MISDownload, IssueNotification, FinanceReport
from transit.urls import urlpatterns as transit_urls


urlpatterns = patterns('awb.views',
                       #url(r'home$',TemplateView.as_view(template_name='client/pincode.html'), name="home"),
                       #url(r'^awb/incoming$', 'awb_incoming', name='awb_incoming'),
                       url(r'^awbs$', AWBList.as_view(), name='awb_list'),
                       url(r'^awb/outgoing$', 'awb_outgoing', name='awb_outgoing'),
                       #url(r'^awb/print_invoice$', 'awb_print_invoice', name='awb_print_invoice'),
                       url(r'^awb/mis$', 'awb_generate_mis', name='awb_generate_mis'),
                       url(r'^awb/report_cc$', 'awb_report_cc', name='awb_report_cc'),
                       url(r'^awb/update_by_cc$', 'awb_update_by_cc', name='awb_update_by_cc'),
                       url(r'^awb/field_update$', 'awb_field_update', name='awb_field_update'),
                       url(r'^awb/(\d+)$', 'awb_history', name='awb_history'),
                       url(r'^tracking/awb/(\w+)$', 'awb_history_external', name='awb_history_external'),
                       url(r'^awb/status_update$', 'awb_status_update', name='status_update'),
                       url(r'^manifest/report$', 'manifest', name='show_manifest'),
                       url(r'^manifest/upload$', 'upload_manifest_file', name='upload_manifest_file'),
                       url(r'^manifest/upload_update$', 'upload_manifest_file_update', name='upload_manifest_file_update'),
                       url(r'^manifest/in_scanning$', 'awb_in_scanning', name='awb_in_scanning'),
                       url(r'^awb/in_scanning$', 'awb_in_scanning', name='awb_in_scanning'),
                       url(r'^manifest/(\d+)$', 'manifest_detail', name='manifest_detail'),
                       #url(r'^manifest/(\d+)/invoice$', 'manifest_invoice', name='manifest_invoice'),
                       url(r'^awb/print(?P<action>.*)(?P<type>.*)$', 'awb_print_sheet', name='awb_print_sheet'),
                       url(r'^search/$', 'search_awb', name='search_awb'),
                       url(r'^tracking(?P<awbs>.*)$', 'search_awb_external', name='search_awb_external'),
                       url(r'^awb/bulk_update$', 'awb_bulk_update_admin', name='awb_bulk_update_admin'),
                       url(r'^awb/delete$', 'awb_delete_admin', name='awb_delete_admin'),
                       url(r'^awb/history(?P<id>.*)(?P<action>.*)$', 'awb_history_action_admin',
                           name='awb_history_action'),
                       url(r'^awb/incoming(?P<type>.*)$', 'get_incoming_awbs', name='get_incoming_awbs'),
                       url(r'^awb/get_count(?P<type>.*)$', 'get_incoming_awbs_count',
                           name='get_incoming_awbs_count'),
                       url(r'^awb/get_barcode(?P<query>.*)$', 'get_barcode', name='get_bacrcode'),
                       url(r'^awb/(?P<awb>\w+)/sms_notification/(?P<type>.*)$', 'awb_sms_notification_ack',
                           name='awb_sms_notification_ack'),
                       url(r'^awb/export_mis_searched_awb$', 'export_mis_searched_awb', name='export_mis_searched_awb'),
                       url(r'^advance_search$', AdvanceSearch.as_view(), name="advance_search"),
                       url(r'^awb/update$', 'awb_upload_file_update', name="awb_upload_file_update"),
                       url(r'^awb/mis_download$', MISDownload.as_view()),
                       url(r'^finance_report$', FinanceReport.as_view()),
                       url(r'^send_issue_sms$', IssueNotification.as_view()),
                       url(r'^awb/download_csv$', 'awb_download_csv', name='awb_download_csv'),
                       url(r'^awb/manifest_up$', 'upload_manifest', name='upload_manifest'),
                       url(r'^awb/volumetric_upload$', 'awb_volumetric_upload', name='awb_volumetric_upload'),
                       url(r'^awb_vendor_upload$', 'awb_vendor_upload', name='awb_vendor_upload'),
                       url(r'^awb/admin$', 'awb_admin', name='awb_admin'),
                       url(r'^awb/admin_update/(?P<awb_id>.*)$', 'awb_admin_update', name='awb_admin_update')
                       # url(r'^calling_outbound_upload$', 'calling_outbound_upload', name='calling_outbound_detail'),
                       # url(r'^awb/calling_details$', 'calling_details', name='calling_details')
)

urlpatterns += transit_urls
