# import json
# from django.views.decorators.csrf import csrf_exempt
# from django.http import HttpResponse
# from django.contrib.auth import authenticate, login
#
# from utils.upload import decode_upload_image
# from utils.common import check_date_gte_current
# from awb.models import AWB, AWB_Images, AWB_Status, AWB_History
# from django.core.serializers import serialize
# # Create your views here.

# def awb_list(request):
#     data = {}
#     try:
#         # data['user'] = {}
#         # data['user']['pk'] = request.user.pk
#         # data['user']['first_name'] = request.user.first_name
#         # data['user']['last_name'] = request.user.last_name
#         awbs = AWB.objects.filter(awb_status__current_fe=request.user, awb_status__current_drs__status='O').exclude(
#             awb_status__status__in=['DEL', 'DBC', 'CNA', 'CAN', 'PC']).order_by('priority', 'category')
#         data['awbs'] = awbs.values()
#         if awbs.exists():
#             data['no_shipments'] = False
#         else:
#             data['no_shipments'] = True
#         data['http_status'] = 200
#         # for i in xrange(0, len(awbs)):
#         #     data[i] = {}
#         #     data[i]['pk'] = awbs[i].pk
#         #     data[i]['awb'] = awbs[i].awb
#         #     data[i]['order_id'] = awbs[i].order_id
#         #     data[i]['status'] = awbs[i].awb_status.get_readable_choice()
#         #     data[i]['priority'] = awbs[i].get_priority()
#         #     data[i]['address_1'] = awbs[i].address_1
#         #     data[i]['address_2'] = awbs[i].address_2
#         #     data[i]['city'] = awbs[i].city
#         #     data[i]['pincode'] = awbs[i].pincode.pincode
#         #     data[i]['phone_1'] = awbs[i].phone_1
#         #     data[i]['phone_2'] = awbs[i].phone_2
#         #     data[i]['category'] = awbs[i].category
#         #     if awbs[i].category == 'COD':
#         #         data[i]['expected_amount'] = awbs[i].expected_amount
#         #return HttpResponse(json.dumps(data), content_type='application/json')
#     except:
#         data['http_status'] = 400
#         data['msg'] = 'Some error occurred'
#     return HttpResponse(json.dumps(data), content_type='application/json')
#
#
# def awb_action(request, awb):
#     awb = AWB.objects.get(awb=awb)
#
#
# @csrf_exempt
# def awb_detail(request, awb):
#     data = {}
#     if request.method == 'POST':
#         awb_obj = AWB.objects.get(awb=request.POST['awb'])
#         dir = 'awb/' + awb_obj.awb + '/'
#         if 'status' in request.POST:
#             if request.POST['status'] == 'CAN' and request.POST['remark'] != '':
#                 AWB_Status.objects.filter(awb=awb_obj).update(status='CAN', remark=request.POST['remark'],
#                                                               updated_by=awb_obj.awb_status.current_fe)
#                 AWB_History.objects.create(awb=awb_obj, status='CAN', remark=request.POST['remark'],
#                                            updated_by=awb_obj.awb_status.current_fe)
#                 data['status'] = 200
#                 data['msg'] = 'Status updated successfully'
#             if request.POST['status'] == 'DBC':
#                 date = '%s-%s-%s' % (str(request.POST['year']), str(request.POST['month']), str(request.POST['day']))
#                 if check_date_gte_current(date, '%Y-%m-%d'):
#                     AWB_Status.objects.filter(awb=awb_obj).update(status='DBC', reason=date,
#                                                                   updated_by=awb_obj.awb_status.current_fe)
#                     AWB_History.objects.create(awb=awb_obj, status='DBC', reason=date,
#                                                updated_by=awb_obj.awb_status.current_fe)
#                     data['status'] = 200
#                     data['msg'] = 'Status updated successfully'
#                 else:
#                     data['status'] = 200
#                     data['msg'] = 'Invalid date'
#             if request.POST['status'] == 'CNA':
#                 AWB_Status.objects.filter(awb=awb_obj).update(status='CNA', updated_by=awb_obj.awb_status.current_fe)
#                 AWB_History.objects.create(awb=awb_obj, status='CNA', updated_by=awb_obj.awb_status.current_fe)
#
#                 data['status'] = 200
#                 data['msg'] = 'Status updated successfully'
#
#             if request.POST['status'] == 'DEL' or request.POST['status'] == 'PC':
#                 invoice_image = decode_upload_image(request.POST['invoice_image'], dir, 'invoice')
#                 AWB_Images.objects.get_or_create(awb=awb_obj, image=invoice_image, type='INV')
#                 if request.POST['status'] == 'PC' and awb_obj.category == 'REV':
#                     shipment_image = decode_upload_image(request.POST['shipment_image'], dir, 'shipment')
#                     AWB_Images.objects.get_or_create(awb=awb_obj, image=shipment_image, type='SHI')
#                     AWB_Status.objects.filter(awb=awb_obj).update(status='PC',
#                                                                   current_branch=awb_obj.awb_status.current_branch,
#                                                                   updated_by=awb_obj.awb_status.current_fe)
#                     AWB_History.objects.create(awb=awb_obj, status=request.POST['status'],
#                                                branch=awb_obj.awb_status.current_branch,
#                                                updated_by=awb_obj.awb_status.current_fe)
#                     data['status'] = 200
#                     data['msg'] = 'Status updated successfully'
#                 else:
#                     if awb_obj.category == 'COD':
#                         if round(float(request.POST['cod_amount'])) == round(
#                                 awb_obj.expected_amount):  #todo changed by nirmal
#                             AWB_Status.objects.filter(awb=awb_obj).update(status='DEL',
#                                                                           collected_amt=request.POST['cod_amount'],
#                                                                           updated_by=awb_obj.awb_status.current_fe)
#                             AWB_History.objects.create(awb=awb_obj, status=request.POST['status'],
#                                                        updated_by=awb_obj.awb_status.current_fe)
#                             data['status'] = 200
#                             data['msg'] = 'Status updated successfully'
#                         else:
#                             data['status'] = 200
#                             data['msg'] = 'Please enter correct amount'
#                     else:
#                         AWB_Status.objects.filter(awb=awb_obj).update(status='DEL',
#                                                                       updated_by=awb_obj.awb_status.current_fe)
#                         AWB_History.objects.create(awb=awb_obj, status=request.POST['status'],
#                                                    updated_by=awb_obj.awb_status.current_fe)
#                         data['status'] = 200
#                         data['msg'] = 'Status updated successfully'
#         else:
#             data['status'] = 200
#             data['msg'] = 'Error: status could not be updated'
#     else:
#         try:
#             awb = AWB.objects.get(awb=awb)
#             data['awb'] = awb.awb
#             data['order_id'] = awb.order_id
#             data['status'] = awb.awb_status.get_readable_choice()
#             data['priority'] = awb.get_priority()
#             data['address_1'] = awb.address_1
#             data['address_2'] = awb.address_2
#             data['city'] = awb.city
#             data['pincode'] = awb.pincode.pincode
#             data['phone_1'] = awb.phone_1
#             data['phone_2'] = awb.phone_2
#             data['category'] = awb.category
#             data['description'] = awb.description
#             if awb.category == 'COD':
#                 data['expected_amount'] = awb.expected_amount
#             if awb.category == 'REV':
#                 data['action'] = ['Pickup Completed', 'Cancelled', 'Customer not Available', 'Deferred by Customer']
#             else:
#                 data['action'] = ['Delivered', 'Cancelled', 'Customer not Available', 'Deferred by Customer']
#         except AWB.DoesNotExist:
#             data['status'] = 200
#             data['msg'] = 'AWB does not exist'
#     return HttpResponse(json.dumps(data), content_type='application/json')
#
#
# @csrf_exempt
# def login_handler(request):
#     data = {}
#     if request.method == 'POST':
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(username=username, password=password)
#         if user is not None:
#             if user.is_active:
#                 login(request, user)
#                 try:
#                     if request.user.profile.role == 'FE':
#                         data['status'] = 200
#                         data['msg'] = 'logged in'
#                     else:
#                         data['status'] = 403
#                         data['msg'] = 'Please login with a FE id'
#                         return HttpResponse(json.dumps(data), content_type='application/json')
#                 except Exception:
#                     data['status'] = 403
#                     data['msg'] = 'user/password incorrect'
#                     return HttpResponse(json.dumps(data), content_type='application/json')
#             else:
#                 data['status'] = 403
#                 data['msg'] = 'user/password incorrect'
#                 return HttpResponse(json.dumps(data), content_type='application/json')
#         else:
#             data['status'] = 403
#             data['msg'] = 'user/password incorrect'
#             return HttpResponse(json.dumps(data), content_type='application/json')
#     else:
#         data['status'] = 403
#         data['msg'] = 'please login'
#         return HttpResponse(json.dumps(data), content_type='application/json')
from django.contrib.auth import authenticate, login, logout
from django.http.response import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from rest_framework.response import Response
import os
from awb.models import AWB, AWB_Images
from custom.custom_mixins import GeneralLoginRequiredMixin
from logistics.settings import MEDIA_ROOT
from mobile.models import AppVersion
from .serializers import FeMobileSerializer, AWBMobileSerializer
from utils.common import get_json_response
from utils.constants import MOBILE_AWB_STATUS
from utils.random import get_remark_dict
from utils.upload import handle_uploaded_media


def get_latest_version(request):
    return get_json_response({"version": AppVersion.objects.get_latest_version().app_version, 'http_status': 200})


def update_app(request):
    response = HttpResponse(AppVersion.objects.all().order_by('-app_version').first().app_executable,
                            content_type='application/vnd.android.package-archive')
    response['Content-Disposition'] = 'attachment;filename=parsler.apk'
    response['Content-Length'] = AppVersion.objects.all().order_by('-app_version').first().app_executable.size
    return response


class MobileLoginRequiredMixin(GeneralLoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            logout(request)
            return get_json_response({'http_status': 403, 'msg': 'login required'})

        return super(GeneralLoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class FeAwbListView(MobileLoginRequiredMixin, View):
    #permission_classes = (IsFe,)
    # authentication_classes = []

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        # print request.COOKIES
        # print 'here'
        # print request.GET
        # return Response({
        #     'awbs': [{'awb': 1, 'category': 't'},
        #              {'awb': 2, 'category': 't2'}],
        #     'status': 200
        # })#todo for test only
        field_executive = request.user
        queryset = AWB.objects.filter(awb_status__current_fe=request.user, awb_status__current_drs__status='O',
                                      awb_status__status__in=['DRS', 'PP']).order_by('priority',
                                                                                     'category')
        #print len(queryset)
        fe_serializer = FeMobileSerializer(field_executive)
        #print queryset, 'awbship queryset'
        awb_serializer = AWBMobileSerializer(queryset, many=True)
        #print awb_serializer.data
        return get_json_response({
            'user': fe_serializer.data,
            'awbs': awb_serializer.data,
            'http_status': 200,
            'msg': 'User login successful',
            'no_shipments': False,
            'rev_count': queryset.filter(category='REV').count(),
            'pre_count': queryset.filter(category='PRE').count(),
            'cod_count': queryset.filter(category='COD').count(),
        })


class AwbActions(MobileLoginRequiredMixin, View):
    # authentication_classes = []

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        # print request.COOKIES, 'cookies'
        return super(AwbActions, self).dispatch(request, *args, **kwargs)

    def get(self, request, awb_id):
        action = request.GET.get('action')
        # print request.GET, 'get'
        awb_shipment = AWB.objects.get(pk=awb_id)
        # print type(awb_shipment)
        if action == 'CAN':
            return get_json_response(get_remark_dict(awb_shipment.category))
        # if action == 'get_reverse_selects':
        #     final_dict = awb_shipment.get_reverse_selects()
        #     final_dict['http_status'] = 200
        #     return get_json_response(final_dict)

        if action == 'get_awb_actions':
            #
            # return Response({'awb_actions': [
            #                         {'awb_action_string': 'Reverse Pickup Completed', "href": "reverse_pickup_complete.html", 'action_type': 'RPC'},
            #                         {'awb_action_string': 'Delivered to store', 'href': "delievered_to_store.html", 'action_type': 'DTS'},
            #                         {'awb_action_string': 'Customer not Available', 'href': "#", 'action_type': 'CNA'},
            #                         {'awb_action_string': 'Deffered By Customer', 'href': "deffered_by_customer.html", 'action_type': 'DFC'},
            #                         {'awb_action_string': 'Store Closed', 'href': "#", 'action_type': 'SC'},
            #                         {'awb_action_string': 'Delievered to customer', 'href': "delievered_to_customer.html", 'action_type': 'DLC'},
            #                         {'awb_action_string': 'Cancelled', 'href': "cancelled.html", 'action_type':'CAN'}
            #                         ],
            #                  'awb_number': 'fake awb',
            #                  'awb_type': 'REV', 'status': 200})
            # print awb_shipment.awb.category
            awb_actions = awb_shipment.get_awb_actions(request)
            awb_actions.update(AWBMobileSerializer(awb_shipment).data)
            if awb_actions.get('http_status') == None:
                awb_actions.update({'http_status': 200})
            # print awb_actions
            #print 'in get awb action'
            return get_json_response(awb_actions)

    def post(self, request, awb_id):
        # with open('test.jpg', 'wb+') as destination:
        #     for chunk in request.FILES['shipment_image'].chunks():
        #         destination.write(chunk)
        # action = request.POST.get('action')
        # if action == 'save_awb_into_session':
        #     request.session['awb_session'] = {}
        #     session_dict = request.session['awb_session']
        #     session_dict['awb'] = request.POST.get('awb')
        #     request.session['awb_sessssion'] = session_dict
        #     return Response({'status': 200, 'msg':'saved into session'})
        print request.POST, 'mobile wala'
        print request.FILES, 'mobile file wala'
        awb_shipment = AWB.objects.get(pk=awb_id)
        if awb_shipment.awb_status.status in MOBILE_AWB_STATUS:

            rt_dict = awb_shipment.handle_changes_from_app(request)
        else:
            rt_dict = {'http_status': 200, 'msg': 'Already Updated'}
        # rt_dict = awb_shipment.handle_changes_from_app(request)

        #print rt_dict, 'rt_dict'
        #print rt_dict, 'asdfjk'
        return get_json_response(rt_dict)

class AWBVideo(MobileLoginRequiredMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AWBVideo, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        # awb = AWB.objects.get(pk=awb_id)
        if request.method == 'POST':
            # print request.POST['awb']
            # print request.POST
            # print 'ee'
            try:
                awb = AWB.objects.get(awb = request.POST['awb'].upper())
                # if awb.get_awb_shipment_video()== '':
                filename = request.FILES['video'].name
                # print filename
                dir = 'awb/' + str(awb.awb)+'/'
                # print dir
                if not os.path.exists(MEDIA_ROOT + dir):
                    os.makedirs(MEDIA_ROOT + dir)
                filepath = handle_uploaded_media(request.FILES['video'], filename, dir)
                # print filepath
                AWB_Images.objects.create(awb = awb , type = 'VID' , image = filepath)
                # awb.type = 'VID'
                # awb.image = filepath
                # awb.save()
                return get_json_response({'http_status': 200, 'msg': 'Video Send'})
            except:
                return get_json_response({'http_status': 400, 'msg': 'Wrong AWB'})
        else:
            msg = {'http_status': 400, 'msg': 'Invalid Video'}
            return get_json_response(msg)


class MobileLoginHandler(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(MobileLoginHandler, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        c = {}
        if request.user.is_authenticated():
            # url = reverse('mobile_awb_list')
            # return HttpResponseRedirect(url)
            c['http_status'] = 200
            c['message'] = 'Login Succesful'
            return Response(c)
        c['http_status'] = 403
        c['message'] = 'Please Login'
        return get_json_response(c)

    def post(self, request):
        c = {}
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        #print type(user), 'user after authenticate'
        c['http_status'] = 403
        c['message'] = 'Invalid username or password'
        if user:
            if user.is_active:
                login(request, user)
                # url = reverse('mobile_awb_list')
                # return HttpResponseRedirect(url)
                return get_json_response({'http_status': 200, 'sessionid': request.COOKIES.get('sessionid')})
        return get_json_response({'http_status': 403, 'msg': 'Invalid User'})


class MobilelogoutHandler(MobileLoginRequiredMixin, View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(MobilelogoutHandler, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        c = {}
        logout(request)
        c['http_status'] = 200
        c['message'] = 'Logged out'
        return get_json_response(c)