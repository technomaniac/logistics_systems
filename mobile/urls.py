# from django.conf.urls import patterns, url
# from .views import *
#
# urlpatterns = patterns('mobile.views',
#                        #url(r'home$',TemplateView.as_view(template_name='client/pincode.html'), name="home"),
#                        # url(r'^awbs$', , name='awbs'),
#                        #  url(r'^awb/(\w+)$', AwbActions.as_view(), name='awb_detail'),
#                        url(r'^login/$', MobileLoginHandler.as_view(), name='login_handler'),
#                        url(r'^update_app/$', update_app),
#                        url(r'^awb_list/$', FeAwbListView.as_view(), name='awb_list'),
#                        url(r'^awb/(?P<awb_id>\w+)/actions/$', AwbActions.as_view(), name='awb_action'),
#                        url(r'^logout/$', MobilelogoutHandler.as_view(), name='login_handler'),
# )
from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns('mobile.views',
                       #url(r'home$',TemplateView.as_view(template_name='client/pincode.html'), name="home"),
                       # url(r'^awbs$', , name='awbs'),
                       #  url(r'^awb/(\w+)$', AwbActions.as_view(), name='awb_detail'),
                       url(r'^update_app/$', update_app),
                       url(r'^login/$', MobileLoginHandler.as_view(), name='login_handler'),
                       url(r'^awb_list/$', FeAwbListView.as_view(), name='awb_list'),
                       url(r'^awb/(?P<awb_id>\w+)/actions/$', AwbActions.as_view(), name='awb_action'),
                       url(r'^logout/$', MobilelogoutHandler.as_view(), name='login_handler'),
                       url(r'^awb/video$', AWBVideo.as_view() , name= 'awb_video'),
)