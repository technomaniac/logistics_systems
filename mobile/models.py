# from django.db import models
# from custom.custom_fields import AutoUserForeignKey
#
#
# class AppManager(models.Manager):
#    def check_next_version(self, version):
#        if self.all().order_by('-app_version').first().app_version == version:
#            return False
#        else:
#            return True
#
#
# class AppVersion(models.Model):
#    client_name = models.CharField(max_length=20)
#    app_version = models.PositiveIntegerField(default=0, blank=True, null=True)
#
#    def get_app_path(self, filename):
#        return '%s/%s' % (self.client_name, filename)
#
#    app_executable = models.FileField(upload_to=get_app_path)
#    objects = AppManager()
#
#    def save(self, *args, **kwargs):
#        self.app_version += 1
#        super(AppVersion, self).save(*args, **kwargs)
#
#    def get_executable(self):
#        return self.app_executable
#
#
# class AppVersionHistory(models.Model):
#    user_edited = AutoUserForeignKey(related_name='AppVersion_edited', blank=True, null=True)
#    user_created = AutoUserForeignKey(related_name='AppVersion_created', blank=True, null=True)
#    to_model = models.ForeignKey(AppVersion, related_name='AppVersion_history', blank=True, null=True)
#    app_version = models.PositiveIntegerField(default=0, blank=True, null=True)
#    version = models.PositiveIntegerField(default=0, blank=True, null=True)
#    client_name = models.CharField(max_length=20)
#
#    def get_app_path(self, filename):
#        return '%s/%s' % (self.client_name, filename)
#
#    app_executable = models.FileField(upload_to=get_app_path)

from django.db import models


class AppManager(models.Manager):
    def check_next_version(self, version):
        if self.all().order_by('-app_version').first().app_version == version:
            return False
        else:
            return True

    def get_latest_version(self):
        return self.all().order_by('-app_version').first()


class AppVersion(models.Model):
    client_name = models.CharField(max_length=20)
    app_version = models.PositiveIntegerField(default=0, blank=True, null=True)

    def get_app_path(self, filename):
        return '%s/%s' % (self.client_name, filename)

    app_executable = models.FileField(upload_to=get_app_path)
    objects = AppManager()

    def __unicode__(self):
        return 'client - %s, version - %s' % (self.client_name, str(self.app_version))

    def get_executable(self):
        return self.app_executable


