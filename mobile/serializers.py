from rest_framework import serializers
from django.contrib.auth.models import User

from awb.models import AWB


class FeMobileSerializer(serializers.ModelSerializer):
    name = serializers.Field(source='__unicode__')

    class Meta:
        model = User
        fields = ['id', 'name']


class AWBMobileSerializer(serializers.ModelSerializer):
    awb = serializers.Field(source='awb')
    category = serializers.Field(source='category')
    order_id = serializers.Field(source='order_id')
    address = serializers.Field(source='get_full_address')
    # delivery_address = serializers.Field(source='delivery_address.address')
    priority = serializers.Field(source='get_readable_choice')
    status = serializers.Field(source='awb_status.get_readable_choice')
    expected_amount = serializers.Field(source='expected_amount')
    customer_name = serializers.Field(source='customer_name')
    description = serializers.Field(source='description')

    class Meta:
        model = AWB
        fields = ['id', 'awb', 'category', 'order_id', 'address', 'phone_1', 'phone_2', 'expected_amount', 'priority',
                  'status','description','customer_name']
