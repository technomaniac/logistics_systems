import os

from django.db import models
from django.contrib.auth.models import User
from django.db.models import Q

from common.models import Time_Model
from awb.models import AWB
from logistics.settings import MEDIA_ROOT
from utils.random import get_title
from utils.constants import SAFE_EXPRESS_URL
from utils.upload import handle_uploaded_media


class TB(Time_Model):
    TB_STATUS = (
        ('D', 'Recieved'),
        ('U', 'Un-Delivered')
    )
    TB_TYPE = (
        ('N', 'Normal'),
        ('M', 'Mixed')
    )
    CO_lOADERS = (
        ('BLUEDART', 'BLUEDART'),
        ('SHEILD', 'SHEILD'),
        ('SAFE XPRESS', 'SAFE XPRESS'),
        ('OVER-NITE', 'OVER-NITE'),
        ('OTHER', 'OTHER')
    )
    tb_id = models.CharField(max_length=15, verbose_name='TB', unique=True, db_index=True)
    origin_branch = models.ForeignKey('internal.Branch', related_name='origin', db_index=True)
    delivery_branch = models.ForeignKey('internal.Branch', related_name='delivery', db_index=True)
    type = models.CharField(choices=TB_TYPE, max_length=1, default='N', db_index=True)
    length = models.FloatField(max_length=12, null=True, blank=False, db_index=True, verbose_name='Length(cm)')
    breadth = models.FloatField(max_length=12, null=True, blank=False, db_index=True, verbose_name='Breadth(cm)')
    height = models.FloatField(max_length=12, null=True, blank=False, db_index=True, verbose_name='Height(cm)')
    weight = models.FloatField(max_length=12, null=True, blank=True, db_index=True, verbose_name='Weight(kg)')
    status = models.CharField(choices=TB_STATUS, max_length=1, default='U', db_index=True)
    co_loader = models.CharField(choices=CO_lOADERS, max_length=100, null=True, blank=True, db_index=True)
    co_loader_awb = models.CharField(max_length=15, null=True, blank=True, db_index=True)

    class Meta:
        index_together = [
            ['tb_id', 'origin_branch', 'delivery_branch', 'type', 'length', 'breadth', 'height', 'weight']]

    def __unicode__(self):
        return self.tb_id

    def get_current_mts(self):
        return self.tb_history.all().order_by('-creation_date').exclude(mts=None)[0].mts

    def get_current_branch(self):
        # self.get_current_status_tb()
        if self.status == 'D':
            return self.delivery_branch
        else:
            return self.origin_branch

    def get_current_status_tb(self):
        # awbs = self.get_awb_count()
        try:
            awb_scanned = self.get_awb_scanned().count()
            # print awb_scanned
            if awb_scanned == 0:
                self.status = 'D'
            else:
                self.status = 'U'
                # return self.status
        except:
            self.status = 'U'

    def get_in_transit_awb(self):
        try:
            s = AWB.objects.filter(awb_status__current_tb=self,
                                   awb_status__status__in=['TB', 'TBD', 'MTS', 'MTD']).count()
            if s == 0:
                self.status = 'D'
            else:
                self.status = 'U'
        except:
            self.status = 'U'

    def get_awbs(self):
        return self.awb_history_set.all()

    def get_awb_count(self):
        return self.get_awbs().count()

    def get_awb_scanned(self):
        return AWB.objects.filter(awb_status__current_tb=self).exclude(awb_status__current_branch=self.delivery_branch)

    def get_transfer_mode(self):
        mts_hist = self.tb_history.exclude(mts=None).order_by('creation_date')
        transfer_mode = ''
        for h in xrange(len(mts_hist)):
            transfer_mode += mts_hist[h].mts.get_readable_type()
            if h < len(mts_hist) - 1:
                transfer_mode += ' + '
        return transfer_mode

    def get_transfer_mode_absolute(self):
        mts_hist = self.tb_history.exclude(mts=None).order_by('creation_date')
        if len([h for h in mts_hist if h.mts.type == 'A']):
            return 'Air'
        elif len([h for h in mts_hist if h.mts.type == 'V']):
            return 'Van'
        else:
            return 'Surface'
            # return transfer_mode

    def get_hand_over_date(self):
        try:
            current_mts = self.get_current_mts()
            # print current_mts
            mts = MTS.objects.get(mts_id=current_mts)
            return mts.creation_date
        except:
            return ''

    def get_mts(self):
        try:
            current_mts = self.get_current_mts()
            mts = MTS.objects.get(mts_id=current_mts)
            return mts.mts_id
        except:
            return ''

    def get_tb_id(self):
        return self.tb_id

    def get_tb_status(self):
        if self.status == 'D':
            return 'Received'
        else:
            return 'Un-Delivered'

    def get_co_loader(self):
        return self.co_loader

    def get_co_loader_awb(self):
        return self.co_loader_awb

    def get_co_loader_mts(self):
        try:
            return self.get_current_mts().co_loader
        except:
            return ''

    def get_co_loader_awb_mts(self):
        try:
            return self.get_current_mts().co_loader_awb
        except:
            return ''

    def get_origin_branch(self):
        return self.origin_branch

    def get_delivery_branch(self):
        return self.delivery_branch

    def get_dimension(self, dimension, unit='inch'):
        try:
            if unit == 'inch':
                return round(float(self.dimension) / 2.54, 2)
            else:
                return round(float(self.dimension), 2)
        except:
            return None

    def get_weight(self):
        try:
            return round(float(self.weight), 2)
        except:
            return None

    def get_length_inch(self):
        try:
            return round(float(self.length) / 2.54, 2)
        except:
            return None

    def get_length(self):
        try:
            return self.length
        except:
            return None

    def get_breadth(self):
        try:
            return self.breadth
        except:
            return None

    def get_height(self):
        try:
            return self.height
        except:
            return None

    def get_breadth_inch(self):
        try:
            return round(float(self.breadth) / 2.54, 2)
        except:
            return None

    def get_height_inch(self):
        try:
            return round(float(self.height) / 2.54, 2)
        except:
            return None

    def get_volumetric_weight(self):
        try:
            return round((float(self.length) * float(self.breadth) * float(self.height)) / 6000, 2)
        except:
            return None

    def get_compiled_weight(self):
        try:
            return self.get_volumetric_weight() if self.get_volumetric_weight() > self.weight else self.weight
        except:
            return None

    def get_received_date(self):
        try:
            return self.tb_history.filter(branch=self.delivery_branch)[0].creation_date.strftime('%d, %b, %Y')
        except:
            return None


class TB_History(Time_Model):
    tb = models.ForeignKey('TB', related_name='tb_history', db_index=True)
    mts = models.ForeignKey('MTS', null=True, blank=True, db_index=True)
    branch = models.ForeignKey('internal.Branch', null=True, blank=True, db_index=True)

    class Meta:
        index_together = [['tb', 'mts', 'branch'], ]


class MTS(Time_Model):
    MTS_STATUS = (
        ('D', 'Received'),
        ('U', 'Un-delivered'),
        ('R', 'Red Alert')
    )

    MTS_TYPE = (
        ('S', 'Surface'),
        ('A', 'Air'),
        ('V', 'Van')
    )

    CO_lOADERS = (
        ('BLUEDART', 'BLUEDART'),
        ('SHEILD', 'SHEILD'),
        ('SAFE XPRESS', 'SAFE XPRESS'),
        ('OVER-NITE', 'OVER-NITE'),
        ('OTHER', 'OTHER')
    )
    mts_id = models.CharField(max_length=15, verbose_name='MTS', unique=True, db_index=True)
    from_branch = models.ForeignKey('internal.Branch', related_name='from', db_index=True)
    to_branch = models.ForeignKey('internal.Branch', related_name='to', db_index=True)
    status = models.CharField(choices=MTS_STATUS, max_length=1, default='U', db_index=True)
    type = models.CharField(choices=MTS_TYPE, max_length=1, blank=False, db_index=True)
    external_track_id = models.CharField(max_length=50, default='', blank=True, db_index=True)
    co_loader = models.CharField(choices=CO_lOADERS, max_length=100, null=True, blank=True, db_index=True)
    co_loader_awb = models.CharField(max_length=15, null=True, blank=True, db_index=True)

    class Meta:
        index_together = [['mts_id', 'from_branch', 'to_branch', 'status', 'type', 'external_track_id'], ]

    def get_readable_choice(self):
        try:
            return dict(self.MTS_STATUS)[self.status]
        except:
            return ''

    def __unicode__(self):
        return self.mts_id

    def get_external_track_url(self):
        try:
            return SAFE_EXPRESS_URL.format(**{'sno': self.external_track_id})
        except:
            return ''

    def get_co_loader(self):
        return self.co_loader

    def get_co_loader_awb(self):
        return self.co_loader_awb

    def get_tbs(self):
        return self.tb_history_set.all()

    def get_tbs_count(self):
        return self.tb_history_set.all().count()

    def get_awbs(self):
        return AWB.objects.filter(awb_status__current_tb__tb_history__mts=self).order_by('awb')

    def get_readable_type(self):
        try:
            return dict(self.MTS_TYPE)[self.type]
        except:
            return ''

    def get_all_tbs(self):
        return TB.objects.filter(tb_history__mts=self).order_by('tb_id')

    def get_total_weight(self):
        awbs = self.get_awbs()
        weight = [float(awb.weight) for awb in awbs if awb.weight != '']
        return sum(weight)

    def get_mts_close(self):
        count = 0
        try:
            tbs = self.get_all_tbs()
            for i in tbs:
                tb = TB.objects.get(tb_id=i)
                if tb.status == 'D':
                    count = count + 1
            if count == len(tbs):
                self.status = 'D'
        except:
            self.status = 'U'

            # def mts_creat(self):
            # return self.creation_date.strftime('%d, %b, %Y')


class DRS(Time_Model):
    DRS_STATUS = (
        ('O', 'Open'),
        ('C', 'Closed'),
        ('R', 'Red Alert')
    )
    drs_id = models.CharField(max_length=15, verbose_name='DRS', unique=True, db_index=True)
    fe = models.ForeignKey(User, db_index=True)
    vehicle = models.ForeignKey('internal.Vehicle', db_index=True)
    opening_km = models.CharField(max_length=20, default='', blank=True, db_index=True)
    closing_km = models.CharField(max_length=20, default='', blank=True, db_index=True)
    branch = models.ForeignKey('internal.Branch', db_index=True)
    status = models.CharField(choices=DRS_STATUS, default='O', max_length=1, db_index=True)

    class Meta:
        index_together = [['drs_id', 'fe', 'vehicle', 'opening_km', 'closing_km', 'branch', 'status'], ]

    def __unicode__(self):
        return self.drs_id

    def get_awbs(self, category):
        return self.awb_history_set.filter(awb__category__in=category).order_by('awb__category', 'awb__awb')

    def get_readable_choice(self):
        return dict(self.DRS_STATUS)[self.status]

    def get_awb_count(self):
        return self.awb_status_set.all().count()

    def get_current_awbs(self):
        return self.awb_status_set.all()

    def get_all_awb_count(self):
        return self.awb_history_set.all().count()

    def get_fe_full_name(self):
        try:
            return get_title(self.fe.get_full_name())
        except:
            return ''

    def get_fe_contact(self):
        try:
            return self.fe.profile.phone
        except:
            return None

    def get_awb_close_count(self):
        fl = self.awb_status_set.filter(
            status__in=['DEL', 'CAN', 'DCR', 'PC', 'CNA', 'DBC', 'RET', 'SCH', 'DTO']).exclude(
            awb__category='REV').count()
        rl = self.awb_status_set.filter(
            status__in=['DEL', 'CAN', 'DCR', 'PC', 'CNA', 'DBC', 'RET', 'SCH', 'DTO', 'TB', 'TBD', 'MTS', 'MTD'],
            awb__category='REV').count()
        return fl + rl

    def get_open_awb_count(self):
        return self.awb_status_set.filter(status__in=['DRS', 'PP']).count()

    def get_expected_amount(self):
        exp_amt = self.awb_status_set.filter(manifest__category='FL').exclude(status__in=['CAN', 'CNA', 'DBC', 'RET']) \
            .aggregate(expected_amount=models.Sum('awb__expected_amount'))['expected_amount']
        return exp_amt if exp_amt > 0 else 0

    def get_collected_amount(self):
        coll_amt = self.awb_status_set.aggregate(collected_amt=models.Sum('collected_amt'))['collected_amt']
        return coll_amt if coll_amt > 0 else 0


class DTO(Time_Model):
    def dto_image_dir(self, filename):
        dir = 'transit/' + str(self.dto_id) + '/'
        if not os.path.exists(MEDIA_ROOT + dir):
            os.makedirs(MEDIA_ROOT + dir)
        filename = 'pod.jpeg'
        return dir + '/' + filename

    DTO_STATUS = (
        ('O', 'Open'),
        ('C', 'Closed'),
        ('R', 'Red Alert')
    )
    dto_id = models.CharField(max_length=15, unique=True, db_index=True)
    fe = models.ForeignKey(User, db_index=True)
    vehicle = models.ForeignKey('internal.Vehicle', db_index=True)
    branch = models.ForeignKey('internal.Branch', db_index=True)
    status = models.CharField(choices=DTO_STATUS, default='O', max_length=1, db_index=True)
    client = models.ForeignKey('client.Client', null=True, blank=True, db_index=True)
    vendor = models.ForeignKey('client.Client_Vendor', null=True, blank=True, db_index=True)
    pod = models.ImageField(upload_to=dto_image_dir, null=True, blank=True)

    class Meta:
        index_together = [['dto_id', 'fe', 'vehicle', 'branch', 'status', 'client', 'vendor'], ]

    def get_awb_count(self):
        return self.awb_status_set.all().count()

    def get_all_awb_count(self):
        return self.awb_history_set.all().count()

    def get_awb_close_count(self):
        return self.awb_status_set.filter(status__in=['DEL', 'CAN', 'RBC', 'CB']).count()

    def get_readable_choice(self):
        return dict(self.DTO_STATUS)[self.status]

    def get_open_awb_count(self):
        return self.awb_status_set.filter(status='DTO').count()

    def get_readable_status(self):
        return

    def __unicode__(self):
        return self.dto_id

    def get_vendor_branch(self):
        try:
            return self.vendor.pincode.branch_pincode.branch
        except:
            return ''
#

class RTO(Time_Model):
    RTO_STATUS = (
        ('O', 'Open'),
        ('C', 'Closed'),
        ('R', 'Red Alert')
    )
    rto_id = models.CharField(max_length=15, unique=True, db_index=True)
    fe = models.ForeignKey(User, db_index=True)
    vehicle = models.ForeignKey('internal.Vehicle', db_index=True)
    branch = models.ForeignKey('internal.Branch', db_index=True)
    status = models.CharField(choices=RTO_STATUS, default='O', max_length=1, db_index=True)
    client = models.ForeignKey('client.Client', null=True, blank=True, db_index=True)

    class Meta:
        index_together = [['rto_id', 'fe', 'vehicle', 'branch', 'status', 'client'], ]

    def get_awb_count(self):
        return self.awb_status_set.all().count()

    def get_all_awb_count(self):
        return self.awb_history_set.all().count()

    def get_awb_close_count(self):
        return self.awb_status_set.filter(status__in=['DEL', 'CAN', 'RBC', 'CB']).count()

    def get_readable_choice(self):
        return dict(self.RTO_STATUS)[self.status]

    def get_open_awb_count(self):
        return self.awb_status_set.filter(status='DFR').count()

    def __unicode__(self):
        return self.rto_id


def close_drs(drs):
    # drs = DRS.objects.get(pk=id)
    if drs.get_open_awb_count() == 0 and drs.closing_km is not None:
        drs.status = 'C'
        drs.save()
        return True
    else:
        return False


def close_dto(dto):
    if dto.get_open_awb_count() == 0:
        dto.status = 'C'
        dto.save()
        return True
    else:
        return False


def upload_dto_pod(dto, pod):
    # print pod
    ext = pod.name.split(".")[-1].lower()
    # print ext
    filename = dto.dto_id + '.' + ext
    dir = 'DTO/POD/'
    dto.pod = handle_uploaded_media(pod, filename, dir)
    dto.save()


def close_rto(rto):
    if rto.get_open_awb_count() == 0:
        rto.status = 'C'
        rto.save()
        return True
    else:
        return False


def get_open_mtss(branch=None):
    filter = {}
    filter['status'] = 'U'
    if branch is not None:
        filter['to_branch__pk'] = branch
    return MTS.objects.filter(**filter)


def get_fe_list(branch_id):
    return User.objects.filter(Q(profile__branch_id=branch_id) | Q(profile__branch_id=None), Q(profile__role='FE'),
                               Q(is_staff=True)).exclude(profile__phone=None).select_related('profile').order_by(
        'first_name')
    # return [get_title(fe.get_full_name()) for fe in fe_list]
