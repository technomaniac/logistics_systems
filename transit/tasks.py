from datetime import datetime
import csv
import cStringIO as StringIO

from celery.task import task
from django.core.mail.message import EmailMessage
from django.db.models import Count

from common.tasks import send_sms
from .models import *
from logistics.settings import EXOTEL_CALLBACK_URL
from utils.common import change_date_format
from utils.constants import DEFERRED_SMS_NOTIFICATION_TEMPLATE_RL, PICKUP_SMS_NOTIFICATION_TEMPLATE, \
    DELIVERED_SMS_NOTIFICATION_TEMPLATE, CANCEL_SMS_NOTIFICATION_TEMPLATE_RL, DEFERRED_SMS_NOTIFICATION_TEMPLATE_FL, \
    CANCEL_SMS_NOTIFICATION_TEMPLATE_FL, ISSUE_SMS_NOTIFICATION_TEMPLATE, NUVOEX_CONTACT


@task
def send_pickup_delivered_notification(drs):
    awbs = AWB.objects.filter(awb_status__current_drs=drs,
                              awb_status__manifest__client__additional__sms_notification=True)
    fe_name = drs.get_fe_full_name()
    fe_contact = drs.get_fe_contact()
    for awb in awbs:
        to = awb.get_phone_1()
        cust_name = awb.get_customer_name()
        client = awb.get_client_obj()
        if client.client_code in ['FFC', 'FFP', 'FOR']:
            awb_no = awb.get_order_id()
        else:
            awb_no = awb.awb
        client_name = client.client_name
        callback_url = EXOTEL_CALLBACK_URL.format(**{'awb_no': awb_no, 'type': 'DRS'})
        temp_dict = {'cust_name': cust_name, 'client_name': client_name, 'awb_no': awb_no, 'fe_name': fe_name,
                     'fe_contact': fe_contact}
        if awb.category == 'REV':
            body = PICKUP_SMS_NOTIFICATION_TEMPLATE.format(**temp_dict)
        else:
            body = DELIVERED_SMS_NOTIFICATION_TEMPLATE.format(**temp_dict)
        send_sms(to, body, callback_url)
        # if sms.status_code == 200:
        # AWB_Notification.objects.create(awb=awb, type='DRS', send=True)
        # print sms


@task
def send_cancel_notification(awb):
    to = awb.get_phone_1()
    cust_name = awb.get_customer_name()
    client = awb.get_client_obj()
    if client.client_code in ['FFC', 'FFP', 'FOR']:
        awb_no = awb.get_order_id()
    else:
        awb_no = awb.awb
    client_name = client.client_name
    # awb_no = awb.awb
    # client_name = awb.get_client()
    callback_url = EXOTEL_CALLBACK_URL.format(**{'awb_no': awb_no, 'type': 'CAN'})
    temp_dict = {'cust_name': cust_name, 'client_name': client_name, 'awb_no': awb_no}
    if awb.category == 'REV':
        body = CANCEL_SMS_NOTIFICATION_TEMPLATE_RL.format(**temp_dict)
    else:
        body = CANCEL_SMS_NOTIFICATION_TEMPLATE_FL.format(**temp_dict)
    sms = send_sms(to, body, callback_url)
    return True if sms.status_code == 200 else False
    # AWB_Notification.objects.create(awb=awb, type='CAN', send=True)


@task
def send_deferred_notification(awb, deferred_date):
    to = awb.get_phone_1()
    cust_name = awb.get_customer_name()
    client = awb.get_client_obj()
    if client.client_code in ['FFC', 'FFP', 'FOR']:
        awb_no = awb.get_order_id()
    else:
        awb_no = awb.awb
    client_name = client.client_name
    # awb_no = awb.awb
    # client_name = awb.get_client()
    callback_url = EXOTEL_CALLBACK_URL.format(**{'awb_no': awb_no, 'type': 'DBC'})
    date = change_date_format(deferred_date, '%Y-%m-%d', '%d, %b, %Y')
    temp_dict = {'cust_name': cust_name, 'client_name': client_name, 'awb_no': awb_no, 'deferred_date': date}
    if awb.category == 'REV':
        body = DEFERRED_SMS_NOTIFICATION_TEMPLATE_RL.format(**temp_dict)
    else:
        body = DEFERRED_SMS_NOTIFICATION_TEMPLATE_FL.format(**temp_dict)
    sms = send_sms(to, body, callback_url)
    return True if sms.status_code == 200 else False
    # AWB_Notification.objects.create(awb=awb, type='CAN', send=True)


@task
def send_issue_notification(awb, reason):
    print "1", awb
    to = awb.get_phone_1()
    cust_name = awb.get_customer_name()
    client = awb.get_client_obj()
    if client.client_code in ['FFC', 'FFP', 'FOR']:
        awb_no = awb.get_order_id()
    else:
        awb_no = awb.awb
    client_name = client.client_name
    # awb_no = awb.awb
    # client_name = awb.get_client()
    callback_url = EXOTEL_CALLBACK_URL.format(**{'awb_no': awb_no, 'type': 'ISS'})
    temp_dict = {'cust_name': cust_name, 'client_name': client_name, 'awb_no': awb_no, 'reason': reason,
                 'nuvoex_contact_no': NUVOEX_CONTACT}
    # if awb.category == 'REV':
    body = ISSUE_SMS_NOTIFICATION_TEMPLATE.format(**temp_dict)
    # else:
    # body = ISSUE_SMS_NOTIFICATION_TEMPLATE_FL.format(**temp_dict)
    sms = send_sms(to, body, callback_url)
    return True if sms.status_code == 200 else False


@task
def send_mts_receiving_mail(mts):
    tbs = mts.get_tbs()
    for tb in tbs:
        awbs = tb.get_awbs()


@task
def delete_duplicate_drs():
    dupes = MTS.objects.values('mts_id').annotate(count=Count('id')).order_by().filter(count__gt=1)
    mtss = MTS.objects.filter(mts_id__in=[item['mts_id'] for item in dupes]).order_by('-creation_date', 'mts_id')
    # mtss.delete()
    print mtss


# @receiver(post_save, sender=TB)
# def create_tb_status_history(sender, **kwargs):
# print kwargs
# if kwargs.get('created'):




@task
def update_fe():
    drs = DRS.objects.all()
    fes = []
    for d in drs:
        try:
            fes.append(d.fe)
        except Exception:
            pass

    for fe in list(set(fes)):
        try:
            DRS.objects.filter(fe=fe).update(fe=fe.profile.pk)
        except Exception:
            pass

    dto = DTO.objects.all()
    fes = []
    for d in dto:
        try:
            fes.append(d.fe)
        except Exception:
            pass

    for fe in list(set(fes)):
        try:
            DTO.objects.filter(fe=fe).update(fe=fe.profile.pk)
        except Exception:
            pass

    rto = RTO.objects.all()
    fes = []
    for d in rto:
        try:
            fes.append(d.fe)
        except Exception:
            pass

    for fe in list(set(fes)):
        try:
            RTO.objects.filter(fe=fe).update(fe=fe.profile.pk)
        except Exception:
            pass


@task()
def close_mts():
    mtss = MTS.objects.all()
    for i in mtss:
        awb_del = AWB.objects.filter(awb_status__status__in=['DEL', 'CAN', 'RBC'],
                                     awb_status__current_tb__tb_history__mts=i).count()
        awb_total = i.get_awbs().count()
        if awb_del == awb_total:
            i.status = 'D'
            i.save()


@task()
def close_tb():
    tbss = TB.objects.all()
    for i in tbss:
        awb_del = AWB.objects.filter(awb_status__status__in=['DEL', 'CAN', 'RBC', 'DTO', 'DCR'],
                                     awb_status__current_tb=i).count()
        awb_total = i.get_awb_count()
        if awb_del == awb_total:
            # print i
            i.status = 'D'
            i.save()


@task()
def linehual_report(filename, tb_report, email):
    csvfile = StringIO.StringIO()
    writer = csv.writer(csvfile)
    writer.writerow(tb_report.get_report_header())
    for row in tb_report.get_report():
        writer.writerow(row)
    #print csvfile
    subject = 'LINEHAUL REPORT ' + datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
    message = EmailMessage(subject, "This is a system generated mail. Please do not reply.", "system@nuvoex.com",
                           [email])

    message.attach(filename, csvfile.getvalue(), 'text/csv')
    return message.send()