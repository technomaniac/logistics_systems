from django.contrib import admin

from transit.models import *

# Register your models here.
class TBAdmin(admin.ModelAdmin):
    list_display = ('tb_id', 'origin_branch', 'delivery_branch', 'status')
    search_fields = ['tb_id', 'status']
    list_filter = ['origin_branch', 'delivery_branch']


class MTSAdmin(admin.ModelAdmin):
    list_display = ('mts_id', 'from_branch', 'to_branch', 'status', 'type', 'external_track_id')
    search_fields = ['mts_id', 'external_track_id']
    list_filter = ['from_branch', 'to_branch', 'status', 'type']


class DRSAdmin(admin.ModelAdmin):
    list_display = ('drs_id', 'branch', 'status', 'fe')
    search_fields = ['drs_id', 'branch__branch_name', 'status', 'fe__username']
    list_filter = ['branch__branch_name', 'status', 'fe']


class DTOAdmin(admin.ModelAdmin):
    list_display = ('dto_id', 'branch', 'status', 'fe')
    search_fields = ['dto_id', 'branch__branch_name', 'status', 'fe__username']
    list_filter = ['branch__branch_name', 'status', 'fe__username']


class RTOAdmin(admin.ModelAdmin):
    list_display = ('rto_id', 'branch', 'status', 'fe')
    search_fields = ['rto_id', 'branch__branch_name', 'status', 'fe__username']
    list_filter = ['branch__branch_name', 'status', 'fe__username']


class TBHistoryAdmin(admin.ModelAdmin):
    list_display = ('tb', 'branch', 'mts', 'creation_date')
    search_fields = ['tb', 'mts']
    #list_filter = ['branch__branch_name', 'status', 'fe__username']


admin.site.register(TB, TBAdmin)
admin.site.register(MTS, MTSAdmin)
admin.site.register(DRS, DRSAdmin)
admin.site.register(DTO, DTOAdmin)
admin.site.register(RTO, RTOAdmin)