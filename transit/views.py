import os
import re
import csv
import json
from time import gmtime, strftime
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from datetime import datetime
# import datetime
from django.db import transaction
from django.db.models import Q
from django.db.models.aggregates import Count
from django.http.response import StreamingHttpResponse
from django.views.generic.base import View
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate, Table
from django.shortcuts import render, HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django_tables2 import RequestConfig
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.views.decorators.csrf import csrf_exempt
from custom.custom_mixins import GeneralLoginRequiredMixin, CommonViewMixin

from internal.models import Branch, Vehicle
from .models import TB, TB_History, MTS, DRS, DTO, close_drs, RTO, close_dto, close_rto, get_fe_list
from .forms import CreateMTSForm, CreateTBForm, CreateDRSForm, CreateDTOForm, CreateRTOForm
from awb.models import AWB, AWB_History, AWB_Status, get_tb_awbs, get_rto_awbs, inscan_awb
from client.models import Client, Client_Vendor
from .tables import TBTable, DRSTable, MTSTable, DTOTable, MTSOutgoingTable, RTOTable, TBLineHaulReportTable
from awb.tables import AWBTable
from transit.models import upload_dto_pod
from transit.tasks import send_pickup_delivered_notification, send_cancel_notification, send_deferred_notification, \
    linehual_report
from utils.common import set_message, QueryBuilder, Echo, convert_to_integer_list
from utils.constants import AWB_RL_REMARKS, AWB_FL_REMARKS, DTO_REPORT_HEADER
from utils.random import format_string
from report.tb_report import TBReport


@login_required(login_url='/login')
def tb_incoming(request):
    if 'branch' in request.session:
        table = TBTable(
            TB.objects.select_related().filter(delivery_branch=request.session['branch']).exclude(status='D').order_by(
                '-creation_date'))
    else:
        table = TBTable(TB.objects.select_related().exclude(status='D').order_by('-creation_date'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html',
                  {'table': table, 'url': '/transit/tb/create', 'model': 'tb', 'type': 'incoming'})

# def tb_received(request):
#     if 'branch' in request.session:
#         table = TBTable(
#             TB.objects.filter(delivery_branch=request.session['branch']).exclude(status='U').order_by(
#                 '-creation_date'))
#     else:
#         table = TBTable(TB.objects.exclude(status='U').order_by('-creation_date'))
#     RequestConfig(request, paginate={"per_page": 100}).configure(table)
#     return render(request, 'common/table.html',
#                   {'table': table, 'url': '/transit/tb/create', 'model': 'tb', 'type': 'received'})

def tb_outgoing(request):
    if 'branch' in request.session:
        table = TBTable(
            TB.objects.select_related().filter(origin_branch=request.session['branch']).exclude(status='D').order_by(
                '-creation_date'))
    else:
        table = TBTable(TB.objects.select_related().exclude(status='D').order_by('-creation_date'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html',
                  {'table': table, 'url': '/transit/tb/create', 'model': 'tb', 'type': 'outgoing'})


def show_tb_form(request):
    form = CreateTBForm()
    return render(request, 'transit/create_tb.html', {'form': form, 'model': 'tb'})


def ajax_in_scanning_awb(request):
    if request.method == 'POST' and request.is_ajax():
        if request.POST['delivery_branch'] != '':
            del_branch = Branch.objects.get(pk=int(request.POST['delivery_branch']))
            request.session['message'] = {}
            try:
                awb = inscan_awb(request.POST['awb'])
                awb.readable_status = awb.awb_status.get_readable_choice()
                if awb.awb_status.status in ['ISC', 'PC', 'DCR'] and awb.awb_status.current_branch_id == \
                        request.session[
                            'branch'] and (
                                awb.get_delivery_branch() == del_branch or del_branch.branch_name[-3:] == 'HUB'):
                    awb.readable_category = awb.get_readable_choice()
                    request.session['message']['class'] = 'success'
                    request.session['message']['report'] = "AWB: " + str(awb.awb) + " - " + awb.readable_status
                    return render(request, 'awb/awb_in_scanning.html', {'awb': awb})
                elif awb.awb_status.status in ['CAN', 'RET'] and awb.awb_status.current_branch_id == request.session[
                    'branch'] and awb.get_rto_branch() == del_branch:
                    awb.readable_category = awb.get_readable_choice()
                    request.session['message']['class'] = 'success'
                    request.session['message']['report'] = "AWB: " + str(
                        awb.awb) + " | Status: " + awb.readable_status + ' | RTO Branch: ' + awb.get_rto_branch().branch_name
                    return render(request, 'awb/awb_in_scanning.html', {'awb': awb})
                else:
                    request.session['message']['class'] = 'error'
                    request.session['message']['report'] = "AWB: " + str(awb.awb) + "  - " + awb.readable_status + \
                                                           " - Destination Branch: " + awb.get_delivery_branch().branch_name
                    return render(request, 'awb/awb_in_scanning.html')
            except AWB.DoesNotExist:
                set_message('AWB Does Not Exist','error')
                return render(request, 'awb/awb_in_scanning.html')
    return render(request, 'awb/awb_in_scanning.html')


@csrf_exempt
def ajax_get_tb_awbs(request):
    if request.method == "POST" and request.is_ajax():
        if 'branch' in request.session:
            curr_branch = request.session['branch']
        else:
            curr_branch = None
            # fl = AWB.objects.filter(category__in=['COD', 'PRE'],
        #                         pincode__branch_pincode__branch=request.POST['delivery_branch'],
        #                         awb_status__current_branch=request.session['branch'],
        #                         awb_status__status__in=['ISC'])
        fl = get_tb_awbs(['PRE', 'COD'], ['ISC'], request.POST['delivery_branch'], curr_branch)
        # rl = AWB.objects.filter(category='REV',
        #                         awb_status__manifest__branch_id=request.POST['delivery_branch'],
        #                         awb_status__current_branch=request.session['branch'],
        #                         awb_status__status__in=['ISC', 'PC'])
        rl = get_tb_awbs(['REV'], ['ISC', 'PC', 'DCR'], request.POST['delivery_branch'], curr_branch)
        rto = get_rto_awbs(request.POST['delivery_branch'], curr_branch)
        return render(request, 'transit/tb_awb_table.html', {'fl': fl, 'rl': rl, 'rto': rto})


def reports(request, type='generic'):
    if not request.GET.keys():
        category = zip(*AWB.AWB_TYPE)[1]
        branches = Branch.objects.all()
        awb_status = zip(*AWB_Status.STATUS)[1]
        if type == 'generic':
            return render(request, 'report_generic_input.html',
                          {'category': category, 'branches': branches, 'awb_status': awb_status})
        if type == 'cash-report':
            return render(request, 'report_cash_input.html', {'branches': branches})
    else:
        branch = Branch.objects.all()[0]  #todo change to request.user.branch
        awbs = AWB.objects.none()
        if type == 'generic':
            category = 'COD'
            status_type = 'ISC'

            if request.GET.get('category'):
                type_dict = dict(AWB.AWB_TYPE)
                type_dict = {v: k for k, v in type_dict.items()}
                type_of_awbs = type_dict[request.GET.get('category')]

            if request.GET.get('awb_status'):
                status_dict = dict(AWB_Status.STATUS)
                status_dict = {v: k for k, v in status_dict.items()}
                status_type = status_dict[request.GET.get('awb_status')]

            if request.GET.get('branch'):
                branch = Branch.objects.get(pk=int(request.GET.get('branch')))

            awbs = AWB.objects.filter(awb_status__current_tb__tb_history__branch=branch,
                                      category=category,
                                      awb_status__status=status_type)
            #awbs = AWB.objects.all() #todo remove
        elif type == 'cash-report':
            start = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
            end = datetime.now().replace(microsecond=0)
            if request.GET.get('branch'):
                branch = Branch.objects.get(pk=int(request.GET.get('branch')))

            if request.GET.get('start'):
                start = datetime.strptime(request.GET.get('start'), '%Y-%m-%d')

            if request.GET.get('end'):
                end = datetime.strptime(request.GET.get('end'), '%Y-%m-%d')
            awbs = AWB.objects.filter(on_update__gte=start, on_update__lte=end,
                                      awb_status__current_branch=branch)  #todo remove

        doc = SimpleDocTemplate("awbs.pdf", pagesize=(31 * inch, 35 * inch))
        # container for the 'Flowable' objects
        elements = []
        fields = AWB._meta.get_all_field_names()
        fields.remove('awb_status')  #todo later add function in models
        fields.remove('awb_history')
        fields.remove('creation_date')
        fields.remove('on_update')
        fields.remove('is_active')
        fields.remove('id')
        awbs = awbs.values_list(*fields)
        data = [fields]
        for awb in awbs:
            data.append(list(awb))
        print data
        t = Table(data)
        elements.append(t)
        # write the document to disk
        doc.build(elements)
        pdf_file = open(doc.filename, 'rb+')
        response = HttpResponse(doc, content_type='application/pdf')
        #response['Content-Disposition'] = 'attachment; filename="awbs.pdf"'
        response.write(pdf_file.read())
        pdf_file.close()

        os.remove(doc.filename)
        return response


def ajax_create_tb(request):
    if request.method == "POST" and request.is_ajax():
        tb = TB.objects.create(
            tb_id=generateId(TB, request.session['branch'], request.POST['delivery_branch']),
            origin_branch=Branch.objects.get(pk=request.session['branch']),
            delivery_branch=Branch.objects.get(pk=request.POST['delivery_branch']),
            length=format_string(request.POST['length'], float),
            breadth=format_string(request.POST['breadth'], float),
            height=format_string(request.POST['height'], float),
            weight=format_string(request.POST['weight'], float),
            # co_loader=request.POST['co_loader'],
            # co_loader_awb=request.POST['co_loader_awb']
        )
        tb_history = TB_History(
            tb=tb,
            branch=Branch.objects.get(pk=request.session['branch'])
        )
        tb_history.save()
        awbs = json.loads(request.POST['awbs'])
        for awb in awbs:
            awb_status = AWB_Status.objects.get(awb=int(awb))
            if awb_status.status == 'CAN' and awb_status.manifest.category == 'FL':
                AWB_Status.objects.filter(awb=int(awb)).update(current_tb=tb, status='ITR', updated_by=request.user)
                awb_history = AWB_History(
                    awb=AWB.objects.get(pk=int(awb)),
                    tb=tb, updated_by=request.user
                )
                awb_history.save()
            else:
                AWB_Status.objects.filter(awb=int(awb)).update(current_tb=tb, status='TB', updated_by=request.user)
                awb_history = AWB_History(
                    awb=AWB.objects.get(pk=int(awb)),
                    tb=tb, updated_by=request.user
                )
                awb_history.save()
        return HttpResponse(True)
    else:
        return HttpResponse(False)


def mts_incoming(request):
    if 'branch' in request.session:
        table = MTSTable(
            MTS.objects.select_related().filter(to_branch=request.session['branch']).exclude(status='D').order_by('-creation_date'))
    else:
        table = MTSTable(MTS.objects.select_related().exclude(status='D').order_by('-creation_date'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html',
                  {'table': table, 'url': '/transit/mts/create', 'model': 'mts', 'type': 'incoming'})


def mts_outgoing(request):
    if 'branch' in request.session:
        table = MTSOutgoingTable(
            MTS.objects.select_related().filter(from_branch=request.session['branch']).exclude(status='D').order_by('-creation_date'))
    else:
        table = MTSOutgoingTable(MTS.objects.select_related().exclude(status='D').order_by('-creation_date'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html',
                  {'table': table, 'url': '/transit/mts/create', 'model': 'mts', 'type': 'outgoing'})

# def mts_received(request):
#     if 'branch' in request.session:
#         table = MTSOutgoingTable(
#             MTS.objects.filter(from_branch=request.session['branch']).exclude(status='U').order_by('-creation_date'))
#     else:
#         table = MTSOutgoingTable(MTS.objects.exclude(status='U').order_by('-creation_date'))
#     RequestConfig(request, paginate={"per_page": 100}).configure(table)
#     return render(request, 'common/table.html',
#                   {'table': table, 'url': '/transit/mts/create', 'model': 'mts', 'type': 'received'})

def show_mts_form(request):
    form = CreateMTSForm()
    return render(request, 'common/form.html', {'form': form, 'model': 'mts'})


def ajax_get_tbs(request):
    if request.method == "POST" and request.is_ajax():
        if 'branch' in request.session:
            tbs = TB.objects.filter(tb_history__branch_id=int(request.session['branch'])).exclude(
                tb_history__mts__from_branch_id=request.session['branch']).prefetch_related('tb_history',
                                                                                            'tb_history__branch')
            tblist = [tb for tb in tbs if tb.get_current_branch().pk != tb.delivery_branch.pk]
        else:
            return HttpResponse('Please Select Branch')
        if len(tblist) > 0:
            return render(request, 'transit/ajax_get_tbs.html', {'tbs': tblist})
        else:
            return HttpResponse('No TB available for delivery')


def ajax_create_mts(request):
    if request.method == "POST" and request.is_ajax():
        if request.POST['type'] != '':
            mts = MTS(
                mts_id=generateId(MTS, request.session['branch'], request.POST['to_branch']),
                from_branch=Branch.objects.get(pk=int(request.session['branch'])),
                to_branch=Branch.objects.get(pk=int(request.POST['to_branch'])),
                type=str(request.POST['type']),
                external_track_id=request.POST['external_track_id'],
                co_loader=request.POST['co_loader'],
                co_loader_awb=request.POST['co_loader_awb']
            )
            mts.save()
            tbs = json.loads(request.POST['tbs'])
            for tb in tbs:
                tb_history = TB_History(
                    tb=TB.objects.get(pk=int(tb)),
                    mts=mts
                )
                tb_history.save()
                awb_status = AWB_Status.objects.filter(current_tb_id=int(tb),
                                                       status__in=['TB', 'TBD', 'MTS', 'MTD', 'ITR'])
                #awb_status.update(current_mts=mts, status='MTS', updated_by=request.user)
                for status in awb_status:
                    if status.status != 'ITR':
                        status.status = 'MTS'
                    status.current_mts = mts
                    status.updated_by = request.user
                    status.save()
                    awb_history = AWB_History(
                        awb=status.awb,
                        mts=mts, updated_by=request.user
                    )
                    awb_history.save()
            #send_mts_receiving_mail.delay(mts)
            return HttpResponse(True)
        else:
            return HttpResponse('Please select type Air/Surface/Van.')
    else:
        return HttpResponse(False)


def drs(request):
    if 'branch' in request.session:
        table = DRSTable(
            DRS.objects.filter(branch=request.session['branch']).exclude(status='C').order_by('-creation_date'))
    else:
        table = DRSTable(DRS.objects.exclude(status='C').order_by('-creation_date'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'url': 'drs/create', 'model': 'drs', 'type': 'open'})


@csrf_exempt
def ajax_get_drs_awbs(request):
    if request.method == "GET" and request.is_ajax():
        now = datetime.now()
        current_date = now.date()
        rl_sch_show = []
        if 'branch' in request.session:
            fl = AWB.objects.filter(awb_status__status__in=['DCR', 'CAN', 'DBC', 'CNA', 'SCH', 'NA'],
                                    pincode__branch_pincode__branch_id=request.session['branch']).exclude(
                category='REV').order_by(request.GET['sort']).prefetch_related('awb_status',
                                                                               'pincode__branch_pincode__branch')
            rl = AWB.objects.filter(awb_status__status__in=['DR', 'CNA', 'NA', 'SCH'], category='REV',
                                    pincode__branch_pincode__branch_id=request.session['branch']).order_by(
                request.GET['sort']).prefetch_related('awb_status',
                                                      'pincode__branch_pincode__branch')
            rl_sch = AWB.objects.filter(awb_status__status__in=['DBC'], category='REV',
                                    pincode__branch_pincode__branch_id=request.session['branch']).order_by(request.GET['sort'])
            for awb in rl_sch:
                try:
                    # if awb.awb_status.status == 'SCH' and awb.awb_status.reason == '':
                    #     sch_date = current_date
                    # else:
                    sch_date = datetime.strptime(awb.awb_status.reason, '%Y-%m-%d').date()
                except:
                    sch_date = ''
                if sch_date == current_date:
                    rl_sch_show.append(awb)
            rl = (list(rl) + rl_sch_show)
        else:
            fl = AWB.objects.filter(awb_status__status__in=['DCR', 'CAN', 'DBC', 'CNA', 'SCH', 'NA']).exclude(
                category='REV').order_by(request.GET['sort']).prefetch_related('awb_status',
                                                                               'pincode__branch_pincode__branch')
            rl = AWB.objects.filter(awb_status__status__in=['DR', 'DBC', 'CNA', 'SCH', 'NA'], category='REV').order_by(
                request.GET['sort']).prefetch_related('awb_status',
                                                      'pincode__branch_pincode__branch')
        cache.set('invoice_awbs_' + str(request.user.pk), rl)
        # if 'invoice_awbs' in request.session:
        #     del request.session['invoice_awbs']
        #request.session['invoice_awbs'] = [awb.pk for awb in rl]
        return render(request, 'transit/drs_awb_table.html', {'fl': fl, 'rl': rl})

    if request.method == 'POST' and request.is_ajax():
        # print request.POST
        pincodes = convert_to_integer_list(request.POST.get('pincodes'), ',')
        # print filter
        now = datetime.now()
        current_date = now.date()
        rl_sch_show = []
        if 'branch' in request.session:
            fl = AWB.objects.filter(awb_status__status__in=['DCR', 'CAN', 'DBC', 'CNA', 'SCH', 'NA'],
                                    pincode__branch_pincode__branch_id=request.session['branch'], pincode__pk__in=pincodes).exclude(
                category='REV').order_by('creation_date').prefetch_related('awb_status',
                                                                               'pincode__branch_pincode__branch')
            rl = AWB.objects.filter(awb_status__status__in=['DR', 'CNA', 'NA', 'SCH'], category='REV',
                                    pincode__branch_pincode__branch_id=request.session['branch'], pincode__pk__in=pincodes).order_by(
                'creation_date').prefetch_related('awb_status',
                                                      'pincode__branch_pincode__branch')
            rl_sch = AWB.objects.filter(awb_status__status__in=['DBC'], category='REV',
                                    pincode__branch_pincode__branch_id=request.session['branch'], pincode__pk__in=pincodes).order_by('creation_date')
            for awb in rl_sch:
                try:
                    # if awb.awb_status.status == 'SCH' and awb.awb_status.reason == '':
                    #     sch_date = current_date
                    # else:
                    sch_date = datetime.strptime(awb.awb_status.reason, '%Y-%m-%d').date()
                except:
                    sch_date = ''
                if sch_date == current_date:
                    rl_sch_show.append(awb)
            rl = (list(rl) + rl_sch_show)
        else:
            fl = AWB.objects.filter(awb_status__status__in=['DCR', 'CAN', 'DBC', 'CNA', 'SCH', 'NA'], pincode__pk__in=pincodes).exclude(
                category='REV').order_by('creation_date').prefetch_related('awb_status',
                                                                               'pincode__branch_pincode__branch')
            rl = AWB.objects.filter(awb_status__status__in=['DR', 'DBC', 'CNA', 'SCH', 'NA'], category='REV', pincode__pk__in=pincodes).order_by(
                'creation_date').prefetch_related('awb_status','pincode__branch_pincode__branch')
        cache.set('invoice_awbs_' + str(request.user.pk), rl)
        # if 'invoice_awbs' in request.session:
        #     del request.session['invoice_awbs']
        #request.session['invoice_awbs'] = [awb.pk for awb in rl]
        return render(request, 'transit/drs_awb_table.html', {'fl': fl, 'rl': rl})


@csrf_exempt
def ajax_get_dto_awbs(request):
    if request.method == "POST" and request.is_ajax():
        filter = {}
        filter['awb_status__status__in'] = ['DCR', 'CB', 'RBC']
        filter['category'] = 'REV'
        rl = AWB.objects.filter(**filter).prefetch_related('awb_status',
                                                           'awb_status__manifest__branch',
                                                           'awb_status__manifest__client',
                                                           'pincode__branch_pincode__branch')
        if 'branch' in request.session:
            rl = rl.filter(awb_status__manifest__branch_id=request.session['branch']) | \
                 rl.filter(vendor__pincode__branch_pincode__branch_id=request.session['branch'])
        if request.POST['client'] != '':
            rl = rl.filter(awb_status__manifest__client_id=request.POST['client'])
            request.session['dto_client'] = int(request.POST['client'])
            # fl = AWB.objects.filter(awb_status__status='CAN').exclude(category='REV')
        # pages = len(rl)/20
        len_rl = len(rl)
        page = request.GET.get('page')
        paginator = Paginator(rl, 100)
        try:
            rl = paginator.page(page)
        except PageNotAnInteger:
            rl = paginator.page(1)
        except EmptyPage:
            rl = paginator.page(paginator.num_pages)
        # rl = paginator.page(paginator.num_pages)
        return render(request, 'transit/dto_awb_table.html', {'rl': rl, 'len_rl': len_rl })


def ajax_create_drs(request):
    if request.method == "POST" and request.is_ajax():
        try:
            if 'branch' not in request.session:
                return HttpResponse(False)
            awbs = list(set(json.loads(request.POST['fl']) + json.loads(request.POST['rl'])))
            if len(awbs) > 0:
                fe = User.objects.get(pk=int(request.POST['fe']))
                drs = DRS(
                    drs_id=generateId(DRS, request.session['branch']),
                    fe=fe,
                    vehicle_id=int(request.POST['vehicle']),
                    opening_km=request.POST['opening_km'],
                    branch_id=int(request.session['branch'])
                )
                drs.save()
                # fl = list(set())
                # rl = list(set(json.loads(request.POST['rl'])))
                for awb in awbs:
                    awb_obj = AWB.objects.get(pk=int(awb))
                    if awb_obj.category == 'REV':
                        AWB_Status.objects.filter(awb=int(awb)).update(current_drs=drs, current_fe=fe,
                                                                       status='PP', updated_by=request.user)
                    else:
                        AWB_Status.objects.filter(awb=int(awb)).update(current_drs=drs, current_fe=fe,
                                                                       status='DRS', updated_by=request.user)
                    awb_history = AWB_History(
                        awb=AWB.objects.get(pk=int(awb)),
                        drs=drs, updated_by=request.user,
                        fe=fe,
                    )
                    awb_history.save()
                send_pickup_delivered_notification.delay(drs)
                if 'drs_awbs' in request.session:
                    del request.session['drs_awbs']

                # for awb in rl:
                #     AWB_Status.objects.filter(awb=int(awb)).update(current_drs=drs, status='PP', updated_by=request.user)
                #     awb_history = AWB_History(
                #         awb=AWB.objects.get(pk=int(awb)),
                #         drs=drs, updated_by=request.user
                #     )
                #     awb_history.save()

                return HttpResponse(True)
            else:
                return HttpResponse(False)
        except:
            return HttpResponse(False)
    else:
        return HttpResponse(False)


def tb_detail(request, id):
    tb = TB.objects.get(pk=id)
    table = AWBTable(AWB.objects.filter(awb_history__tb=tb))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'id': tb.tb_id})


def mts_detail(request, id):
    mts = MTS.objects.get(pk=id)
    if request.method == 'POST' and request.POST['external_track_id'] != '':
        mts.external_track_id = request.POST['external_track_id']
        mts.save()
        return HttpResponseRedirect('/transit/mts/' + id)
    else:
        table = TBTable(TB.objects.filter(tb_history__mts=mts))
        RequestConfig(request, paginate={"per_page": 100}).configure(table)
        context = {'table': table, 'id': mts.mts_id}
        if mts.external_track_id == '':
            context['mts'] = mts
        else:
            context['track_url'] = mts.get_external_track_url()
        return render(request, 'common/table.html', context)


def mts_update_status(request):
    if request.method == 'POST' and request.is_ajax():
        mts = MTS.objects.filter(mts_id=request.POST['mts_id'])
        mts.update(status=request.POST['status'])
        if mts[0].status == 'D':
            tbs = mts[0].get_tbs()
            for tb in tbs:
                tb_history = TB_History(tb_id=tb.tb_id, branch=mts[0].to_branch)
                tb_history.save()
                AWB_Status.objects.filter(current_tb=tb).update(current_branch=mts[0].to_branch)
        return HttpResponse(mts[0].get_readable_choice())

def mts_update_co_loader_awb(request):
    if request.method == 'POST' and request.is_ajax():
        mts = MTS.objects.filter(mts_id=request.POST['mts_id'])
        mts.update(co_loader_awb=request.POST['co_loader_awb'])
        return HttpResponse(mts[0].get_co_loader_awb())


def drs_update_status(request):
    if request.method == 'POST' and request.is_ajax():
        drs = DRS.objects.filter(pk=int(request.POST['drs_id']))
        drs.update(status=request.POST['status'])
        return HttpResponse(drs[0].get_readable_choice())


def drs_search(request):
    context = {}
    context['branchs'] = Branch.objects.exclude(pk=1)
    if request.is_ajax():
        filter = {}

        if request.GET['drs'] != '':
            filter['drs_id__startswith'] = str(request.GET['drs']).upper()
        if request.GET['branch'] != '':
            filter['branch_id'] = request.GET['branch']
        if request.GET['status'] != '':
            filter['status'] = request.GET['status']
        if 'branch' in request.session:
            filter['branch'] = request.session['branch']

        if request.GET['start_date'] != '' and request.GET['end_date'] == '':
            filter['creation_date__startswith'] = request.GET['start_date']
        elif request.GET['start_date'] == '' and request.GET['end_date'] != '':
            filter['creation_date__startswith'] = request.GET['end_date']
        elif request.GET['start_date'] != '' and request.GET['end_date'] != '':
            filter['creation_date__range'] = (
                request.GET['start_date'] + ' 00:00:00', request.GET['end_date'] + ' 23:59:59')

        queryset = DRS.objects.filter(**filter).order_by('-creation_date')

        if request.GET['type'] == 'COD':
            for drs in queryset:
                if drs.get_expected_amount() == 0:
                    queryset.filter(pk=drs.pk).delete()

        cache.set('drs_searched', queryset)

        table = DRSTable(queryset)
        RequestConfig(request, paginate={"per_page": 100}).configure(table)
        return render(request, 'common/ajax_table.html', {'table': table})
    else:
        return render(request, 'transit/drs_search.html', context)


def drs_detail(request, pk):
    drs = DRS.objects.get(pk=pk)
    fl = AWB.objects.filter(awb_status__current_drs=drs).exclude(category='REV').order_by('category', 'awb')
    rl = AWB.objects.filter(awb_status__current_drs=drs, category='REV').order_by('category', 'awb')
    cache.set('print_drs_awbs_' + str(request.user.pk), rl)
    # if 'print_drs_awbs' in request.session:
    #     del request.session['print_drs_awbs']
    request.session['print_drs_awbs'] = [awb.pk for awb in rl]
    return render(request, 'transit/awb_status_update.html',
                  {'fl': fl, 'rl': rl, 'drs': drs, 'model': 'drs', 'rl_remarks': AWB_RL_REMARKS,
                   'fl_remarks': AWB_FL_REMARKS})


def dto_detail(request, dto_id):
    rl = AWB.objects.filter(awb_history__dto_id=dto_id).order_by('awb')
    cache.set('print_dto_awbs_' + str(request.user.pk), rl)
    # if 'print_dto_awbs' in request.session:
    #     del request.session['print_dto_awbs']
    request.session['print_dto_awbs'] = [awb.pk for awb in rl]
    return render(request, 'transit/awb_status_update.html',
                  {'rl': rl, 'dto': DTO.objects.get(pk=dto_id), 'model': 'dto'})


def drs_in_scanning(request):
    if request.method == 'POST' and request.is_ajax():
        request.session['message'] = {}
        if 'drs_awbs' not in request.session:
            request.session['drs_awbs'] = []
            scanned_awbs = request.session['drs_awbs']
        else:
            scanned_awbs = request.session['drs_awbs']
        try:

            awb = AWB.objects.get(awb=request.POST['awb'])
            if awb.category in ['PRE', 'COD']:
                if awb.get_delivery_branch().pk == request.session['branch']:
                    if awb.awb_status.status in ['DCR', 'CAN', 'SCH', 'DBC', 'CNA', 'NA']:
                        request.session['message']['class'] = 'success'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        scanned_awbs.append(awb.pk)
                        request.session['drs_awbs'] = scanned_awbs
                        return render(request, 'awb/awb_in_scanning.html', {'awb': awb})
                    else:
                        request.session['message']['class'] = 'error'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        return HttpResponse('')
                else:
                    request.session['message']['class'] = 'error'
                    request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                        awb.awb_status.get_readable_choice()) + " | Delivery Branch: " + str(
                        awb.get_delivery_branch().branch_name)
                    return HttpResponse('')
            else:
                if awb.get_pickup_branch().pk == request.session['branch']:
                    if awb.awb_status.status in ['DR', 'CAN', 'SCH', 'DBC', 'CB', 'CNA', 'NA']:
                        request.session['message']['class'] = 'success'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        scanned_awbs.append(awb.pk)
                        request.session['drs_awbs'] = scanned_awbs
                        return render(request, 'awb/awb_in_scanning.html', {'awb': awb})
                    else:
                        request.session['message']['class'] = 'error'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        return HttpResponse('')
                else:
                    request.session['message']['class'] = 'error'
                    request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                        awb.awb_status.get_readable_choice()) + " | Pickup Branch: " + str(
                        awb.get_pickup_branch().branch_name)
                    return HttpResponse('')
        except AWB.DoesNotExist:
            request.session['message']['class'] = 'error'
            request.session['message']['report'] = "   AWB: " + request.POST['awb'] + " | Status: Does Not Exists"
            return HttpResponse('')
    else:
        form = CreateDRSForm()
        context = {}
        context['form'] = form
        context['model'] = 'drs'
        if 'branch' in request.session:
            form.fields['fe'].queryset = get_fe_list(request.session['branch'])
            form.fields['vehicle'].queryset = Vehicle.objects.filter(
                Q(branch_id=request.session['branch']) | Q(branch_id=None))
            context['form'] = form
        if 'drs_awbs' in request.session:
            context['scanned_awbs'] = AWB.objects.filter(pk__in=request.session['drs_awbs']).prefetch_related(
                'awb_status',
                'awb_status__manifest__branch',
                'pincode__branch_pincode__branch')
        return render(request, 'transit/drs_creation_form.html', context)


def drs_get_print_sheet(request):
    data = list(set(json.loads(request.GET['awbs'])))
    awbs = AWB.objects.filter(pk__in=data).order_by('category', 'awb')
    drs = awbs[0].awb_status.current_drs
    #print user.branch.branch_name
    context = {
        'drs': drs,
        'awbs': awbs,
        'CODs': len([awb for awb in awbs if awb.category == 'COD']),
        'PREs': len([awb for awb in awbs if awb.category == 'PRE']),
        'REVs': len([awb for awb in awbs if awb.category == 'REV']),
        #'branch': Branch.objects.get(pk=int(request.session['branch'])).branch_name,
        'datetime': strftime("%Y-%m-%d %H:%M", gmtime())
    }
    return render(request, 'transit/drs_print_sheet.html', context)


def drs_get_cash(request):
    if request.is_ajax() and 'drs' in request.GET:
        drs = DRS.objects.get(drs_id=request.GET['drs'])
        if request.GET['type'] == 'total':
            return HttpResponse(drs.get_expected_amount())
        else:
            return HttpResponse(drs.get_collected_amount())


def dto_get_print_sheet(request, id):
    # data = list(set(json.loads(request.POST['awbs'])))
    dto = DTO.objects.get(pk=id)
    # awbs = []
    # #print data
    # for awb in data:
    #     awbs.append(AWB.objects.get(pk=int(awb)))
    awbs = AWB.objects.filter(awb_status__current_dto=dto)
    #user = request.user.get_profile()
    # dto = awbs[0].awb_status.current_dto
    #print user.branch.branch_name
    context = {
        'dto': dto,
        'awbs': awbs,
        'model': 'dto',
        'datetime': strftime("%Y-%m-%d", gmtime())
    }
    return render(request, 'transit/dto_print_sheet.html', context)


def rto_get_print_sheet(request):
    data = list(set(json.loads(request.GET['awbs'])))
    awbs = []
    #print data
    for awb in data:
        awbs.append(AWB.objects.get(pk=int(awb)))
        #user = request.user.get_profile()
    rto = awbs[0].awb_status.current_rto
    #print user.branch.branch_name
    context = {
        'dto': rto,
        'awbs': awbs,
        'model': 'rto',
        'datetime': strftime("%Y-%m-%d", gmtime())
    }
    return render(request, 'transit/dto_print_sheet.html', context)


def mts_print_sheet(request, mts_id):
    #try:
    mts = MTS.objects.get(mts_id=mts_id)
    awbs = mts.get_awbs()
    for awb in awbs:
        print awb
        print awb.package_value
    total_value = sum(
        [float(re.sub(r'[^0-9\.]+', '', awb.package_value)) for awb in awbs if awb.package_value != ''])
    context = {
        'awbs': awbs,
        'mts': mts,
        'total_value': total_value,
        'datetime': strftime("%Y-%m-%d", gmtime())
    }
    return render(request, 'transit/mts_print_sheet.html', context)
    # except:
    #     return HttpResponse('Some error occurred')


def dto(request):
    if 'branch' in request.session:
        table = DTOTable(
            DTO.objects.filter(branch=request.session['branch']).exclude(status='C').annotate(
                get_all_awb_count=Count('awb_history')).order_by(
                '-creation_date').prefetch_related('fe', 'branch', 'client', 'vehicle')
        )
    else:
        table = DTOTable(DTO.objects.exclude(status='C').annotate(
            get_all_awb_count=Count('awb_history')).order_by(
            '-creation_date').prefetch_related('fe', 'branch', 'client', 'vehicle'))
    RequestConfig(request, paginate={"per_page": 100}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'url': 'dto/create', 'model': 'dto', 'type': 'open'})


def dto_in_scanning(request):
    if request.method == 'POST' and request.is_ajax():
        request.session['message'] = {}
        if 'dto_awbs' not in request.session:
            request.session['dto_awbs'] = []
            scanned_awbs = request.session['dto_awbs']
        else:
            scanned_awbs = request.session['dto_awbs']
        try:
            awb = inscan_awb(request.POST['awb'])
            if awb.category == 'REV' and 'dto_client' in request.session and \
                            awb.get_client_obj().pk == request.session['dto_client']:
                if awb.get_delivery_branch().pk == request.session['branch']:
                    if awb.awb_status.status in ['DCR', 'CAN', 'RBC', 'CB']:
                        request.session['message']['class'] = 'success'
                        if awb.get_vendor_branch():
                            request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                                awb.awb_status.get_readable_choice()) + " | Vendor Branch: " + str(awb.get_vendor_branch())
                            # print request.session['message']['report']
                        else:
                            request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                                awb.awb_status.get_readable_choice())

                        scanned_awbs.append(awb.pk)
                        request.session['dto_awbs'] = scanned_awbs
                        return render(request, 'transit/dto_in_scanning.html', {'awb': awb})
                    else:
                        request.session['message']['class'] = 'error'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        return HttpResponse('')
                else:
                    request.session['message']['class'] = 'error'
                    request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                        awb.awb_status.get_readable_choice()) + " | Delivery Branch: " + str(
                        awb.get_delivery_branch().branch_name)
                    return HttpResponse('')
            else:
                if awb.get_delivery_branch().pk == request.session['branch']:
                    if awb.awb_status.status in ['CAN', 'ITR']:
                        request.session['message']['class'] = 'error'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice()) + ' | Client: ' + awb.get_client()
                        return HttpResponse('')
                    else:
                        request.session['message']['class'] = 'error'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice()) + ' | Client: ' + awb.get_client()
                        return HttpResponse('')
                else:
                    request.session['message']['class'] = 'error'
                    request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                        awb.awb_status.get_readable_choice()) + ' | Client: ' + awb.get_client()
                    return HttpResponse('')
        except AWB.DoesNotExist:
            request.session['message']['class'] = 'error'
            request.session['message']['report'] = "AWB: " + request.POST['awb'] + " | Status: Does Not Exists"
            return HttpResponse('')
    else:
        form = CreateDTOForm()
        context = {}
        context['form'] = form
        context['model'] = 'dto'

        if 'branch' in request.session:
            form.fields['fe'].queryset = get_fe_list(request.session['branch'])
            form.fields['vehicle'].queryset = Vehicle.objects.filter(
                branch_id=request.session['branch']) | Vehicle.objects.filter(branch=None)
            context['form'] = form
        if 'dto_awbs' in request.session:
            context['scanned_awbs'] = AWB.objects.filter(pk__in=request.session['dto_awbs'])
        return render(request, 'transit/dto_creation_form.html', context)


def ajax_create_dto(request):
    if request.method == "POST" and request.is_ajax():
        #try:
        fe = User.objects.get(pk=request.POST['fe'])
        awbs_pk_list = list(set(json.loads(request.POST['awbs'])))
        awbs = AWB.objects.filter(pk__in=awbs_pk_list)
        vendors_list = list(set([awb.vendor for awb in awbs]))
        vendor_dict = {}
        for vendor in vendors_list:
            vendor_dict[vendor] = [awb for awb in awbs if awb.vendor == vendor]
        for vendor in vendor_dict.keys():
            if vendor is not None:
                vendor_obj = Client_Vendor.objects.get(pk=vendor.pk)
            else:
                vendor_obj = None
            dto = DTO(
                dto_id=generateId(DTO, request.session['branch']),
                fe=fe,
                vehicle_id=int(request.POST['vehicle']),
                branch_id=int(request.session['branch']),
                client_id=int(request.POST['client']),
                vendor=vendor_obj
            )
            dto.save()
            for awb in vendor_dict[vendor]:
                AWB_Status.objects.filter(awb=awb).update(current_dto=dto, current_fe=fe,
                                                          status='DTO', updated_by=request.user)
                awb_history = AWB_History(
                    status='DTO',
                    awb=awb,
                    dto=dto, updated_by=request.user,
                    fe=fe,
                )
                awb_history.save()
        # for awb in awbs:
        #     AWB_Status.objects.filter(awb=awb).update(current_dto=dto, current_fe=fe,
        #                                               status='DTO', updated_by=request.user)
        #     awb_history = AWB_History(
        #         status='DTO',
        #         awb=awb,
        #         dto=dto, updated_by=request.user,
        #         fe=fe,
        #     )
        #     awb_history.save()
        if 'dto_awbs' in request.session:
            del request.session['dto_awbs']
        return HttpResponse(True)
        # except:
        #     return HttpResponse(False)
    else:
        return HttpResponse(False)


def drs_awb_invoice_inscan(request):
    if request.method == 'POST' and request.is_ajax():
        awb = AWB.objects.get(pk=int(request.POST['awb']))
        if 'branch' in request.session:
            if awb.get_delivery_branch().pk == request.session['branch']:
                if awb.awb_status.status != 'PC':
                    awb.invoice_no = request.POST['invoice_no']
                    awb.save()
                    AWB_Status.objects.filter(awb=awb.pk).update(status='DCR', current_branch=request.session['branch'],
                                                                 updated_by=request.user)
                    AWB_History.objects.create(awb=awb, status='PC', branch_id=request.session['branch'],
                                               updated_by=request.user)
                    AWB_History.objects.create(awb=awb, status='DCR', branch_id=request.session['branch'],
                                           updated_by=request.user)
                else:
                    awb.invoice_no = request.POST['invoice_no']
                    awb.save()
                    AWB_Status.objects.filter(awb=awb.pk).update(status='DCR', current_branch=request.session['branch'],
                                                                 updated_by=request.user)
                    AWB_History.objects.create(awb=awb, status='DCR', branch_id=request.session['branch'],
                                           updated_by=request.user)
            else:
                if awb.awb_status.status != 'PC':
                    awb.invoice_no = request.POST['invoice_no']
                    awb.save()
                    AWB_Status.objects.filter(awb=awb.pk).update(status='PC', current_branch=request.session['branch'],
                                                                 updated_by=request.user)
                    AWB_History.objects.create(awb=awb, status='PC', branch_id=request.session['branch'],
                                               updated_by=request.user)
                else:
                    awb.invoice_no = request.POST['invoice_no']
                    awb.save()
                    AWB_Status.objects.filter(awb=awb.pk).update(status='PC', current_branch=request.session['branch'],
                                                                 updated_by=request.user)

            request.session['message'] = {}
            request.session['message']['class'] = 'success'
            request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Invoice No: " + request.POST[
                'invoice_no'] + \
                                                   " | Status: Pick-up Completed" + " | DTO Branch: " + \
                                                   str(awb.get_delivery_branch().branch_name)
            close_drs(awb.awb_status.current_drs)
            return HttpResponse(True)
        else:
            request.session['message'] = {}
            request.session['message']['class'] = 'error'
            request.session['message']['report'] = 'Please select ' + awb.get_pickup_branch().branch_name + ' branch'
            return HttpResponse(False)


def drs_awb_cancel_scan(request):
    if request.method == 'POST' and request.is_ajax():
        request.session['message'] = {}
        awb = AWB.objects.get(awb=str(request.POST['awb']))
        if request.POST['status'] == 'CAN':
            remark = request.POST['remark']
        else:
            remark = ''
        reason = request.POST['reason']
        if remark == 'Other' and cache.get('reason_' + str(request.POST['awb']) + '_' + str(request.user.pk)):
            reason = cache.get('reason_' + str(request.POST['awb']) + '_' + str(request.user.pk))
            cache.delete('reason_' + str(request.POST['awb']) + '_' + str(request.user.pk))
        if awb.pk == int(request.POST['id']):
            if awb.category == 'REV':
                if awb.get_delivery_branch().pk == request.session['branch']:
                    AWB_Status.objects.filter(awb=awb.pk).update(status='DCR', current_branch=request.session['branch'],
                                                                 reason=reason, updated_by=request.user)
                    AWB_History.objects.create(awb=awb, status='PC', branch_id=request.session['branch'],
                                               updated_by=request.user, reason=reason)
                    AWB_History.objects.create(awb=awb, status='DCR', branch_id=request.session['branch'],
                                               updated_by=request.user, reason=reason)
                else:
                    AWB_Status.objects.filter(awb=awb.pk).update(status='PC', current_branch=request.session['branch'],
                                                                 reason=reason, updated_by=request.user)
                    AWB_History.objects.create(awb=awb, status='PC', branch_id=request.session['branch'],
                                               updated_by=request.user, reason=reason)
                request.session['message']['class'] = 'success'
                request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: Pick-up Completed" + \
                                                       " | DTO Branch: " + \
                                                       str(awb.get_delivery_branch().branch_name)
                close_drs(awb.awb_status.current_drs)
                return HttpResponse('PC')
            else:
                AWB_Status.objects.filter(awb=awb.pk).update(status=request.POST['status'], remark=remark,
                                                             current_branch=request.session['branch'],
                                                             updated_by=request.user, reason=reason)
                AWB_History.objects.create(awb=awb, status=request.POST['status'], remark=remark, reason=reason,
                                           branch_id=request.session['branch'], updated_by=request.user)

                notified = ''
                if request.POST['status'] == 'CAN' and awb.get_sms_notification():
                    sms = send_cancel_notification.delay(awb)
                    if sms:
                        notified = ' | Cancellation SMS : Sent'

                if request.POST['status'] == 'DBC' and awb.get_sms_notification():
                    sms = send_deferred_notification.delay(awb, reason)
                    if sms:
                        notified = ' | Deferred SMS : Sent'
                request.session['message']['class'] = 'success'
                request.session['message']['report'] = "AWB: " + str(
                    awb.awb) + " | Status: " + awb.awb_status.get_readable_choice() + notified
                close_drs(awb.awb_status.current_drs)
                return HttpResponse('DCR')
        else:
            request.session['message']['class'] = 'error'
            request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                awb.awb_status.get_readable_choice())
            close_drs(awb.awb_status.current_drs)
            return HttpResponse('')


def drs_awb_update_status(request):
    if request.method == 'POST' and request.is_ajax():
        awbs = json.loads(request.POST['awbs'])
        collected_amts = json.loads(request.POST['collected_amts'])
        awbs_dict = dict(zip(awbs, collected_amts))
        for k, v in awbs_dict.items():
            awb = AWB.objects.get(pk=int(k))
            if awb.category == 'REV':
                # if awb.awb_status.status in ['PC', 'DCR']:
                #     AWB_Status.objects.filter(awb=awb).update(status=str(request.POST['awb_status']))
                pass
            else:
                if awb.awb_status.status in ['DRS', 'DCR']:
                    if awb.category == 'COD':
                        AWB_Status.objects.filter(awb=awb).update(status=str(request.POST['awb_status']),
                                                                  updated_by=request.user)
                        AWB_Status.objects.filter(awb=awb).update(collected_amt=str(v))
                    else:
                        AWB_Status.objects.filter(awb=awb).update(status=str(request.POST['awb_status']),
                                                                  updated_by=request.user)
            close_drs(awb.awb_status.current_drs)
        return HttpResponse(True)


@transaction.atomic
def dto_awb_update_status(request):
    if request.method == 'POST' and request.is_ajax():
        awbs = AWB_Status.objects.filter(awb__pk__in=json.loads(request.POST['awbs']))
        awbs.update(status=str(request.POST['awb_status']), updated_by=request.user,
                    current_branch=request.session['branch'])
        for awb in awbs:
            AWB_History.objects.create(awb=awb.awb, branch_id=request.session['branch'],
                                       status=request.POST['awb_status'], updated_by=request.user)
        dto = awbs.first().current_dto
        if request.FILES.get('pod'):
            upload_dto_pod(dto, request.FILES['pod'])
        #print request.FILES
        #pod_image_form_class = automodelform_factory(model=DTO, fields=['dto'])
        if close_dto(dto):
            return HttpResponse('/transit/dto')

        return HttpResponse(True)


def rto_awb_update_status(request):
    if request.method == 'POST' and request.is_ajax():
        awbs = json.loads(request.POST['awbs'])
        for id in awbs:
            awb = AWB.objects.get(pk=int(id))
            if awb.awb_status.status != request.POST['awb_status']:
                AWB_Status.objects.filter(awb=awb).update(status=str(request.POST['awb_status']),
                                                          updated_by=request.user)
                AWB_History.objects.create(awb=awb, branch_id=request.session['branch'],
                                           status=request.POST['awb_status'], updated_by=request.user)
            if close_rto(awb.awb_status.current_rto):
                return HttpResponse('/transit/rto')
        return HttpResponse(True)


def drs_awb_status_update(request):
    if request.method == 'POST' and request.is_ajax():
        print request.POST
        request.session['message'] = {}
        if 'reason' in request.POST:
            reason = request.POST['reason']
        else:
            reason = ''
        if request.POST['status'] == 'CAN':
            remark = request.POST['remark']
        else:
            remark = ''
        if remark == 'Other' and cache.get('reason_' + str(request.POST['awb']) + '_' + str(request.user.pk)):
            reason = cache.get('reason_' + str(request.POST['awb']) + '_' + str(request.user.pk))
            cache.delete('reason_' + str(request.POST['awb']) + '_' + str(request.user.pk))
        awb = AWB.objects.get(pk=int(request.POST['awb']))
        if awb.category == 'REV':
            AWB_Status.objects.filter(awb=awb.pk).update(status=str(request.POST['status']),
                                                         remark=remark,
                                                         reason=reason, updated_by=request.user)
        else:
            if awb.category == 'COD':
                if request.POST['coll_amt'] != '':
                    AWB_Status.objects.filter(awb=awb.pk).update(status=str(request.POST['status']),
                                                                 remark=remark,
                                                                 collected_amt=float(request.POST['coll_amt']),
                                                                 reason=reason, updated_by=request.user)
                else:
                    AWB_Status.objects.filter(awb=awb.pk).update(status=str(request.POST['status']),
                                                                 remark=remark,
                                                                 reason=reason, updated_by=request.user)
            else:
                AWB_Status.objects.filter(awb=awb.pk).update(status=str(request.POST['status']), remark=remark,
                                                             reason=reason, updated_by=request.user)
        AWB_History.objects.create(awb=awb, status=str(request.POST['status']), branch_id=request.session['branch'],
                                   updated_by=request.user, remark=remark, reason=reason)

        notified = ''
        if request.POST['status'] == 'CAN' and awb.get_sms_notification():
            sms = send_cancel_notification.delay(awb)
            if sms:
                notified = ' | Cancellation SMS : Sent'

        if request.POST['status'] == 'DBC' and awb.get_sms_notification():
            sms = send_deferred_notification.delay(awb, reason)
            if sms:
                notified = ' | Deferred SMS : Sent'
        request.session['message']['class'] = 'success'
        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
            AWB_Status.objects.get(awb=awb.pk).get_readable_choice()) + notified
        close_drs(awb.awb_status.current_drs)
        return HttpResponse(True)


def get_count(request):
    if request.is_ajax():
        if request.GET['model'] == 'awb':
            if request.GET['type'] == 'incoming':
                if 'branch' in request.session:
                    if request.GET['category'] != '':
                        return HttpResponse(
                            AWB.objects.filter(pincode__branch_pincode__branch_id=request.session['branch'],
                                               category=request.GET['category']).exclude(
                                awb_status__status__in=['DEL', 'RET']).count())
                    else:
                        return HttpResponse(
                            AWB.objects.filter(pincode__branch_pincode__branch_id=request.session['branch']).exclude(
                                awb_status__status__in=['DEL', 'RET']).count())
                else:
                    if request.GET['category'] != '':
                        return HttpResponse(AWB.objects.filter(category=request.GET['category']).exclude(
                            awb_status__status__in=['DEL', 'RET']).count())
                    else:
                        return HttpResponse(AWB.objects.exclude(awb_status__status__in=['DEL', 'RET']).count())
            else:
                if 'branch' in request.session:
                    if request.GET['category'] != '':
                        return HttpResponse(
                            AWB.objects.filter(awb_status__current_branch_id=request.session['branch'],
                                               category=request.GET['category']).exclude(
                                awb_status__status__in=['DEL', 'RET']).count())
                    else:
                        return HttpResponse(
                            AWB.objects.filter(awb_status__current_branch_id=request.session['branch']).exclude(
                                awb_status__status__in=['DEL', 'RET']).count())
                else:
                    if request.GET['category'] != '':
                        return HttpResponse(AWB.objects.filter(category=request.GET['category']).exclude(
                            awb_status__status__in=['DEL', 'RET']).count())
                    else:
                        return HttpResponse(AWB.objects.exclude(awb_status__status__in=['DEL', 'RET']).count())
        elif request.GET['model'] == 'tb':
            if request.GET['type'] == 'incoming':
                if 'branch' in request.session:
                    return HttpResponse(TB.objects.filter(delivery_branch_id=request.session['branch']).exclude(
                        tb_history__mts__status='D').count())
                else:
                    return HttpResponse(TB.objects.exclude(tb_history__mts__status='D').count())
            else:
                if 'branch' in request.session:
                    return HttpResponse(TB.objects.filter(origin_branch_id=request.session['branch']).exclude(
                        tb_history__mts__status='D').count())
                else:
                    return HttpResponse(TB.objects.exclude(tb_history__mts__status='D').count())
        elif request.GET['model'] == 'mts':
            if request.GET['type'] == 'incoming':
                if 'branch' in request.session:
                    return HttpResponse(
                        MTS.objects.filter(to_branch_id=request.session['branch']).exclude(status='D').count())
                else:
                    return HttpResponse(MTS.objects.exclude(status='D').count())
            else:
                if 'branch' in request.session:
                    return HttpResponse(
                        MTS.objects.filter(from_branch_id=request.session['branch']).exclude(status='D').count())
                else:
                    return HttpResponse(MTS.objects.exclude(status='D').count())
        elif request.GET['model'] == 'drs':
            if 'branch' in request.session:
                return HttpResponse(DRS.objects.filter(branch_id=request.session['branch']).exclude(status='C').count())
            else:
                return HttpResponse(DRS.objects.exclude(status='C').count())
        elif request.GET['model'] == 'dto':
            if 'branch' in request.session:
                return HttpResponse(DTO.objects.filter(branch_id=request.session['branch']).exclude(status='C').count())
            else:
                return HttpResponse(DTO.objects.exclude(status='C').count())
        elif request.GET['model'] == 'rto':
            if 'branch' in request.session:
                return HttpResponse(RTO.objects.filter(branch_id=request.session['branch']).exclude(status='C').count())
            else:
                return HttpResponse(RTO.objects.exclude(status='C').count())


def drs_update_closing_km(request):
    request.session['message'] = {}
    drs = DRS.objects.get(drs_id=request.GET['drs_id'])
    drs.closing_km = request.GET['closing_km']
    drs.save()
    request.session['message']['class'] = 'success'
    request.session['message']['report'] = 'DRS : ' + drs.drs_id + ' | Closing Km : ' + request.GET[
        'closing_km'] + ' Km'
    close_drs(drs)
    return HttpResponse(True)


def rto_detail(request, rto_id):
    fl = AWB.objects.filter(awb_history__rto=rto_id).order_by('awb')
    return render(request, 'transit/awb_status_update.html',
                  {'fl': fl, 'rto': RTO.objects.get(pk=int(rto_id)), 'model': 'rto'})


def rto(request):
    if 'branch' in request.session:
        table = RTOTable(
            RTO.objects.filter(branch=request.session['branch']).exclude(status='C').order_by('-creation_date'))
    else:
        table = RTOTable(RTO.objects.exclude(status='C').order_by('-creation_date'))
    RequestConfig(request, paginate={"per_page": 10}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'url': 'rto/create', 'model': 'rto', 'type': 'open'})


def rto_in_scanning(request):
    if request.method == 'POST' and request.is_ajax():
        request.session['message'] = {}
        if 'rto_awbs' not in request.session:
            request.session['rto_awbs'] = []
            scanned_awbs = request.session['rto_awbs']
        else:
            scanned_awbs = request.session['rto_awbs']

        try:
            awb = AWB.objects.get(awb=request.POST['awb'])
            if awb.category in ['COD', 'PRE'] and awb.get_client_obj().pk == request.session['rto_client'] \
                    and 'rto_client' in request.session:
                if awb.get_rto_branch().pk == request.session['branch'] and \
                                awb.awb_status.current_branch_id == request.session['branch']:
                    if awb.awb_status.status in ['CAN', 'RET', 'PFR']:
                        request.session['message']['class'] = 'success'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        scanned_awbs.append(awb.pk)
                        request.session['rto_awbs'] = scanned_awbs
                        return render(request, 'transit/rto_in_scanning.html', {'awb': awb})
                    else:
                        request.session['message']['class'] = 'error'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        return HttpResponse('')
                else:
                    request.session['message']['class'] = 'error'
                    request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                        awb.awb_status.get_readable_choice()) + " | Delivery Branch: " + str(
                        awb.get_delivery_branch().branch_name)
                    return HttpResponse('')
            else:
                if awb.get_delivery_branch().pk == request.session['branch']:
                    if awb.awb_status.status in ['CAN', 'ITR']:
                        request.session['message']['class'] = 'error'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        return HttpResponse('')
                    else:
                        request.session['message']['class'] = 'error'
                        request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                            awb.awb_status.get_readable_choice())
                        return HttpResponse('')
                else:
                    request.session['message']['class'] = 'error'
                    request.session['message']['report'] = "AWB: " + str(awb.awb) + " | Status: " + str(
                        awb.awb_status.get_readable_choice()) + ' | Client: ' + awb.get_client()
                    return HttpResponse('')
        except AWB.DoesNotExist:
            request.session['message']['class'] = 'error'
            request.session['message']['report'] = "AWB: " + request.POST['awb'] + " | Status: Does Not Exists"
            return HttpResponse('')
    else:
        form = CreateRTOForm()
        context = {}
        context['form'] = form
        context['model'] = 'rto'
        if 'branch' in request.session:
            form.fields['fe'].queryset = get_fe_list(request.session['branch'])
            form.fields['vehicle'].queryset = Vehicle.objects.filter(branch_id=request.session['branch'])
        if 'rto_awbs' in request.session:
            context['scanned_awbs'] = AWB.objects.filter(pk__in=request.session['rto_awbs'])
            print context['scanned_awbs']
        return render(request, 'transit/dto_creation_form.html', context)


def ajax_get_rto_awbs(request):
    if request.method == "POST" and request.is_ajax():
        # if 'branch' in request.session:
        #     fl = AWB.objects.filter(awb_status__status__in=['CAN', 'RET', 'PFR'],
        #                             awb_status__current_branch_id=request.session['branch'],
        #                             awb_status__manifest__branch_id=request.session['branch']).exclude(
        #         category='REV').order_by('-awb_status__status')
        # else:
        #     fl = AWB.objects.filter(awb_status__status__in=['CAN', 'RET', 'PFR']).exclude(category='REV').order_by(
        #         '-awb_status__status')
        # if request.POST['client'] != '':
        #     fl.filter(awb_status__manifest__client_id=request.POST['client'])
        # fl = AWB.objects.filter(awb_status__status='CAN').exclude(category='REV')
        filter = {}
        filter['awb_status__status__in'] = ['CAN', 'RET', 'PFR']
        if 'branch' in request.session:
            filter['awb_status__current_branch_id'] = request.session['branch']
            filter['awb_status__manifest__branch_id'] = request.session['branch']
        if request.POST['client'] != '':
            filter['awb_status__manifest__client_id'] = request.POST['client']
            request.session['rto_client'] = int(request.POST['client'])
            # fl = AWB.objects.filter(awb_status__status='CAN').exclude(category='REV')
        fl = AWB.objects.filter(**filter).exclude(category='REV').order_by('-awb_status__status')
        return render(request, 'transit/rto_awb_table.html', {'fl': fl})


def ajax_create_rto(request):
    if request.method == "POST" and request.is_ajax():
        try:
            fe = User.objects.get(pk=request.POST['fe'])
            awbs = list(set(json.loads(request.POST['awbs'])))
            if len(awbs) > 0:
                rto = RTO(
                    rto_id=generateId(RTO, request.session['branch']),
                    fe=fe,
                    vehicle=Vehicle.objects.get(pk=int(request.POST['vehicle'])),
                    branch=Branch.objects.get(pk=int(request.session['branch'])),
                    client=Client.objects.get(pk=int(request.POST['client']))
                )
                rto.save()

                for awb in awbs:
                    if AWB_Status.objects.get(awb=int(awb)).manifest.category == 'FL':
                        AWB_Status.objects.filter(awb=int(awb)).update(current_rto=rto, current_fe=fe,
                                                                       status='DFR', updated_by=request.user)
                        awb_history = AWB_History(
                            awb=AWB.objects.get(pk=int(awb)),
                            fe=fe,
                            rto=rto, updated_by=request.user
                        )
                        awb_history.save()
                if 'rto_awbs' in request.session:
                    del request.session['rto_awbs']
                return HttpResponse(True)
            else:
                return HttpResponse(False)
        except:
            return HttpResponse(False)
    else:
        return HttpResponse(False)


def generateId(self, *args):
    branch = ''
    for i in args:
        branch += ('0' * (2 - len(str(i))) + str(i))
    try:
        id = str(int(self.objects.all().order_by('-id')[0].pk + 1))
    except self.DoesNotExist:
        id = '1'
    return self.__name__ + branch + ('0' * (5 - len(id))) + id


def tb_linehaul_report(request):
    branch = Branch.objects.exclude(branch_name='HQ')
    if request.method == 'GET':
        return render(request, 'transit/tb_linehaul_report.html', {'branch': branch})
    elif request.method == 'POST':
        if request.POST['start_date'] != '' and request.POST['end_date'] != '':
            kwargs = {}
            kwargs['start_date'] = request.POST['start_date']
            kwargs['end_date'] = request.POST['end_date']
            if request.POST['origin_branch'] != '':
                kwargs['origin_branch'] = request.POST['origin_branch']
            if request.POST['delivery_branch'] != '':
                kwargs['delivery_branch'] = request.POST['delivery_branch']
            tb_report = TBReport(**kwargs)
            tbs = tb_report.get_tbs()

            if len(tbs) > 0:
                if request.POST['action'] == 'Submit':
                    table = TBLineHaulReportTable(tbs)
                    RequestConfig(request, paginate={"per_page": 100}).configure(table)
                    return render(request, 'transit/tb_linehaul_report.html', {'branch': branch, 'table': table})
                else:
                    filename = 'LINEHAUL_' + request.POST['start_date'] + '_' + request.POST['end_date'] + '.csv'
                    # response = HttpResponse(content_type='text/csv')
                    # response['Content-Disposition'] = 'attachment; filename="' + filename + '.csv"'
                    email = request.user.email
                    # print email
                    set_message('Linehual Report will be emailed to you shortly', 'success')
                    message = 'Linehual will be emailed to you shortly'
                    linehual_report.delay(filename, tb_report, email)
                    # return render(request, 'common/message.html', {'msg': message})
                    return HttpResponse('Linehual will be emailed to you shortly')
            else:
                return render(request, 'transit/tb_linehaul_report.html',
                              {'branch': branch, 'no_data_msg': 'No TB found in this range.'})
        else:
            return render(request, 'transit/tb_linehaul_report.html',
                          {'branch': branch, 'error_msg': 'Please Select date'})


def cache_reason(request):
    if request.is_ajax() and request.GET.get('reason') != '':
        #print 'reason_' + str(request.GET.get('awb')) + '_' + str(request.user.pk)
        cache.set('reason_' + str(request.GET.get('awb')) + '_' + str(request.user.pk), request.GET.get('reason'))
        return HttpResponse(True)
    else:
        return HttpResponse('Please enter reason for cancellation')

def dto_report(request):
    dtos = request.GET['dtos']
    awbs = []
    dtos = dtos.split(',')
    dto = DTO.objects.filter(pk__in = dtos)
    for i in dto:
        awb = AWB.objects.filter(awb_status__current_dto__dto_id = i)
        awbs.extend(awb)
    rows = [[awb.awb, awb.order_id, awb.weight, awb.length, awb.breadth, awb.height, awb.awb_status.current_dto] for awb in awbs]
    rows.insert(0, DTO_REPORT_HEADER)
    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer)
    response = StreamingHttpResponse(
        (writer.writerow(row) for row in rows),
        content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="dto_report_' + datetime.today().strftime(
        '%Y-%m-%d_%H:%M:%S') + '.csv"'
    return response




