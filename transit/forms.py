from django import forms

from transit.models import TB, MTS, DRS, DTO, RTO
from internal.models import Vehicle, Employee
from client.models import Client


class CreateTBForm(forms.ModelForm):
    class Meta:
        model = TB
        exclude = ['is_active', 'type', 'tb_id', 'origin_branch', 'status', 'co_loader', 'co_loader_awb']
        widgets = {
            'delivery_branch': forms.Select(attrs={'style': 'width:100px;'}),
            'length': forms.TextInput(attrs={'class': 'input-mini txtDecimal'}),
            'breadth': forms.TextInput(attrs={'class': 'input-mini txtDecimal'}),
            'height': forms.TextInput(attrs={'class': 'input-mini txtDecimal'}),
            'weight': forms.TextInput(attrs={'class': 'input-mini txtDecimal'}),
            # 'co_loader': forms.Select(attrs={'style': 'width:100px'}),
            # 'co_loader_awb': forms.TextInput(attrs={'class': 'input-mini txtDecimal'}),
            'seal_id': forms.TextInput(attrs={'class': 'input-small'}),
        }


class CreateMTSForm(forms.ModelForm):
    class Meta:
        model = MTS
        exclude = ['is_active', 'status', 'mts_id', 'from_branch']


class CreateDRSForm(forms.ModelForm):
    fe = forms.ModelChoiceField(
        queryset=Employee.objects.filter(role='FE', user__is_staff=True).exclude(phone='').order_by(
            'user__first_name').prefetch_related('user'),
        required=True)
    vehicle = forms.ModelChoiceField(queryset=Vehicle.objects.all())

    class Meta:
        model = DRS
        exclude = ['is_active', 'status', 'drs_id', 'branch', 'closing_km']


class CreateDTOForm(forms.ModelForm):
    fe = forms.ModelChoiceField(
        queryset=Employee.objects.filter(role='FE', user__is_staff=True).exclude(phone='').order_by(
            'user__first_name').prefetch_related('user'),
        required=True)
    vehicle = forms.ModelChoiceField(queryset=Vehicle.objects.all())
    client = forms.ModelChoiceField(queryset=Client.objects.filter(category='RL'))

    class Meta:
        model = DTO
        exclude = ['is_active', 'status', 'dto_id', 'branch', 'vendor', 'pod']



class CreateRTOForm(forms.ModelForm):
    fe = forms.ModelChoiceField(
        queryset=Employee.objects.filter(role='FE', user__is_staff=True).exclude(phone='').order_by(
            'user__first_name').prefetch_related('user'),
        required=True)
    vehicle = forms.ModelChoiceField(queryset=Vehicle.objects.all())
    client = forms.ModelChoiceField(queryset=Client.objects.filter(category='FL'))

    class Meta:
        model = RTO
        exclude = ['is_active', 'status', 'rto_id', 'branch']