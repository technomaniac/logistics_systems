import itertools

import django_tables2 as tables

from common.tables import TableMixin
from transit.models import TB, MTS, DRS, DTO, RTO


class TBTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super(TBTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )
    tb_id = tables.TemplateColumn(
        template_code='<a href="/transit/tb/{{ record.id }}" target="_blank">{{ record.tb_id }}</a>',
        verbose_name='TB')
    mts = tables.Column(accessor='get_current_mts', verbose_name='MTS')
    awb = tables.TemplateColumn(template_code='<a href="/transit/tb/{{ record.id }}">{{ record.get_awb_count }}</a>',
                                verbose_name='AWB', sortable=False, )
    co_loader_awb_mts = tables.Column(accessor='get_co_loader_awb_mts', verbose_name='Co Loader AWB')
    co_loader_mts = tables.Column(accessor='get_co_loader_mts', verbose_name='Co Loader')
    current_branch = tables.Column(accessor='get_current_branch')
    # status = tables.Column(accessor='get_current_status')
    date = tables.Column(accessor='creation_date', verbose_name='Date')
    #action = tables.TemplateColumn(
    #    template_code='<div class="hidden-phone visible-desktop btn-group"><button class="btn btn-mini btn-info" id="editTB"><i class="icon-edit bigger-120"></i></button></div>')

    class Meta:
        model = TB
        exclude = ['on_update', 'is_active', 'type', 'creation_date', 'id', 'co_loader', 'co_loader_awb']
        attrs = {"class": "table table-striped table-bordered table-hover"}
        sequence = ('s_no',)

    def render_s_no(self):
        return next(self.counter) + 1


class MTSTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super(MTSTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )
    mts_id = tables.TemplateColumn(
        template_code='<a href="/transit/mts/{{ record.id }}" target="_blank">{{ record.mts_id }}</a>',
        verbose_name='MTS')
    type = tables.Column(accessor='get_readable_type')
    co_loader_awb = tables.TemplateColumn(template_code='<div>{{record.co_loader_awb}}</div>'
                        '<div class="hidden-phone visible-desktop btn-group">'
                      '<button class="btn-mini btn-primary" id="editMTSCo_awb"><i class="icon-refresh icon-edit bigger-120"></i></button>')

    tb = tables.TemplateColumn(template_code='<a href="/transit/mts/{{ record.id }}">{{ record.get_tbs_count }}</a>',
                               verbose_name='TB', sortable=False, )
    # edit_co_loader_awb = tables.TemplateColumn(
    #     template_code='<div class="hidden-phone visible-desktop btn-group">'
    #                   '<button class="btn btn-mini" id="editMTSCo_awb"><i class="icon-edit bigger-120"></i></button>'
    # )
    action = tables.TemplateColumn(
        template_code='<div class="hidden-phone visible-desktop btn-group">'
                      '<button class="btn btn-mini btn-danger" id="editMTS"><i class="icon-edit bigger-120"></i></button>'
                      '<a href="/transit/mts/{{ record.mts_id }}/print_sheet" target="_blank">'
                      '<button class="btn btn-mini"><i class="icon-print bigger-120"></i></button>'
                      '</a>'
                      '</div>',
        sortable=False, )
    date = tables.Column(accessor='creation_date', verbose_name='Date')
    track = tables.TemplateColumn(
        template_code='<a href="{{ record.get_external_track_url }}" target="_blank">{{ record.external_track_id }}</a>')

    class Meta:
        model = MTS
        exclude = ['creation_date', 'on_update', 'is_active', 'id', 'external_track_id']
        attrs = {"class": "table table-striped table-bordered table-hover"}
        sequence = ('s_no',)

    def render_s_no(self):
        return next(self.counter) + 1


class MTSOutgoingTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super(MTSOutgoingTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )
    mts_id = tables.TemplateColumn(
        template_code='<a href="/transit/mts/{{ record.id }}" target="_blank">{{ record.mts_id }}</a>',
        verbose_name='MTS')
    co_loader_awb = tables.TemplateColumn(template_code='<div>{{record.co_loader_awb}}</div>'
                        '<div class="hidden-phone visible-desktop btn-group">'
                      '<button class="btn-mini btn-primary" id="editMTSCo_awb"><i class="icon-refresh icon-edit bigger-120"></i></button>')

    tb = tables.TemplateColumn(template_code='<a href="/transit/mts/{{ record.id }}">{{ record.get_tbs_count }}</a>',
                               verbose_name='TB', sortable=False, )

    date = tables.Column(accessor='creation_date', verbose_name='Date')
    action = tables.TemplateColumn(
        template_code='<div class="hidden-phone visible-desktop btn-group">'
                      '<a href="/transit/mts/{{ record.mts_id }}/print_sheet" target="_blank">'
                      '<button class="btn btn-mini"><i class="icon-print bigger-120"></i></button>'
                      '</a>'
                      '</div>',
        sortable=False, )
    track = tables.TemplateColumn(
        template_code='<a href="{{ record.get_external_track_url }}" target="_blank">{{ record.external_track_id }}</a>')

    class Meta:
        model = MTS
        exclude = ['creation_date', 'on_update', 'is_active', 'id', 'external_track_id']
        attrs = {"class": "table table-striped table-bordered table-hover"}
        sequence = ('s_no',)

    def render_s_no(self):
        return next(self.counter) + 1


class DRSTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super(DRSTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )
    drs_id = tables.TemplateColumn(
        template_code='<a href="/transit/drs/{{ record.id }}" target="_blank">{{ record.drs_id }}</a>',
        verbose_name='DRS')
    awb = tables.TemplateColumn(
        template_code='<a href="/transit/drs/{{ record.id }}">{{ record.get_awb_count }}</a>', verbose_name='AWB',
        sortable=False)
    open_awb = tables.TemplateColumn(
        template_code='<a href="/transit/drs/{{ record.id }}">{{ record.get_open_awb_count }}</a>', verbose_name='Open',
        sortable=False)
    expected_amount = tables.Column(accessor='get_expected_amount', sortable=False)
    collected_amount = tables.Column(accessor='get_collected_amount', sortable=False)
    date = tables.Column(accessor='creation_date', verbose_name='Date')
    #action = tables.TemplateColumn(
    #    template_code='<div class="hidden-phone visible-desktop btn-group"><button class="btn btn-mini btn-info" id="editDRS"><i class="icon-edit bigger-120"></i></button></div>')
    class Meta:
        model = DRS
        exclude = ['id', 'creation_date', 'on_update', 'is_active', 'opening_km', 'closing_km']
        sequence = ('s_no',)
        attrs = {"class": "table table-striped table-bordered table-hover"}

    def render_s_no(self):
        return next(self.counter) + 1


class DTOTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super(DTOTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()
    select = tables.TemplateColumn(
        template_code='<label><input type="checkbox" checked="true" id="{{ record.id }}"><span class="lbl"></span></label>')
    # s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )
    dto_id = tables.TemplateColumn(
        template_code='<a href="/transit/dto/{{ record.id }}" target="_blank">{{ record.dto_id }}</a>',
        verbose_name='DTO')
    vendor = tables.Column(accessor='vendor.vendor_name')
    vendor_branch = tables.Column(accessor='get_vendor_branch', verbose_name='Vendor Branch')
    awb = tables.TemplateColumn(
        template_code='<a href="/transit/dto/{{ record.id }}">{{ record.get_all_awb_count }}</a>',
        verbose_name='AWB', sortable=False)
    open_awb = tables.TemplateColumn(
        template_code='<a href="/transit/dto/{{ record.id }}">{{ record.get_open_awb_count }}</a>',
        verbose_name='Open', order_by='dto_id', sortable=False)
    date = tables.Column(accessor='creation_date', verbose_name='Date')
    #action = tables.TemplateColumn(
    #    template_code='<div class="hidden-phone visible-desktop btn-group"><button class="btn btn-mini btn-info" id="editDTO"><i class="icon-edit bigger-120"></i></button></div>')

    class Meta:
        model = DTO
        exclude = ['creation_date', 'on_update', 'is_active', 'id']
        # fields = ('select',)
        attrs = {"class": "table table-striped table-bordered table-hover", "id": "dto_search_tbl"}
        sequence = ('select',)

    def render_s_no(self):
        return next(self.counter) + 1


class RTOTable(tables.Table):
    def __init__(self, *args, **kwargs):
        super(RTOTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    rto_id = tables.TemplateColumn(
        template_code='<a href="/transit/rto/{{ record.id }}" target="_blank">{{ record.rto_id }}</a>',
        verbose_name='RTO')
    awb = tables.TemplateColumn(template_code='<a href="/transit/rto/{{ record.id }}">{{ record.get_awb_count }}</a>',
                                verbose_name='AWB', sortable=False)
    open_awb = tables.TemplateColumn(
        template_code='<a href="/transit/rto/{{ record.id }}">{{ record.get_open_awb_count }}</a>',
        verbose_name='Open', order_by='rto_id', sortable=False)
    date = tables.Column(accessor='creation_date', verbose_name='Date')

    class Meta:
        model = RTO
        exclude = ['creation_date', 'on_update', 'is_active', 'id']
        attrs = {"class": "table table-striped table-bordered table-hover"}
        sequence = ('s_no',)

    def render_s_no(self):
        return next(self.counter) + 1


class TBLineHaulReportTable(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)
    creation_date = tables.Column(accessor='get_hand_over_date',
                                      verbose_name='Hand-Over Date')
    tb_id = tables.TemplateColumn(
        template_code='<a href="/transit/tb/{{ record.id }}" target="_blank">{{ record.tb_id }}</a>',
        verbose_name='TB')
    status = tables.Column(accessor='status', verbose_name='Status')
    origin_branch = tables.Column(accessor='origin_branch', verbose_name='From')
    delivery_branch = tables.Column(accessor='delivery_branch', verbose_name='To')
    mode = tables.Column(accessor='get_transfer_mode_absolute')
    length = tables.Column(accessor='get_length_inch', verbose_name='L(inch)')
    breadth = tables.Column(accessor='get_breadth_inch', verbose_name='B(inch)')
    height = tables.Column(accessor='get_height_inch', verbose_name='H(inch)')
    received_date = tables.Column(accessor='get_received_date')
    weight = tables.Column(accessor='weight', verbose_name='Dead Weight(kg)')
    volumetric_weight = tables.Column(accessor='get_volumetric_weight', verbose_name='Volumetric Weight (WGT)',
                                      orderable=False)
    compiled_weight = tables.Column(accessor='get_compiled_weight', verbose_name='Compiled Weight (WGT)',
                                    orderable=False)
    mts_id = tables.Column(accessor='get_current_mts', verbose_name='Current MTS')
    co_loader = tables.Column(accessor='get_co_loader_mts', verbose_name='Co Loader', orderable=False)
    co_loader_awb = tables.Column(accessor='get_co_loader_awb_mts', verbose_name='Co Loader AWB')


    class Meta:
        model = TB
        fields = ('tb_id',)
        attrs = {"class": "table table-striped table-bordered table-hover"}
        sequence = ( 's_no', 'creation_date', 'tb_id', )