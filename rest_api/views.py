import json

from django.contrib.auth import logout, authenticate, login

from django.http.response import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from rest_framework.response import Response
from rest_framework.views import APIView
import requests

from awb.models import AWB

from awb.serializers import AWBDetail, AWBStatusDetails, AWBHistoryDetails
from custom.custom_mixins import GeneralLoginRequiredMixin
from customer_service.models import AWBCallOutHistory, AWB_Out_Calling
from logistics.settings.local import EXOTEL_SID, EXOTEL_TOKEN
from customer_service.models import AWBCallOutHistory
from logistics.settings.base import EXOTEL_SID, EXOTEL_TOKEN
from utils.common import get_json_response
from utils.searching.searching import SearchGetAttrs


class ApiLoginRequiredMixin(GeneralLoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            logout(request)
            return Response({'status': 'error', 'msg': 'login required'})

        return super(GeneralLoginRequiredMixin, self).dispatch(
            request, *args, **kwargs)


class AWBDetails(APIView):
    def get(self, request, *args, **kwargs):
        if kwargs.get('awb_number'):
            awb_no = kwargs.get('awb_number')
            try:
                awb = AWB.objects.get(awb=awb_no)
                serialized = AWBDetail(awb)
                return Response(serialized.data)
            except:
                return HttpResponse('AWB not found')
                # else:
                # awb = SearchGetAttrs(request.GET, model=AWB)


class GetAWBStatus(APIView):
    # @method_decorator(csrf_exempt)
    # def dispatch(self, request, *args, **kwargs):
    #     return super(GetAWBStatus, self).dispatch(request, *args, **kwargs)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        # print request.POST.get('awb_list'), 'awb'
        try:
            awb_list = request.POST.get('awb_list').replace(',', ' ').split()
            awb_queryset = AWB.objects.filter(awb__in=awb_list)
            serialized = AWBStatusDetails(awb_queryset, many=True)
            # print serialized.data,'json'
            return Response(serialized.data)
        except:
            return HttpResponse('Blank data Please input the POST parameter and its value')


class GetAWBHistory(APIView):
    def get(self, request, *args, **kwargs):
        if kwargs.get('awb_number'):
            awb_no = kwargs.get('awb_number')
            awb = AWB.objects.get(awb=awb_no)
        else:
            awb = SearchGetAttrs(request.GET, model=AWB)
        serialized = AWBHistoryDetails(awb)
        return Response(serialized.data)


class ApiLoginHandler(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ApiLoginHandler, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        c = {}
        if request.user.is_authenticated():
            # url = reverse('mobile_awb_list')
            # return HttpResponseRedirect(url)
            c['status'] = 'successful'
            c['message'] = 'Login successful'
            return Response(c)
        c['status'] = 'error'
        c['msg'] = 'Please Login'
        return get_json_response(c)

    def post(self, request):
        c = {}
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        c['status'] = 'error'
        c['msg'] = 'Invalid username or password'
        if user:
            if user.is_active:
                login(request, user)
                # url = reverse('mobile_awb_list')
                # return HttpResponseRedirect(url)
                return get_json_response({'status': 'successful', 'sessionid': request.COOKIES.get('sessionid')})
        return get_json_response({'status': 'error', 'msg': 'Invalid User'})


class ApiLogoutHandler(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ApiLogoutHandler, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        c = {}
        logout(request)
        c['http_status'] = 200
        c['message'] = 'Logged out'
        return get_json_response(c)


# exotel api integration

class AWBOutCall(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(AWBOutCall, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        awb = request.GET['awb']
        # agent_no = request.GET['agent_no']
        agent_no = request.GET['agent_no']
        customer_no = request.GET['customer_no']
        callerid = '01130018036'
        dnd = dnd_check(customer_no)
        if dnd == 'Yes':
            try:
                awb_g = AWB.objects.get(awb=awb)
                awb_c = AWB_Out_Calling.objects.get(awb=awb_g)
                awb_c.dnd_no = True
                awb_c.save()
            except:
                pass
            return HttpResponse('DND Number')
        else:
            call = awb_outbound_call(agent_no, customer_no, callerid)
            r = json.loads(call.content)
            # print r
            sid = r['Call']['Sid']
            customer_awb = AWB.objects.get(awb=awb)
            call_status = r['Call']['Status']
            start_time = r['Call']['StartTime']
            end_time = r['Call']['EndTime']
            call_duration = str(r['Call']['Duration'])
            call_price = str(r['Call']['Price'])
            # print r['Call']['RecordingUrl']
            recording_url = r['Call']['RecordingUrl']
            # print recording_url
            AWBCallOutHistory.objects.create(agent_no=agent_no, customer_no=customer_no, call_sid=sid,
                                             customer_awb=customer_awb, call_status=call_status,
                                             start_time=start_time, end_time=end_time, call_duration=call_duration,
                                             call_price=call_price, recording_url=recording_url)
            return HttpResponse(True)


def awb_outbound_call(agent_no, customer_no, callerid,
                      timelimit=None, timeout=None, calltype='trans'):
    return requests.post('https://twilix.exotel.in/v1/Accounts/{sid}/Calls/connect.json'.format(sid=EXOTEL_SID),
                         auth=(EXOTEL_SID, EXOTEL_TOKEN),
                         data={
                             'From': agent_no,
                             'To': customer_no,
                             'CallerId': callerid,
                             'TimeLimit': timelimit,
                             'TimeOut': timeout,
                             'CallType': calltype,
                         })


def awb_out_details_api(sid):
    # EXOTEL_SID = 'nuvoex1'
    # EXOTEL_TOKEN = '92a19c313f4398d1cd2e4388a6e15c3cca21cd54'
    r = requests.post('https://{e_sid}:{e_token}@twilix.exotel.in/v1/Accounts/{e_sid}/Calls/{sid}.json'.format(sid=sid,
                                                                                                               e_sid=EXOTEL_SID,
                                                                                                               e_token=EXOTEL_TOKEN),
                      auth=(EXOTEL_SID, EXOTEL_TOKEN))
    return r.content


def dnd_check(customer_no):
    # EXOTEL_SID = 'nuvoex1'
    # EXOTEL_TOKEN = '92a19c313f4398d1cd2e4388a6e15c3cca21cd54'
    dnd = requests.get(
        'https://{sid}:{exotel_sid}@twilix.exotel.in/v1/Accounts/{sid}/Numbers/{customer_no}.json'.format(
            sid=EXOTEL_SID,
            exotel_sid=EXOTEL_TOKEN, customer_no=customer_no),
        auth=(EXOTEL_SID, EXOTEL_TOKEN))

    r = dnd.content
    d = json.loads(r)
    # print d['Numbers']['DND']
    return d['Numbers']['DND']

