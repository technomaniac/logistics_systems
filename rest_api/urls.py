from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns('',
                       # Examples:
                       url(r'^awb/(?P<awb_number>[0-9a-zA-Z]+)/$', AWBDetails.as_view()),
                       # url(r'^awb$', AWBDetails.as_view()),
                       url(r'^awbs/status/$', GetAWBStatus.as_view()),

                       # url(r'^awb/(?P<awb_number>[0-9a-zA-Z]+)/history/$', GetAWBHistory.as_view()),
                       url(r'^login/$', ApiLoginHandler.as_view()),
                       url(r'^logout/$', ApiLogoutHandler.as_view()),
                       url(r'^awb/calling$', AWBOutCall.as_view()),
                       # url(r'^call/details$', AWB_Out_Response.as_view()),
                       url(r'^call/dnd$', 'rest_api.views.dnd_check', name='dnd_check'),
                       url(r'^call/det$', 'rest_api.views.awb_out_details_api', name='awb_out_details_api')
                       # url(r'^api/awb_outbound_call$', 'awb_outbound_call', name='awb_outbound_call')
                       # url(r'^awb/sddd$', sddd_api.as_View(), name='sddd')

)