import json

from django.db import models
from django.db.models import OneToOneField
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.fields.related import SingleRelatedObjectDescriptor
from south.modelsinspector import add_introspection_rules
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db.models import ForeignKey, Manager
from django.db.models.loading import get_model
from django.conf import settings


class AutoUserForeignKey(ForeignKey):
    '''
    '''

    def __init__(self, **kwargs):
        app_label, model_name = settings.AUTH_USER_MODEL.split('.')
        user_model = get_model(app_label, model_name)
        # user_model = CustomUser
        # print user_model, 'usermodel'
        kwargs['to'] = user_model
        self.manager_name = kwargs.pop('manager_name', 'for_user')
        super(AutoUserForeignKey, self).__init__(**kwargs)

    def contribute_to_class(self, cls, name):  # todo tothink vitual_only kwarg is missing
        super(AutoUserForeignKey, self).contribute_to_class(cls, name)
        field_instance = self
        app_label, model_name = settings.AUTH_USER_MODEL.split('.')
        user_model = get_model(app_label, model_name)

        class UserManager(Manager):
            def __call__(self, user):
                if isinstance(user, user_model):
                    return cls.objects.filter(**{field_instance.name: user})
                else:
                    return None

        #todo find way to delete manager_name
        cls.add_to_class('objects', Manager())  #todo hack
        cls.add_to_class(self.manager_name, UserManager())


add_introspection_rules([
                            (
                                (AutoUserForeignKey,),
                                [],
                                {
                                    "to": ["rel.to", {}],
                                    "to_field": ["rel.field_name", {"default_attr": "rel.to._meta.pk.name"}],
                                    "related_name": ["rel.related_name", {"default": None}],
                                    "db_index": ["db_index", {"default": True}],
                                },
                            )
                        ],
                        ["^custom\.custom_fields\.AutoUserForeignKey"])


class AutoSingleRelatedObjectDescriptor(SingleRelatedObjectDescriptor):
    def __get__(self, instance, instance_type=None):
        try:
            return super(AutoSingleRelatedObjectDescriptor, self).__get__(instance, instance_type)
        except self.related.model.DoesNotExist:
            obj = self.related.model(**{self.related.field.name: instance})
            obj.save()
            return super(AutoSingleRelatedObjectDescriptor, self).__get__(instance, instance_type)


class AutoSaveOneToOneField(OneToOneField):
    '''
    OneToOneField creates related object on first call if it doesnt exist yet.
    Use it instead of original OneToOne field.
    '''

    def contribute_to_related_class(self, cls, related):
        setattr(cls, related.get_accessor_name(), AutoSingleRelatedObjectDescriptor(related))


add_introspection_rules([
                            (
                                (AutoSaveOneToOneField,),
                                [],
                                {
                                    "to": ["rel.to", {}],
                                    "to_field": ["rel.field_name", {"default_attr": "rel.to._meta.pk.name"}],
                                    "related_name": ["rel.related_name", {"default": None}],
                                    "db_index": ["db_index", {"default": True}],
                                },
                            )
                        ],
                        ["^custom\.custom_fields\.AutoOneToOneField"])


class NoMSDateTime(models.DateTimeField):
    '''
    Datetimefield with microsecond=0
    '''
    __metaclass__ = models.SubfieldBase
    description = _("DateTimeField with no microseconds")

    def pre_save(self, model_instance, add):
        if self.auto_now or (self.auto_now_add and add):
            value = timezone.now().replace(microsecond=0)
            setattr(model_instance, self.attname, value)
            return value
        else:
            value = super(models.DateTimeField, self).pre_save(model_instance, add)
            if value:
                value = value.replace(microsecond=0)
            return value


add_introspection_rules([], ["^custom.custom_fields.NoMSDateTime"])


class JSONField(models.TextField):
    """
    JSONField is a generic textfield that serializes/unserializes
    """

    __metaclass__ = models.SubfieldBase

    def to_python(self, value):
        if value == "":
            return None

        try:
            if isinstance(value, basestring):
                return json.loads(value)
        except ValueError:
            pass
        return value

    def get_db_prep_save(self, value, *args, **kwargs):
        if value == "":
            return None
        if isinstance(value, dict) or isinstance(value, list):
            value = json.dumps(value, cls=DjangoJSONEncoder)
        return super(JSONField, self).get_db_prep_save(value, *args, **kwargs)

    def value_from_object(self, obj):
        value = super(JSONField, self).value_from_object(obj)
        if self.null and value is None:
            return None
        return json.dumps(value)


add_introspection_rules([], ["^custom.custom_fields.JSONField"])
