from django.contrib.auth.models import User


class EmailOrUsernameModelBackend(object):
    def authenticate(self, username=None, password=None):
        email = False
        if '@' in username:
            email = True
            kwargs = {'email': username}
        else:
            kwargs = {'username': username}
        user = self.user_or_none(password, kwargs)
        if email and user == None:
            user = self.user_or_none(password, {'username': username})
        return user

    def user_or_none(self, password, lookup_dict):
        try:
            user = User.objects.get(**lookup_dict)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None