from itertools import chain
import warnings
import types
import copy

from django.core.exceptions import ValidationError
from django.forms.forms import BaseForm
from django.forms.models import ModelForm, InlineForeignKeyField, construct_instance


class AutoRelatedModelForm(ModelForm):
    def __init__(self, *args, **kwargs):
        related = list(getattr(self.Meta, 'onetoone_fields', []))
        self.one_to_one_related = related
        if kwargs.get('validate_unique_fields'):
            self.validate_unique_value = kwargs.get('validate_unique_fields')
        else:
            self.validate_unique_value = True
        self.f_keys = list(getattr(self.Meta, 'foreign_keys', []))
        self.reverse = list(getattr(self.Meta, 'reverse', []))
        for field_name in chain(self.one_to_one_related, self.f_keys, self.reverse):
            field = kwargs.pop(field_name, None)
            if field:
                if AutoRelatedModelForm.check_if_form_subclass(field):
                    if 'data' in kwargs:
                        setattr(self, field_name, field if self.check_form_is_bound(field) else field(kwargs['data']))
                    elif args:
                        setattr(self, field_name, field if self.check_form_is_bound(field) else field(args[0]))
                    else:
                        setattr(self, field_name, field)
                else:
                    setattr(self, field_name, field)
            else:
                setattr(self, field_name, None)
        super(AutoRelatedModelForm, self).__init__(*args, **kwargs)

    def clean(self):
        self._validate_unique = self.validate_unique
        return self.cleaned_data

    def check_form_is_bound(self, f):
        try:
            if f.is_bound:
                return True
        except AttributeError:
            return False

    @classmethod
    def check_if_form_subclass(cls, f_key):
        import inspect

        try:
            if BaseForm in inspect.getmro(f_key):
                return True
        except:
            if BaseForm in inspect.getmro(f_key.__class__):
                return True
        return False

    def _post_clean(self):
        opts = self._meta
        # Update the model instance with self.cleaned_data.
        self.instance = construct_instance(self, self.instance, opts.fields, opts.exclude)
        for field_name in chain(self.f_keys, self.one_to_one_related):
            field = getattr(self, field_name, None)
            if field:
                if not AutoRelatedModelForm.check_if_form_subclass(field):
                    setattr(self.instance, field_name, field)
        exclude = self._get_validation_exclusions()

        # Foreign Keys being used to represent inline relationships
        # are excluded from basic field value validation. This is for two
        # reasons: firstly, the value may not be supplied (#12507; the
        # case of providing new values to the admin); secondly the
        # object being referred to may not yet fully exist (#12749).
        # However, these fields *must* be included in uniqueness checks,
        # so this can't be part of _get_validation_exclusions().
        for f_name, field in self.fields.items():
            if isinstance(field, InlineForeignKeyField):
                exclude.append(f_name)

        try:
            self.instance.full_clean(exclude=exclude,
                                     validate_unique=False)
        except ValidationError as e:
            self._update_errors(e)

        # Validate uniqueness if needed.
        if self.validate_unique_value:
            self.validate_unique()

    def is_valid(self):
        valid_bools = []
        for field_name in chain(self.one_to_one_related, self.f_keys):
            field = getattr(self, field_name, None)
            if field:
                if AutoRelatedModelForm.check_if_form_subclass(field):
                    valid_bools.append(field.is_valid())
        valid_bools.append(super(AutoRelatedModelForm, self).is_valid())
        # print valid_bools, 'valid bools', self.Meta.model, 'model name'
        return reduce(lambda i, j: i and j, valid_bools, True)

    def save(self, commit=True):
        if not commit:
            warnings.warn('call save_related_fields to save related', RuntimeWarning)
        self_copy = copy.deepcopy(self)
        self_form = super(AutoRelatedModelForm, self).save(commit=False)

        def save_forward_fields(self):
            for field_name in chain(self_copy.f_keys, self_copy.one_to_one_related):
                field = getattr(self_copy, field_name, None)
                if field:
                    if not AutoRelatedModelForm.check_if_form_subclass(field):
                        setattr(self_form, field_name, field)
                    else:
                        setattr(self_form, field_name, field.save(commit=True))

        def save_related_fields(self):
            for reverse_field in self_copy.reverse:
                field = getattr(self_copy, reverse_field, None)
                if field:
                    if not AutoRelatedModelForm.check_if_form_subclass(field):
                        setattr(field, reverse_field, self_form)
                    else:
                        rev_inst = field.save(commit=False)
                        setattr(rev_inst, reverse_field, self_form)
                        if hasattr(rev_inst, 'save_related_fields'):
                            rev_inst.save()
                        rev_inst.save()

        self_form.save_related_fields = types.MethodType(save_related_fields, self_form)
        self_form.save_forward_fields = types.MethodType(save_forward_fields, self_form)
        if commit:
            self_form.save_forward_fields()
            self_form.save()
            self_form.save_related_fields()
        else:
            return self_form
        return self_form


def automodelform_factory(model, onetoones=(), foreign_keys=(), reverse=(), exclude=(), fields=()):
    _model = model
    _exclude = list(exclude)
    _foreign_keys = foreign_keys
    _reverse = reverse
    _exclude.append('active')
    _fields = fields
    if _fields and _exclude:
        class SampleForm(AutoRelatedModelForm):
            class Meta:
                model = _model
                exclude = _exclude
                onetoone_fields = onetoones
                foreign_keys = _foreign_keys
                reverse = _reverse
                fields = _fields

        SampleForm.__name__ = model.__name__ + 'AutoModelForm'
        return SampleForm
    elif _fields:
        class SampleForm(AutoRelatedModelForm):
            class Meta:
                model = _model
                onetoone_fields = onetoones
                foreign_keys = _foreign_keys
                reverse = _reverse
                fields = _fields

        SampleForm.__name__ = model.__name__ + 'AutoModelForm'
        return SampleForm
    elif _exclude:
        class SampleForm(AutoRelatedModelForm):
            class Meta:
                model = _model
                exclude = _exclude
                onetoone_fields = onetoones
                foreign_keys = _foreign_keys
                reverse = _reverse

        SampleForm.__name__ = model.__name__ + 'AutoModelForm'
        return SampleForm