from django.contrib.sites.models import Site
from django.conf import settings


class SubdomainMiddleware(object):
    def process_request(self, request):
        """adds subdomain name in request
        subdomains.
        """
        scheme = "http" if not request.is_secure() else "https"
        path = request.path
        domain = request.META.get('HTTP_HOST') or request.META.get('SERVER_NAME')
        pieces = domain.split('.')
        subdomain = ".".join(pieces[:-2])  # join all but primary domain
        default_domain = Site.objects.get(id=settings.SITE_ID)
        # if domain in {default_domain.domain, "testserver", "localhost", '127.0.0.1'}: #todo uncomment it later
        #    return None

        request.subdomain = subdomain
        return None
