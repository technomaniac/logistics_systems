from django import test
from django.forms.models import model_to_dict


class TestAutoModelForm(test.TestCase):
    def setUp(self):
        from apps.users.forms import CustomUserForm
        from apps.contacts.models import Contact, Address
        from .forms import automodelform_factory

        self.vendor_form = automodelform_factory(model=Vendor)
        self.user_form = CustomUserForm
        self.branch_form = automodelform_factory(model=Branch, onetoones=['vendor', 'address'],
                                                 exclude=['vendor', 'address'])
        self.vendor_admin = automodelform_factory(model=VendorAdmin, onetoones=['contact', 'vendor'],
                                                  exclude=['contact', 'vendor'])
        self.contact_form = automodelform_factory(model=Contact, onetoones=['user'], exclude=['user'])
        self.address_form = automodelform_factory(model=Address)

    def test_saving_forms(self):
        vendor = {'code': 'test_code', 'name': 'test_name', 'legal_name': 'test_legal'}
        branch = {'code': 'code', 'name': 'name', 'type_of_branch': 'M'}
        contact = {'first_name': 'chaman', 'last_name': 'chutiya', 'email': 'abc@xyz.com',
                   'mobile_number': '1234567890', 'phone_number': '12345'}
        user = {'first_name': 'chaman', 'last_name': 'chutiya', 'email': 'abc@xyz.com', 'password': 'asdfljk'}
        address = {'address': 'gand gali', 'landmark': 'land'}
        vendor_form = self.vendor_form(vendor)
        address_form = self.address_form(address)
        branch_form = self.branch_form(branch, vendor=vendor_form, address=address_form)
        user_form = self.user_form(user)
        contact_form = self.contact_form(contact, user=user_form)
        vendor_admin_form = self.vendor_admin(contact=contact_form)
        print vendor_form.errors
        print address_form.errors
        print vendor_admin_form.errors
        print branch_form.errors
        print address_form.errors
        print user_form.errors
        branch_inst = branch_form.save()
        vendor_admin_form.vendor = branch_inst.vendor
        vendor_admin_form.save()
        contact['phone_number'] = '214900'
        user['first_name'] = 'opopopopp'
        vendor_form = self.vendor_form(vendor, instance=branch_inst.vendor)
        address_form = self.address_form(address, instance=branch_inst.address)
        branch_form = self.branch_form(branch, vendor=vendor_form, address=address_form, instance=branch_inst)
        user_form = self.user_form(user, instance=branch_inst.vendor.vendor_admin.contact.user)
        contact_form = self.contact_form(contact, user=user_form, instance=branch_inst.vendor.vendor_admin.contact)
        vendor_admin_form = self.vendor_admin(contact=contact_form, instance=branch_inst.vendor.vendor_admin)
        branch_inst2 = branch_form.save()
        vendor_admin_form.vendor = branch_inst2.vendor
        vendor_admin_form.save()
        self.printins(branch_inst2.vendor)

    def printins(self, vendor):
        print model_to_dict(vendor.vendor_admin.contact), 'contact'
        print model_to_dict(vendor.branches.all()[0]), 'branch'
        print model_to_dict(vendor), 'vendor'

    def test_saving_related_later(self):
        vendor = {'code': 'test_code', 'name': 'test_name', 'legal_name': 'test_legal'}
        branch = {'code': 'code', 'name': 'name', 'type_of_branch': 'M'}
        contact = {'first_name': 'chaman', 'last_name': 'chutiya', 'email': 'abc@xyz.com',
                   'mobile_number': '1234567890', 'phone_number': '12345'}
        user = {'first_name': 'chaman', 'last_name': 'chutiya', 'email': 'abc@xyz.com', 'password': 'asdfljk'}
        address = {'address': 'gand gali', 'landmark': 'land'}
        vendor_form = self.vendor_form(vendor)
        address_form = self.address_form(address)
        branch_form = self.branch_form(branch, vendor=vendor_form, address=address_form)
        user_form = self.user_form(user)
        contact_form = self.contact_form(contact, user=user_form)
        vendor_admin_form = self.vendor_admin(contact=contact_form)
        print vendor_form.errors
        print address_form.errors
        print vendor_admin_form.errors
        print branch_form.errors
        print address_form.errors
        print user_form.errors
        # branch_contact.save(commit=False)
        # branch_contact.save_related_fields()