import re

PINCODE_SINGLE_REGEX = re.compile(r'^[1-9]\d{5}$')
PINCODE_MULTIPLE_REGEX = re.compile(
    r'\b[1-9]\d{5}\b')  # if length of the resultant list is 1 then the pincode is a part of the address
PINCODE_PARTIAL_REGEX = re.compile(r'^[1-9]\d{2}\d{0,2}$')  # user will have to input atleast 3 digits

DATE_REGEXES = [
    (re.compile(r'^\d{2}-\d{2}-\d{4}$'), '%d-%m-%Y'),
    (re.compile(r'^\d{2}/\d{2}/\d{4}$'), '%d/%m/%Y')
]