class MixinMetaClass(type):
    def __new__(cls, name, bases, dct):
        ret = type.__new__(cls, name, bases, dct)

        if name != 'AddToModelModelMixin':
            assert 'model' in dct
            model = dct.pop('model')

            for k, v in dct.iteritems():
                if k not in MixinMetaClass.__dict__:
                    model.add_to_class(k, v)

        return ret


class AddToModelModelMixin(object):
    __metaclass__ = MixinMetaClass