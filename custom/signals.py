from django.dispatch import Signal


model_saved_by_user = Signal(providing_args=["model", "model_object", "request"])