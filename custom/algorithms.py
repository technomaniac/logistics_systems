from .datastuctures import Queue


def get_all_tree_nodes(root, get_next_leaves, next_args=(), next_kwargs={}):
    '''
    works only for querysets, get_next_leaves is a string name of function which is a method of root
    '''
    q = Queue(initial_data=getattr(root, get_next_leaves)(*next_args, **next_kwargs))
    final_list = getattr(root, get_next_leaves)(*next_args, **next_kwargs)
    while not q.isempty():
        try:
            i = q.pop()
        except StopIteration:
            break
        next_nodes = getattr(i, get_next_leaves)(*next_args, **next_kwargs)
        if final_list & next_nodes:
            break

        final_list = final_list | (next_nodes)
        q.bulk_push(next_nodes)

    return final_list


