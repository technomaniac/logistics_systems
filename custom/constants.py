NO_HISTORY_MODELS = ('CustomUser', 'ModelsCreatedRecord', 'CommonInfo', 'GlobalDictionary', 'KeyValuePair')
MODEL_VIEWSETS_TO_EXCLUDE = []  # TODO  ADD model names for to exclude viewsets

TRY_FIELDS = {
    'awb': [
        'awb', 'order_id', 'customer__contact_details__mobile_number', 'customer__contact_details__phone_number',
        'customer__address__pincode__pincode', 'customer__address__address'
    ],
    'clientwarehouse': ['name', 'code', 'address__address', 'address__pincode__pincode'],
    'awbshipment': ['awb__awb', 'awb__customer__contact_details__mobile_number',
                    'awb__customer__contact_details__phone_number',
                    'pickup_address__address', 'delivery_address__address', 'field_executive__contact__first_name',
                    'field_executive__contact__last_name'],
    'vendor': ['code', 'name', 'legal_name'],
    'vehicle': ['name', 'vehicle_number', 'branch'],
}

CUSTOM_MESSAGES = {
    'AWBShipmentHistory': {
        'awb': {
            1: '',
            2: '',
            3: ''
        },
        'flag': {
            'A': {
                1: '',
                2: '',
                3: ''
            },
            'N': {
                1: '',
                2: '',
                3: ''
            }
        },
        'shortlisted_vendors': {
            1: 'Vendors shortlisted for this shipment',
            2: 'Vendors shortlisted for this shipment',
            3: 'Vendors shortlisted for this shipment'
        },
        'status': {
            'CNA': {
                1: 'Customer Not Available',
                2: 'Customer Not Available',
                3: 'Customer Not Available'
            },
            'DBC': {
                1: 'Deferred by Customer',
                2: 'Deferred by Customer',
                3: 'Deferred by Customer'
            },
            'CAN': {
                1: 'Cancelled',
                2: 'Cancelled',
                3: 'Cancelled'
            },
            'PCL': {
                1: 'Pickup Completed',
                2: 'Pickup Completed',
                3: 'Pickup Completed'
            },
            'RTO': {
                1: 'Returned to Origin',
                2: 'Returned to Origin',
                3: 'Returned to Origin'
            },
            'DTO': {
                1: 'Delivered to Origin',
                2: 'Delivered to Origin',
                3: 'Delivered to Origin'
            },
            'DEL': {
                1: 'Delivered',
                2: 'Delivered',
                3: 'Delivered',
            },
            'NIN': {
                1: 'Not Initiated',
                2: 'Not Initiated',
                3: 'Not Initiated'
            },
            'PIN': {
                1: 'Pickup Initiated',
                2: 'Pickup Initiated',
                3: 'Pickup Initiated'
            },
            'DIN': {
                1: 'Delivery Initiated',
                2: 'Delivery Initiated',
                3: 'Delivery Initiated'
            },
        },
        'defer_date': {
            1: 'Shipment deferred to date %s on request of the customer',
            2: 'Shipment deferred to date %s on request of the customer',
            3: 'Shipment deferred to date %s on request of the customer'
        },
        'vendor_branch': {
            1: 'Accepted by vendor %s',
            2: 'Accepted by vendor %s',
            3: 'Accepted by vendor %s'
        },
        'field_executive': {
            1: 'Field executive %s assigned to this shipment',
            2: 'Field executive %s assigned to this shipment',
            3: 'Field executive %s assigned to this shipment'
        }
    }
}