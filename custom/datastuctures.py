from itertools import islice, chain
from collections import OrderedDict


class XlrdDict(OrderedDict):
    col_names = []
    _dict = None

    def __init__(self, *args, **kwargs):
        if 'col_names' in kwargs:
            col_names = kwargs.pop('col_names')
            self.col_names = [i.value.lower() if isinstance(i.value, (str, unicode)) else i.value for i in col_names]
        super(XlrdDict, self).__init__(*args, **kwargs)

    def __call__(self, row):
        if not self.col_names:
            raise RuntimeError('no col names exists')
        self.clear()
        self.__init__(zip(self.col_names, [i.value for i in row]))

    def get_dict(self):
        return dict(self)


class Queue(object):
    def __init__(self, initial_data=()):
        self._values = list(initial_data)
        self._length = len(self._values)

    def pop(self):
        if not self.isempty():
            value = self._values.pop(self._length - 1)
            self._length -= 1
            return value
        else:
            raise StopIteration('Queue empty')

    def push(self, value):
        self._values.insert(0, value)
        self._length += 1

    def isempty(self):
        if self._length == 0:
            return True
        else:
            return False

    def bulk_push(self, iterator):
        for i in iterator:
            self.push(i)


class ChainedQueryset(object):
    '''
    '''

    def __init__(self, *subquerysets):
        self.querysets = subquerysets

    def count(self):
        """
        total number of
        """
        return sum(qs.count() for qs in self.querysets)

    def _clone(self):
        "Returns a clone of this queryset chain"
        return self.__class__(*self.querysets)

    def __all(self):
        "Iterates records in all subquerysets"
        return chain(*self.querysets)

    def __getitem__(self, indx):
        """
        Retrieves an item or slice from the chained set of results from all
        subquerysets.
        """
        if type(indx) is slice:
            return list(islice(self.__all(), indx.start, indx.stop, indx.step or 1))
        else:
            return islice(self.__all(), indx, indx + 1).next()