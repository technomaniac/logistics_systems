import itertools
import json
from datetime import datetime, timedelta

from django.contrib.auth import login, authenticate, logout
from django.http.response import HttpResponseRedirect
from django.template.context import Context, RequestContext
from django.template.response import SimpleTemplateResponse
from django.views.generic.base import TemplateResponseMixin
import django_tables2 as tables
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage

from utils.time_funcs import string_time_to_datetime
from utils.common_functions import get_json_response
from logistics.settings import LOGIN_URL


class DateFilterManagerMixin(object):
    def filter_by_date_created(self, date_start, date_end, queryset=None, logic_operator='AND'):
        queryset = self.none() if queryset is None else queryset
        new_queryset = None
        if date_start:
            new_queryset = self.filter(date_created__gte=date_start)
        if date_end:
            new_queryset = new_queryset.filter(date_created__lte=date_end)

        if logic_operator == 'OR':
            return new_queryset | queryset
        else:
            return new_queryset & queryset

    def filter_by_date_edited(self, date_start, date_end, queryset=None, logic_operator='AND'):
        queryset = self.none() if queryset is None else queryset
        new_queryset = None
        if date_start:
            new_queryset = self.filter(date_edited__gte=date_start)
        if date_end:
            new_queryset = new_queryset.filter(date_edited__lte=date_end)
        print new_queryset, 'new_queryset'
        if logic_operator == 'OR':
            return new_queryset | queryset
        else:
            return new_queryset & queryset


class MultipleFormMixin(object):
    '''
    declare form class ending with _form eg. contact_form
    '''

    def _get_form_names(self):
        for attr in dir(self):
            if attr.endswith('form'):
                yield attr
            else:
                continue

    def initial_values(self, form_name):
        try:
            try:
                garbage = self.Initial_values
            except Exception:
                print 'first'
                self.add_initial_values()
            return self.Initial_values[form_name]
        except Exception:
            return {}

    def add_initial_values_to_existing_instances(self, forms_dict):
        final_dict = {}
        new_forms_dict = dict(forms_dict)
        for key, value in new_forms_dict.items():
            value.initial = self.initial_values(key)
            final_dict[key] = value

        return final_dict

    def add_form_to_context(self, exclude=None):
        for attr in self._get_form_names():
            if exclude is not None and attr in exclude:
                continue
            initials = self.initial_values(attr)
            self._check_or_add_context()
            self.context[attr] = getattr(self, attr)(initials)
        return

    def _check_or_add_context(self):
        try:
            garbage = self.context
        except Exception:
            self.context = {}

    def bound_forms(self, request, classname=None, all=False):
        form_dict = {}
        if classname is None and all is False:
            raise Exception('invalid arguments')

        if all:
            valid = True
            o = 0
            for form in self._get_form_names():
                _form = getattr(self, form)(request.POST)
                if _form.is_valid():
                    form_dict[form] = _form
                    o += 1
                else:
                    print form
                    valid = False
            return form_dict, valid

        form = classname(request.POST)
        return form

    def add_initial_values(self):
        self.Initial_values = {}


class LoginMixin(object):
    def authenticate(self, username, password):
        user = authenticate(username=username, password=password)
        if user:
            return user, None
        else:
            return None, {'msg': 'Invalid credentials'}

    def logger(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user, message = self.authenticate(username=username, password=password)
        if user is None:
            return False, message
        else:
            login(request, user)
            return True, {}


class GeneralLoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.is_ajax():
            print 'si ajax'
        if not request.user.is_authenticated():
            logout(request)
            if request.is_ajax():
                # print 'is ajax'
                return get_json_response({'status': 403, 'error_msg': 'login required'})
            else:
                # print 'not ajax'
                return HttpResponseRedirect(LOGIN_URL + '?next=%s' % request.path)  # todo TEST

        return super(GeneralLoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class SearchFromGetAttributesMixin(object):
    '''
    Deprecated
    '''
    model = None  # this is model name, it cannot be none

    def _get_param_values(self, request, param):
        params = []
        if hasattr(param, '__iter__'):
            for param_key in param:
                param_value = request.GET.get(param_key, '')
                if param_value:
                    if hasattr(self, 'format_' + param_key):
                        param_value = getattr(self, 'format_' + param_key)(param_value)
                    elif param in self.GET_PARAM_TYPE.keys():
                        param_value = self.format_by_type(param_key, param_value)
                    params.append(param_value)
                else:
                    params.append(None)
            return params, True
        else:
            param_value = request.GET.get(param, '')
            if param_value:
                if hasattr(self, 'format_' + param):
                    param_value = getattr(self, 'format_' + param)(param_value)
                elif param in self.GET_PARAM_TYPE.keys():
                    param_value = self.format_by_type(param, param_value)
                return param_value, False
            else:
                return param_value, False

    def _params_are_valid(self, param_values):
        if param_values == '' or param_values == None:
            return False
        else:
            if hasattr(param_values, '__iter__'):
                all_none = True
                for value in param_values:
                    if value != None:
                        all_none = False
                if all_none:
                    return False
        return True

    def get_valid_queryset(self, request):
        queryset = self.model_manager.none()
        first_param = True
        if request.GET.get('all', '') == 'true':
            return self.model_manager.all()

        for param, func in self.GET_PARAMS:
            param_value, multi_args = self._get_param_values(request, param)
            if self._params_are_valid(param_value):
                print param_value, param
                if first_param:
                    if multi_args:
                        queryset = getattr(self.model_manager, func)(*param_value,
                                                                     queryset=queryset,
                                                                     logic_operator='OR')
                    else:
                        queryset = getattr(self.model_manager, func)(param_value,
                                                                     queryset=queryset,
                                                                     logic_operator='OR')
                    first_param = False
                else:
                    if multi_args:
                        queryset = getattr(self.model_manager, func)(*param_value,
                                                                     queryset=queryset,
                                                                     logic_operator='AND')
                    else:
                        queryset = getattr(self.model_manager, func)(param_value,
                                                                     queryset=queryset,
                                                                     logic_operator='AND')
            else:
                continue

        return queryset

    def format_by_type(self, param, param_value):
        type_name = self.GET_PARAM_TYPE[param]
        return getattr(self, 'format_' + type_name)(param_value)

    def format_list(self, param):
        params = param.split(',')
        if '' in params:
            params.remove('')
        return params

    def format_int(self, param):
        return int(param.strip())

    def format_float(self, param):
        return float(param.strip())

    def format_date(self, param):
        string_time_to_datetime(param, date=True)

    def format_datetime(self, param):
        string_time_to_datetime(param, date=False)

    def format_bool(self, param):
        if param == 'false':
            return False
        else:
            return True


class PaginationMixin(object):
    def get_paginated_objects(self, request, queryset, per_page=12):
        total_objs = queryset.count()
        total_pages = 0
        total_pages = total_objs / per_page
        # print 'totl objs', total_objs
        # print total_pages
        if total_objs % per_page != 0:
            total_pages += 1
            # print 'new', total_pages
        if request.GET.get('page_no'):
            page = 1
            start = 0
            end = per_page
            try:
                page = int(request.GET.get('page_no'))
                page = 1 if page <= 0 else page
                if page > total_pages:
                    page = total_pages

                if page >= 1:
                    start = per_page * (page - 1)
                end = per_page + (start)
            except Exception:
                start = 0
                end = per_page
            try:
                return queryset[start:end], total_pages
            except Exception:
                return queryset, total_pages
        else:
            return queryset[0:per_page], total_pages


class TemplateResponse(SimpleTemplateResponse):
    rendering_attrs = SimpleTemplateResponse.rendering_attrs + \
                      ['_request', '_current_app']

    def __init__(self, request, template, context=None, content_type=None,
                 status=None, mimetype=None, current_app=None):
        # self.request gets over-written by django.test.client.Client - and
        # unlike context_data and template_name the _request should not
        # be considered part of the public API.
        self._request = request
        # As a convenience we'll allow callers to provide current_app without
        # having to avoid needing to create the RequestContext directly
        self._current_app = current_app
        super(TemplateResponse, self).__init__(
            template, context, content_type, status, mimetype)

    def resolve_context(self, context):
        """Convert context data into a full RequestContext object
        (assuming it isn't already a Context object).
        """
        if isinstance(context, Context):
            return context
        return RequestContext(self._request, context, current_app=self._current_app)


class CommonViewMixin(TemplateResponseMixin):
    authorization = ''
    permissions = []

    def get(self, request, *args, **kwargs):
        rt_val = self.initialize_view(request, *args, **kwargs)
        self.kwargs_from_url = kwargs
        if rt_val:
            return rt_val
        if request.is_ajax():
            if request.GET.get('template') == 'true':
                return self.render_page(request, *args, **kwargs)
            else:
                if not self.is_valid(request) or not self._authorized(request, *args, **kwargs):
                    logout(request)
                    return get_json_response({'status': 403, 'error_msg': 'Not logged in'})
                return self.non_template_get_requests(request, *args, **kwargs)
        else:
            return self.render_page(request, *args, **kwargs)

    def _authorized(self, request, *args, **kwargs):
        print self.authorization, 'asljfdlkdjsf'
        for auth_callables in self.authorization:
            if callable(auth_callables):
                if not auth_callables(request, *args, **kwargs):
                    return False
        return True

    def post(self, request, *args, **kwargs):
        rt_val = self.initialize_view(request, *args, **kwargs)
        not_valid = False
        if rt_val:
            return rt_val
        if not self.is_valid(request) or not self._authorized(request, *args, **kwargs):
            logout(request)
            not_valid = True
        if request.is_ajax():
            if not_valid:
                logout(request)
                return get_json_response({'status': 403, 'error_msg': 'Invalid or unauthorized request'})
            return self.ajax_post(request, *args, **kwargs)
        else:
            if not_valid:
                return HttpResponseRedirect('/logout/')
            return self.non_ajax_post(request, *args, **kwargs)

    def ajax_post(self, request, *args, **kwargs):
        raise NotImplementedError

    def non_ajax_post(self, request, *args, **kwargs):
        raise NotImplementedError

    def render_page(self, request, *args, **kwargs):
        if not self.is_valid(request) or not self._authorized(request, *args, **kwargs):
            return HttpResponseRedirect('/logout/')
        context = self.get_context(request, *args, **kwargs)
        context.update(self.get_persistent_context(request, *args, **kwargs))
        self.before_response(request)
        return self.render_to_response(context=RequestContext(request, context))

    def valid_or_get_form_context(self, request, *args, **kwargs):
        pass

    def get_context(self, request, *args, **kwargs):
        return {}

    def get_persistent_context(self, request, *args, **kwargs):
        return {}

    def non_template_get_requests(self, request, *args, **kwargs):
        pass

    def is_valid(self, request):
        return True

    def before_response(self, request, *args, **kwargs):
        pass

    def initialize_view(self, request, *args, **kwargs):
        return None


class HandleMultipleTemplates(CommonViewMixin):
    pass


class TableMixin(tables.Table):
    # s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False)

    def __init__(self, *args, **kwargs):
        super(TableMixin, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_s_no(self):
        return next(self.counter) + 1


class SetDateRange(object):
    def set_date_range(self, **kwargs):
        self.start_date = (datetime.today() - timedelta(days=int(kwargs['duration']))).strftime('%Y-%m-%d')
        self.end_date = (datetime.today()).strftime('%Y-%m-%d')

    @property
    def start_date(self):
        return self._start_date + ' 00:00:00'

    @start_date.setter
    def start_date(self, value):
        self._start_date = value

    @property
    def end_date(self):
        return self._end_date + ' 23:59:59'

    @end_date.setter
    def end_date(self, value):
        self._end_date = value


class WebSocket(object):
    def prepare_socket(self, **kwargs):
        self.redis_publisher = RedisPublisher(**kwargs)


    def send_message(self, **kwargs):
        self.redis_publisher.publish_message(RedisMessage(json.dumps(kwargs)))