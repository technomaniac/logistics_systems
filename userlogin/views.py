import json
import re

from django.contrib.auth.models import User
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, logout, login
from django.shortcuts import render
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django_tables2 import RequestConfig
from django.core.serializers.json import DjangoJSONEncoder

from custom.custom_mixins import GeneralLoginRequiredMixin, CommonViewMixin
from customer_service.models import PhoneNumbers
from internal.models import Employee, Branch, UserEmails
from .forms import LoginForm, UserForm, UserExtendedForm
from report.performance import PerformanceReportMixin
from .tables import UserTable
from common.forms import ExcelUploadForm
from utils.upload import upload_user_list_file, handle_uploaded_file
from logistics.settings import MEDIA_ROOT
from client.views import client_dashboard


class AuthenticateUser:
    def __init__(self, request):
        self.request = request
        self.type = ''
        self.type_of_user = ''

    def authenticate(self, username, password):
        if '@' in username:
            self.type_of_user = 'username is email'  # todo change
            try:
                user = User.objects.get(email=username)
                if user:
                    if user.check_password(password):
                        user = authenticate(username=username, password=password)
                        return user, None
                    else:
                        return None, {'password_msg': 'Wrong password'}
                else:
                    return None, {'username_msg': 'Wrong Username'}
            except Exception:
                return None, {'username_msg': 'Wrong Username'}
        else:
            self.type_of_user = 'no email required'  # todo change
            try:
                user = User.objects.get(username=username)
                if user:
                    if user.check_password(password):
                        user = authenticate(username=username, password=password)
                        return user, None
                    else:
                        return None, {'password_msg': 'Wrong Password'}
                else:
                    return None, {'username_msg': 'Wrong Username'}
            except Exception:
                return None, {'username_msg': 'Wrong Username'}

    def logger(self):
        username = self.request.POST['username']
        password = self.request.POST['password']

        user, message = self.authenticate(username=username, password=password)
        if user is None:
            return HttpResponse(
                loader.get_template('userlogin/login_form.html').render(RequestContext(self.request, message))), message
        else:
            # try:
            # res = user.resident
            # except Exception:
            # return HttpResponse(loader.get_template('Login.html').render(RequestContext(self.request,{'msg':'ResidentOnly'})))
            login(request=self.request, user=user)
            return HttpResponseRedirect('/'), ''


@csrf_exempt
def login_handler(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            # print 'yes'
            if user.is_active:
                login(request, user)
                request.session['user'] = request.user.pk
                try:
                    if request.user.profile.branch.pk != 1:
                        request.session['branch'] = request.user.profile.branch.pk
                except Exception:
                    pass

                try:
                    if request.user.profile.role == 'CS':
                        try:
                            phone = PhoneNumbers.objects.get(assigned_to=request.user.profile)
                            request.session['agent_no'] = str(phone.phone_no)
                        except:
                            pass
                        return HttpResponseRedirect('/transit/awb/report_cc')
                except Exception:
                    pass

                try:
                    request.session['client'] = request.user.profile.client.client_code
                    # return HttpResponseRedirect('/transit/awb/mis')
                    return HttpResponseRedirect('/')
                except Exception:
                    pass
                return HttpResponseRedirect('/transit/awb/in_scanning')
            else:
                form = LoginForm()
                return render(request, 'userlogin/login_form.html', {'form': form})
        else:
            form = LoginForm()
            return render(request, 'userlogin/login_form.html', {'form': form})
    elif request.method == 'GET':
        form = LoginForm()
        return render(request, 'userlogin/login_form.html', {'form': form})


# no need to define seperate logout handler for the si login
def logout_handler(request):
    logout(request)
    return HttpResponseRedirect('/')


class HomePage(GeneralLoginRequiredMixin, CommonViewMixin, View):
    template_name = 'front_dashboard.html'

    def initialize_view(self, request, *args, **kwargs):
        if 'client' in request.session:
            return client_dashboard(request)
        if 'dashboard_date_range' not in request.session:
            request.session['dashboard_date_range'] = 1
        duration = int(request.session['dashboard_date_range'])
        kwargs = dict()
        kwargs['duration'] = duration
        kwargs['category'] = 'REV'
        if 'branch' in request.session:
            kwargs['pickup_branch'] = request.session['branch']
        self.dashboard = PerformanceReportMixin(**kwargs)
        # self.dashboard.get_dtod_awb_perc()

    def get_context(self, request, *args, **kwargs):
        args = ['get_total_awb', 'get_pickup_awb', 'get_pickup_awb_perc', 'get_cancelled_awb',
                'get_cancelled_awb_perc', 'get_connected_awb', 'get_connected_awb_perc',
                'get_dtod_awb', 'get_out_pickup_awb',
                'get_out_pickup_awb_perc']
        dashboard = self.dashboard.get_custom_report(*args)
        # print dashboard
        return {'dashboard': dashboard}

        # def get(self, request, *args, **kwargs):
        # awbs = request.GET['awbs']


@login_required(login_url='/login')
def home(request):
    if 'client' in request.session:
        return client_dashboard(request)
    else:
        if not request.GET.keys():
            return render(request, 'home.html')
        elif request.GET.get('type') == 'charts':
            json_dict = {}
            if request.GET.get('graph_type') == 'branch_incoming':
                json_dict['series_name'] = ['Reverse pickup', 'Cash on Delivery', 'Prepaid']
                branch = request.session['branch']
                reverse = Branch.objects.get(pk=branch).get_awbs(['DR', 'ISC', 'TB', 'TBD', 'MTS', 'MTD'],
                                                                 'REV').count()
                cod = Branch.objects.get(pk=branch).get_awbs(['DR', 'ISC', 'TB', 'TBD', 'MTS', 'MTD'], 'COD').count()
                pre_paid = Branch.objects.get(pk=branch).get_awbs(['DR', 'ISC', 'TB', 'TBD', 'MTS', 'MTD'],
                                                                  'PRE').count()
                json_dict['data_list'] = [reverse, cod, pre_paid]
                json_dict['title'] = 'Expected Incoming AWBs'

            # elif request.GET.get('graph_type') == 'branch_drs':
            # reverse = AWB.objects.filter(type='REV', awb_status__status='DR').count()
            # cod = AWB.objects.filter(type='COD', awb_status__status='DR').count()
            # pre_paid = AWB.objects.filter(type='PRE', awb_status__status='DR').count()
            # json_dict['series_name'] = ['Reverse pickup', 'Cash on Delivery', 'Prepaid']
            # json_dict['data_list'] = [reverse, cod, pre_paid]
            # json_dict['title'] = 'Expected Incoming AWBs'

            if json_dict:
                json_dict['invalid_request'] = False
            else:
                json_dict['invalid_request'] = True

            json_data = json.dumps(json_dict, cls=DjangoJSONEncoder)
            return HttpResponse(json_data, mimetype='application/json')


def users(request):
    if 'branch' in request.session:
        table = UserTable(
            User.objects.filter(profile__branch_id=request.session['branch'], is_staff=True).order_by('-date_joined'))
    else:
        table = UserTable(User.objects.filter(is_staff=True).order_by('-date_joined'))
    RequestConfig(request, paginate={"per_page": 1000}).configure(table)
    return render(request, 'common/table.html', {'table': table, 'model': 'user', 'url': '/users/add'})


def add_user(request):
    if request.method == "POST":
        user_form = UserForm(request.POST, instance=User())
        extended_form = UserExtendedForm(request.POST, instance=Employee())

        if user_form.is_valid() and extended_form.is_valid():
            user = user_form.save()
            u = User.objects.get(username__exact=request.POST['username'])
            u.set_password(request.POST['password'])
            u.is_staff = True
            u.save()
            emails = list(set(re.split('[\s,;]+', request.POST['emails'])))
            for email in emails:
                if email != '' and re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email):
                    UserEmails.objects.create(user=u, email=email)
            if request.POST['role'] == 'BM':
                branch = Branch.objects.get(pk=request.POST['branch'])
                branch.branch_manager = u
                branch.save()
            instance = extended_form.save(commit=False)
            instance.user_id = user.pk
            instance.save()
            return HttpResponseRedirect('/users')
    else:
        user_form = UserForm()
        extended_form = UserExtendedForm()
    return render(request, 'userlogin/add_user.html', {'user_form': user_form, 'extended_form': extended_form})


def upload_user_list(request):
    if request.method == "POST":
        form = ExcelUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = handle_uploaded_file(request.FILES['file'], request.FILES['file'].name,
                                        MEDIA_ROOT + 'uploads/internal/')
            upload_user_list_file(file)
            return HttpResponseRedirect('/users')
    else:
        form = ExcelUploadForm()
    return render(request, 'common/user_upload_form.html', {'form': form})


def set_branch(request):
    if request.method == 'GET' and request.is_ajax():
        # print request.GET
        if int(request.GET['branch']) == 1:
            del request.session['branch']
        else:
            request.session['branch'] = int(request.GET['branch'])
        return HttpResponse(int(request.GET['branch']))


def set_city(request):
    if request.method == 'GET' and request.is_ajax():
        if request.GET['city'] == '':
            del request.session['city']
        else:
            request.session['city'] = int(request.GET['city'])
        return HttpResponse(int(request.GET['city']))


def get_message(request):
    if 'message' in request.session:
        print request.session
        context = {
            'class': request.session['message']['class'],
            'report': request.session['message']['report']
        }
        return render(request, 'message.html', context)


def set_dashboard_date_range(request):
    if request.method == 'GET' and request.is_ajax():
        request.session['dashboard_date_range'] = int(request.GET['dashboard_date_range'])
        return HttpResponse(int(request.GET['dashboard_date_range']))


def get_dashboard_date_range(request):
    if request.method == 'GET' and request.is_ajax():
        if 'dashboard_date_range' in request.session:
            return HttpResponse(request.session['dashboard_date_range'])
        else:
            return HttpResponse(1)


