from django.conf.urls import patterns, url

from userlogin.views import HomePage


urlpatterns = patterns('userlogin.views',
                       url(r'^$', HomePage.as_view(), name="home"),
                       # url(r'^$', 'home', name="home"),
                       # url(r'^msg$', 'socket', name='socket'),
                       url(r'^login$', 'login_handler', name='login_handler'),
                       url(r'^logout$', 'logout_handler', name='logout_handler'),
                       url(r'^users$', 'users', name='show_users'),
                       url(r'^users/add$', 'add_user', name='add_user'),
                       url(r'^users/upload_list$', 'upload_user_list', name='upload_user_list'),
                       url(r'^user/set_branch$', 'set_branch', name='set_branch'),
                       url(r'^user/get_message$', 'get_message', name='get_message'),
                       url(r'^user/set_dashboard_date_range$', 'set_dashboard_date_range',
                           name='set_dashboard_date_range'),
                       url(r'^user/get_dashboard_date_range$', 'get_dashboard_date_range',
                           name='get_dashboard_date_range'),
)

urlpatterns += patterns('django.contrib.auth.views',
                        url(r'^/admin/auth/user/(?P<user_id>\d+)/user_edit/password/$', 'password_change')
)