import django_tables2 as tables
from django.contrib.auth.models import User
from common.tables import TableMixin


class UserTable(TableMixin):
    s_no = tables.Column(empty_values=(), verbose_name='S.No.', sortable=False, )
    name = tables.Column(accessor='get_full_name')
    branch = tables.Column(accessor='profile.branch')
    role = tables.Column(accessor='profile.role')
    # city = tables.Column(accessor='profile.city')
    # address = tables.Column(accessor='profile.city')
    phone = tables.Column(accessor='profile.phone')
    date_joined = tables.DateColumn(accessor='date_joined')

    class Meta:
        model = User
        fields = ('s_no', 'username', 'name', 'email')
        attrs = {"class": "table table-striped table-bordered table-hover"}

