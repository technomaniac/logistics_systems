/**
 * Created by technomaniac on 27/8/14.
 */


var ws = new WebSocket('{{ WEBSOCKET_URI }}mis?subscribe-broadcast');

ws.onopen = function (evt) {
    console.log("websocket connected");
    $('#get_message').hide();
};


ws.onmessage = function (e) {
    //alert(e.data);
    if (e.data == '--heartbeat--' || typeof e.data === 'undefined') {
        console.log("Received: " + e.data);
    } else {
        var json = JSON.parse(e.data);
        //console.log("Received: " + json.message);
        pop_message(json.message, json.type);
    }
//    } else {
//        $('#get_message').hide();
//    }
};
ws.onerror = function (e) {
    console.error(e);
    $('#get_message').hide();
};
ws.onclose = function (e) {
    console.log("connection closed");
    $('#get_message').hide();
};
function send_message(msg) {
    ws.send(msg);
}