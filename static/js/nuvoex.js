/**
 * Created by technomaniac on 27/11/13.
 */
$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }

        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}
function addRemover(element) {
    $(element).html('<div class="hidden-phone visible-desktop btn-group"><button class="btn btn-mini btn-danger" onclick="removeRow(this)"><i class="icon-trash bigger-120"></i></button></div>');
}

function removeRow(element) {
    var awb = $(element).closest('tr').find('#awb').html();
    var inscanned = $('#awb_scanned_count').val();
    if (inscanned.match(awb)) {
        $('#awb_scanned_count').attr('value', inscanned.replace(awb, ''));
    }
    $(element).closest('tr').remove();
}

$('.txtDecimal').live('keyup', function () {
    if ($(this).val().match(/[^0-9.]/g)) {
        $(this).attr('value', $(this).val().replace(/[^0-9.]/g, ''));
    }
});


function ajaxloader(element) {
    $(element).html('<img src="/static/img/loader.gif" id="ajaxLoader">');
    var height = element.height();
    var width = $('body').width();
    var pos = element.position();

    $('#ajaxLoader').css({
        top: height / 2 + pos.top,
        left: width / 2,
        position: 'absolute'
    });
}

function closeLoader() {
    $('#ajaxLoader').hide();
}

$('.collapse_table').live('click', function () {
    if ($(this).nextAll('div:first').is(":visible")) {
        $(this).nextAll('div:first').slideUp();
        $(this).find('i').attr('class', 'icon-double-angle-down');
    } else {
        $(this).nextAll('div:first').slideDown();
        $(this).find('i').attr('class', 'icon-double-angle-right');
    }
});

function set_dashboard_date_filter(element) {
    var range = $(element).val();
    $.ajax({
        type: 'GET',
        url: '/user/set_dashboard_date_range',
        data: {
            dashboard_date_range: range
        },
        success: function (response) {
            $(element).val(response);
            window.location.reload();
        }
    });
}


$(document).ready(function () {

    $('#start_date input').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    $('#end_date input').datepicker({
        dateFormat: 'yy-mm-dd'
    });
//    $('#id_scheduling_time').timepicker(  );
    $('#id_scheduling_time').val('');

    if ($('#select_branch').length) {
        $.ajax({
            type: 'POST',
            url: '/internal/branch/get_all',
            //cache: true,
            success: function (response) {
                $('#select_branch').html(response);
            }
        });
    }
    if ($('#frontDashboardDateFilter').length) {
        $.ajax({
            type: 'GET',
            url: '/user/get_dashboard_date_range',
            success: function (response) {
                $('#frontDashboardDateFilter').val(response);
            }
        });
    }
    if ($('#select_city').length) {
        $.ajax({
            type: 'GET',
            url: '/zoning/city/get_all',
            //cache: true,
            success: function (response) {
                $('#select_city').html(response);
            }
        });
    }
    if ($("#create_mts_form #table").length) {
        $.ajax({
            type: 'POST',
            url: '/transit/mts/get_tbs',
            data: {from_branch: $(this).val()},
//            beforeSend: function () {
//                ajaxloader($('#create_mts_form #table'));
//            },
            success: function (response) {
                $("#create_mts_form #table").html(response);
                //closeLoader();
            }
        });
    }
    if ($("#drs_awb_table").length) {
        get_drs_awbs('-priority');
    }
    if ($("#dto_awb_table").length) {
        $.ajax({
            type: 'POST',
            url: '/transit/dto/get_awbs',
            data: {
                client: $('#create_dto_form #id_client').val()
            },
            beforeSend: function () {
                ajaxloader($('#dto_awb_table'));
            },
            success: function (response) {
                $("#dto_awb_table").html(response);
                closeLoader();
            }
        });

    }
    $("#create_dto_form #id_client").on('change', function () {
        $.ajax({
            type: 'POST',
            url: '/transit/dto/get_awbs',
            data: {
                client: $(this).val()
            },
            beforeSend: function () {
                ajaxloader($('#dto_awb_table'));
            },
            success: function (response) {
                $("#dto_awb_table").html(response);
                closeLoader();
            }
        });
    });

    if ($("#rto_awb_table").length) {
        $.ajax({
            type: 'POST',
            url: '/transit/rto/get_awbs',
            data: {
                client: $('#create_rto_form #id_client').val()
            },
            success: function (response) {
                $("#rto_awb_table").html(response);
                //closeLoader();
            }
        });
    }
    $("#create_rto_form #id_client").on('change', function () {
        $.ajax({
            type: 'POST',
            url: '/transit/rto/get_awbs',
            data: {
                client: $(this).val()
            },
            success: function (response) {
                $("#rto_awb_table").html(response);
                //closeLoader();
            }
        });
    });

    if ($("#tb_awb_table").length) {
        $.ajax({
            type: 'POST',
            url: 'get_awbs',
            ////cache: true,
            data: {delivery_branch: $(this).val()},
            beforeSend: function () {
                ajaxloader($("#tb_awb_table"));
            },
            success: function (response) {
                $("#tb_awb_table").html(response);
            }
        });
    }
    call_count_functions();

    $("#warehouse_pincode").autocomplete({
        source: "/zoning/pincode/search",
        minLength: 1,
        focus: function (event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            $('#hidden_pincode').val(ui.item.value);
            $(this).val(ui.item.label);
            return false;
        }
    });

    if ($('#awb_status_update_table').length) {
        $('#awb_status_update_table tbody tr').map(function () {
            id = this.id;
            $(this).find('#reason_' + id).datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 1
            })
        }).get();
    }
    if ($('#drs_total_cash').length) {
        get_drs_cash($('#drs_total_cash strong').attr('id'), 'total');
    }
    if ($('#drs_collected_cash').length) {
        get_drs_cash($('#drs_collected_cash strong').attr('id'), 'collected');
    }

    if ($('table .select').length) {
        $(this).find('thead tr th:first-child').html('<label><input type="checkbox" checked="true" class="select_all"><span class="lbl"></span></label>').addClass('center');
    }

    if ($('#incoming_mts_awbs_count').length) {
        get_incoming_awbs_count('mts', '#incoming_mts_awbs_count');
    }
    if ($('#incoming_manifest_awbs_count').length) {
        get_incoming_awbs_count('manifest', '#incoming_manifest_awbs_count');
    }
    $('button[type="submit"]').removeAttr('disabled');

    if ($('#dto_in_scanning_table tbody tr').length > 1) {
        $('#dto_in_scanning_table').show();
    }
    if ($('#rto_in_scanning_table tbody tr').length > 1) {
        $('#rto_in_scanning_table').show();
    }
    if ($('#drs_in_scanning_table tbody tr').length > 1) {
        $('#drs_in_scanning_table').show();
    }
});

function call_awb_count_functions() {
//    get_incoming_awbs_count('mts', '#incoming_mts_awbs_count');
    get_incoming_awbs_count('manifest', '#incoming_manifest_awbs_count');
}

function get_incoming_awbs_count(type, element) {
    $.ajax({
        type: 'GET',
        url: '/transit/awb/get_count?type=' + type,
        cache: true,
        success: function (response) {
            //alert(response);
            $(element).html(response);
            if (response == 0) {
                $(element).attr('href', 'javascript:void(0)');
            }
        }
    });
}

function export_mis() {
    $.ajax({
        type: 'GET',
        url: '/transit/awb/export_mis_searched_awb',
//        data: {
//            awbs: JSON.stringify(awbs),
//            status: status
//        },
        success: function (response) {
//            if (response == 'True') {
//                location.reload();
//            }
            get_message();
        }
    });
}


function export_dto() {
    var dtos = get_selected($('#dto_search_tbl tbody'));
    location.replace('/transit/dto/report?dtos=' + dtos);
//    $.ajax({
//        type: 'GET',
//        url: '/transit/dto/report?dtos='+dtos,
////        data: {
////            dtos: JSON.stringify(dtos)
////        }
//        success: function (response) {
////            if (response == 'True') {
////                alert('dffd');
////            }
//            get_message();
//        }
//    });
}

function awb_status_update_admin() {
    var status = $('#drs_search_form #status').val();
    var awbs = get_selected($('#awb_search_tbl tbody'));

    $.ajax({
        type: 'POST',
        url: '/transit/awb/bulk_update',
        data: {
            awbs: JSON.stringify(awbs),
            status: status
        },
        success: function (response) {
            if (response == 'True') {
                location.reload();
            }
        }
    });
}

function awb_delete_admin() {
    if (confirm("Are you sure want to delete ?")) {
        var awbs = get_selected($('#awb_search_tbl tbody'));

        $.ajax({
            type: 'POST',
            url: '/transit/awb/delete',
            data: {
                awbs: JSON.stringify(awbs)
            },
            success: function (response) {
                if (response == 'True') {
                    location.reload();
                }
            }
        });
    }
}

function update_drs_cash() {
    if ($('#drs_total_cash').length) {
        get_drs_cash($('#drs_total_cash strong').attr('id'), 'total');
    }
    if ($('#drs_collected_cash').length) {
        get_drs_cash($('#drs_collected_cash strong').attr('id'), 'collected');
    }
}

function get_drs_cash(drs, type) {
    $.ajax({
        type: 'GET',
        url: '/transit/drs/get_cash',
        data: {
            'drs': drs,
            'type': type
        },
        success: function (response) {
            $("#drs_" + type + "_cash strong").html(response);
        }
    });
}

function get_drs_awbs(sort, element) {
    if ($(this).attr('id') == sort) {
        sort = '-' + sort;
        $(this).attr('id', sort);
    } else {
        $(this).attr('id', sort);
    }
    $.ajax({
        type: 'GET',
        url: '/transit/drs/get_awbs',
        //cache: true,
        data: {
            sort: sort
        },
        beforeSend: function () {
            ajaxloader($('#drs_awb_table'));
        },
        success: function (response) {
            $("#drs_awb_table").html(response);
            closeLoader();
        }
    });
}

//setInterval(function () {
//    call_count_functions();
//}, 60000);

function call_count_functions() {
    get_cash('expected');
    get_cash('collected');
    get_count('awb', 'incoming', '');
    get_count('awb', 'outgoing', '');
    get_count('awb', 'incoming', 'COD');
    get_count('awb', 'incoming', 'PRE');
    get_count('awb', 'incoming', 'REV');
    get_count('awb', 'outgoing', 'COD');
    get_count('awb', 'outgoing', 'PRE');
    get_count('awb', 'outgoing', 'REV');
    get_count('tb', 'incoming', '');
    get_count('tb', 'outgoing', '');
    get_count('mts', 'incoming', '');
    get_count('mts', 'outgoing', '');
    get_count('drs', '', '');
    get_count('dto', '', '');
    get_count('rto', '', '');
}

function get_cash(type) {
    if ($("#get_" + type + "_cash").length) {
        $.ajax({
            type: 'GET',
            url: '/internal/branch/cash',
            data: {
                'type': type
            },
            success: function (response) {
                $("#get_" + type + "_cash").html(response);
            }
        });
    }
}

function get_count(model, type, category) {
    if ($("#get_count_" + model + "_" + type + category).length) {
        $.ajax({
            type: 'GET',
            url: '/transit/get_count',
            data: {
                'model': model,
                'type': type,
                'category': category
            },
            //cache: true,
            success: function (response) {
                $("#get_count_" + model + "_" + type + category).html(response);
            }
        });
    }
}

$('#select_branch #id_branch').live('change', function () {
    var branch = 1;
    $.ajax({
        type: 'GET',
        url: '/user/set_branch',
        //cache: true,
        data: {
            branch: $(this).val()
        },
        success: function (response) {
            branch = response;
            $(this).val(branch);
            setTimeout('window.location.reload()', 10);
        }
    });
});


function get_message() {
    $.ajax({
        type: 'GET',
        url: '/user/get_message',
        ////cache: true,
        beforeSend: function () {
            if ($('#get_message').html() != '') {
                $('#get_message').fadeOut();
            }
        },
        success: function (response) {
            $('#get_message').html(response).fadeIn(100);
        }
    });
}

$("#create_tb_form #id_delivery_branch").change(function () {
    $.ajax({
        type: 'POST',
        url: 'get_awbs',
        ////cache: true,
        data: {delivery_branch: $(this).val()},
        beforeSend: function () {
            ajaxloader($("#tb_awb_table"));
        },
        success: function (response) {
            $("#tb_awb_table").html(response);
            closeLoader();
        }
    });
});

function awb_history_action(ele, id, action) {
    var row = $(ele).closest('tr');
    $.ajax({
        type: 'GET',
        url: '/transit/awb/history?id=' + id + '&action=' + action,
        success: function (response) {
            row.attr('class', response);
            get_message();
        }
    });
}

function get_selected(element) {
    var selected = $(element).find(':checkbox:checked').map(function () {
        return this.id;
    }).get();

    return selected;
}

$("#create_tb_form").submit(function (e) {
    var awbs = $('#tb_in_scanning_table tbody tr :hidden').map(function () {
        return this.value;
    }).get();
    var length = $('#id_length').val();
    var breadth = $('#id_breadth').val();
    var height = $('#id_height').val();
    var weight = $('#id_weight').val();
//    var co_loader = $('#id_co_loader').val();
//    var co_loader_awb = $('#id_co_loader_awb').val();
    //var seal_id = $('#id_seal_id').val();
    if (length == '') {
        alert('Please enter Length in inches');
        $('#id_length').focus();
    } else if (breadth == '') {
        alert('Please enter Breadth in inches');
        $('#id_breadth').focus();
    }
    else if (height == '') {
        alert('Please enter Height in inches');
        $('#id_height').focus();
    }
    else if (weight == '') {
        alert('Please enter Weight in KG');
        $('#id_weight').focus();
//    }
//    else if (co_loader == '') {
//        alert('Please select Co Loader');
//        $('#id_co_leader').focus();
//    } else if (seal_id == '') {
//        alert('Please enter Seal Id. If Seal is not there enter TB No. itself.');
//        $('#id_seal_id').focus();
    } else if (awbs != '') {
        disableButton(this);
        $.ajax({
            type: 'POST',
            url: '/transit/tb/create_tb',
            ////cache: true,
            data: {
                origin_branch: $('#id_origin_branch').val(),
                delivery_branch: $('#id_delivery_branch').val(),
                awbs: JSON.stringify(awbs),
                length: length,
                breadth: breadth,
                height: height,
                weight: weight
//                co_loader: co_loader,
//                co_loader_awb: co_loader_awb
                //seal_id: seal_id
            },
            success: function (response) {
                if (response == 'True') {
                    window.location = '/transit/tb/outgoing';
                } else {
                    alert(response);
                    $('button[type="submit"]').removeAttr('disabled');
                }
            }
        });
    } else {
        alert("Please in-scan shipments");
        $('button[type="submit"]').removeAttr('disabled');
        $('#tb_in_scanning_table #awb_in_scan').focus();
    }
    e.preventDefault();
    return false;
});


$('#tb_in_scanning_table tbody tr:first-child input[name="awb"]').live('change', function () {
    $('#awb_alert').fadeOut(400, function () {
        $(this).remove();
    });
    var element = $(this);
    var awb = $(this).val();
    $(this).val('');
    if (awb != '') {
        $.ajax({
            type: 'POST',
            url: '/transit/tb/in_scanning',
            //cache: true,
            data: {
                awb: awb,
                delivery_branch: $("#create_tb_form #id_delivery_branch").val()
            },
            success: function (response) {
                if (response != '') {
                    if (update_awb_scanned_counter(awb)) {
                        $('#tb_in_scanning_table tbody tr:first-child').clone().insertAfter('#tb_in_scanning_table tbody tr:first-child');
                        $('#tb_in_scanning_table tbody tr:nth-child(2)').html(response);
                        $('#tb_in_scanning_table tbody tr:first-child td:first-child input').focus();
                        $('#tb_in_scanning_table tbody tr:nth-child(2) #s_no').html($('#awb_scanned_count').val().split(',').length);
                        addRemover($('#tb_in_scanning_table tbody tr:nth-child(2) .remove'));
                        get_message();
                        element.focus();
                    } else {
                        already_scanned_error_msg(awb);
                        element.focus();
                    }
                } else {
                    get_message();
                    element.focus();
                }
            }
        });
    }
    element.focus();
});

function disableButton(element) {
    $(element).find('button[type="submit"]').attr('disabled', 'disabled');
}

$("#create_mts_form").submit(function (e) {
    var co_loader = $('#id_co_loader').val();
    var co_loader_awb = $('#id_co_loader_awb').val();
    var co_loader_awb_length = co_loader_awb.length;
    var tbs = $('#create_mts_form table td :checkbox:checked').map(function () {
        return this.id;
    }).get();
    if (co_loader == '') {
        alert('Please select Co Loader');
        $('#id_co_loader').focus();
    }
    else if (co_loader_awb == '' || co_loader_awb_length < 2 || co_loader_awb_length > 11) {
        alert('Please input Co Loader AWB length more than 2 and less than or equal to 11');
        $('#id_co_loader_awb').focus();
    }
    else if (tbs != '') {
        disableButton(this);
        $.ajax({
            type: 'POST',
            url: 'create_mts',
            //cache: true,
            data: {
                from_branch: $('#id_from_branch').val(),
                to_branch: $('#id_to_branch').val(),
                external_track_id: $('#id_external_track_id').val(),
                type: $('#id_type').val(),
                co_loader: co_loader,
                co_loader_awb: co_loader_awb,
                tbs: JSON.stringify(tbs)
            },
            success: function (response) {
                if (response == 'True') {
                    window.location = '/transit/mts/outgoing';
                } else {
                    alert(response);
                    $('button[type="submit"]').removeAttr('disabled');
                }
            }
        });
    } else {
        alert('Please select atleast one TB');
        $('button[type="submit"]').removeAttr('disabled');
    }
    e.preventDefault();
    return false;
});


$("#create_drs_form").submit(function (e) {
    var fe = $('#id_fe').val();
    var vehicle = $('#id_vehicle').val();
    var opening_km = $('#id_opening_km').val();
    var fl = $('#drs_in_scanning_table tbody tr :hidden').map(function () {
        return this.value;
    }).get();
    var rl = get_selected("#reverse_awb_table tbody");
    if (fe == '') {
        alert('Please select FE');
        $('#id_fe').focus();
    } else if (vehicle == '') {
        alert('Please select Vehicle');
        $('#id_vehicle').focus();
    } else if (opening_km == '') {
        alert('Please enter KM');
        $('#id_opening_km').focus();
    } else if (fl == '' && rl == '') {
        alert("Please In-scanned Forward shipments or select Reverse Pickups");
        $('button[type="submit"]').removeAttr('disabled');
    } else {
        disableButton(this);
        $.ajax({
            type: 'POST',
            url: 'create_drs',
            //cache: true,
            data: {
                fe: fe,
                vehicle: vehicle,
                opening_km: opening_km,
                fl: JSON.stringify(fl),
                rl: JSON.stringify(rl)
            },
            success: function (response) {
                if (response == 'True') {
                    window.location = '/transit/drs';
                } else {
                    $('button[type="submit"]').removeAttr('disabled');
                }
            }
        });
    }
    e.preventDefault();
    return false;
});

$("#create_dto_form").submit(function () {
    var awbs = $('#dto_in_scanning_table tbody tr :hidden').map(function () {
        return this.value;
    }).get();
    var fe = $('#id_fe').val();
    var vehicle = $('#id_vehicle').val();
    var client = $('#id_client').val();
    if(fe == ''){
        alert('Please select FE');
        $('#id_fe').focus();
    }
    else if(vehicle == ''){
        alert('Please select vehicle');
        $('#id_vehicle').focus();
    }
    else if(client == ''){
        alert('Please select Client');
        $('#id_client').focus();
    }
    else if (awbs != '') {
        disableButton(this);
        $.ajax({
            type: 'POST',
            url: '/transit/dto/create_dto',
            //cache: true,dto_in_scanning_table
            data: {
                fe: $('#id_fe').val(),
                vehicle: $('#id_vehicle').val(),
                client: $('#id_client').val(),
                awbs: JSON.stringify(awbs)
            },
            success: function (response) {
                if (response == 'True') {
                    window.location = '/transit/dto';
                } else {
                    $('button[type="submit"]').removeAttr('disabled');
                }
            }
        });
    } else {
        alert("Please in scanned shipments");
        $('button[type="submit"]').removeAttr('disabled');
    }
    return false;
});

$('#dto_in_scanning_table tbody tr input[type="text"]').live('change', function (e) {
//    if (e.which == 13) {
    var awb = $(this).closest('tr').attr('id');
    var type = $(this).attr('name');
    var val = $(this).val();
    $.ajax({
        type: 'POST',
        url: '/transit/awb/field_update',
        //cache: true,
        data: {
            awb: awb,
            field: type,
            val: val
        },
        success: function (response) {
            if (response != '') {
                get_message();
//                $('#dto_in_scanning_table tbody tr input[name="weight"]').focus();
            } else {
                get_message();
            }
        }
    });
    if ($(this).attr('name') == 'height') {
        $('#awb_in_scan').focus();
    }
});

function already_scanned_error_msg(awb) {
    $('#get_message').html(msg).fadeOut();
    var msg = '<div class="alert alert-block alert-error">';
    msg += '<button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>';
    msg += 'AWB : ' + awb + ' | Status : Already In-scanned</div>';
    $('#get_message').html(msg).fadeIn(100);
}

$('#manifest_in_scanning_table tbody tr:first-child input[name="awb"]').live('change', function () {
    var awb = $(this).val();
    $(this).val('');
    if (awb != '') {
        $.ajax({
            type: 'POST',
            url: '/transit/awb/in_scanning',
            //cache: true,
            data: {
                awb: awb
            },
            success: function (response) {
                if (update_awb_scanned_counter(awb)) {
                    $('#manifest_in_scanning_table tbody tr:first-child').clone().insertAfter('#manifest_in_scanning_table tbody tr:first-child');
                    $('#manifest_in_scanning_table tbody tr:nth-child(2)').html(response);
                    $('#manifest_in_scanning_table tbody tr:nth-child(2) #s_no').html($('#awb_scanned_count').val().split(',').length);
//                    element.focus();
                    get_message();
                    $('#manifest_in_scanning_table tbody tr #awb_in_scan').focus();
                    call_awb_count_functions();
                } else {
                    already_scanned_error_msg(awb);
                    $('#manifest_in_scanning_table tbody tr #awb_in_scan').focus();
                }
            }
        });
    }
    $('#manifest_in_scanning_table tbody tr #awb_in_scan').focus();
});

$('#drs_in_scanning_table tbody tr input[name="awb"]').live('change', function () {
    var element = $(this);
    var awb = $(this).val();
    $(this).val('');
    if (awb != '') {
        $.ajax({
            type: 'POST',
            url: '/transit/drs/create',
            //cache: true,
            data: {
                awb: awb
            },
            success: function (response) {
                //alert(response);
                if (response != '') {
                    if (update_awb_scanned_counter(awb)) {
                        $('#drs_in_scanning_table tbody tr:first-child').clone().insertAfter('#drs_in_scanning_table tbody tr:first-child');
                        $('#drs_in_scanning_table tbody tr:nth-child(2)').html(response);
                        $('#drs_in_scanning_table tbody tr:nth-child(2) #s_no').html($('#awb_scanned_count').val().split(',').length);
                        addRemover($('#drs_in_scanning_table tbody tr:nth-child(2) .remove'));
                        get_message();
                        element.focus();
                    } else {
                        already_scanned_error_msg(awb);
                        element.focus();
                    }
                } else {
                    get_message();
                    element.focus();
                }
//                var alert = $('#awb_status_alert').clone();
//                $('#awb_status_alert').hide();
//                $(alert).insertBefore('#manifest_in_scanning_table').show('medium');
            }
        });
    }
    element.focus();
});

$('#dto_in_scanning #awb_in_scan').live('change', function () {
    var awb = $(this).val();
    $(this).val('');
    var element = $(this);
    if (awb != '') {
        $.ajax({
            type: 'POST',
            url: '/transit/dto/create',
            //cache: true,
            data: {
                awb: awb
            },
            success: function (response) {
                //alert(response);
                if (response != '') {
                    if (update_awb_scanned_counter(awb)) {
                        $('#dto_in_scanning_table').show();
                        $(response).insertBefore('#dto_in_scanning_table tbody tr:first-child');
                        $('#dto_in_scanning_table tbody tr:first-child #s_no').html($('#awb_scanned_count').val().split(',').length);
                        addRemover($('#dto_in_scanning_table tbody tr:first-child .remove'));
//                        get_message();
                        $('#dto_in_scanning_table tbody tr:first-child input[name="weight"]')
                        //get_barcode_image(awb);
                        download_csv(awb);
                        get_message();
                        element.focus();
                        //print_barcode();
                    } else {
                        already_scanned_error_msg(awb);
                        element.focus();
                    }
                } else {
                    get_message();
                    element.focus();
                }
//                var alert = $('#awb_status_alert').clone();
//                $('#awb_status_alert').hide();
//                $(alert).insertBefore('#manifest_in_scanning_table').show('medium');
            }
        });
    }
});

$('#rto_in_scanning #awb_in_scan').live('change', function () {
    var awb = $(this).val();
    $(this).val('');

    if (awb != '') {
        $.ajax({
            type: 'POST',
            url: '/transit/rto/create',
            //cache: true,
            data: {
                awb: awb
            },
            success: function (response) {
                //alert(response);
                if (response != '') {
                    if (update_awb_scanned_counter(awb)) {
                        $('#rto_in_scanning_table').show();
                        $(response).insertBefore('#rto_in_scanning_table tbody tr:first-child');
                        //$('#rto_in_scanning_table tbody tr:first-child input[name="weight"]').focus();
                        $('#rto_in_scanning_table tbody tr:first-child #s_no').html($('#awb_scanned_count').val().split(',').length);
                        addRemover($('#rto_in_scanning_table tbody tr:first-child .remove'));
                        get_message();
                        $('#rto_in_scanning #awb_in_scan').focus();
                    } else {
                        already_scanned_error_msg(awb);
                        $('#rto_in_scanning #awb_in_scan').focus();
                    }
                } else {
                    get_message();
                    $('#rto_in_scanning #awb_in_scan').focus();
                }
//                var alert = $('#awb_status_alert').clone();
//                $('#awb_status_alert').hide();
//                $(alert).insertBefore('#manifest_in_scanning_table').show('medium');
            }
        });
    }
    $('#rto_in_scanning #awb_in_scan').focus();
});

$("#create_rto_form").submit(function () {
    var awbs = $('#rto_in_scanning_table tbody tr :hidden').map(function () {
        return this.value;
    }).get();
    var fe = $('#id_fe').val();
    var vehicle = $('#id_vehicle').val();
    var client = $('#id_client').val();
    if(fe == ''){
        alert('Please select FE');
        $('#id_fe').focus();
    }
    else if(vehicle == ''){
        alert('Please select Vehicle');
        $('#id_vehicle').focus();
    }
    else if(client == ''){
        alert('Please select Client');
        $('#id_client').focus();
    }
    else if (awbs != '') {
        disableButton(this);
        $.ajax({
            type: 'POST',
            url: '/transit/rto/create_rto',
            //cache: true,dto_in_scanning_table
            data: {
                fe: $('#id_fe').val(),
                vehicle: $('#id_vehicle').val(),
                client: $('#id_client').val(),
                awbs: JSON.stringify(awbs)
            },
            success: function (response) {
                if (response == 'True') {
                    window.location = '/transit/rto';
                } else {
                    $('button[type="submit"]').removeAttr('disabled');
                }
            }
        });
    } else {
        alert("Please in scanned shipments");
        $('button[type="submit"]').removeAttr('disabled');
    }
    return false;
});

function update_awb_scanned_counter(awb) {
    var awbs = $('#awb_scanned_count').val();
    if (awbs != '') {
        var array = awbs.split(',')
        for (var i = 0; i < array.length; i++) {
            if (array[i] == awb) {
                return false;
            }
        }
        awbs += ',' + awb;
        $('#awb_scanned_count').val(awbs);
        return true;
    } else {
        awbs = awb;
        $('#awb_scanned_count').val(awbs);
        return true;
    }
}

$('.select_all').live('change', function () {
    if ($(this).is(":checked")) {
        $(this).closest('table').find('tbody :checkbox').attr('checked', true);
    } else {
        $(this).closest('table').find('tbody :checkbox').attr('checked', false);
    }
});

function awb_print_sheet(action, type) {
    window.open('/transit/awb/print?action=' + action + '&type=' + type, 'Invoice Printing');
}

$('#editMTS').live('click', function () {
    var status = '<select id="updateMTSStatus" >';
    status += '<option selected value="" ></option>';
    //status += '<option value="U">Un-Delivered</option>';
    status += '<option value="D">Received</option>';
    // status += '<option value="R">Red Alert</option>';
    status += '</select>';
    $(this).closest('tr').find('.status').html(status);
});

$('#editMTSCo_awb').live('click', function () {
    var co_loader_awb = '<input type="type" id="updateMTSCo_awb" >';
    co_loader_awb += '<input type="submit" value="Submit">';
    $(this).closest('tr').find('.co_loader_awb').html(co_loader_awb);
});

$('#editDRS').live('click', function () {
    var status = '<select id="updateDRSStatus" >';
    status += '<option selected value="" style="display: none"></option>';
    status += '<option value="O">Open</option>';
    status += '<option value="C">Closed</option>';
    status += '</select>';
    $(this).closest('tr').find('.status').html(status);
});

$('#updateMTSCo_awb').live('change', function () {
    var element = $(this);
    if (element.val().length < 2 || element.val().length > 11) {
        alert('Co Loader Awb value must lie between 2 to 11');
        return false;
    }
    if (confirm("Are you sure want to change the co loader awb ?")) {
        $.ajax({
            type: 'POST',
            url: '/transit/mts/update_co_loader_awb',
            //cache: true,
            data: {
                mts_id: $(element).closest('tr').find('.mts_id').text(),
                co_loader_awb: $(element).val()
            },
            success: function (response) {
                $(element).closest('td').html(response);
            }
        });
    }
});


$('#updateMTSStatus').live('change', function () {
    var element = $(this);
    if (confirm("Are you sure want to change the status ?")) {
        $.ajax({
            type: 'POST',
            url: '/transit/mts/update_status',
            //cache: true,
            data: {
                mts_id: $(element).closest('tr').find('.mts_id').text(),
                status: $(element).val()
            },
            success: function (response) {
                $(element).closest('td').html(response);
            }
        });
    }
});


$('#updateDRSStatus').live('change', function () {
    var element = $(this);
    if (confirm("Are you sure want to change the status ?")) {
        $.ajax({
            type: 'POST',
            url: '/transit/drs/update_status',
            //cache: true,
            data: {
                drs_id: $(element).closest('tr').find('.id').text(),
                status: $(element).val()
            },
            success: function (response) {
                $(element).closest('td').html(response);
                update_drs_cash();
            }
        });
    }
});


$("#drs_status_update_form").submit(function () {
    status = $('#drs_status_update_form #awb_status').val();
    if (status == 'DEL') {
        var i = 0;
        $('#awb_status_update_table tbody tr').each(function () {
            if ($(this).find(':checkbox').is(':checked')) {
                if ($(this).find('#status').text() == 'Dispatched') {
                    if ($(this).find('#type').text() == 'COD') {
                        if ($(this).find('#collected_amount').val() < $(this).find('#expected_amount').text()) {
                            $(this).find('#collected_amount').focus().attr('class', 'input-small red');
                            i++;
                        }
                    }
                } else if ($(this).find('#status').text() == 'Cancelled') {
                    alert('Cannot Delivered Cancelled Shipments. Create another DRS.');
                    return false;
                } else if ($(this).find('#status').text() == 'Delivered') {
                    alert('Already Updated');
                    return false;
                }
            }
        });
        if (i > 0) {
            alert('Please enter collected amount for delivered shipments');
            return false;
        }
    } else if (status == 'CAN') {
        var c = 0;
        $('#awb_status_update_table tbody tr').each(function () {
            if ($(this).find('#status').text() != 'Pending for Delivery') {
                $(this).find('input:last').focus().attr('class', 'input-small red');
                c++;
            }
        });
        if (c > 0) {
            alert('Please in-scan cancelled shipments');
            return false;
        }
    } else if (status == 'ISC') {
        var d = 0;
        $('#awb_status_update_table tbody tr').each(function () {
            if ($(this).find('#status').text() == 'Pending for Pickup') {
                $(this).find('input:last').focus().attr('class', 'input-small red');
                d++;
            }
        });
        if (d > 0) {
            alert('Please in-scan reverse picked-up shipments');
            return false;
        }
    }
    var awbs = get_selected($('#awb_status_update_table tbody'));

    var collected_amts = $('#awb_status_update_table tbody').find(':checkbox:checked').map(function () {
        return $(this).closest('tr').find('#collected_amount').val();
    }).get();

    if (awbs != '') {
        $.ajax({
            type: 'POST',
            url: '/transit/drs/awb_status_update',
            data: {
                awb_status: status,
                collected_amts: JSON.stringify(collected_amts),
                awbs: JSON.stringify(awbs)
            },
            success: function (response) {
                //alert(response);
                location.reload();
                update_drs_cash();
            }
        });
    } else {
        alert("Please select atleast one AWB")
    }
    return false;
});

$("#rto_status_update_form").submit(function () {
    var awbs = get_selected($('#awb_status_update_table tbody tr'));
    if (awbs != '') {
        $.ajax({
            type: 'POST',
            url: '/transit/rto/awb_status_update',
            data: {
                awb_status: $('#rto_status_update_form #awb_status').val(),
                awbs: JSON.stringify(awbs)
            },
            success: function (response) {
                if (response == 'True') {
                    location.reload();
                } else {
                    window.location.href = response;
                }
            }
        });
    } else {
        alert("Please select atleast one AWB")
    }
    return false;
});

function beforeSubmit() {
    //check whether client browser fully supports all File API
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        var file = $('input[type="file"]');
        var fsize = file[0].files[0].size; //get file size
        var ftype = file[0].files[0].type; // get file type
        //allow file types
        switch (ftype) {
            case 'image/png':
            case 'image/gif':
            case 'image/jpeg':
            case 'image/pjpeg':
            case 'text/plain':
            case 'text/html':
            case 'application/x-zip-compressed':
            case 'application/pdf':
            case 'application/msword':
            case 'application/vnd.ms-excel':
            case 'video/mp4':
                break;
            default:
                $("#output").html("<b>" + ftype + "</b> Unsupported file type!");
                return false
        }

        //Allowed file size is less than 5 MB (1048576 = 1 mb)
        if (fsize > 5242880) {
            alert("<b>" + fsize + "</b> Too big file! <br />File is too big, it should be less than 5 MB.");
            return false
        }
    }
    else {
        //Error for older unsupported browsers that doesn't support HTML5 File API
        alert("Please upgrade your browser, because your current browser lacks some new features we need!");
        return false
    }
}

$("#dto_status_update_form").submit(function () {
    //beforeSubmit();
    var awbs = get_selected($('#awb_status_update_table tbody'));
    //var pod = $(this).find('input[type="file"]')[0].files[0];
    var data = new FormData(this);
    data.append("awbs", JSON.stringify(awbs));
    //alert(pod);
    if (awbs != '') {
        $.ajax({
            type: 'POST',
            url: '/transit/dto/awb_status_update',
            //enctype: "multipart/form-data",
            data: data,
            success: function (response) {
                // alert(response);
                if (response == 'True') {
                    location.reload();
                } else {
                    window.location.href = response;
                }
            },
            processData: false,
            contentType: false
        });
        return false;
    } else {
        alert("Please select atleast one AWB")
    }
    return false;
});

$('#drs_print_sheet').click(function () {
    var awbs = JSON.stringify(get_selected($('#awb_status_update_table tbody')));
    if (awbs != '[]') {
        window.open('/transit/drs/get_print_sheet?awbs=' + awbs, 'DRS Printing')
    } else {
        alert("Please select atleast one AWB")
    }
    return false;
});

$('#dto_print_sheet').click(function () {
    var awbs = JSON.stringify(get_selected($('#awb_status_update_table tbody')));
    if (awbs != '[]') {
        window.open('/transit/dto/get_print_sheet?awbs=' + awbs, 'DTO Printing')
    } else {
        alert("Please select atleast one AWB")
    }
    return false;
});

$('#rto_print_sheet').click(function () {
    var awbs = JSON.stringify(get_selected($('#awb_status_update_table tbody')));
    if (awbs != '[]') {
        window.open('/transit/rto/get_print_sheet?awbs=' + awbs, 'RTO Printing')
    } else {
        alert("Please select atleast one AWB")
    }
    return false;
});


//$('#awb_status_update_form select').live('change', function () {
//    if ($(this).val() == 'CAN') {
//        if ($('#awb_status_update_table thead tr th').length == 10) {
//            $('<th>In-Scan</th>').insertAfter('#awb_status_update_table thead th:last');
//            $('#awb_status_update_table tbody tr').each(function () {
//                $('<td><input type="text" class="input-small" onchange="updateAWBStatus(this)"></td>').insertAfter($(this).find('td:last'));
//            });
//        }
//    } else {
//        if ($('#awb_status_update_table thead tr th').length > 10) {
//            $('#awb_status_update_table thead tr th:last').remove();
//            $('#awb_status_update_table tbody tr').each(function () {
//                $(this).find('td:last').remove();
//            });
//        }
//    }
//});


function inScanInvoice(element, awb) {
    var invoice_no = $(element).val();
//    var invoice_len = invoice_no.length;
    if (invoice_no != '') {
        var tr = $(element).closest('tr');
//        if(!invoice_no.match('^NV') || invoice_len < 8){
//            alert('wrong invoice no');
//        }
        if (confirm('Are you sure want to update status ?')) {
            $.ajax({
                type: 'POST',
                url: '/transit/drs/awb_invoice_inscan',
                data: {
                    invoice_no: invoice_no,
                    awb: awb
                },
                success: function (response) {
                    if (response == 'True') {
                        tr.find('#status').attr('disabled', 'disabled');
                        tr.find('input[type="text"]').attr('disabled', 'disabled');
                        tr.find('select').attr('disabled', 'disabled');
                        get_message();
                    } else {
                        get_message();
                        $(element).focus();
                        $(element).val('');
                    }
                }
            });
        }
    } else {
        alert('Please In-Scan Invoice No.');
        $(element).focus();
    }
}


function inScanDRSAWB(element) {
    var tr = $(element).closest('tr');
    var id = tr.attr('id');
    var status = tr.find('#status').val();
    var remark = tr.find('#remark').val();
    if (tr.find('#reason_' + id).length) {
        var reason = tr.find('#reason_' + id).val();
    } else {
        reason = '';
    }
    if ($(element).val() == tr.find('#awb a').html()) {
        //alert(tr.find('#awb a').html());
        if (status == 'DBC' && reason == '') {
            alert('Please select deferred date');
            tr.find('#reason_' + id).removeAttr('disabled').focus();
            $(element).attr('disabled', 'disabled');
        } else if (status == 'CAN' && remark == '') {
            alert('Please select remark');
            tr.find('#remark').removeAttr('disabled').focus();
            $(element).attr('disabled', 'disabled');
        } else {
            if (confirm('Are you sure want to update status ?')) {
                $.ajax({
                    type: 'POST',
                    url: '/transit/drs/awb_cancel_scan',
                    data: {
                        id: id,
                        status: status,
                        reason: reason,
                        awb: $(element).val(),
                        remark: remark
                    },
                    success: function (response) {
                        if (response != '') {
                            tr.find('#status').attr('disabled', 'disabled');
                            tr.find('input[type="text"]').attr('disabled', 'disabled');
                            tr.find('select').attr('disabled', 'disabled');
                            update_drs_cash();
                            get_message();
                        } else {
                            update_drs_cash();
                            get_message();
                        }
                    }
                });
            }
        }
    } else {
        alert('Please in-scan shipment');
        $(element).removeAttr('disabled').focus();
    }
}

$('#collected_amount').live('keydown', function (e) {
    var element = $(this);
    var exp_amt = parseFloat(element.closest('tr').find('#expected_amount').html().replace(/\.[0]*$/, ''));
    //alert(exp_amt);
    if (element.closest('tr').find('#type').html() == 'COD') {
        if (e.which == 13 || e.which == 9) {
            if (parseFloat(element.val()) != exp_amt) {
                alert('Please enter correct amount');
                element.focus();
            } else {
                if (confirm('Are you sure want to change status to delivered ?')) {
                    $.ajax({
                        type: 'POST',
                        url: '/transit/drs/drs_awb_status_update',
                        data: {
                            awb: element.closest('tr').attr('id'),
                            status: 'DEL',
                            coll_amt: $(this).val()
                        },
                        success: function (response) {
                            if (response == 'True') {
                                element.closest('tr').find('select').val('DEL');
                                element.closest('tr').find('select').attr('disabled', 'disabled');
                                element.closest('tr').find('input[type="text"]').attr('disabled', 'disabled');
                                update_drs_cash();
                                get_message();
                            }
                        }
                    });
                }
            }
        }
    }
});

function update_awb_reason(element) {
    var tr = $(element).closest('tr');
    var awb = tr.attr('id');
    var status = tr.find('#status');
    if (status.val() == 'DBC') {
        if (tr.find('#in_scan').val() != tr.find('#awb a').html() && tr.find('#type').html() != 'Reverse Pickup') {
            alert("Please in-scan shipment");
            $(element).closest('tr').find('#in_scan').removeAttr('disabled').focus();
        } else {
            updateDRSAWBStatus(status);
        }
    }
}

function showOtherRemarkReasonPopup(awb) {
    $('#mask').show();
    $('#reasonPopUp').show();
    $('#reasonPopUp input[name="reason"]').attr('id', awb);
}

function closeReasonPopUp() {
    $('#reasonPopUp').hide();
    $('#mask').hide();
}

function cache_reason() {
    var awb = $('#reasonPopUp input[name="reason"]').attr('id');
    var reason = $('#reasonPopUp #' + awb).val();
    var tr = $('#awb_status_update_table #' + awb);
    var status = tr.find('#status');
    if (reason != '') {
        $.ajax({
            type: 'GET',
            url: '/transit/drs/cache_reason',
            data: {
                awb: awb,
                reason: reason
            },
            success: function (response) {
                if (response == 'True') {
                    $('#reasonPopUp #' + awb).removeAttr('id');
                    $('#reasonPopUp').hide();
                    removeMask();
                } else {
                    alert(response);
                }
            }
        });
        if (tr.find('#in_scan').val() != tr.find('#awb a').html() && tr.find('#type').html() != 'Reverse Pickup') {
            alert("Please in-scan shipment");
            tr.find('#in_scan').removeAttr('disabled').focus();
        } else {
            updateDRSAWBStatus(status);
        }
    } else {
        alert('Please enter reason for cancellation');
    }
}
function update_awb_remark(element) {
    var tr = $(element).closest('tr');
    var awb = tr.attr('id');
    var status = tr.find('#status');
    if (status.val() == 'CAN') {
        if ($(element).val() == 'Other') {
            showOtherRemarkReasonPopup(awb);
        } else {
            if (tr.find('#in_scan').val() != tr.find('#awb a').html() && tr.find('#type').html() != 'Reverse Pickup') {
                alert("Please in-scan shipment");
                $(element).closest('tr').find('#in_scan').removeAttr('disabled').focus();
            } else {
                updateDRSAWBStatus(status);
            }
        }
    }
}

function updateDRSAWBStatus(element) {
    var tr = $(element).closest('tr');
    var awb = tr.attr('id');
    var remark = tr.find('#remark').val();
    if (tr.find('#reason_' + awb).length) {
        var reason = tr.find('#reason_' + awb).val();
    } else {
        reason = '';
    }
    var status = $(element).val();
    if (tr.find('#collected_amount').length) {
        var coll_amt = tr.find('#collected_amount').val();
    } else {
        coll_amt = ''
    }

    if (status == 'DEL' && tr.find('#type').html() == 'COD') {
        alert('Please enter collected amount');
        tr.find('#collected_amount').removeAttr('disabled').focus();
    } else if (status == 'DBC' && reason == '') {
        alert('Please select deferred date');
        $("#reason_" + awb).removeAttr('disabled').focus();
    } else if (status == 'CAN' && remark == '') {
        alert('Please select remark');
        tr.find('#remark').removeAttr('disabled').focus();
    } else if ((status == 'CAN' || status == 'CNA' || status == 'DBC' || status == 'NA') &&
        tr.find('#in_scan').val() != tr.find('#awb a').html() &&
        (tr.find('#type').html() == 'COD' ||
            tr.find('#type').html() == 'Prepaid')) {
        alert('Please in-scan shipment');
        tr.find('#in_scan').removeAttr('disabled').focus();
    } else if (status == 'PC' && tr.find('#in_scan').val() != tr.find('#awb a').html()) {
        alert('Please in-scan shipment');
        tr.find('#in_scan').removeAttr('disabled').focus();
    } else {
        if (confirm('Are you sure want to change status ?')) {
            $.ajax({
                type: 'POST',
                url: '/transit/drs/drs_awb_status_update',
                data: {
                    awb: awb,
                    status: status,
                    coll_amt: coll_amt,
                    reason: reason,
                    remark: remark
                },
                success: function (response) {
                    if (response == 'True') {
                        $(element).closest('tr').find('select').attr('disabled', 'disabled');
                        $(element).closest('tr').find('input[type="text"]').attr('disabled', 'disabled');
                        update_drs_cash();
                        get_message();
                    }
                }
            });
        }
    }
}

function email_client_dashboard() {
    $.ajax({
        type: 'GET',
        url: '/client/export/dashboard',
        beforeSend: function () {
            $('#get_message').html('').fadeOut(100);
            var msg = '<div class="alert alert-block alert-success">';
            msg += '<button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>';
            msg += 'Generated MIS File will be emailed to you shortly...</div>';
            $('#get_message').html(msg).fadeIn(100);
        },
        success: function (response) {
            if (response == 'True') {
                if ($('#get_message').length == 0) {
                    $('<div id="get_message"></div>').insertAfter('.page-header');
                    var msg = '<div class="alert alert-block alert-success">';
                    msg += '<button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>';
                    msg += 'MIS sent successfully please check you mail.</div>';
                    $('#get_message').html(msg).fadeIn(100);
                } else {
                    get_message();
                }
            } else {
                get_message();
            }
        }
    });
    return false;
}

function email_mis(element) {
    $.ajax({
        type: 'POST',
        url: '/transit/awb/mis',
        beforeSend: function () {
            var msg = '<div class="alert alert-block alert-success">';
            msg += '<button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>';
            msg += 'Generated MIS File will be emailed to you shortly...</div>';
            $('#get_message').html(msg).fadeIn(100);
        },
        data: $(element).closest('form').serialize(),
        success: function (response) {
            if (response == 'True') {
                if ($('#get_message').length == 0) {
                    $('<div id="get_message"></div>').insertAfter('.page-header');
                    var msg = '<div class="alert alert-block alert-success">';
                    msg += '<button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>';
                    msg += 'MIS sent successfully please check you mail.</div>';
                    $('#get_message').html(msg).fadeIn(100);
                } else {
                    get_message();
                }
            } else {
                get_message();
            }
        }
    });
    return false;
}


$('#awb_report_cc_search').click(function () {
    $.ajax({
        type: 'GET',
        url: '/transit/awb/report_cc',
        beforeSend: function () {
            ajaxloader($('#awb_table_cc'));
        },
        data: {
            awb: $('#awb_report_cc_form #awb').val(),
            client: $('#awb_report_cc_form #client').val(),
            status: $('#awb_report_cc_form #status').val(),
            call_status: $('#awb_report_cc_form #call_status').val(),
            start_date: $('#awb_report_cc_form #start_date input').val(),
            end_date: $('#awb_report_cc_form #end_date input').val(),
            priority: $('#awb_report_cc_form #priority').val(),
            remark: $('#awb_report_cc_form #remark').val(),
            role: $('#awb_report_cc_form #role').val()
        },
        success: function (response) {
            $('#awb_table_cc').html(response);
        }
    });
});

//$('#awb_report_cc_form #awb').on('keyup', function () {
//    if ($(this).val().length > 3) {
//        get_report_cc_awbs();
//    }
//});
//
//$('#awb_report_cc_form select').on('change', function () {
//    get_report_cc_awbs();
//});
//
//$('#awb_report_cc_form input').on('change', function () {
//    get_report_cc_awbs();
//});
//

$('#awb_table_cc #status').live('change', function () {
    //cs_dashboard(this);
    var awb = $(this).closest('tr').attr('id');
    var status = $(this).closest('tr').find('#status').val();
    if (status == 'SCH' || status == 'DBC') {
        $(this).closest('tr').find('#reason_' + awb).datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0
        }).focus();
    } else {
        $(this).closest('tr').find('#reason_' + awb).datepicker('destroy');
    }
});

$('#awb_table_cc #remark').live('change', function () {
    cs_dashboard(this);
});
function cs_dashboard(ele) {
    var awb = $(ele).closest('tr').attr('id');
    var remark = $(ele).closest('tr').find('#remark').val();
    if (remark == 'Couldn\'t speak to customer') {
        var select = $('#reason_hidden select').clone();
        $('#reason_td_' + awb).html(select);
        $('#reason_td_' + awb + ' select').attr('id', 'reason_' + awb);
    }
    else if (remark == 'Confirmed Cancel') {
        select = $('#reason_hidden_cancel select').clone();
        $('#reason_td_' + awb).html(select);
        $('#reason_td_' + awb + ' select').attr('id', 'reason_' + awb);
    }
    else {
        //#alert(1);
        var input = $('#reason_hidden input').clone();
        $('#reason_td_' + awb).html(input);
        $('#reason_td_' + awb + ' input').attr('id', 'reason_' + awb);
    }

}


//$('#awb_table_cc:checkbox').on('change', function(){
//    var call =$(this).checked;
//    var connected_call = $(this).checked;
//    console.log(call);
//    alert(call)


//});

function updateByCC(element) {
    var tr = $(element).closest('tr');
    var awb = tr.attr('id');
//    var call = $("#call").is(':checked').val();
//    var call = 0;
//    var connected_call=0;
    if ((tr.find("#call").prop("checked")) == true) {
        var call = 1;
        // alert(call);
        //console.log(call, 'calltrue');
    }
//      else{
//        call =0;
//    }
    //alert(call+' call');
    else {
        call = 0;
        //console.log(call, 'callfalse');
    }
    if ((tr.find("#connected_call").prop("checked")) == true) {
        var connected_call = 1;
        //console.log(connected_call, 'dddd');
    }
    else {
        connected_call = 0;
        //console.log(connected_call, 'eeeee');
    }
//    var connected_call = $("#connected_call").is(':checked').val();
//    console.log(connected_call);
//    alert(call);
    var status = tr.find('#status').val();
    var remark = tr.find('#remark').val();
    var reason = tr.find('#reason_' + awb).val();
    //if (status == 'SCH' || status == 'CAN' || status == 'CNA' || status == 'DBC' || status == 'NA') {
    //alert(status);


    if ((call == 0) && (status == 'SCH' || status == 'CAN')) {
        alert('Please call the customer');
        $('#call').focus();
        return false;
    }
    //    if (connected_call == 'False'){
    //        alert('Please call the customer');
    //        $('#connected_call').focus();
    //        return false
    //    }
    if ((status == 'SCH' || status == 'DBC') && reason == '') {
        alert('Please choose scheduled date');
        tr.find('#reason_' + awb).focus();
    } else if (remark == 'Other' && reason == '') {
        alert('Please enter a reason');
        tr.find('#reason_' + awb).focus();
    } else if (remark == '' && status != 'SCH') {
        alert('Please select a remark');
        tr.find('#remark').focus();
    } else {
        $.ajax({
            type: 'POST',
            url: '/transit/awb/update_by_cc',
            ////cache: true,
            data: {
                awb: awb,
                call: call,
                connected_call: connected_call,
                status: status,
                remark: remark,
                reason: reason
            },
            success: function (response) {
                get_message();
            }
        });
        if (call == 1) {
            tr.fadeOut('slow');
        }
//        if (call ==1 && connected_call == 0) {
//            tr.fadeOut('slow');
//        }

    }
}

function update_warehouse(element) {
    var warehouse = $(element).closest('form').find('#id_warehouse');
    $.ajax({
        type: 'GET',
        url: '/client/get_warehouses',
        data: {
            client: $(element).val()
        },
        success: function (response) {
            warehouse.html(response);
        }
    });
}

function save_drs_closing_km(drs_id) {
    closing_km = $('#drs_closing_km_form #closing_km').val();
    if (closing_km != '') {
        $.ajax({
            type: 'GET',
            url: '/transit/drs/update_closing_km',
            data: {
                drs_id: drs_id,
                closing_km: closing_km
            },
            success: function (response) {
                if (response == 'True') {
                    setTimeout('get_message()', 100);
                    window.location = '/transit/drs';
                } else {
                    get_message();
                    alert("Please close all shipments");
                }
            }
        });
    } else {
        alert('Please enter closing km');
    }
}

function drs_search() {
    if ($('#drs_search_form #branch').length) {
        branch = $('#drs_search_form #branch').val();
    } else {
        branch = ''
    }

    var start = $('#drs_search_form #start_date input').val();
    var end = $('#drs_search_form #end_date input').val();
    var drs = $('#drs_search_form #drs').val();

    if (drs.length > 3 || drs == '') {
        $.ajax({
            type: 'GET',
            url: '/transit/drs/search',
            beforeSend: function () {
                ajaxloader($('#drs_search_table'));
            },
            data: {
                drs: drs,
                type: $('#drs_search_form #type').val(),
                branch: branch,
                start_date: start,
                end_date: end,
                status: $('#drs_search_form #status').val()
            },
            success: function (response) {
                $('#drs_search_table').html(response);
                closeLoader();
            }
        });
    }
}

function showLightBox(url) {
    var img = '<img src="/media/' + url + '" >';
    $('#lightbox').html(img).show();
    $('#mask').show();
}

function removeMask() {
//    $('#lightbox').hide();
    $('#mask').hide();
}


function get_barcode_image(query) {
    //$('#barcode').barcode(query, "code39", {barWidth: 1, barHeight: 30});
    $.ajax({
        type: 'GET',
        url: '/transit/awb/get_barcode',
        data: {
            query: query
        },
        success: function (response) {
            print_barcode(response);
        }
    });
}

$('table .removeFE').click(function () {
    var fe_id = $(this).find('input[type="hidden"]').val();
    if (confirm("Are you sure want to remove this FE ?")) {
        $.ajax({
            type: 'POST',
            url: '/internal/manage_fe/remove',
            data: {
                fe_id: fe_id
            },
            success: function (response) {
                if (response == 'True') {
                    window.location.reload();
                } else {
                    alert(response);
                }
            }
        });
    }
});


function send_issue_sms() {
    var clients = get_selected('#issue_notification_table');
    var reason = $('#issue_sms_reason').val();
    if (clients != '') {
        if (confirm('Are Sure want to send SMS ?')) {
            $.ajax({
                type: 'POST',
                url: '/transit/send_issue_sms',
                data: {
                    clients: JSON.stringify(clients),
                    reason: reason
                },
                success: function (response) {
                    get_message();
                }
            });
        }
    } else {
        alert('Please Select at least one Client');
    }
    return false;
}

function remove_value(list, value, separator) {
    console.log(list);
    separator = separator || ",";
    var values = list.split(separator);
    for (var i = 0; i < values.length; i++) {
        if (values[i] == value) {
            values.splice(i, 1);
            return values.join(separator);
        }
    }
    return list;
}

function autosuggest(element, app_name, model_name, query_string) {
    //alert(model);
    var hidden = $(element).parent().find(':hidden');
    $(element).autocomplete({
        source: "/report/autosuggest?model_name=" + model_name + '&app_name=' + app_name + '&query_string=' + query_string,
        minLength: 1,
        focus: function (event, ui) {
            $(element).val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            var terms = split($(hidden).val());
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            $(hidden).val(terms.join(","));
            $(element).val("");
            $('<span class="tag">' + ui.item.label + '<button class="close" type="button" id="' + ui.item.value + '">×</button></span>').insertBefore(element);
            return false;
        }
    });
}

$('#autosuggest_box .close').live('click', function () {
    var item = $(this).attr('id');
    var hidden = $(this).closest('div').find(':hidden');
    var list = $(hidden).val();
    //alert(list);
    hidden.val(remove_value(list, item, ','));
    $(this).parent('span').remove();
});

function download_csv(awb) {
    location.replace('/transit/awb/download_csv?awb=' + awb);
//    $.ajax({
//        type: 'GET',
//        url: '/transit/awb/download_csv',
//        data: {
//            awb: awb
//        },
//        success: function (response) {
//            //print_barcode(response);
//        }
//    });
}

$('#advance_report_form').submit(function () {
    $.ajax({
        type: 'POST',
        url: '/report/advance_report',
        data: $(this).serialize(),
        success: function (response) {

        }
    });
    return false;
});

function pop_message(message, type) {
    var msg_div = $('#get_message #msgDiv');
    message += '<button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>';
    msg_div.html(message);
    msg_div.removeAttr('class');
    msg_div.addClass('alert alert-block alert-' + type);
    $('#get_message').fadeIn(100);
}

function update_incoming_calls(json) {
    var table = $('#' + json.call_sid);
    table.find('#call_status span').html(json.status);

    if (json.status == 'Completed') {

        table.closest('#call_details').hide('slow', function () {
            table.closest('#call_details').remove();
        });
    }
}

function get_incoming_call_details() {
    var div = $('#call_notifications');
    var call_sids = $('#call_notifications table').map(function () {
        return this.id;
    }).get();
    //alert('call id ' + call_sids);
    $.ajax({
        type: 'POST',
        url: '/cs/call/details/',
        data: {
            call_sids: JSON.stringify(call_sids)
        },
        success: function (response) {
            if (response != '') {
                if ($('#call_notifications table').length) {
                    //alert(call_sids);
                    var res = $(response);
                    res.map(function () {
                        var id = this.id;
                        if ($('#call_notifications #' + id).length) {
                            res.find('#' + id).closest('#call_details').remove();
                        }
                    });
                    res.insertBefore('#call_notifications div:first-child').show('slow');

                } else {
                    div.html(response);
                    div.find('div').show('slow');
                }
                $('#call_notifications #call_details').addClass('table-success');
                $('#call_notifications #call_details:first-child').removeClass('table-success');
                $('#call_notifications #call_details:first-child').addClass('table-alert');
                //close_completed_calls();
            }
        }
    });
}

if ($('#call_notifications').length) {
    setInterval(function () {
        get_incoming_call_details();
    }, 1000);
}

$('#close_incoming_call').live('click', function () {
    var table = $(this).closest('table');
    var call_sid = table.attr('id');
    $.ajax({
        type: 'POST',
        url: '/cs/call/close/',
        data: {
            call_sid: call_sid
        },
        success: function (response) {
            table.closest('#call_details').hide('slow', function () {
                table.remove();
            });
        }
    });
});

function get_customer_awbs(ele) {
    var q = $(ele).val();

    if (q.length >= 10) {
        var table = $(ele).closest('table');
        var call_sid = table.attr('id');
        var div = $(ele).closest('#call_details').find('#customer_awbs');
        div.hide();

        $.ajax({
            type: 'GET',
            url: '/cs/get_customer_awbs',
            data: {
                call_sid: call_sid,
                q: q
            },
            success: function (response) {
                if (response != '') {
                    div.html(response).show('slow');
                    var customer_name = div.find(':hidden').val();
                    table.find('tr #customer_name').html(customer_name);
                    $(ele).closest('td').html(q);
                    $(ele).closest('td').attr('id', 'customer_phone');
                }
            }
        });
    }
}

$('.customer_registered_no').live('blur', function () {
    get_customer_awbs(this);
});

$('.customer_registered_no').live("keypress", function (e) {
    if (e.keyCode == 13 || e.keyCode == 9)
        get_customer_awbs(this);
});

//naveen functions
$("#out_call").live('click', function () {
    var call = $(this).closest('tr').find('.call').html();
    var tr = $(this).closest('tr');
    call_api_out(this);
//    alert('call connected');
    call_connected(this);
    call_image_change(this);
//    el.src = "/media/call.jpeg"
});

function call_image_change(ele) {
//    $(ele).closest('a').html('<img src="/media/cut_call.jpeg" style="width:35px;height:35px" >');
    $(ele).closest('tr').find('.call_done').html('<img src="/static/img/cut_call.jpeg" style="width:35px;height:35px" >');
//    alert(cal);
}

function call_connected(ele) {
//    var call = $(ele).closest('tr').find('.call').html();
    var awb = $(ele).closest('tr').find('.awb').html();
//    document.getElementsByClassName('.call').src = "/media/cut_call.jpeg";
    var call_made = true;
    $.ajax({
        type: 'POST',
        url: '/cs/call_made',
        data: {
            call_made: call_made,
            awb: awb
        },
        success: function (response) {
            return true;
        }
    });
}

function call_api_out(ele) {
    var awb = $(ele).closest('tr').find('.awb').html();
    var agent_no = $(ele).closest('tr').find('.agent_no').html();
    var customer_no = $(ele).closest('tr').find('.customer_no').html();
//    var id = $(ele).closest('tr').find("#out_call").val();
//    var tr = $(this).attr('#call_o');
//    var agent_no =$('#agent_no').val();
//    alert(customer_no);
//    alert(agent_no);
//    alert(awb);
    $.ajax({
        type: 'GET',
        url: '/api/awb/calling',
        ////cache: true,
        data: {
            agent_no: agent_no,
            awb: awb,
            customer_no: customer_no
        },
        success: function (response) {
            if (response == 'DND Number') {
                alert('DND Number');
                return false;
            }
        }
    });
//    var awb = $(ele).closest('tr').attr('id');

}

$("#ph_submit").click(function () {
    var agent_no = $("#agent_no").val();
    $.ajax({
        type: 'POST',
        url: '/cs/awb/calling_details',
        data: {
            agent_no: agent_no
        },
        beforeSend: function () {
            ajaxloader($('#call_table'));
        },
        success: function (response) {
//            alert('response');
//            return response
            $('#call_table').html(response);
//            closeLoader();
        }
    });
});

$('#drs_pincode_search').click(function () {
   var pincodes = $('#pincode_hidden').val();
   $.ajax({
        type: 'POST',
        url: '/transit/drs/get_awbs',
        data: {
            pincodes: pincodes
        },
        beforeSend: function () {
            ajaxloader($('#pincode_search_table'));
        },
        success: function (response) {
//            alert('response');
//            return response
            $('#pincode_search_table').html(response);
//            closeLoader();
        }
    });
});

function linehual_report() {
    get_message();
}