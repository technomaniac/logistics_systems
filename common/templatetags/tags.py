from django import template
from datetime import datetime
from django.core.cache import cache
from middlewares.global_request import get_current_request

register = template.Library()


@register.filter
def format_data(value):
    try:
        return datetime.strptime(str(value), '%Y-%m-%d').strftime('%d %b, %Y')
    except ValueError:
        try:
            if len(str(value - int(value))[2:]) == 1:
                return str(value) + '0'
            else:
                return value
        except:
            return value


@register.filter
def format_date(value, format):
    try:
        return datetime.strptime(str(value), '%Y-%m-%d').strftime(format)
    except ValueError:
        try:
            if len(str(value - int(value))[2:]) == 1:
                return str(value) + '0'
            else:
                return value
        except:
            return value


@register.filter
def format_phone(value):
    try:
        return str(value).split('.')[0]
    except:
        return value


@register.filter
def cache_query(value, key):
    request = get_current_request()
    cache.set(key + '_' + str(request.user.pk), value)
    return len(value)


@register.filter
def get_user(value, user):
    pass