import itertools

import django_tables2 as tables


class TableMixin(tables.Table):

    def __init__(self, *args, **kwargs):
        super(TableMixin, self).__init__(*args, **kwargs)
        self.counter = itertools.count()

    def render_s_no(self):
        return next(self.counter) + 1
