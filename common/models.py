from datetime import datetime

from django.db.models.query import QuerySet
from django.db import models


class UpdateQuery(QuerySet):
    def update(self, *args, **kwargs):
        super(UpdateQuery, self).update(**kwargs)
        for i in self.all():
            # try:
            #     print i.status,'in update query'
            # except:
            #     pass
            i.on_update = datetime.now()
            i.save()

    def delete(self):
        for i in self.all():
            i.is_active = False
            i.save()
            #print "---",i  
            #return super(UpdateQuery, self).delete()


class UpdateManager(models.Manager):
    def get_queryset(self):
        return UpdateQuery(self.model)


class Time_Model(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    on_update = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    objects = UpdateManager()

    class Meta:
        abstract = True




