import os
import re
from datetime import datetime

import requests
from celery import task
from celery.task.base import periodic_task
from celery.schedules import crontab
from django.core.mail.message import EmailMessage

from client.models import Client_Vendor
from internal.models import Branch
from logistics.settings import MEDIA_ROOT, EXOTEL_SID, EXOTEL_TOKEN, EXOTEL_URL, EXOTEL_NO
from utils.constants import FE_REMOVE_TEMPLATE, FE_CREATE_TEMPLATE
from utils.random import get_title


@periodic_task(run_every=crontab(hour=10, minute=00))
def pg_backup():
    password = os.environ.get('DATABASE_PASSWORD')
    db_name = os.environ.get('DATABASE_NAME')
    user = os.environ.get('DATABASE_USER')
    host = os.environ.get('DATABASE_HOST')
    dir = os.path.join(MEDIA_ROOT, 'backup/')
    if not os.path.exists(dir):
        os.makedirs(dir)
    filename = datetime.today().strftime('%d-%m-%Y_%H') + '.dump.gz'
    # file = open(dir + filename)
    # command = '' % ()
    # command += 'pg_dump -Fp -s -h %s -U %s %s | gzip > %s' % (host, user, db, dir + filename)
    command = 'export PGPASSWORD=%s\npg_dump -Fc --no-acl --no-owner -h %s -U %s %s | gzip -c > %s' % (
        password, host, user, db_name, dir + filename)
    os.system(command)

    # f = open()
    # message = EmailMessage(filename, "This is a system generated mail. Please do not reply.", "system@nuvoex.com",
    # ['system@nuvoex.com'])
    # message.attach(filename, file, 'application/zip')
    # print message.send()


# def send_customer_notification(awbs_list):
# awbs = AWB.objects.filter(pk__in=awbs_list, awb_status__manifest__client__client_additional__sms_notification=True)
#

def send_sms(to, body, callback_url):
    params = {'To': to, 'From': EXOTEL_NO, 'Body': body, 'StatusCallback': callback_url}
    auth = (EXOTEL_SID, EXOTEL_TOKEN)
    return requests.post(EXOTEL_URL.format(sid=EXOTEL_SID), auth=auth, data=params)


@task
def send_email(subject, body, frm, to):
    message = EmailMessage(subject, body, frm, to)
    # message.attach(filename, csvfile.getvalue(), 'text/csv')
    return message.send()


def send_fe_action_notification(action, fe, user):
    if action == 'removed':
        template = FE_REMOVE_TEMPLATE
    else:
        template = FE_CREATE_TEMPLATE
    fe_name = get_title(fe.user.get_full_name())
    branch = Branch.objects.get(pk=fe.branch.pk)
    branch_name = branch.get_readable_branch_name()
    user_name = get_title(user.get_full_name())
    subject = 'FE ' + fe_name + '(' + branch_name + ' Branch) ' + action + ' by ' + user_name
    format = dict()
    format['fe_name'] = fe_name
    format['branch_name'] = branch_name
    format['user_name'] = user_name
    body = template.format(**format)
    to = ['ops@nuvoex.com', 'hr@nuvoex.com']
    try:
        to.append(branch.branch_manager.email)
    except:
        pass
    send_email.delay(subject, body, 'system@nuvoex.com', to)


@task
def vendor_update():
    c = Client_Vendor.objects.filter(client__client_code='PTM')
    for i in range(len(c)):
        if c[i].awb_count().count() == 0:
            c[i].delete()

    for i in range(len(c)):
        c[i].vendor_code = re.sub('\.[^.]*$', '', c[i].vendor_code)
        c[i].save()